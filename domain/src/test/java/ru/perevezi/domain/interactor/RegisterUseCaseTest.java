/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;


import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.impl.RegisterUseCase;
import ru.perevezi.domain.repository.UserRepository;

public class RegisterUseCaseTest {

    @Mock
    UserRepository mMockUserRepository;

    RegisterUseCase mUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mUseCase = new RegisterUseCase(mMockUserRepository);
    }

    @Test
    public void testShouldRegisterUser() {
        PereveziAccount up = new PereveziAccount();

        up.setName("Alex");
        up.setMail("akommv@gmail.com");
        up.setPhone("+79221389922");

        Mockito.when(mValidatorsInterface.isEmailValid(Mockito.anyString())).thenReturn(true);
        Mockito.when(mValidatorsInterface.isEmpty(Mockito.anyString())).thenReturn(false);
        Mockito.when(mValidatorsInterface.isPhoneValid(Mockito.anyString())).thenReturn(true);

        mUseCase.execute(up);

        Mockito.verify(mMockUserRepository).register(Mockito.eq(up));
    }

    @Test
    public void testShouldThrowWhenFieldsMissing() {
        PereveziAccount up = new PereveziAccount();

        Mockito.when(mValidatorsInterface.isEmpty(Mockito.anyString())).thenReturn(true);

        assertFailsWithValidationException(up);
    }

    @Test
    public void testShoudThrowWhenEmailInvalid() {
        PereveziAccount up = new PereveziAccount();

        Mockito.when(mValidatorsInterface.isEmpty(Mockito.anyString())).thenReturn(false);
        Mockito.when(mValidatorsInterface.isEmailValid(Mockito.anyString())).thenReturn(false);
        Mockito.when(mValidatorsInterface.isPhoneValid(Mockito.anyString())).thenReturn(true);

        assertFailsWithValidationException(up);
    }

    @Test
    public void testShoudThrowWhenPhoneInvalid() {
        PereveziAccount up = new PereveziAccount();

        Mockito.when(mValidatorsInterface.isEmpty(Mockito.anyString())).thenReturn(false);
        Mockito.when(mValidatorsInterface.isEmailValid(Mockito.anyString())).thenReturn(true);
        Mockito.when(mValidatorsInterface.isPhoneValid(Mockito.anyString())).thenReturn(false);

        assertFailsWithValidationException(up);
    }

    private void assertFailsWithValidationException(PereveziAccount up) {
        try {
            mUseCase.execute(up);
            Assert.fail("ValidationException was not thrown");
        } catch (ValidationException e) {
            Mockito.verifyZeroInteractions(mMockUserRepository);
        }
    }
}
