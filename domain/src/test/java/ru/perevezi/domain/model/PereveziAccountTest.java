/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.model;

import org.junit.Before;
import org.junit.Test;

import ru.perevezi.domain.entities.PereveziAccount;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PereveziAccountTest {

    private static final String FAKE_ACCOUNT_NAME = "Alex";
    private static final String FAKE_ACCOUNT_PASSWORD = "qwerty";
    
    private PereveziAccount mPereveziAccount;
    
    @Before
    public void setUp() {
        mPereveziAccount = new PereveziAccount(FAKE_ACCOUNT_NAME, FAKE_ACCOUNT_PASSWORD);
    }

    @Test
    public void testPereveziAccountConstructor() throws Exception {
        String name = mPereveziAccount.getName();

        assertThat(name, is(FAKE_ACCOUNT_NAME));
    }
}
