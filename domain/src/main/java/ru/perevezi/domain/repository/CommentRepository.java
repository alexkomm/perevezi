/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.repository;

import ru.perevezi.domain.entities.PereveziComment;

public interface CommentRepository {

    PereveziComment createComment(PereveziComment comment);

}
