/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.entities.PereveziAccount;

public interface UserRepository {

    String register(PereveziAccount pereveziAccount);

    PereveziAccount confirm(String requestId, int confirmCode);

    String retrySms(String requestId);

    PereveziAccount getInfo();

    PereveziAccount login(String login, String password);

    String bindDeviceId(String registrationId);

    String unbindDeviceId(String regId);

    List<PereveziLot> getUserLots(Map<String, Integer> argumentMap);

    List<PereveziLot> getUserBids(Map<String, Object> argumentsMap);

    List<PereveziLot> getUserReviews(int userId, int vote, Map<String, Integer> queryArguments);

    Map<String, Object> getUserCommissions(int userId);

    ArrayList<Map<String, Object>> getUserBidsStats(int userId);

    ArrayList<Map<String, Integer>> getUserLotsStats(int userId);

    ArrayList<Map<String, Integer>> getUserReviewsStats(int userId);

    Integer[] getNewLotsCount(int userId);

    Boolean logout();

    void addReview(Map<String, String> reviewData);

    PereveziLot checkMissingVote(int userId);

    PereveziAccount getUserById(int userId);

    void addUserPhone(int userId, String userPhone);

    void confirmUserPhone(int userId, int code);

    void resendUserPhoneCode(int userId);
}
