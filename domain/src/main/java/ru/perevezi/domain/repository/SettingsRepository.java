/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.repository;

import ru.perevezi.domain.entities.PereveziSettings;

public interface SettingsRepository {
    PereveziSettings getGeneralSettings();
}
