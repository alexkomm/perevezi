/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.repository;

import java.util.Map;

import ru.perevezi.domain.entities.PereveziBid;

public interface BidRepository {

    PereveziBid createBid(PereveziBid pereveziBid);

    void declineBid(Map<String, Object> declineData);

    void acceptBid(Map<String, Object> acceptData);

    Map<String, Object> getConfirmationInfo(int id);
}
