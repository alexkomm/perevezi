/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.entities.PereveziLot;

public interface LotRepository {

    List<PereveziLot> getLotList(HashMap<String, Object> options);

    List<PereveziLot> getLotList(String[] nids);

    PereveziLot getLotById(int nid, boolean resetCache);

    PereveziLot createLot(PereveziLot pereveziLot);

    List<PereveziBid> getLotBids(int nid);

    java.util.Collection<PereveziComment> getLotComments(int nid);

    String checkBidAccess(int nid);

    String checkCommentAccess(int nid);

    void confirmCompletion(Map<String, Object> args);

    boolean acceptLot(Map<String, Object> args);

    String refundRequest(Map<String, String> args);

    void markAsViewed(int nid);
}
