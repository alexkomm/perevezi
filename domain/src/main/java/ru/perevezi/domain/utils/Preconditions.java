/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.utils;

/*
 * Note: this file was copied from Guava to cut down significantly on the library size, since we're only
 * using a small subset of Guava.
 */
public class Preconditions {


    /**
     * Ensures the truth of an expression involving the state of the calling
     * instance, but not involving any parameters to the calling method.
     *
     * @param expression a boolean expression
     * @param errorMessage the exception message to use if the check fails; will
     * be converted to a string using {@link String#valueOf(Object)}
     * @throws IllegalStateException if {@code expression} is false
     */
    public static void checkState(
            boolean expression, Object errorMessage) {
        if (!expression) {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }
    }
}
