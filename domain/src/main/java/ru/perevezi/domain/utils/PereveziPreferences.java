/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.utils;

import java.util.Set;

public interface PereveziPreferences {

    void clearAll();

    void clearAccountData();

    String getCurrentAccount();

    void setCurrentAccount(String accountName);

    int getCurrentAccountExtId();

    void setCurrentAccountExtId(Integer extId);

    long getLastUpdatedTime();

    void setLastUpdatedTime(long updatedTime);

    int getAppVersion();

    void setAppVersion(int appVersion);

    String getRegistrationId();

    void setRegistrationId(String registrationId);

    void setLoggedOut(boolean loggedOut);

    boolean isLoggedOut();

    void setTokenSent(boolean sent);

    boolean isTokenSent();

    void setNewLots(Set<String> newLots);

    Set<String> getNewLots();


}
