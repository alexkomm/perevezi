/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.account;

import java.util.List;

import ru.perevezi.domain.entities.PereveziAccount;

public interface PereveziAccountManager {
    List<PereveziAccount> getAccounts();

    void addAccount(PereveziAccount pereveziAccount);

    void removeAccount(PereveziAccount pereveziAccount);

    boolean accountExists(PereveziAccount pereveziAccount);

    PereveziAccount getAccount(String name);

    void invalidateAuthToken(PereveziAccount pereveziAccount);

    void updateCredentials(PereveziAccount pereveziAccount);
}
