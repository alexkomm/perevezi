/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.mapper;

import java.util.Collection;

public interface RequestMapper<T, A> {

    T mapRequest(A entity);

    Collection<T> mapRequest(Collection<A> entityCollection);

}
