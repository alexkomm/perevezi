/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.mapper;

public interface BidirectionalMapper<T, A> extends ResponseMapper<T, A>, RequestMapper<T, A> {

}
