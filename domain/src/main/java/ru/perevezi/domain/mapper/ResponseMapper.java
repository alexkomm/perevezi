/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.mapper;

import java.util.Collection;

public interface ResponseMapper<T, A> {

    A mapResponse(T entity);

    Collection<A> mapResponse(Collection<T> entityCollection);
}
