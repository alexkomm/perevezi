/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class RegisterUseCase implements UseCase<String, PereveziAccount> {

    private UserRepository mUserRepository;

    @Inject
    public RegisterUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public String execute(PereveziAccount pereveziAccount) {
        Preconditions.checkState(pereveziAccount != null, "should not be empty");

        return mUserRepository.register(pereveziAccount);
    }
}
