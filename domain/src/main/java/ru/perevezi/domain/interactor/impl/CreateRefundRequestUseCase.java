/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CreateRefundRequestUseCase implements UseCase<String, Map<String, String>> {

    private final LotRepository mLotRepository;

    @Inject
    public CreateRefundRequestUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public String execute(Map<String, String> args) {
        Preconditions.checkState(args.get("lotId") != null, "Lot id argument cannot be null");
        Preconditions.checkState(args.get("reason") != null, "Reason argument cannot be null");

        return mLotRepository.refundRequest(args);
    }
}
