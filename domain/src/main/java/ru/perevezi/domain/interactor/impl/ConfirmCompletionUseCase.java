/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class ConfirmCompletionUseCase implements UseCase<Void, Map<String, Object>> {

    private final LotRepository mLotRepository;

    @Inject
    public ConfirmCompletionUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public Void execute(Map<String, Object> args) {
        Preconditions.checkState(args.get("lotId") != null, "Lot id cannot be null");

        mLotRepository.confirmCompletion(args);

        return null;
    }
}
