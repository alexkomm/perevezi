/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetNewLotsCountUseCase implements UseCase<Integer[], Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public GetNewLotsCountUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Integer[] execute(Integer userId) {
        Preconditions.checkState(userId > 0, "userId cannot be less than zero");

        return mUserRepository.getNewLotsCount(userId);
    }
}
