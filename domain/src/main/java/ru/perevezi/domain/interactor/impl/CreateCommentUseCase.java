/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.CommentRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CreateCommentUseCase implements UseCase<PereveziComment, PereveziComment> {

    private final CommentRepository mCommentRepository;

    @Inject
    public CreateCommentUseCase(CommentRepository commentRepository) {
        mCommentRepository = commentRepository;
    }

    @Override
    public PereveziComment execute(PereveziComment comment) {
        Preconditions.checkState(comment != null, "comment cannot be null");

        return mCommentRepository.createComment(comment);
    }
}
