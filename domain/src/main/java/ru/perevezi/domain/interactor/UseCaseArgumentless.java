/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor;

public interface UseCaseArgumentless<T> {

    T execute();
}
