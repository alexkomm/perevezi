/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class ConfirmRegistrationUseCase implements UseCase<PereveziAccount, Map<String, Object>> {

    UserRepository mUserRepository;

    @Inject
    public ConfirmRegistrationUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public PereveziAccount execute(Map<String, Object> arg) {
        Preconditions.checkState(arg.get("requestId") != null, "requestId cannot be null");
        Preconditions.checkState(arg.get("confirmCode") != null, "confirmCode cannot be null");

        return mUserRepository.confirm(arg.get("requestId").toString(), Integer.parseInt(arg.get
                ("confirmCode").toString()));
    }

}
