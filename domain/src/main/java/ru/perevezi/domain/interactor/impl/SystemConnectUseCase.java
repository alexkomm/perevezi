/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.UseCaseArgumentless;
import ru.perevezi.domain.repository.UserRepository;

public class SystemConnectUseCase implements UseCaseArgumentless<PereveziAccount> {

    private UserRepository mUserRepository;

    @Inject
    public SystemConnectUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public PereveziAccount execute() {
        return mUserRepository.getInfo();
    }
}
