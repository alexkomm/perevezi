/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Collection;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class GetLotCommentsUseCase implements UseCase<Collection<PereveziComment>,Integer> {
    private final LotRepository mLotRepository;

    @Inject
    public GetLotCommentsUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public Collection<PereveziComment> execute(Integer lotId) {
        if (lotId == null) {
            throw new IllegalArgumentException("Lot id cannot be null!");
        }

        return mLotRepository.getLotComments(lotId);
    }
}
