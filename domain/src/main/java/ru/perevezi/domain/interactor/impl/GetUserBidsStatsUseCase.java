/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserBidsStatsUseCase implements UseCase<ArrayList<Map<String, Object>>, Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserBidsStatsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }


    @Override
    public ArrayList<Map<String, Object>> execute(Integer userId) {
        Preconditions.checkState(userId > 0, "User id cannot be zero");

        return mUserRepository.getUserBidsStats(userId);
    }
}
