/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class BindDeviceIdUseCase implements UseCase<String, String> {

    private final UserRepository mUserRepository;

    @Inject
    public BindDeviceIdUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public String execute(String regId) {
        Preconditions.checkState(regId != null, "Registration id cannot be null");

        return mUserRepository.bindDeviceId(regId);
    }
}
