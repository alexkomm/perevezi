/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CreateLotUseCase implements UseCase<PereveziLot, PereveziLot> {

    private LotRepository mLotRepository;

    @Inject
    public CreateLotUseCase(LotRepository lotRepository) {
        Preconditions.checkState(lotRepository != null, "LotRepository cannot be null");

        mLotRepository = lotRepository;
    }

    @Override
    public PereveziLot execute(PereveziLot lot) {
        return mLotRepository.createLot(lot);
    }
}
