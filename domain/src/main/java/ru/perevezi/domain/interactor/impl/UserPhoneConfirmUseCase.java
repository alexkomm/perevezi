/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class UserPhoneConfirmUseCase implements UseCase<Void, Map<String, Integer>> {

    private final UserRepository mUserRepository;

    @Inject
    public UserPhoneConfirmUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Void execute(Map<String, Integer> args) {

        Preconditions.checkState(args.get("userId") != null, "User id cannot be null");
        Preconditions.checkState(args.get("code") != null, "Confirm code cannot be null");

        mUserRepository.confirmUserPhone(args.get("userId"), args.get("code"));

        return null;
    }
}
