/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserProfileUseCase implements UseCase<PereveziAccount, Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserProfileUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public PereveziAccount execute(Integer userId) {
        Preconditions.checkState(userId > 0, "userId must be specified");

        return mUserRepository.getUserById(userId);
    }
}
