/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class LoginUseCase implements UseCase<PereveziAccount, Map<String, String>> {

    UserRepository mUserRepository;

    @Inject
    public LoginUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public PereveziAccount execute(Map<String, String> arg) {
        Preconditions.checkState(arg.get("login") != null, "login cannot be null");
        Preconditions.checkState(arg.get("password") != null, "password cannot be null");

        return mUserRepository.login(arg.get("login"), arg.get("password"));
    }
}
