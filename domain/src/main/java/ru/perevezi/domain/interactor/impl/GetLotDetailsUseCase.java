/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class GetLotDetailsUseCase implements UseCase<PereveziLot, Map<String, Object>> {

    private final LotRepository mLotRepository;

    @Inject
    public GetLotDetailsUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public PereveziLot execute(Map<String, Object> args) {

        int lotId;
        boolean resetCache = false;

        try {
            lotId = Integer.valueOf(args.get("lotId").toString());
        } catch (Exception e) {
            throw new IllegalArgumentException("Lot id missing on incorrect format");
        }

        if (args.get("reset") != null) {
            resetCache = (boolean) args.get("reset");
        }

        return mLotRepository.getLotById(lotId, resetCache);
    }
}
