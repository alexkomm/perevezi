/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserReviewsUseCase implements UseCase<List<PereveziLot>, Map<String, Integer>> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserReviewsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public List<PereveziLot> execute(Map<String, Integer> arg) {
        Integer userId = arg.get("userId");
        Integer vote   = arg.get("vote");

        Preconditions.checkState(userId != null, "User id cannot be null");
        Preconditions.checkState(vote != null, "Vote type cannot be null");

        Map<String, Integer> queryArgs = new HashMap<>(2);

        queryArgs.put("offset", arg.get("offset"));
        queryArgs.put("limit", arg.get("limit"));

        return mUserRepository.getUserReviews(userId, vote, queryArgs);
    }
}
