/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.List;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class GetLotBidsUseCase implements UseCase<List<PereveziBid>, Integer> {

    private final LotRepository mLotRepository;

    @Inject
    public GetLotBidsUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public List<PereveziBid> execute(Integer lotId) {
        if (lotId == null) {
            throw new IllegalArgumentException("Lot id cannot be null!");
        }

        return mLotRepository.getLotBids(lotId);
    }
}
