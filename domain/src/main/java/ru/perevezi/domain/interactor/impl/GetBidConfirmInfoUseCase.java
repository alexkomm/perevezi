/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.BidRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetBidConfirmInfoUseCase implements UseCase<Map<String, Object>, Integer> {

    private final BidRepository mBidRepository;

    @Inject
    public GetBidConfirmInfoUseCase(BidRepository bidRepository) {
        mBidRepository = bidRepository;
    }

    @Override
    public Map<String, Object> execute(Integer id) {
        Preconditions.checkState(id > 0, "id cannot be zero");

        return mBidRepository.getConfirmationInfo(id);
    }
}
