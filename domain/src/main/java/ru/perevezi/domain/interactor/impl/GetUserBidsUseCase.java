/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserBidsUseCase implements UseCase<List<PereveziLot>, Map<String, Object>> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserBidsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public List<PereveziLot> execute(Map<String, Object> argumentsMap) {
        Preconditions.checkState(argumentsMap.get("user") != null, "user id is missing");
        Preconditions.checkState(argumentsMap.get("section") != null, "section is missing");

        return mUserRepository.getUserBids(argumentsMap);
    }
}
