/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class AcceptLotUseCase implements UseCase<Boolean, Map<String, Object>> {

    private final LotRepository mLotRepository;

    @Inject
    public AcceptLotUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public Boolean execute(Map<String, Object> args) {

        Preconditions.checkState((Integer) args.get("lotId") > 0, "Lot id must be specified");
        Preconditions.checkState(args.get("confirmed") != null, "Confirmed parameter cannot be null");

        boolean confirmed = (boolean) args.get("confirmed");

        if (confirmed) {
            Preconditions.checkState(args.get("payLater") != null, "Pay later flag must be supplied when confirming");
        }

        return mLotRepository.acceptLot(args);

    }
}
