/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class UserPhoneAddUseCase implements UseCase<Void, Map<String, Object>> {

    private final UserRepository mUserRepository;

    @Inject
    public UserPhoneAddUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Void execute(Map<String, Object> args) {

        Preconditions.checkState(args.get("userId") != null, "User id cannot be null");
        Preconditions.checkState(args.get("number") != null, "Phone number cannot be null");

        mUserRepository.addUserPhone((Integer) args.get("userId"), (String) args.get("number"));

        return null;
    }
}
