/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class CheckCommentAccessUseCase implements UseCase<String, Integer> {

    private final LotRepository mLotRepository;

    @Inject
    public CheckCommentAccessUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public String execute(Integer lotId) {
        return mLotRepository.checkCommentAccess(lotId);
    }
}
