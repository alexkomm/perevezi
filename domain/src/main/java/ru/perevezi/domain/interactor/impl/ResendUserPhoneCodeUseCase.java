/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class ResendUserPhoneCodeUseCase implements UseCase<Void, Integer> {


    private final UserRepository mUserRepository;

    @Inject
    public ResendUserPhoneCodeUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }
    @Override
    public Void execute(Integer userId) {
        Preconditions.checkState(userId != null, "User id cannot be null");

        mUserRepository.resendUserPhoneCode(userId);

        return null;
    }
}
