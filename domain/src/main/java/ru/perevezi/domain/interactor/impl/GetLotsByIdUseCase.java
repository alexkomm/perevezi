/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.List;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class GetLotsByIdUseCase implements UseCase<List<PereveziLot>, String[]> {

    private final LotRepository mLotRepository;

    @Inject
    public GetLotsByIdUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public List<PereveziLot> execute(String[] ids) {
        if (ids == null) {
            throw new IllegalArgumentException("Lot id cannot be null!");
        }

       return mLotRepository.getLotList(ids);
    }
}
