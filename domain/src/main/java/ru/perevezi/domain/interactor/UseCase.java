/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor;

/**
 * Base UseCase interface
 *
 * @author komm
 */
public interface UseCase<T, A> {
    T execute(A arg);
}
