/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class ResendSmsUseCase implements UseCase<String, String> {

    private UserRepository mUserRepository;

    @Inject
    public ResendSmsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public String execute(String requestId) {
        Preconditions.checkState(requestId != null, "should not be null");

        return mUserRepository.retrySms(requestId);
    }
}
