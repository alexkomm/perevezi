/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;

public class CheckMissingVoteUseCase implements UseCase<PereveziLot, Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public CheckMissingVoteUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public PereveziLot execute(Integer targetUserId) {
        return mUserRepository.checkMissingVote(targetUserId);
    }
}
