/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CreateReviewUseCase implements UseCase<Void, Map<String, String>> {

    private final UserRepository mUserRepository;

    @Inject
    public CreateReviewUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Void execute(Map<String, String> reviewData) {
        Preconditions.checkState(reviewData.get("lotId") != null, "Lot id cannot be null");
        Preconditions.checkState(reviewData.get("vote") != null, "Vote cannot be null");
        Preconditions.checkState(reviewData.get("targetUserId") != null, "target user id cannot be null");
        Preconditions.checkState(reviewData.get("comment") != null, "Comment cannot be null");

        mUserRepository.addReview(reviewData);

        return null;
    }
}
