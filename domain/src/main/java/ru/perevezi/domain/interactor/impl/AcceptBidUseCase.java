/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.BidRepository;
import ru.perevezi.domain.utils.Preconditions;

public class AcceptBidUseCase implements UseCase<Void, Map<String, Object>>{

    private final BidRepository mBidRepository;

    @Inject
    public AcceptBidUseCase(BidRepository bidRepository) {
        mBidRepository = bidRepository;
    }

    @Override
    public Void execute(Map<String, Object> acceptData) {
        Preconditions.checkState(acceptData.get("bid_id") != null, "bid id cannot be null");
        Preconditions.checkState(acceptData.get("lot_id") != null, "lot id cannot be null");

        mBidRepository.acceptBid(acceptData);

        return null;
    }
}
