/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.BidRepository;
import ru.perevezi.domain.utils.Preconditions;

public class DeclineBidUseCase implements UseCase<Void, Map<String, Object>> {

    private final BidRepository mBidRepository;

    @Inject
    public DeclineBidUseCase(BidRepository bidRepository) {
        mBidRepository = bidRepository;
    }

    @Override
    public Void execute(Map<String, Object> declineData) {
        Preconditions.checkState(declineData.get("bid_id") != null, "bid id cannot be null");
        Preconditions.checkState(declineData.get("lot_id") != null, "lot id cannot be null");
        Preconditions.checkState(declineData.get("decline_options") != null, "decline options cannot be null");

        int declineOption = (int) declineData.get("decline_options");

        if (declineOption == 9) {
            Preconditions.checkState(declineData.get("reason") != null, "decline reason must be specified");
        }

        mBidRepository.declineBid(declineData);

        return null;
    }
}
