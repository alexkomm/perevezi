/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class MarkAsViewedUseCase implements UseCase<Void, Integer> {

    private final LotRepository mLotRepository;

    @Inject
    public MarkAsViewedUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public Void execute(Integer lotId) {
        Preconditions.checkState(lotId != null && lotId > 0, "Lot id must be present");

        mLotRepository.markAsViewed(lotId);

        return null;
    }
}
