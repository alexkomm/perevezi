/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserCommissionsUseCase implements UseCase<Map<String, Object>, Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserCommissionsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Map<String, Object> execute(Integer userId) {
        Preconditions.checkState(userId != null, "User id cannot be null");

        return mUserRepository.getUserCommissions(userId);
    }
}
