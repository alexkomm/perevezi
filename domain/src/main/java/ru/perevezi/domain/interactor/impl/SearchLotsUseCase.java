/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;

public class SearchLotsUseCase implements UseCase<List<PereveziLot>, HashMap<String, Object>> {

    private final LotRepository mLotRepository;

    @Inject
    public SearchLotsUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public List<PereveziLot> execute(HashMap<String, Object> options) {
        return mLotRepository.getLotList(options);
    }
}
