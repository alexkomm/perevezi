/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCaseArgumentless;
import ru.perevezi.domain.repository.UserRepository;

public class LogoutUseCase implements UseCaseArgumentless<Boolean> {

    private final UserRepository mUserRepository;

    @Inject
    public LogoutUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }

    @Override
    public Boolean execute() {
        return mUserRepository.logout();
    }
}
