/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CheckBidAccessUseCase implements UseCase<String, Integer> {

    private final LotRepository mLotRepository;

    @Inject
    public CheckBidAccessUseCase(LotRepository lotRepository) {
        mLotRepository = lotRepository;
    }

    @Override
    public String execute(Integer lotId) {
        Preconditions.checkState(lotId != null, "lotId cannot be null");

        return mLotRepository.checkBidAccess(lotId);
    }
}
