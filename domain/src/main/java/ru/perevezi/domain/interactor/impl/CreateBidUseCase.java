/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.BidRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CreateBidUseCase implements UseCase<PereveziBid, PereveziBid> {

    private final BidRepository mBidRepository;

    @Inject
    public CreateBidUseCase(BidRepository bidRepository) {

        mBidRepository = bidRepository;
    }

    @Override
    public PereveziBid execute(PereveziBid pereveziBid) {
        Preconditions.checkState(pereveziBid != null, "Perevezi bid cannot be null");

        return mBidRepository.createBid(pereveziBid);
    }
}
