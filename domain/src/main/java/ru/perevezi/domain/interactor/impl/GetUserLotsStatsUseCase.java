/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.domain.interactor.UseCase;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.Preconditions;

public class GetUserLotsStatsUseCase implements UseCase<ArrayList<Map<String, Integer>>, Integer> {

    private final UserRepository mUserRepository;

    @Inject
    public GetUserLotsStatsUseCase(UserRepository userRepository) {
        mUserRepository = userRepository;
    }


    @Override
    public ArrayList<Map<String, Integer>> execute(Integer userId) {
        Preconditions.checkState(userId > 0, "User id cannot be zero");

        return mUserRepository.getUserLotsStats(userId);
    }
}
