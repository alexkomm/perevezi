/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.interactor.impl;

import javax.inject.Inject;

import ru.perevezi.domain.entities.PereveziSettings;
import ru.perevezi.domain.interactor.UseCaseArgumentless;
import ru.perevezi.domain.repository.SettingsRepository;

public class GetGeneralSettingsUseCase implements UseCaseArgumentless<PereveziSettings> {

    private final SettingsRepository mSettingsRepository;

    @Inject
    public GetGeneralSettingsUseCase(SettingsRepository settingsRepository) {
        mSettingsRepository = settingsRepository;
    }

    @Override
    public PereveziSettings execute() {
        return mSettingsRepository.getGeneralSettings();
    }
}
