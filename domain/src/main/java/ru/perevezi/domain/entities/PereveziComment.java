/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.entities;

import java.math.BigDecimal;

public class PereveziComment {

    private int cid;
    private int pid;
    private int nid;
    private int uid;
    private String subject;
    private String comment;
    private long timestamp;
    private int status;
    private String thread;
    private String name;
    private String mail;
    private BigDecimal bidAmount;
    private String authorName;

    /**
     *
     * @return
     * The cid
     */
    public int getCid() {
        return cid;
    }

    /**
     *
     * @param cid
     * The cid
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     *
     * @return
     * The pid
     */
    public int getPid() {
        return pid;
    }

    /**
     *
     * @param pid
     * The pid
     */
    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     *
     * @return
     * The nid
     */
    public int getNid() {
        return nid;
    }

    /**
     *
     * @param nid
     * The nid
     */
    public void setNid(int nid) {
        this.nid = nid;
    }

    /**
     *
     * @return
     * The uid
     */
    public int getUid() {
        return uid;
    }

    /**
     *
     * @param uid
     * The uid
     */
    public void setUid(int uid) {
        this.uid = uid;
    }

    /**
     *
     * @return
     * The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     * The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     * The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     * The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     * The timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     *
     * @param timestamp
     * The timestamp
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     *
     * @return
     * The status
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The thread
     */
    public String getThread() {
        return thread;
    }

    /**
     *
     * @param thread
     * The thread
     */
    public void setThread(String thread) {
        this.thread = thread;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The mail
     */
    public String getMail() {
        return mail;
    }

    /**
     *
     * @param mail
     * The mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }


    /**
     *
     * @return
     * The bidAmount
     */
    public BigDecimal getBidAmount() {
        return bidAmount;
    }

    /**
     *
     * @param bidAmount
     * The bid_amount
     */
    public void setBidAmount(BigDecimal bidAmount) {
        this.bidAmount = bidAmount;
    }

    /**
     *
     * @return
     * The authorName
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     *
     * @param authorName
     * The author_name
     */
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
