/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.entities;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Perevezi account model
 */
public class PereveziAccount {
    private String mName;
    private String mPassword;
    private String mAuthToken;

    private int mExtId;

    private String mFullName;
    private String mEmail;
    private String mPhone;

    private boolean mVendor;

    private boolean mClient;

    private int mCountLots;

    private int mCompletedLots;

    private int mCountBids;

    private int mCompletedBids;

    private List<Map<String, Object>> mCars;

    private Calendar mRegistrationDate;

    private Calendar mLastAccessDate;

    private String mCompany;

    private String mHomeRegion;

    private int mPositiveVotes;

    private int mNeutralVotes;

    private int mNegativeVotes;

    private String mInfo;

    private String mProfileImageUrl;

    private float mRating;

    private BigDecimal balance;

    private int proDaysLeft;

    private int smsLeft;

    private int phoneConfirmStatus;

    public PereveziAccount(String name, String password) {
        mName = name;
        mPassword = password;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public int getExtId() {
        return mExtId;
    }

    public void setExtId(int extId) {
        mExtId = extId;
    }

    public boolean isVendor() {
        return mVendor;
    }

    public void setVendor(boolean isVendor) {
        mVendor = isVendor;
    }

    public boolean isClient() {
        return mClient;
    }

    public void setClient(boolean isClient) {
        mClient = isClient;
    }

    public int getCountLots() {
        return mCountLots;
    }

    public void setCountLots(int countLots) {
        mCountLots = countLots;
    }

    public int getCountBids() {
        return mCountBids;
    }

    public void setCountBids(int countBids) {
        mCountBids = countBids;
    }

    public int getCompletedBids() {
        return mCompletedBids;
    }

    public void setCompletedBids(int completedBids) {
        mCompletedBids = completedBids;
    }

    public List<Map<String, Object>> getCars() {
        return mCars;
    }

    public void setCars(List<Map<String, Object>> cars) {
        mCars = cars;
    }

    public Calendar getRegistrationDate() {
        return mRegistrationDate;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        mRegistrationDate = registrationDate;
    }

    public Calendar getLastAccessDate() {
        return mLastAccessDate;
    }

    public void setLastAccessDate(Calendar lastAccessDate) {
        mLastAccessDate = lastAccessDate;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getHomeRegion() {
        return mHomeRegion;
    }

    public void setHomeRegion(String homeRegion) {
        mHomeRegion = homeRegion;
    }

    public int getCompletedLots() {
        return mCompletedLots;
    }

    public void setCompletedLots(int completedLots) {
        mCompletedLots = completedLots;
    }

    public int getPositiveVotes() {
        return mPositiveVotes;
    }

    public void setPositiveVotes(int positiveVotes) {
        mPositiveVotes = positiveVotes;
    }

    public int getNeutralVotes() {
        return mNeutralVotes;
    }

    public void setNeutralVotes(int neutralVotes) {
        mNeutralVotes = neutralVotes;
    }

    public int getNegativeVotes() {
        return mNegativeVotes;
    }

    public void setNegativeVotes(int negativeVotes) {
        mNegativeVotes = negativeVotes;
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    public String getProfileImageUrl() {
        return mProfileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        mProfileImageUrl = profileImageUrl;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        mRating = rating;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getProDaysLeft() {
        return proDaysLeft;
    }

    public void setProDaysLeft(int proDaysLeft) {
        this.proDaysLeft = proDaysLeft;
    }

    public int getSmsLeft() {
        return smsLeft;
    }

    public void setSmsLeft(int smsLeft) {
        this.smsLeft = smsLeft;
    }

    public int getPhoneConfirmStatus() {
        return phoneConfirmStatus;
    }

    public void setPhoneConfirmStatus(int phoneConfirmStatus) {
        this.phoneConfirmStatus = phoneConfirmStatus;
    }
}
