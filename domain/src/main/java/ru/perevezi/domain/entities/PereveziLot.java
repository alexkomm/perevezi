/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.domain.entities;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class PereveziLot {

    public static final int LOT_ACTION_CLIENT_VOTE_PLACED = 1;
    public static final int LOT_ACTION_CLIENT_NEED_VOTE = 2;
    public static final int LOT_ACTION_CANCELED = 3;
    public static final int LOT_ACTION_CLIENT_EXPIRED_WITH_BIDS = 4;
    public static final int LOT_ACTION_CLIENT_EXPIRED_NO_BIDS = 5;
    public static final int LOT_ACTION_CLIENT_CONFIRM_COMPLETION = 6;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_COMPLETION = 7;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_WON = 8;
    public static final int LOT_ACTION_CLIENT_WAIT_CONFIRMATION = 9;
    public static final int LOT_ACTION_CLIENT_REVIEW_BIDS = 10;
    public static final int LOT_ACTION_CLIENT_WAIT_BIDS = 11;
    public static final int LOT_ACTION_PROVIDER_MAKE_BIDS = 12;

    public static final int LOT_STATUS_NEW = 0;
    public static final int LOT_STATUS_ACTIVE = 1;
    public static final int LOT_STATUS_WON = 2;
    public static final int LOT_STATUS_IN_PROCESS = 3;
    public static final int LOT_STATUS_COMPLETED = 4;
    public static final int LOT_STATUS_EXPIRED = 5;
    public static final int LOT_STATUS_CANCELED = 6;

    private PereveziAccount mAuthor;

    private int mCategoryId;

    private int mNid;

    private Calendar mCreatedDate;

    private int mStatus;

    private Cargo[] mCargos;

    private String mTitleString;

    private boolean mWithShipTime;

    private Calendar mShipDate;

    private Calendar mExpirationDate;

    private int mLoadingOptions;

    private float mWeight;

    private String mWeightUnit;

    private float mVolume;

    private String mDetails;

    private List<RoutePoint> mRoutePoints;

    private BigDecimal mMaxPrice;

    private BigDecimal mBlitzPrice;

    private boolean mForPro;

    private int mCountBids;

    private PereveziBid mRelatedBid;

    private BigDecimal mMinBid;

    private String mAuthorPhone;

    private int mActionCode;

    private Map<String, String> mReview;

    private int mMark;

    private long mCacheUpdatedTimestamp;

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }

    public Cargo[] getCargos() {
        return mCargos;
    }

    public void setCargos(Cargo[] cargos) {
        mCargos = cargos;
    }

    public String getTitleString() {
        return mTitleString;
    }

    public void setTitleString(String titleString) {
        mTitleString = titleString;
    }

    public Calendar getShipDate() {
        return mShipDate;
    }

    public void setShipDate(Calendar shipDate) {
        mShipDate = shipDate;
    }

    public boolean isWithShipTime() {
        return mWithShipTime;
    }

    public void setWithShipTime(boolean withShipTime) {
        mWithShipTime = withShipTime;
    }

    public int getLoadingOptions() {
        return mLoadingOptions;
    }

    public void setLoadingOptions(int loadingOptions) {
        mLoadingOptions = loadingOptions;
    }

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        mWeight = weight;
    }

    public float getVolume() {
        return mVolume;
    }

    public void setVolume(float volume) {
        mVolume = volume;
    }

    public String getDetails() {
        return mDetails;
    }

    public void setDetails(String details) {
        mDetails = details;
    }

    public List<RoutePoint> getRoutePoints() {
        return mRoutePoints;
    }

    public void setRoutePoints(List<RoutePoint> routePoints) {
        mRoutePoints = routePoints;
    }

    public BigDecimal getMaxPrice() {
        return mMaxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        mMaxPrice = maxPrice;
    }

    public BigDecimal getBlitzPrice() {
        return mBlitzPrice;
    }

    public void setBlitzPrice(BigDecimal blitzPrice) {
        mBlitzPrice = blitzPrice;
    }

    public boolean isForPro() {
        return mForPro;
    }

    public void setForPro(boolean forPro) {
        mForPro = forPro;
    }

    public Calendar getExpirationDate() {
        return mExpirationDate;
    }

    public void setExpirationDate(Calendar expirationDate) {
        mExpirationDate = expirationDate;
    }

    public int getNid() {
        return mNid;
    }

    public void setNid(int nid) {
        mNid = nid;
    }

    public Calendar getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        mCreatedDate = createdDate;
    }

    public String getWeightUnit() {
        return mWeightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        mWeightUnit = weightUnit;
    }

    public int getCountBids() {
        return mCountBids;
    }

    public void setCountBids(int countBids) {
        mCountBids = countBids;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public PereveziBid getRelatedBid() {
        return mRelatedBid;
    }

    public void setRelatedBid(PereveziBid relatedBid) {
        mRelatedBid = relatedBid;
    }

    public BigDecimal getMinBid() {
        return mMinBid;
    }

    public void setMinBid(BigDecimal minBid) {
        mMinBid = minBid;
    }

    public String getAuthorPhone() {
        return mAuthorPhone;
    }

    public void setAuthorPhone(String authorPhone) {
        mAuthorPhone = authorPhone;
    }

    public int getActionCode() {
        return mActionCode;
    }

    public void setActionCode(int actionCode) {
        mActionCode = actionCode;
    }

    public long getCacheUpdatedTimestamp() {
        return mCacheUpdatedTimestamp;
    }

    public void setCacheUpdatedTimestamp(long cacheUpdatedTimestamp) {
        mCacheUpdatedTimestamp = cacheUpdatedTimestamp;
    }

    public PereveziAccount getAuthor() {
        return mAuthor;
    }

    public void setAuthor(PereveziAccount author) {
        mAuthor = author;
    }

    public Map<String, String> getReview() {
        return mReview;
    }

    public void setReview(Map<String, String> review) {
        mReview = review;
    }

    public int getMark() {
        return mMark;
    }

    public void setMark(int mark) {
        mMark = mark;
    }

    public static class Cargo {
        public String title;

        public int amount;
    }

    public static class RoutePoint {

        public double latitude;

        public double longitude;

        public String country;

        public String locality;

        public String region;

        public String address;
    }
}
