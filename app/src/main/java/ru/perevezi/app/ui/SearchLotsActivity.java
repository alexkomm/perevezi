/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import java.util.HashMap;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.fragments.search.LotFilterFragment;
import ru.perevezi.app.ui.fragments.search.LotListFragment;

public class SearchLotsActivity extends BaseActivity {

    public static final String EXTRA_OPTIONS = "options";


    private LotListFragment mLotListFragment;

    private LotFilterFragment mLotFilterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();

        mLotListFragment = (LotListFragment) fm.findFragmentByTag(LotListFragment.TAG);
        mLotFilterFragment = (LotFilterFragment) fm.findFragmentByTag(LotFilterFragment.TAG);

        if (mLotListFragment == null && mLotFilterFragment == null) {

            String[] lotIds = null;

            if (getIntent().getExtras() != null) {
                Bundle extraBundle = getIntent().getBundleExtra(BaseActivity.EXTRA_BUNDLE);

                if (extraBundle != null) {
                    lotIds = extraBundle.getStringArray(LotListFragment.ARG_LOT_IDS);
                }
            }

            if (lotIds != null) {
                mLotListFragment = LotListFragment.newInstance(lotIds);
            } else {
                mLotListFragment = new LotListFragment();
            }


            fm.beginTransaction().add(R.id.container, mLotListFragment, LotListFragment.TAG).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.removeItem(BaseActivity.MENU_SEARCH);

        return true;
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }
}
