/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.UserBidsHostUi;
import ru.perevezi.domain.interactor.impl.GetUserBidsStatsUseCase;
import ru.perevezi.app.state.UserState;

public class UserBidsHostController implements Controller<UserBidsHostUi>, UserBidsHostUi.UiCallbacks {

    private final GetUserBidsStatsUseCase mUseCase;
    private final UserState mUserState;
    private UserBidsHostUi mUi;

    @Inject
    public UserBidsHostController(GetUserBidsStatsUseCase useCase, UserState userState) {
        mUseCase = useCase;
        mUserState = userState;
    }

    @Override
    public void initialize(UserBidsHostUi userBidsHostUi) {
        mUi = userBidsHostUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void fetchBidStats() {
        mUi.toggleOverlayProgress(true);

        AccountModel accountModel = mUserState.getCurrentAccount();

        RxUseCase.callback(mUseCase, accountModel.getExtId()).register(new RxUseCase.AsyncCallback<ArrayList<Map<String, Object>>>() {

            @Override
            public void onResultOk(ArrayList<Map<String, Object>> result) {
                mUi.initViewPager(result);
                mUi.toggleOverlayProgress(false);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleOverlayProgress(false);
            }
        });
    }
}
