/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CarModel;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.CarCard;
import ru.perevezi.domain.utils.Preconditions;

public class CarsFragment extends BaseFragment {

    private static final String KEY_CARS = "cars";
    private List<CarModel> mCars;

    CardRecyclerView mCarListRecyclerView;

    public static CarsFragment newInstance(List<CarModel> cars) {
        CarsFragment fragment = new CarsFragment();

        Bundle args = new Bundle();
        args.putParcelable(KEY_CARS, Parcels.wrap(cars));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            mCars = Parcels.unwrap(args.getParcelable(KEY_CARS));
        }

        Preconditions.checkState(mCars != null, "Cars list cannot be null");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cars, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Card> cards = new ArrayList<>(mCars.size());

        for (CarModel car : mCars) {
            cards.add(new CarCard(getActivity(), car));
        }


        mCarListRecyclerView = (CardRecyclerView) view.findViewById(R.id.rv_car_list);
        mCarListRecyclerView.setHasFixedSize(true);
        mCarListRecyclerView.setAdapter(new CardArrayRecyclerViewAdapter(getActivity(), cards));
        mCarListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCarListRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }
}
