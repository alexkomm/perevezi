/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import dagger.ObjectGraph;
import ru.perevezi.app.Injector;
import ru.perevezi.app.R;
import ru.perevezi.app.di.ActivityModule;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.StateSaver;
import ru.perevezi.app.ui.fragments.RouterFragment;
import ru.perevezi.app.ui.views.BaseUi;
import ru.perevezi.app.ui.views.RouterUi;
import ru.perevezi.app.utils.RoundImageTransformation;
import ru.perevezi.domain.utils.Preconditions;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity implements Injector, BaseUi {

    public static final int MENU_SEARCH = Menu.FIRST;
    public static final String EXTRA_BUNDLE = "extra_bundle";

    @Inject
    StateSaver mStateSaver;

    @Inject
    RxBus mBus;

    @InjectView(R.id.progress)
    @Optional
    ProgressBar mProgressBar;

    @InjectView(R.id.progress_logo_holder)
    @Optional
    ViewGroup mProgressLogoHolder;

    @InjectView(R.id.progress_overlay_stub)
    @Optional
    View mProgressOverlayHolder;

    private Toolbar mToolbar;

    private ObjectGraph mObjectGraph;

    private UiLifecycleNotifier mNotifier = new UiLifecycleNotifier();

    private ProgressDialog mProgressDialog;

    private RouterFragment mRouterFragment;
    private AccountHeader mDrawerHeader;
    private Drawer mDrawer;
    private boolean mBackArrowEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getContentViewLayoutId());

        ButterKnife.inject(this);

        ObjectGraph appGraph = ((Injector) getApplication()).getObjectGraph();
        List<Object> activityModules = getModules();
        mObjectGraph = appGraph.plus(activityModules.toArray());

        inject(this);

        if (savedInstanceState != null) {
            mStateSaver.restoreInstanceState(savedInstanceState);
        }

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();

        mRouterFragment = (RouterFragment) fm.findFragmentByTag(RouterFragment.TAG);
        if (mRouterFragment == null) {
            mRouterFragment = new RouterFragment();
            fm.beginTransaction().add(mRouterFragment, RouterFragment.TAG).commit();
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            initDrawer();

        }

        mProgressDialog = new ProgressDialog(this);
    }

    private void initDrawer() {
        DrawerImageLoader.init(new DrawerImageLoader.IDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Picasso.with(imageView.getContext()).load(uri).transform(new
                        RoundImageTransformation()).placeholder(placeholder).into
                        (imageView);
            }

            @Override
            public void cancel(ImageView imageView) {
                Picasso.with(imageView.getContext()).cancelRequest(imageView);
            }

            @Override
            public Drawable placeholder(Context ctx) {
                return null;
            }

            @Override
            public Drawable placeholder(Context context, String s) {
                return null;
            }
        });

        mDrawerHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.primary_dark)
                .withSelectionListEnabledForSingleProfile(false)
                .build();


        mDrawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(mDrawerHeader)
                .withActionBarDrawerToggle(true)
                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View view) {
                        if (isBackArrowEnabled()) {
                            onBackPressed();
                            return true;
                        }

                        return false;
                    }
                })
                .withToolbar(mToolbar)
                .withSelectedItem(-1)
                .build();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.add(0, MENU_SEARCH, Menu.NONE, R.string.menu_search_lots).setIcon(new
                IconicsDrawable(this)
                .color(Color.WHITE)
                .actionBar()
                .icon(FontAwesome.Icon.faw_search)).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_SEARCH:
                getRouterUi().navigateToSearchLots();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mStateSaver.saveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNotifier.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNotifier.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }

    @Override
    public void inject(Object target) {
        Preconditions.checkState(mObjectGraph != null, "Object graph must be assigned before injecting");
        mObjectGraph.inject(target);
    }

    public List<Object> getModules() {
        List<Object> result = new ArrayList<>();
        result.add(new ActivityModule(this, this));
        return result;
    }

    @Override
    public void onBackPressed() {
        if (mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        }
        else {
            super.onBackPressed();
        }
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

    public void registerUiLifecycleObserver(UiLifecycleObserver observer) {
        mNotifier.registerUiObserver(observer);
    }

    @Override
    public void toggleToolbarProgress(boolean show) {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void toggleOverlayProgress(boolean show) {
        if (mProgressOverlayHolder == null) {
            return;
        }

        if (mProgressOverlayHolder instanceof ViewStub) {
            mProgressOverlayHolder = ((ViewStub)mProgressOverlayHolder).inflate();
        }

        if (show) {
            AlphaAnimation inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.setDuration(200);
            mProgressOverlayHolder.setAnimation(inAnimation);
            mProgressOverlayHolder.setVisibility(View.VISIBLE);
        } else {
            AlphaAnimation outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.setDuration(200);
            mProgressOverlayHolder.setAnimation(outAnimation);
            mProgressOverlayHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void toggleLogoProgress(boolean show) {
        if (mProgressLogoHolder == null) {
            return;
        }

        if (show) {
            AlphaAnimation inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.setDuration(200);
            mProgressLogoHolder.setAnimation(inAnimation);
            mProgressLogoHolder.setVisibility(View.VISIBLE);
        } else {
            AlphaAnimation outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.setDuration(200);
            mProgressLogoHolder.setAnimation(outAnimation);
            mProgressLogoHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void toggleDialogProgress(boolean show) {
        if (mProgressDialog == null) {
            return;
        }

        if (show) {
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(getResources().getString(R.string.progress_text));
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public RouterUi getRouterUi() {
        return mRouterFragment;
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int resId) {
        showMessage(getString(resId));
    }

    @Override
    public Drawer getDrawer() {
        return mDrawer;
    }

    @Override
    public void setToolbarTitle(int resId) {
        getSupportActionBar().setTitle(resId);
    }

    @Override
    public void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void toggleBackArrow(boolean enabled) {
        mBackArrowEnabled = enabled;

        if (enabled) {
            mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        }
    }

    private boolean isBackArrowEnabled() {
        return mBackArrowEnabled;
    }

    @Override
    public AccountHeader getDrawerHeader() {
        return mDrawerHeader;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected abstract int getContentViewLayoutId();
}
