/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;
import ru.perevezi.app.BuildConfig;
import ru.perevezi.app.Injector;
import ru.perevezi.app.di.ContextModule;
import ru.perevezi.app.di.ServiceModule;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.domain.interactor.impl.BindDeviceIdUseCase;
import ru.perevezi.domain.utils.PereveziPreferences;
import ru.perevezi.domain.utils.Preconditions;

public class RegistrationIntentService extends IntentService implements Injector {

    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    @Inject
    PereveziPreferences mPreferences;

    @Inject
    BindDeviceIdUseCase mBindDeviceIdUseCase;

    private static final String TAG = "GcmRegistration";
    private ObjectGraph mObjectGraph;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mObjectGraph = ((Injector) getApplication()).getObjectGraph().plus(getModules().toArray());
        inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // [START get_token]
            InstanceID instanceID = InstanceID.getInstance(this);

            String authorizedEntity = BuildConfig.SENDER_ID;

            String token = instanceID.getToken(authorizedEntity,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            sendRegistrationToServer(token);

            mPreferences.setTokenSent(true);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);


            mPreferences.setTokenSent(false);
        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        RxUseCase.callback(mBindDeviceIdUseCase, token).register(new RxUseCase
                .AsyncCallback<String>() {


            @Override
            public void onResultOk(String result) {
                Log.i(TAG, "GCM Registration Token successfully sent to server");
            }

            @Override
            public void onError(Throwable throwable) {
                Log.i(TAG, "Error while sending Registration Token to server");
            }
        });
    }

    @Override
    public ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }

    @Override
    public void inject(Object target) {
        Preconditions.checkState(mObjectGraph != null, "Object graph must be assigned before injecting");
        mObjectGraph.inject(target);
    }

    protected List<Object> getModules() {
        List<Object> result = new ArrayList<Object>();
        result.add(new ContextModule(getApplicationContext()));
        result.add(new ServiceModule(this, this));
        return result;
    }
}
