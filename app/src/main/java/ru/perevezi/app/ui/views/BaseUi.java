/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

public interface BaseUi {
    void toggleToolbarProgress(boolean show);

    void toggleLogoProgress(boolean show);

    void toggleOverlayProgress(boolean show);

    void toggleDialogProgress(boolean show);

    void showMessage(String error);

    void showMessage(int resId);

    void setToolbarTitle(int resId);

    void setToolbarTitle(String title);

    void toggleBackArrow(boolean enabled);

    Drawer getDrawer();

    AccountHeader getDrawerHeader();

    RouterUi getRouterUi();
}
