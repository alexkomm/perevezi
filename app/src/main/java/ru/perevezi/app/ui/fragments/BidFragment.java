/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.AccountInfoCard;
import ru.perevezi.app.ui.components.OnSwipeTouchListener;
import ru.perevezi.app.ui.components.SimpleFragmentConfirmDialog;
import ru.perevezi.app.ui.controllers.BidViewController;
import ru.perevezi.app.ui.fragments.lot.LotHostListener;
import ru.perevezi.app.ui.views.BidViewUi;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.app.utils.ViewUtils;
import ru.perevezi.domain.utils.Preconditions;

public class BidFragment extends BaseFragment implements ViewSwitcher.ViewFactory, BidViewUi {

    private static final int DIALOG_FRAGMENT = 1;

    @Inject
    SettingsState mSettingsState;

    @Inject
    BidViewController mBidViewController;

    @InjectView(R.id.bid_status_text)
    TextView mBidStatusText;

    @InjectView(R.id.bid_decline_reason_group)
    ViewStub mDeclineReasonGroup;

    @InjectView(R.id.bid_reject_btn)
    Button mRejectButton;

    @InjectView(R.id.bid_accept_btn)
    Button mAcceptButton;

    @InjectView(R.id.bid_placed_date)
    TextView mBidPlacedDateTextView;

    @InjectView(R.id.bid_amount_text)
    TextView mBidAmountTextView;

    @InjectView(R.id.ship_date_text)
    TextView mShipDateTextView;

    @InjectView(R.id.bid_services_group)
    ViewStub mBidServicesGroup;

    @InjectView(R.id.bid_car_group)
    ViewStub mBidCarGroup;

    @InjectView(R.id.bid_car_photos_group)
    ViewStub mBidCarPhotosGroup;

    @InjectView(R.id.provider_info)
    CardViewNative mProviderInfoCard;

    private SimpleFragmentConfirmDialog mConfirmDialog;


    public static final String TAG = "bid_fragment";
    private static final String ARG_KEY = "bid";
    private BidModel mBid;
    private List<String> mBidPhotos = new ArrayList<>();

    private int mSwitherPosition = 0;
    private Target mImageSwitcherTarget;
    private UiCallbacks mUiCallbacks;
    private LotHostListener mLotHostController;

    GestureDetector gd;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 100;



    public static BidFragment newInstance(BidModel bid) {
        BidFragment fragment = new BidFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY, Parcels.wrap(bid));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();
        if (args != null) {
            mBid = Parcels.unwrap(args.getParcelable(ARG_KEY));
        }

        Preconditions.checkState(mBid != null, "bid cannot be null");

        mBidViewController.initialize(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources()
                .getString(R.string.bid_fragment_title, mBid.getBid()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bid, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement BidHolderInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBidStatusText.setText(StringManager.getBidStatusStrResId(mBid.getStatus()));

        Drawable bidStatusDrawable = getResources().getDrawable(StringManager.getBidStatusDrawableSmall(mBid.getStatus()));

        if (bidStatusDrawable != null) {
            mBidStatusText.setCompoundDrawablePadding(8);
            mBidStatusText.setCompoundDrawablesWithIntrinsicBounds(bidStatusDrawable, null, null, null);
        }

        if (mBid.getStatus() == BidModel.BID_STATUS_DECLINED && !Strings.isNullOrEmpty(mBid.getDeclineReason())) {
            mDeclineReasonGroup.inflate();

            ((TextView)getActivity().findViewById(R.id.reason_text)).setText(mBid.getDeclineReason());
        }

        Card providerInfo = new AccountInfoCard(getActivity(), mBid.getProvider());
        mProviderInfoCard.setCard(providerInfo);


        if (mBid.getCarImagePath() != null) {
            mBidCarPhotosGroup.setVisibility(View.VISIBLE);

            final ImageSwitcher carPhotosSwitcher = (ImageSwitcher) getActivity().findViewById(R.id.bid_car_photos_switcher);

            mBidPhotos.add(mBid.getCarImagePath());

            if (mBid.getCarAddPhotos() != null) {
                Collections.addAll(mBidPhotos, mBid.getCarAddPhotos());
            }

            ImageView[] switcherNavButtons = {
                    (ImageView) getActivity().findViewById(R.id.bid_car_back_btn),
                    (ImageView) getActivity().findViewById(R.id.bid_car_fw_btn)
            };

            if (mBidPhotos.size() < 2) {
                for (ImageView imageView : switcherNavButtons) {
                    imageView.setVisibility(View.GONE);
                }
            } else {
                for (ImageView imageView : switcherNavButtons) {
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onImageSwitch(v);
                        }
                    });
                }

                carPhotosSwitcher.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
                    @Override
                    public void onSwipeRight() {
                        setPositionPrev();
                        loadSwitcherImage();
                    }

                    @Override
                    public void onSwipeLeft() {
                        setPositionNext();
                        loadSwitcherImage();
                    }
                });
            }

            carPhotosSwitcher.setFactory(this);
            Animation inAnim = new AlphaAnimation(0, 1);
            inAnim.setDuration(200);
            Animation outAnim = new AlphaAnimation(1, 0);
            outAnim.setDuration(200);

            carPhotosSwitcher.setInAnimation(inAnim);
            carPhotosSwitcher.setOutAnimation(outAnim);

            mImageSwitcherTarget = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    carPhotosSwitcher.setImageDrawable(new BitmapDrawable(getResources(), bitmap));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    carPhotosSwitcher.setImageDrawable(placeHolderDrawable);
                }
            };

            loadSwitcherImage();
        }

        mBidAmountTextView.setText(ViewUtils.formatCurrency(mBid.getAmount().doubleValue()));

        String createdDateString = DateUtils.formatDateTime(getActivity(), mBid.getCreatedDate().getTimeInMillis(), (DateUtils.FORMAT_SHOW_DATE |
                DateUtils.FORMAT_SHOW_TIME));

        mBidPlacedDateTextView.setText(getResources().getString(R.string.bid_placed_date, createdDateString));

        String offeredDateString;

        if (mBid.getExpirationDate() != null) {
             offeredDateString = DateUtils.formatDateTime(getActivity(), mBid.getExpirationDate().getTimeInMillis(), (DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_SHOW_DATE |
                  DateUtils.FORMAT_SHOW_TIME));
        } else {
             offeredDateString = getResources().getString(R.string.lot_view_flexible_date);
        }

        SettingsModel settings = mSettingsState.getGeneralSettings();
        List<Map<String, String>> loadingOptions = settings.getLotLoadingOptions();

        mShipDateTextView.setText(offeredDateString);

        if (loadingOptions.get(mBid.getLoadingOptions()) != null) {
            mBidServicesGroup.setVisibility(View.VISIBLE);

            ((TextView)getActivity().findViewById(R.id.bid_services_text)).setText(loadingOptions.get(mBid.getLoadingOptions()).get("bid"));
        }

        if (mBid.getCarName() != null) {
            mBidCarGroup.setVisibility(View.VISIBLE);

            ((TextView)getActivity().findViewById(R.id.bid_car_text)).setText(mBid.getCarName());
            ((TextView)getActivity().findViewById(R.id.bid_car_body_text)).setText(mBid.getCarBody());
            ((TextView)getActivity().findViewById(R.id.bid_car_capacity_text)).setText(mBid
                    .getCarCapacity());
        }

        if (mBid.getCarSizes() == null || mBid.getCarSizes().equals("0x0x0")) {
            mBidCarGroup.setVisibility(View.GONE);
        } else {
            ((TextView)getActivity().findViewById(R.id.bid_car_sizes_text)).setText(mBid.getCarSizes());
        }

        if (mBid.isAcceptable()) {
            mAcceptButton.setVisibility(View.VISIBLE);

            mAcceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean hasWonBid = mLotHostController.hasWonBid();

                    if (!hasWonBid) {
                        showBidAcceptConfirmDialog();
                    } else {
                        DeclineBidFragment fragment = DeclineBidFragment.newInstance(mBid, true);

                        FragmentManager fm = getActivity().getSupportFragmentManager();

                        fm.beginTransaction().replace(R.id.container, fragment, DeclineBidFragment
                                .TAG).addToBackStack(DeclineBidFragment.TAG).commit();
                    }

                }
            });
        }

        if (mBid.isDeclinable()) {
            mRejectButton.setVisibility(View.VISIBLE);

            mRejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeclineBidFragment fragment = DeclineBidFragment.newInstance(mBid, false);

                    FragmentManager fm = getActivity().getSupportFragmentManager();

                    fm.beginTransaction().replace(R.id.container, fragment, DeclineBidFragment
                            .TAG).addToBackStack(DeclineBidFragment.TAG).commit();
                }
            });
        }
    }



    private void showBidAcceptConfirmDialog() {

        mConfirmDialog = SimpleFragmentConfirmDialog.newInstance(R.string
                .dialog_bid_accept_confirm_title, R.string.dialog_bid_accept_confirm_text, 0, 0);
        mConfirmDialog.setTargetFragment(this, DIALOG_FRAGMENT);
        mConfirmDialog.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallbacks.onBidAcceptConfirmed(mBid);
                }
                break;
        }
    }

    public void onImageSwitch(View v) {
        switch (v.getId()) {
            case R.id.bid_car_back_btn:
                setPositionPrev();
                loadSwitcherImage();
                break;
            case R.id.bid_car_fw_btn:
                setPositionNext();
                loadSwitcherImage();
                break;
        }
    }

    private void setPositionNext() {
        mSwitherPosition++;

        if (mSwitherPosition > mBidPhotos.size() - 1) {
            mSwitherPosition = 0;
        }
    }

    private void setPositionPrev() {
        mSwitherPosition--;

        if (mSwitherPosition < 0) {
            mSwitherPosition = 0;
        }
    }

    private void loadSwitcherImage() {
        Picasso picasso = Picasso.with(getActivity());
        picasso.load(mBidPhotos.get(mSwitherPosition)).into(mImageSwitcherTarget);
    }

    @Override
    public View makeView() {
        ImageView imgview = new ImageView(getActivity());
        imgview.setAdjustViewBounds(true);
        imgview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imgview.setLayoutParams(new
                ImageSwitcher.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        imgview.setBackgroundColor(0xFFFFFFFF);
        return imgview;
    }



    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }
}
