/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.List;

import ru.perevezi.app.model.BidModel;

public interface LotBidsUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    int getLotId();

    List<BidModel> getBidsList();

    void addBidsToList(List<BidModel> bidModelList);

    interface UiCallbacks {
        void onDataRefresh();
    }
}
