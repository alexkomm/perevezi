/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;

public class CategoryListRecyclerViewAdapter extends RecyclerView.Adapter<CategoryListRecyclerViewAdapter.CategoryViewHolder> {


    private final Context mContext;
    private final List<CategoryModel> mCategoryList;
    private final int mItemLayout;
    private final CategoryItemListener mCategoryItemListener;

    private Integer mSelectedPos;

    public CategoryListRecyclerViewAdapter(Context context, List<CategoryModel> categoryList, CategoryItemListener categoryItemListener) {
        this(context, categoryList, R.layout.card_category, categoryItemListener);
    }

    public CategoryListRecyclerViewAdapter(Context context, List<CategoryModel> categoryList, int itemLayout, CategoryItemListener categoryItemListener) {
        mContext = context;
        mCategoryList = categoryList;
        mItemLayout = itemLayout;
        mCategoryItemListener = categoryItemListener;
    }



    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(mItemLayout, parent, false);

        return new CategoryViewHolder(v, mCategoryItemListener);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        CategoryModel category = mCategoryList.get(position);

        holder.name.setText(category.getName());
        holder.icon.setImageResource(mContext.getResources().getIdentifier("pic_category_" +
                category
                        .getDrawableResName() + "_32dp", "drawable", mContext.getPackageName()));

        holder.itemView.setSelected(mSelectedPos != null && position == mSelectedPos);
        holder.itemView.setTag(category);

        if (holder.itemView.isSelected()) {
            ((CardView)holder.itemView).setCardBackgroundColor(ContextCompat.getColor(mContext, R.color
                    .card_activated));
        } else {
            ((CardView)holder.itemView).setCardBackgroundColor(ContextCompat.getColor(mContext, R
                    .color
                    .card_background));
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public int getSelectedPos() {
        return mSelectedPos;
    }

    public void setSelectedPos(int selectedPos) {
        mSelectedPos = selectedPos;
    }

    public interface CategoryItemListener {
        void onCategoryItemSelected(CategoryModel category, View view);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CategoryItemListener mCategoryItemListener;

        public TextView name;
        public ImageView icon;

        public CategoryViewHolder(View itemView, CategoryItemListener categoryItemListener) {
            super(itemView);

            itemView.setOnClickListener(this);

            this.mCategoryItemListener = categoryItemListener;
            this.name = (TextView) itemView.findViewById(R.id.tv_name);
            this.icon = (ImageView) itemView.findViewById(R.id.iv_icon);
        }

        @Override
        public void onClick(View v) {
            if (mSelectedPos != null) {
                notifyItemChanged(mSelectedPos);
            }

            mSelectedPos = getLayoutPosition();
            notifyItemChanged(mSelectedPos);

            mCategoryItemListener.onCategoryItemSelected((CategoryModel) v.getTag(), v);
        }
    }
}
