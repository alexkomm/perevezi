/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import android.os.Bundle;

import java.util.HashMap;

import ru.perevezi.app.model.AccountModel;

public interface RouterUi extends BaseUi {

    void init();

    void setInterfaceCallbacks(UiCallbacks uiCallbacks);

    void showAccountPickerDialog();

    void navigateToLotScreen(int id);

    void navigateToLoginScreen();

    void navigateToIntroScreen();

    void navigateToCreateLot();

    void navigateToSearchLots();

    void navigateToMyLots();

    void navigateToMyBids();

    void navigateToHomeScreen();

    void navigateToSupportScreen();

    void navigateToAboutScreen();

    void rebuildDrawerItems();

    void setDefaultScreen();

    void navigateToProfileScreen(AccountModel account);

    void navigateToProfileScreen(int userId);

    void navigateToBillingScreen();

    void showPhoneConfirmDialog(boolean cancellable);

    interface UiCallbacks {
        void onInit();

        void onAccountPicked(String name);

        void onUserLogout();

        void onAccountChanged();

    }
}
