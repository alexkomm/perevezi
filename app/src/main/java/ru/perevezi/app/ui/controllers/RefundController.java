/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.RefundUi;
import ru.perevezi.data.exception.NotAcceptableException;
import ru.perevezi.data.exception.NotAuthorizedException;
import ru.perevezi.domain.interactor.impl.CreateRefundRequestUseCase;

public class RefundController implements Controller<RefundUi>, RefundUi.UiCallbacks {
    private final CreateRefundRequestUseCase mUseCase;
    private RefundUi mUi;

    @Inject
    public RefundController(CreateRefundRequestUseCase useCase) {
        mUseCase = useCase;
    }

    @Override
    public void initialize(RefundUi refundUi) {
        mUi = refundUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onRefundRequest(Map<String, String> args) {
        mUi.toggleDialogProgress(true);

        RxUseCase.callback(mUseCase, args).register(new RxUseCase.AsyncCallback<String>() {
            @Override
            public void onResultOk(String resultMsg) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(resultMsg);
                mUi.getRouterUi().navigateToBillingScreen();
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);

                if (throwable instanceof NotAuthorizedException) {
                    mUi.showMessage(R.string.refund_request_forbid_msg);
                }

                if (throwable instanceof NotAcceptableException) {
                    mUi.showMessage(R.string.system_error_msg);
                }
            }
        });
    }
}
