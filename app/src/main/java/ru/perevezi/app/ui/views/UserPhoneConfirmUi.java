/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface UserPhoneConfirmUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void showConfirmForm();

    void showAddForm();

    void setCodeInvalid();

    interface UiCallbacks {
        void onAddButtonClicked(int userId, String userPhone);

        void onConfirmButtonClicked(int userId, int code);

        void onResendButtonClicked(int userId);
    }
}
