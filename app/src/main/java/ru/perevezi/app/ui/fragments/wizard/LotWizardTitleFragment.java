/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;
import java.util.Map;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.ui.components.LotWizardTitleView;
import ru.perevezi.app.model.wizard.TitlePage;
import ru.perevezi.app.utils.Strings;

public class LotWizardTitleFragment extends AbstractWizardFragment implements LotWizardTitleView.TitleCallbacks {

    @InjectView(R.id.lot_title_view)
    LotWizardTitleView mLotWizardTitleView;


    public static LotWizardTitleFragment newInstance(String requestCode) {
        LotWizardTitleFragment fragment = new LotWizardTitleFragment();

        Bundle args = new Bundle();
        args.putString(ARG_KEY, requestCode);

        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_title, container, false);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        final CategoryModel selectedCategory = mPageCallbacks.onGetSelectedCategory();

        if (selectedCategory == null) {
            return;
        }

        final String addMoreLabel = selectedCategory.getSettings().getAddMoreLabel();
        final String amountHint = selectedCategory.getSettings().getQuantityHint();
        final String[] titleAutocomplete = selectedCategory.getSettings().getTitleAutocomplete();

        mLotWizardTitleView.setTitleCallbacks(this);

        if (!Strings.isEmpty(addMoreLabel)) {
            mLotWizardTitleView.setAddMoreLabel(addMoreLabel);
        }

        if (!Strings.isEmpty(amountHint)) {
            mLotWizardTitleView.setAmountHint(amountHint);
        }

        if (titleAutocomplete.length > 0) {
            mLotWizardTitleView.setTitleAutocomplete(titleAutocomplete);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mLotWizardTitleView.addTitleGroup();

                for (int i = 1; i < mPage.getData().getInt("titlegroup_count"); i++) {
                    mLotWizardTitleView.addTitleGroup();
                }

                ArrayList<Map<String, String>> titleData = (ArrayList<Map<String, String>>) mPage.getData().getSerializable(TitlePage.KEY_TITLE_DATA);

                if (titleData != null) {
                    mLotWizardTitleView.loadTitleData(titleData);
                }
            }
        });
    }

    @Override
    public void onAddMoreClicked(int totalCount) {
        mPage.getData().putInt("titlegroup_count", totalCount);
    }

    @Override
    public void onSaveInput(ArrayList<Map<String, String>> titleData) {
        mPage.getData().putSerializable(TitlePage.KEY_TITLE_DATA, titleData);
        mPage.notifyDataChanged();
    }

    @Override
    protected String getPageTitleResId() {
        CategoryModel category = mPageCallbacks.onGetSelectedCategory();

        if (category == null) {
            return getResources().getString(R.string.lot_wizard_title_title);
        }

        String titleLabel = category.getSettings().getTitleHint();

        if (titleLabel == null) {
            return category.getName();
        }

        return titleLabel;
    }
}
