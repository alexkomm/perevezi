/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.base;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.AlphaAnimation;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.views.BaseUi;
import ru.perevezi.app.ui.views.RouterUi;

public abstract class BaseFragment extends Fragment implements BaseUi {

    private UiLifecycleNotifier mNotifier = new UiLifecycleNotifier();
    private BaseUi mBaseUi;

    @InjectView(R.id.progress_overlay_stub)
    @Optional
    View mProgressOverlayHolder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof BaseUi)) {
            throw new ClassCastException("Activity must implement BaseUi");
        }

        mBaseUi = (BaseUi)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mBaseUi = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mNotifier.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mNotifier.onPause();
    }

    public void registerUiLifecycleObserver(UiLifecycleObserver observer) {
        mNotifier.registerUiObserver(observer);
    }

    @Override
    public void toggleToolbarProgress(boolean show) {
        if (mBaseUi != null) {
            mBaseUi.toggleToolbarProgress(show);
        }

    }

    @Override
    public void toggleLogoProgress(boolean show) {
        if (mBaseUi != null) {
            mBaseUi.toggleLogoProgress(show);
        }
    }

    @Override
    public void toggleDialogProgress(boolean show) {
        if (mBaseUi != null) {
            mBaseUi.toggleDialogProgress(show);
        }
    }

    @Override
    public void toggleOverlayProgress(boolean show) {
        if (mProgressOverlayHolder != null) {

            if (mProgressOverlayHolder instanceof ViewStub) {
                mProgressOverlayHolder = ((ViewStub)mProgressOverlayHolder).inflate();
            }

            if (show) {
                AlphaAnimation inAnimation = new AlphaAnimation(0f, 1f);
                inAnimation.setDuration(200);
                mProgressOverlayHolder.setAnimation(inAnimation);
                mProgressOverlayHolder.setVisibility(View.VISIBLE);
            } else {
                AlphaAnimation outAnimation = new AlphaAnimation(1f, 0f);
                outAnimation.setDuration(200);
                mProgressOverlayHolder.setAnimation(outAnimation);
                mProgressOverlayHolder.setVisibility(View.GONE);
            }
        } else if (mBaseUi != null) {
            mBaseUi.toggleOverlayProgress(show);
        }
    }

    @Override
    public void showMessage(String error) {
        if (mBaseUi != null) {
            mBaseUi.showMessage(error);
        }
    }

    @Override
    public void showMessage(int resId) {
        showMessage(getString(resId));
    }

    @Override
    public RouterUi getRouterUi() {
        return mBaseUi.getRouterUi();
    }

    @Override
    public Drawer getDrawer() {
        return mBaseUi.getDrawer();
    }

    @Override
    public void setToolbarTitle(int resId) {
        mBaseUi.setToolbarTitle(resId);
    }

    @Override
    public void setToolbarTitle(String title) {
        mBaseUi.setToolbarTitle(title);
    }

    @Override
    public void toggleBackArrow(boolean enabled) {
        mBaseUi.toggleBackArrow(enabled);
    }

    @Override
    public AccountHeader getDrawerHeader() {
        return mBaseUi.getDrawerHeader();
    }
}
