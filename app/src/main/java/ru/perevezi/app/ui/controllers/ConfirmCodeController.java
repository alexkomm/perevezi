/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.mapper.AccountModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.ApplicationState;
import ru.perevezi.app.ui.views.ConfirmCodeUi;
import ru.perevezi.data.Constants;
import ru.perevezi.data.exception.NotAcceptableException;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.impl.ConfirmRegistrationUseCase;
import ru.perevezi.domain.interactor.impl.ResendSmsUseCase;
import ru.perevezi.domain.utils.PereveziPreferences;

public class ConfirmCodeController implements ConfirmCodeUi.UiCallbacks, Controller<ConfirmCodeUi> {

    ConfirmRegistrationUseCase mConfirmUseCase;

    ResendSmsUseCase mResendUseCase;

    ConfirmCodeUi mUi;

    PereveziAccountManager mAccountManager;
    private final PereveziPreferences mPereveziPreferences;

    private final AccountModelMapper mAccountMapper;
    ApplicationState mApplicationState;

    @Inject
    public ConfirmCodeController(ConfirmRegistrationUseCase confirmUseCase,
                                 ResendSmsUseCase resendUseCase,
                                 PereveziAccountManager accountManager,
                                 PereveziPreferences pereveziPreferences,
                                 AccountModelMapper accountMapper,
                                 ApplicationState applicationState) {
        mConfirmUseCase = confirmUseCase;
        mResendUseCase = resendUseCase;
        mAccountManager = accountManager;
        mPereveziPreferences = pereveziPreferences;
        mAccountMapper = accountMapper;
        mApplicationState = applicationState;
    }

    @Override
    public void initialize(ConfirmCodeUi confirmCodeUi) {
        mUi = confirmCodeUi;
        mUi.setInterfaceCallbacks(this);
    }

    @Override
    public void onConfirmRequest(String requestId, int confirmCode) {
        final Map<String, Object> args = new HashMap<>();

        args.put("requestId", requestId);
        args.put("confirmCode", confirmCode);

        mUi.toggleToolbarProgress(true);

        RxUseCase.callback(mConfirmUseCase, args).register(new RxUseCase.AsyncCallback<PereveziAccount>() {

            @Override
            public void onResultOk(PereveziAccount pereveziAccount) {
                mAccountManager.addAccount(pereveziAccount);
                mApplicationState.setCurrentAccount(mAccountMapper.mapResponse(pereveziAccount));

                mUi.toggleToolbarProgress(false);
                mUi.navigateToCreateLotScreen(mApplicationState.getCurrentAccount());
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleToolbarProgress(false);

                if (throwable instanceof NotAcceptableException) {
                    final int errorCode = ((NotAcceptableException) throwable).getErrorCode();

                    switch (errorCode) {
                        case Constants.API_RESPONSE_SMS_CONFIRM_INVALID:
                            mUi.toggleInvalid(true);
                            break;
                    }
                } else {
                    mUi.showMessage(throwable.getMessage());
                    mUi.navigateToQuickRegisterScreen();
                }
            }
        });
    }

    @Override
    public void onRetry(String requestId) {
        mUi.toggleToolbarProgress(true);

        RxUseCase.callback(mResendUseCase, requestId).register(new RxUseCase.AsyncCallback<String>() {

            @Override
            public void onResultOk(String result) {
                mUi.toggleToolbarProgress(false);
                mUi.onCodeRetry();
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleToolbarProgress(false);
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
