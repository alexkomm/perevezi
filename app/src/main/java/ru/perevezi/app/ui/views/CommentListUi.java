/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.List;

import ru.perevezi.app.model.CommentModel;

public interface CommentListUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    int getLotId();

    List<CommentModel> getCommentsList();

    void addCommentsToList(List<CommentModel> comments);

    void toggleButtonsVisibility(boolean enabled);

    void showCommentForbiddenMessage(String message);

    interface UiCallbacks {
        void onDataRefresh();
    }
}
