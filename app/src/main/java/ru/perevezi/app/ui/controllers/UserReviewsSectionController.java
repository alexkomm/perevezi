/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.Constants;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.UserReviewsSectionUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetUserReviewsUseCase;

public class UserReviewsSectionController implements Controller<UserReviewsSectionUi>, UserReviewsSectionUi.UiCallbacks {

    private final GetUserReviewsUseCase mUseCase;
    private final LotModelMapper mLotModelMapper;

    private UserReviewsSectionUi mUi;

    @Inject
    public UserReviewsSectionController(GetUserReviewsUseCase getUserReviewsUseCase,
                                        LotModelMapper lotModelMapper) {
        mUseCase = getUserReviewsUseCase;
        mLotModelMapper = lotModelMapper;
    }

    @Override
    public void initialize(UserReviewsSectionUi myLotsUi) {
        mUi = myLotsUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {
        if (!mUi.isItemsFetched()) {
            fetchReviews(0);
        }
    }

    @Override
    public void onPause() {

    }

    private void fetchReviews(final int offset) {
        toggleProgress(true, offset);

        Map<String, Integer> argumentMap = new HashMap<>();
        argumentMap.put("userId", mUi.getUserId());
        argumentMap.put("vote", mUi.getSection());
        argumentMap.put("limit", Constants.PEREVEZI_REVIEWS_LIST_LIMIT);
        argumentMap.put("offset", offset);

        RxUseCase.callback(mUseCase, argumentMap).register(new RxUseCase.AsyncCallback<List<PereveziLot>>() {


            @Override
            public void onResultOk(List<PereveziLot> result) {
                toggleProgress(false, offset);
                mUi.addReviewsToList((List<LotModel>) mLotModelMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                toggleProgress(false, offset);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onVisibleThresholdReached(int offset) {
        fetchReviews(offset);
    }

    private void toggleProgress(boolean show, int offset) {
        if (offset == 0) {
            mUi.toggleOverlayProgress(show);
        } else {
            mUi.toggleToolbarProgress(show);
        }
    }
}
