/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.tech.freak.wizardpager.model.Page;

import ru.perevezi.domain.utils.Preconditions;

class SavingDataTextWatcher implements TextWatcher {
    private View mView;
    private final Page mPage;


    SavingDataTextWatcher(View view, Page page) {
        Preconditions.checkState(view != null, "view cannot be null");
        Preconditions.checkState(page != null, "page cannot be null");
        Preconditions.checkState(view.getTag() != null, "view tag is missing");

        mView = view;
        mPage = page;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(final Editable s) {
        if (s != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    mPage.getData().putString((String) mView.getTag(), s.toString());
                    mPage.notifyDataChanged();
                }
            });

        }
    }
}
