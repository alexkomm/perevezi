/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import javax.inject.Inject;

import ru.perevezi.app.model.mapper.AccountModelMapper;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.ProfileUi;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.CheckMissingVoteUseCase;
import ru.perevezi.domain.interactor.impl.GetUserProfileUseCase;

public class ProfileController implements Controller<ProfileUi>, ProfileUi.UiCallbacks {

    private final GetUserProfileUseCase mGetUserProfileUseCase;
    private final CheckMissingVoteUseCase mCheckMissingVoteUseCase;
    private final AccountModelMapper mMapper;
    private final LotModelMapper mLotModelMapper;
    private ProfileUi mProfileUi;

    @Inject
    public ProfileController(GetUserProfileUseCase getUserProfileUseCase, CheckMissingVoteUseCase checkMissingVoteUseCase, AccountModelMapper mapper, LotModelMapper lotModelMapper) {
        mGetUserProfileUseCase = getUserProfileUseCase;
        mCheckMissingVoteUseCase = checkMissingVoteUseCase;
        mMapper = mapper;
        mLotModelMapper = lotModelMapper;
    }

    @Override
    public void initialize(ProfileUi profileUi) {
        mProfileUi = profileUi;
        mProfileUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onCheckMissingVote(int userId) {
        mProfileUi.toggleToolbarProgress(true);

        RxUseCase.callback(mCheckMissingVoteUseCase, userId).register(new RxUseCase.AsyncCallback<PereveziLot>() {


            @Override
            public void onResultOk(PereveziLot result) {
                mProfileUi.toggleToolbarProgress(false);
                mProfileUi.showNewReviewDialog(mLotModelMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                mProfileUi.toggleToolbarProgress(false);
            }
        });
    }

    @Override
    public void onGetProfileInfo(int userId) {
        mProfileUi.toggleOverlayProgress(true);

        RxUseCase.callback(mGetUserProfileUseCase, userId).register(new RxUseCase.AsyncCallback<PereveziAccount>() {


            @Override
            public void onResultOk(PereveziAccount result) {
                mProfileUi.toggleOverlayProgress(false);
                mProfileUi.showProfileInfo(mMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                mProfileUi.toggleOverlayProgress(false);
            }
        });
    }
}
