/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.model.CommentModel;
import ru.perevezi.app.model.mapper.CommentModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.CommentListUi;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.interactor.impl.GetLotCommentsUseCase;

public class LotCommentsController implements Controller<CommentListUi>, CommentListUi.UiCallbacks {
    private final GetLotCommentsUseCase mGetLotCommentsUseCase;
    private CommentListUi mCommentListUi;
    private int mLotId;

    @Inject
    public LotCommentsController(GetLotCommentsUseCase getLotCommentsUseCase) {
        mGetLotCommentsUseCase = getLotCommentsUseCase;
    }

    @Override
    public void initialize(CommentListUi commentListUi) {
        mCommentListUi = commentListUi;
        mCommentListUi.setUiCallbacks(this);

        mLotId = mCommentListUi.getLotId();
    }

    @Override
    public void onResume() {
        if (mCommentListUi.getCommentsList() == null) {
            mCommentListUi.toggleOverlayProgress(true);
            fetchLotComments();
        }
    }

    @Override
    public void onPause() {

    }

    private void fetchLotComments() {
        RxUseCase.callback(mGetLotCommentsUseCase, mLotId).register(new RxUseCase.AsyncCallback<Collection<PereveziComment>>() {


            @Override
            public void onResultOk(Collection<PereveziComment> result) {
                mCommentListUi.toggleOverlayProgress(false);

                CommentModelMapper commentModelMapper = new CommentModelMapper();
                mCommentListUi.addCommentsToList((List<CommentModel>) commentModelMapper
                        .mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                mCommentListUi.toggleOverlayProgress(false);
                mCommentListUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onDataRefresh() {
        fetchLotComments();
    }
}
