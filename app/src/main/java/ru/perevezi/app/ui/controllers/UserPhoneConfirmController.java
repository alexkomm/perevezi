/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.UserPhoneConfirmUi;
import ru.perevezi.data.exception.NotAcceptableException;
import ru.perevezi.domain.interactor.impl.ResendUserPhoneCodeUseCase;
import ru.perevezi.domain.interactor.impl.UserPhoneAddUseCase;
import ru.perevezi.domain.interactor.impl.UserPhoneConfirmUseCase;

public class UserPhoneConfirmController implements Controller<UserPhoneConfirmUi>, UserPhoneConfirmUi.UiCallbacks {

    private final UserState mUserState;
    private final UserPhoneAddUseCase mAddUseCase;
    private final UserPhoneConfirmUseCase mConfirmUseCase;
    private final ResendUserPhoneCodeUseCase mResendUseCase;
    private UserPhoneConfirmUi mUi;

    @Inject
    public UserPhoneConfirmController(UserState userState, UserPhoneAddUseCase addUseCase, UserPhoneConfirmUseCase confirmUseCase, ResendUserPhoneCodeUseCase resendUseCase) {
        mUserState = userState;
        mAddUseCase = addUseCase;
        mConfirmUseCase = confirmUseCase;
        mResendUseCase = resendUseCase;
    }

    @Override
    public void initialize(UserPhoneConfirmUi userPhoneConfirmUi) {
        mUi = userPhoneConfirmUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {
        final AccountModel account = mUserState.getCurrentAccount();

        if (account.getPhoneConfirmStatus() == AccountModel.PHONE_CONFIRM_STATUS_PENDING) {
            mUi.showConfirmForm();
        } else {
            mUi.showAddForm();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onAddButtonClicked(int userId, final String userPhone) {
        mUi.toggleDialogProgress(true);

        Map<String, Object> args = new HashMap<>(2);
        args.put("userId", userId);
        args.put("number", userPhone);

        RxUseCase.callback(mAddUseCase, args).register(new RxUseCase.AsyncCallback<Void>() {
            @Override
            public void onResultOk(Void result) {
                final AccountModel account = mUserState.getCurrentAccount();
                account.setPhone(userPhone);

                mUserState.setCurrentAccount(account);

                mUi.toggleDialogProgress(false);
                mUi.showConfirmForm();
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onConfirmButtonClicked(int userId, int code) {
        mUi.toggleDialogProgress(true);

        Map<String, Integer> args = new HashMap<>(2);
        args.put("userId", userId);
        args.put("code", code);

        RxUseCase.callback(mConfirmUseCase, args).register(new RxUseCase.AsyncCallback<Void>() {
            @Override
            public void onResultOk(Void result) {
                mUi.toggleDialogProgress(false);
                mUi.getRouterUi().navigateToHomeScreen();
                mUi.showMessage(R.string.phone_confirm_success_msg);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);

                if (throwable instanceof NotAcceptableException) {
                    mUi.setCodeInvalid();
                } else {
                    mUi.showMessage(throwable.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public void onResendButtonClicked(int userId) {
        mUi.toggleDialogProgress(true);

        RxUseCase.callback(mResendUseCase, userId).register(new RxUseCase.AsyncCallback<Void>() {
            @Override
            public void onResultOk(Void result) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(R.string.user_phone_confirm_resend_success_msg);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }
}
