/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.wizard.CategoryPage;
import ru.perevezi.app.ui.components.CategoryListRecyclerViewAdapter;
import ru.perevezi.app.ui.components.MarginDecoration;

public class LotWizardCategoryFragment extends AbstractWizardFragment {

    @InjectView(R.id.rv_category_grid)
    RecyclerView mCategoryRecyclerView;

    public static LotWizardCategoryFragment newInstance(String requestCode) {
        LotWizardCategoryFragment fragment = new LotWizardCategoryFragment();

        Bundle args = new Bundle();
        args.putString(ARG_KEY, requestCode);

        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_category_grid, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        final CategoryModel currentCategory = mPageCallbacks.onGetSelectedCategory();

        mCategoryRecyclerView.setHasFixedSize(true);
        CategoryListRecyclerViewAdapter adapter = new CategoryListRecyclerViewAdapter(getContext(),
                mCategoryList, new CategoryListRecyclerViewAdapter.CategoryItemListener() {
            @Override
            public void onCategoryItemSelected(CategoryModel category, View view) {


                if (category != null && (currentCategory == null || !currentCategory.equals
                        (category))) {
                    mPageCallbacks.onCategoryChanged(category);

                    mPage.getData().putInt(CategoryPage.KEY_CATEGORY_ID, category.getId());
                    mPage.getData().putString(CategoryPage.KEY_CATEGORY_NAME, category.getName());
                    mPage.notifyDataChanged();
                }
            }
        });

        mCategoryRecyclerView.setAdapter(adapter);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mCategoryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 5));
        } else {
            mCategoryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        }


        mCategoryRecyclerView.addItemDecoration(new MarginDecoration(getContext(), 2));

        if (currentCategory != null) {
            for (int i = 0; i < mCategoryList.size(); i++) {
                if (!currentCategory.equals(mCategoryList.get(i))) {
                    continue;
                }

                adapter.setSelectedPos(i);
                adapter.notifyItemChanged(i);
            }
        }

    }

    @Override
    protected String getPageTitleResId() {
        return getResources().getString(R.string.lot_wizard_title_category);
    }
}
