/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.crashlytics.android.Crashlytics;

import java.io.IOException;

import ru.perevezi.app.R;
import ru.perevezi.app.utils.Ln;
import ru.perevezi.app.utils.LocationUtils;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class GeocoderIntentService extends IntentService {

    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;

    public static final String EXTRA_RECEIVER = "ru.perevezi.app.ui.services.extra.EXTRA_RECEIVER";
    public static final String EXTRA_LOCATION_DATA = "ru.perevezi.app.ui.services.extra.EXTRA_LOCATION_DATA";
    public static final String EXTRA_LOCATION_NAME = "ru.perevezi.app.ui.services.extra.EXTRA_LOCATION_NAME";

    public static final String RESULT_DATA_KEY = "ru.perevezi.app.ui.services.extra.RESULT_DATA_KEY";
    public static final String RESULT_STATUS_KEY = "ru.perevezi.app.ui.services.extra.RESULT_STATUS_KEY";
    public static final String RESULT_MESSAGE_KEY = "ru.perevezi.app.ui.services.extra.RESULT_MESSAGE_KEY";
    public static final String RESULT_ACTION_KEY = "ru.perevezi.app.ui.services.extra.RESULT_ACTION_KEY";
    public static final String RESULT_INTENT_EXTRAS = "ru.perevezi.app.ui.services.extra.RESULT_INTENT_EXTRAS";

    protected ResultReceiver mReceiver;

    private String mAction;
    private Intent mIntent;

    public GeocoderIntentService() {
        super("GeocoderIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mIntent   = intent;
        mReceiver = intent.getParcelableExtra(EXTRA_RECEIVER);

        String errorMessage = "";
        Location location = intent.getParcelableExtra(EXTRA_LOCATION_DATA);
        String locationName = intent.getStringExtra(EXTRA_LOCATION_NAME);
        mAction = intent.getAction();

        Address address = null;

        if (location != null) {
            address = LocationUtils.reverseGeocodeAddress(location);
        } else if (locationName != null) {
            address = LocationUtils.geocodeAddress(locationName);
        }


        // Handle case where no address was found.
        if (address == null) {
            if (errorMessage.isEmpty()) {
                errorMessage = getString(R.string.error_geoservice_no_address_found);
            }
            deliverResultToReceiver(FAILURE_RESULT, errorMessage, null);
        } else {

            deliverResultToReceiver(SUCCESS_RESULT,
                    null, address);
        }
    }

    private void deliverResultToReceiver(int resultCode, String message, Address address) {
        Bundle bundle = new Bundle();

        bundle.putInt(RESULT_STATUS_KEY, resultCode);


        if (message != null) {
            bundle.putString(RESULT_MESSAGE_KEY, message);
        }

        if (address != null) {
            bundle.putParcelable(RESULT_DATA_KEY, address);
        }

        if (mAction != null) {
            bundle.putString(RESULT_ACTION_KEY, mAction);
        }

        if (mIntent != null) {
            bundle.putBundle(RESULT_INTENT_EXTRAS, mIntent.getExtras());
        }

        mReceiver.send(resultCode, bundle);
    }
}
