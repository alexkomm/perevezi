/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.mapper.BidModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.BidCreateUi;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.interactor.impl.CheckBidAccessUseCase;
import ru.perevezi.domain.interactor.impl.CreateBidUseCase;

import static com.google.common.base.Strings.*;

public class BidCreateController implements Controller<BidCreateUi>, BidCreateUi.UiCallBacks {
    private final CreateBidUseCase mCreateBidUseCase;
    private final CheckBidAccessUseCase mAccessUseCase;
    private final BidModelMapper mBidModelMapper;
    private BidCreateUi mBidCreateUi;

    @Inject
    public BidCreateController(CreateBidUseCase createBidUseCase, CheckBidAccessUseCase accessUseCase, BidModelMapper bidModelMapper) {
        mCreateBidUseCase = createBidUseCase;
        mAccessUseCase = accessUseCase;
        mBidModelMapper = bidModelMapper;
    }

    @Override
    public void initialize(BidCreateUi bidCreateUi) {
        mBidCreateUi = bidCreateUi;
        mBidCreateUi.setUiCallbacks(this);
    }

    @Override
    public void checkBidAccess() {
        int lotId = mBidCreateUi.getLotId();

        mBidCreateUi.toggleDialogProgress(true);

        RxUseCase.callback(mAccessUseCase, lotId).register(new RxUseCase.AsyncCallback<String>() {
            @Override
            public void onResultOk(String forbidMessage) {
                mBidCreateUi.toggleDialogProgress(false);

                if (isNullOrEmpty(forbidMessage)) {
                    mBidCreateUi.showBidForm();
                } else {
                    mBidCreateUi.showForbiddenMessage(forbidMessage);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                mBidCreateUi.toggleDialogProgress(false);
                mBidCreateUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onBidSubmitted(BidModel bidModel) {
        mBidCreateUi.toggleDialogProgress(true);

        RxUseCase.callback(mCreateBidUseCase, mBidModelMapper.mapRequest(bidModel)).register(new RxUseCase.AsyncCallback<PereveziBid>() {
            @Override
            public void onResultOk(PereveziBid result) {
                mBidCreateUi.toggleDialogProgress(false);
                mBidCreateUi.navigateToBidList(R.string.bid_success_msg);
            }

            @Override
            public void onError(Throwable throwable) {
                mBidCreateUi.toggleDialogProgress(false);
                mBidCreateUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
