/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.LotViewUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetLotDetailsUseCase;
import ru.perevezi.domain.interactor.impl.MarkAsViewedUseCase;
import ru.perevezi.domain.utils.Preconditions;

public class LotDetailsController implements LotViewUi.UiCallbacks, Controller<LotViewUi> {
    private final GetLotDetailsUseCase mGetLotDetailsUseCase;
    private final MarkAsViewedUseCase mMarkAsViewedUseCase;
    private final LotModelMapper mResponseMapper;

    private LotViewUi mUi;
    private int mLotId;

    @Inject
    public LotDetailsController(GetLotDetailsUseCase getLotDetailsUseCase, MarkAsViewedUseCase markAsViewedUseCase,
                                LotModelMapper responseMapper) {
        mGetLotDetailsUseCase = getLotDetailsUseCase;
        mMarkAsViewedUseCase = markAsViewedUseCase;
        mResponseMapper = responseMapper;
    }

    @Override
    public void initialize(LotViewUi lotViewUi) {
        mUi = lotViewUi;
        mUi.setUiCallbacks(this);

        mLotId = mUi.getLotId();
    }

    @Override
    public void onResume() {
        final LotModel lot = mUi.getLot();

        if (lot == null) {
            fetchLotInfo(false);
        }
    }

    private void markLotAsViewed() {
        RxUseCase.callback(mMarkAsViewedUseCase, mLotId).register(new RxUseCase.AsyncCallback<Void>() {
            @Override
            public void onResultOk(Void result) {
                // No op
            }

            @Override
            public void onError(Throwable throwable) {
                // No op
            }
        });
    }

    @Override
    public void onPause() {

    }

    private void fetchLotInfo(boolean resetCache) {
        Preconditions.checkState(mLotId > 0, "Lot id not found");

        if (!resetCache) {
            mUi.toggleOverlayProgress(true);
        }

        Map<String, Object> args = new HashMap<>(2);
        args.put("lotId", mLotId);
        args.put("reset", resetCache);

        RxUseCase.callback(mGetLotDetailsUseCase, args).register(new RxUseCase.AsyncCallback<PereveziLot>() {

            @Override
            public void onResultOk(PereveziLot result) {
                mUi.toggleOverlayProgress(false);

                final LotModel lot = mResponseMapper.mapResponse(result);

                if (lot.getMark() == LotModel.LOT_MARK_NEW) {
                    markLotAsViewed();
                }

                mUi.showLotInfo(lot);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleOverlayProgress(false);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onDataRefresh() {
        fetchLotInfo(true);
    }
}
