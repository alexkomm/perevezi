/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotFilterCategoryCard;
import ru.perevezi.app.ui.card.LotFilterRegionCard;
import ru.perevezi.app.ui.card.LotFilterSettingsCard;

public class LotFilterFragment extends BaseFragment {

    public static final String TAG = "lot_filter_fragment";

    public static final String ARG_KEY = "options";

    @Inject
    SettingsState mSettingsState;

    @InjectView(R.id.filter_region_card)
    CardViewNative mFilterRegionCard;

    @InjectView(R.id.filter_category_card)
    CardViewNative mFilterCategoryCard;

    @InjectView(R.id.filter_settings_card)
    CardViewNative mFilterSettingsCard;

    @InjectView(R.id.btn_search)
    Button mSearchButton;

    @InjectView(R.id.btn_clear)
    Button mClearButton;

    private HashMap<String, Object> mFilterOptions = new HashMap<>();

    public static LotFilterFragment newInstance(HashMap<String, Object> options) {
        LotFilterFragment fragment = new LotFilterFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_KEY, options);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();
        if (args != null) {
            mFilterOptions = (HashMap<String, Object>) args.getSerializable(ARG_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("options", mFilterOptions);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_filter, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            mFilterOptions = (HashMap<String, Object>) savedInstanceState.getSerializable("options");
        }

        CardHeader filterCardHeader = new CardHeader(getActivity());
        filterCardHeader.setTitle(getResources().getString(R.string.header_card_filter_region));

        Card filterRegionCard = new LotFilterRegionCard(getActivity(), mFilterOptions);
        filterRegionCard.addCardHeader(filterCardHeader);

        mFilterRegionCard.setCard(filterRegionCard);

        List<CategoryModel> categoriesList = mSettingsState.getGeneralSettings().getCategories();

        CardHeader categoryCardHeader = new CardHeader(getActivity());
        categoryCardHeader.setTitle(getResources().getString(R.string.header_card_filter_category));

        Card filterCategoryCard = new LotFilterCategoryCard(getActivity(), categoriesList, mFilterOptions);
        filterCategoryCard.addCardHeader(categoryCardHeader);

        mFilterCategoryCard.setCard(filterCategoryCard);

        CardHeader settingsCardHeader = new CardHeader(getActivity());
        settingsCardHeader.setTitle(getResources().getString(R.string.header_card_filter_settings));

        Card filterSettingsCard = new LotFilterSettingsCard(getActivity(), mFilterOptions);
        filterSettingsCard.addCardHeader(settingsCardHeader);

        mFilterSettingsCard.setCard(filterSettingsCard);

        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                LotListFragment listFragment = LotListFragment.newInstance(mFilterOptions);
                fm.beginTransaction().replace(R.id.container, listFragment, LotListFragment.TAG)
                        .addToBackStack(null).commit();
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFilterOptions = new HashMap<>();
                mFilterOptions.put("offset", 0);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                LotListFragment listFragment = LotListFragment.newInstance(mFilterOptions);
                fm.beginTransaction().replace(R.id.container, listFragment, LotListFragment.TAG)
                        .addToBackStack(null).commit();
            }
        });
    }


}
