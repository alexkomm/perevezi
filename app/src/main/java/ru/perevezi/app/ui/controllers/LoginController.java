/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.app.model.mapper.AccountModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.LoginUi;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.impl.LoginUseCase;
import ru.perevezi.domain.utils.PereveziPreferences;

public class LoginController implements Controller<LoginUi>, LoginUi.UiCallbacks {
    private final LoginUseCase mLoginUseCase;
    private final PereveziAccountManager mAccountManager;
    private final AccountModelMapper mAccountMapper;
    private final UserState mUserState;
    private final PereveziPreferences mPereveziPreferences;
    private LoginUi mLoginUi;

    @Inject
    public LoginController(LoginUseCase loginUseCase, PereveziAccountManager accountManager,
                           AccountModelMapper accountMapper,
                           UserState userState,
                           PereveziPreferences pereveziPreferences) {
        mLoginUseCase = loginUseCase;
        mAccountManager = accountManager;
        mAccountMapper = accountMapper;
        mUserState = userState;
        mPereveziPreferences = pereveziPreferences;
    }

    @Override
    public void initialize(LoginUi loginUi) {
        mLoginUi = loginUi;
        mLoginUi.setUiCallbacks(this);
    }

    @Override
    public void onLogin(final String login, final String password) {
        mLoginUi.toggleDialogProgress(true);

        Map<String, String> loginArgs = new HashMap<>();
        loginArgs.put("login", login);
        loginArgs.put("password", password);

        PereveziAccount currentAccount = mAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());

        if (currentAccount != null) {
            mAccountManager.invalidateAuthToken(currentAccount);
        }

        RxUseCase.callback(mLoginUseCase, loginArgs).register(new RxUseCase.AsyncCallback<PereveziAccount>() {

            @Override
            public void onResultOk(PereveziAccount pereveziAccount) {
                mLoginUi.toggleDialogProgress(false);

                if (mAccountManager.accountExists(pereveziAccount)) {
                    mAccountManager.updateCredentials(pereveziAccount);
                } else {
                    mAccountManager.addAccount(pereveziAccount);
                }

                mPereveziPreferences.setLoggedOut(false);
                mUserState.setCurrentAccount(mAccountMapper.mapResponse(pereveziAccount));

                mLoginUi.finishLogin(pereveziAccount.getName(), pereveziAccount.getAuthToken());
            }

            @Override
            public void onError(Throwable throwable) {
                mLoginUi.toggleDialogProgress(false);
                mLoginUi.showMessage(R.string.error_invalid_credentials);
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
