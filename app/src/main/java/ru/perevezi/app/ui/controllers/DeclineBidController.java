/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.DeclineBidUi;
import ru.perevezi.domain.interactor.impl.AcceptBidUseCase;
import ru.perevezi.domain.interactor.impl.DeclineBidUseCase;

public class DeclineBidController implements Controller<DeclineBidUi>, DeclineBidUi.UiCallbacks {

    private final DeclineBidUseCase mDeclineBidUseCase;
    private final AcceptBidUseCase mAcceptBidUseCase;
    private DeclineBidUi mDeclineBidUi;

    @Inject
    public DeclineBidController(DeclineBidUseCase declineBidUseCase, AcceptBidUseCase acceptBidUseCase) {
        mDeclineBidUseCase = declineBidUseCase;
        mAcceptBidUseCase = acceptBidUseCase;
    }

    @Override
    public void initialize(DeclineBidUi declineBidUi) {
        mDeclineBidUi = declineBidUi;
        mDeclineBidUi.setUiCallbacks(this);
    }

    @Override
    public void onDeclineButtonClicked(Map<String, Object> declineData, boolean acceptNew) {
        mDeclineBidUi.toggleDialogProgress(true);

        RxUseCase.callback(acceptNew ? mAcceptBidUseCase : mDeclineBidUseCase, declineData).register(new RxUseCase.AsyncCallback<Void>() {

            @Override
            public void onResultOk(Void result) {
                mDeclineBidUi.toggleDialogProgress(false);
                mDeclineBidUi.navigateToBidList();
            }

            @Override
            public void onError(Throwable throwable) {
                mDeclineBidUi.toggleDialogProgress(false);
                mDeclineBidUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
