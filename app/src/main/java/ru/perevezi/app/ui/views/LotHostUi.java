/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface LotHostUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    interface UiCallbacks {
        void onLotCompletionConfirmed(int lotId);

        void onLotCompletionDeclined(int lotId);
    }
}
