/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.components.DelayAutoСompleteTextView;
import ru.perevezi.app.utils.LocationUtils;

public class LotFilterRegionCard extends Card {
    private final HashMap<String, Object> mFilterOptions;

    public LotFilterRegionCard(Context context, HashMap<String, Object> filterOptions) {
        this(context, R.layout.card_filter_region, filterOptions);
    }

    public LotFilterRegionCard(Context context, int innerLayout, HashMap<String, Object> filterOptions) {
        super(context, innerLayout);
        mFilterOptions = filterOptions;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        DelayAutoСompleteTextView regionAutocompleteField = (DelayAutoСompleteTextView) parent.findViewById(R.id.et_filter_region);
        regionAutocompleteField.setAdapter(new RegionAutocompleteAdapter(mContext));
        regionAutocompleteField.setThreshold(4);
        regionAutocompleteField.setLoadingIndicator((android.widget.ProgressBar) parent.findViewById(R.id.progress_region));
        regionAutocompleteField.setOnItemClickListener(new LocationSelectedListener(regionAutocompleteField));
        regionAutocompleteField.setText((CharSequence) mFilterOptions.get("region"));
        regionAutocompleteField.addTextChangedListener(new RegionTextWatcher());
    }


    private class RegionAutocompleteAdapter extends BaseAdapter implements Filterable {

        private final Context mContext;
        private ArrayList<Map<String, String>> mResultList = new ArrayList<>();

        private RegionAutocompleteAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return mResultList.size();
        }

        @Override
        public Map<String, String> getItem(int position) {
            return mResultList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            }

            Map<String, String> item = getItem(position);
            ((TextView)convertView.findViewById(android.R.id.text1)).setText(item.get("description"));

            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (constraint != null) {
                        List<String> filterTypes = new ArrayList<>();
                        filterTypes.add("country");
                        filterTypes.add("administrative_area_level_1");

                        mResultList = LocationUtils.autocomplete(constraint.toString(), filterTypes);
                    }

                    filterResults.values = mResultList;
                    filterResults.count = mResultList.size();

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        mResultList = (ArrayList<Map<String, String>>) results.values;
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
        }
    }


    private class LocationSelectedListener implements AdapterView.OnItemClickListener {
        private DelayAutoСompleteTextView mRegionField;

        private LocationSelectedListener(DelayAutoСompleteTextView regionField) {
            mRegionField = regionField;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Map<String, String> locationItem = (Map<String, String>)parent.getItemAtPosition(position);

            mRegionField.setText(locationItem.get("description"));
            mFilterOptions.put("region", locationItem.get("description"));
        }
    }

    private class RegionTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mFilterOptions.put("region", s.toString());
        }
    }
}
