/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.CarModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BasePagerFragment;
import ru.perevezi.app.ui.controllers.ProfileController;
import ru.perevezi.app.ui.fragments.NewReviewFragment;
import ru.perevezi.app.ui.fragments.ReviewDialog;
import ru.perevezi.app.ui.fragments.profile.CarsFragment;
import ru.perevezi.app.ui.fragments.profile.ProfileFragment;
import ru.perevezi.app.ui.views.ProfileUi;
import ru.perevezi.domain.utils.Preconditions;

public class ProfileActivity extends BaseActivity implements ProfileUi, BasePagerFragment.PagerFragmentListener {

    public static final String EXTRA_USER_ID = "user_id";
    public static final String EXTRA_ACCOUNT = "user_object";

    private static final int POSITION_INFO = 0;
    private static final int POSITION_CARS = 1;

    private Integer mUserId;
    private AccountModel mAccount;
    private Fragment mPagerFragment;
    private UiCallbacks mUiCallbacks;

    @Inject
    ProfileController mProfileController;

    private ReviewDialog mReviewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            mUserId = extras.getInt(EXTRA_USER_ID);
            mAccount = Parcels.unwrap(extras.getParcelable(EXTRA_ACCOUNT));
        }

        Preconditions.checkState(mUserId != null || mAccount != null, "Id or account object must be supplied");

        mProfileController.initialize(this);

        if (savedInstanceState == null) {
            mUiCallbacks.onCheckMissingVote(mUserId);
        }

        if (mAccount == null) {
            mUiCallbacks.onGetProfileInfo(mUserId);
        } else {
            showProfileInfo(mAccount);
        }
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Fragment onGetItem(int position) {
        switch (position) {
            case POSITION_INFO:
                return ProfileFragment.newInstance(mAccount);
            case POSITION_CARS:
                return CarsFragment.newInstance(mAccount.getCars());
            default:
                return null;
        }
    }

    @Override
    public CharSequence onGetPageTitle(int position) {
        return position == 0 ? "Информация" : "Автомобили";
    }

    @Override
    public int onGetCount() {
        int count = 1;

        if (mAccount != null) {
            List<CarModel> cars = mAccount.getCars();
            if (cars != null && cars.size() > 0) {
                count++;
            }
        }

        return count;
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void showProfileInfo(AccountModel accountModel) {
        mAccount = accountModel;

        if (onGetCount() > 0) {
            FragmentManager fm = getSupportFragmentManager();

            mPagerFragment = fm.findFragmentByTag(BasePagerFragment.TAG);

            if (mPagerFragment == null) {
                mPagerFragment = BasePagerFragment.newInstance(0);
                fm.beginTransaction().replace(R.id.container, mPagerFragment, BasePagerFragment.TAG).commit();
            }
        }
    }

    @Override
    public void showNewReviewDialog(LotModel lotModel) {
        if (mReviewDialog == null) {
            mReviewDialog = ReviewDialog.newInstance(lotModel);
        }

        mReviewDialog.show(getSupportFragmentManager(), ReviewDialog.TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ReviewDialog.REVIEW_DIALOG_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    LotModel lot = Parcels.unwrap(data.getParcelableExtra(ReviewDialog.ARG_KEY_LOT));

                    if (lot != null) {
                        FragmentManager fm = getSupportFragmentManager();
                        NewReviewFragment newReviewFragment = NewReviewFragment.newInstance(lot);
                        fm.beginTransaction().replace(R.id.container, newReviewFragment, NewReviewFragment.TAG).addToBackStack(NewReviewFragment.TAG).commit();
                    }
                }
                break;
        }
    }
}


