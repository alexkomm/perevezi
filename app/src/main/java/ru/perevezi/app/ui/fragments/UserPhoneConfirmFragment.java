/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.controllers.UserPhoneConfirmController;
import ru.perevezi.app.ui.views.UserPhoneConfirmUi;

public class UserPhoneConfirmFragment extends BaseFragment implements UserPhoneConfirmUi, View.OnClickListener {

    public static final String TAG = "user_phone_confirm_fragment";

    @Inject
    UserPhoneConfirmController mController;

    @Inject
    UserState mUserState;

    @InjectView(R.id.et_user_phone)
    EditText mUserPhoneEditText;

    @InjectView(R.id.text_user_phone)
    TextView mUserPhoneTextView;

    @InjectView(R.id.user_phone_add_group)
    ViewGroup mUserPhoneAddGroup;

    @InjectView(R.id.et_sms_code)
    EditText mUserPhoneSmsCode;

    @InjectView(R.id.user_phone_confirm_group)
    ViewGroup mUserPhoneConfirmGroup;

    @InjectView(R.id.user_phone_tip)
    TextView mUserPhoneConfirmTip;

    @InjectView(R.id.btn_user_phone_add)
    Button mUserPhoneAddButton;

    @InjectView(R.id.btn_user_phone_confirm)
    Button mUserPhoneConfirmButton;

    @InjectView(R.id.btn_user_phone_resend)
    Button mUserPhoneResendButton;

    private UiCallbacks mUiCallbacks;

    public static UserPhoneConfirmFragment newInstance() {

        UserPhoneConfirmFragment fragment = new UserPhoneConfirmFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        mController.initialize(this);
        registerUiLifecycleObserver(mController);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_phone_confirm, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string
                .user_phone_confirm_title);


        final AccountModel account = mUserState.getCurrentAccount();

        mUserPhoneAddButton.setOnClickListener(this);
        mUserPhoneConfirmButton.setOnClickListener(this);
        mUserPhoneResendButton.setOnClickListener(this);

        mUserPhoneConfirmTip.setText(getString(R.string.user_phone_confirm_tip, account.getExtId()));
    }

    @Override
    public void showAddForm() {
        final AccountModel account = mUserState.getCurrentAccount();

        mUserPhoneAddGroup.setVisibility(View.VISIBLE);
        mUserPhoneConfirmGroup.setVisibility(View.GONE);

        mUserPhoneEditText.setText(account.getPhone());
    }

    @Override
    public void showConfirmForm() {
        final AccountModel account = mUserState.getCurrentAccount();

        mUserPhoneAddGroup.setVisibility(View.GONE);
        mUserPhoneConfirmGroup.setVisibility(View.VISIBLE);

        mUserPhoneTextView.setText(getString(R.string.user_phone_confirm_phone_item, account
                .getPhone()));
    }

    @Override
    public void setCodeInvalid() {
        mUserPhoneSmsCode.setError(getString(R.string.error_invalid_sms_code));
        mUserPhoneSmsCode.requestFocus();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void onClick(View v) {
        final AccountModel account = mUserState.getCurrentAccount();

        switch (v.getId()) {
            case R.id.btn_user_phone_add:
                String userPhone = mUserPhoneEditText.getText().toString();

                if (userPhone.isEmpty()) {
                    mUserPhoneEditText.setError(getString(R.string.error_field_required));
                    mUserPhoneEditText.requestFocus();

                    return;
                }

                mUiCallbacks.onAddButtonClicked(account.getExtId(), userPhone);

                break;

            case R.id.btn_user_phone_confirm:
                String smsCode = mUserPhoneSmsCode.getText().toString();

                if (smsCode.isEmpty()) {
                    mUserPhoneSmsCode.setError(getString(R.string.error_field_required));
                    mUserPhoneSmsCode.requestFocus();

                    return;
                }

                mUiCallbacks.onConfirmButtonClicked(account.getExtId(), Integer.parseInt(smsCode));

                break;
            case R.id.btn_user_phone_resend:

                mUiCallbacks.onResendButtonClicked(account.getExtId());

                break;
        }
    }
}
