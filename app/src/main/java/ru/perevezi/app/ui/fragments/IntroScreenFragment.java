/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseFragment;

public class IntroScreenFragment extends BaseFragment {

    @InjectView(R.id.btn_sign_in)
    Button mButtonSignIn;

    @InjectView(R.id.btn_create_lot)
    Button mButtonCreateLot;

    IntroScreenListener mIntroScreenListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        return inflater.inflate(R.layout.fragment_intro_screen, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Card card = new Card(getActivity(), R.layout.card_intro_steps);

        CardHeader cardHeader = new CardHeader(getActivity());
        cardHeader.setTitle(getResources().getString(R.string.header_into_steps));

        card.addCardHeader(cardHeader);

        CardViewNative cardViewNative = (CardViewNative)view.findViewById(R.id.intro_card);
        cardViewNative.setCard(card);

        mButtonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIntroScreenListener != null) {
                    mIntroScreenListener.onSignInButtonClicked();
                }
            }
        });

        mButtonCreateLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIntroScreenListener != null) {
                    mIntroScreenListener.onCreateLotButtonClicked();
                }
            }
        });
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mIntroScreenListener = (IntroScreenListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement IntroScreenListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mIntroScreenListener = null;
    }

    public interface IntroScreenListener {
        void onSignInButtonClicked();

        void onCreateLotButtonClicked();
    }
}
