/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import com.google.common.base.Strings;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.CommentUi;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.interactor.impl.CheckCommentAccessUseCase;
import ru.perevezi.domain.interactor.impl.CreateCommentUseCase;

public class CommentController implements Controller<CommentUi>, CommentUi.UiCallbacks {

    private final CreateCommentUseCase mCreateCommentUseCase;
    private final CheckCommentAccessUseCase mAccessUseCase;
    private CommentUi mCommentUi;

    @Inject
    public CommentController(CreateCommentUseCase createCommentUseCase, CheckCommentAccessUseCase accessUseCase) {
        mCreateCommentUseCase = createCommentUseCase;
        mAccessUseCase = accessUseCase;
    }

    @Override
    public void initialize(CommentUi commentUi) {
        mCommentUi = commentUi;
        commentUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void checkCommentAccess(int nid) {
        mCommentUi.toggleDialogProgress(true);

        RxUseCase.callback(mAccessUseCase, nid).register(new RxUseCase.AsyncCallback<String>() {
            @Override
            public void onResultOk(String result) {
                mCommentUi.toggleDialogProgress(false);

                if (Strings.isNullOrEmpty(result)) {
                    mCommentUi.showCommentForm();
                } else {
                    if (result.matches(".*<a href =\"/user/\\d+/mobile\">.*")) {
                        mCommentUi.getRouterUi().showPhoneConfirmDialog(false);
                    }

                    mCommentUi.showCommentForbiddenMessage(result);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                mCommentUi.toggleDialogProgress(false);
                mCommentUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onCommentSubmitted(String comment, int nid, int pid) {
        mCommentUi.toggleDialogProgress(true);

        PereveziComment pereveziComment = new PereveziComment();
        pereveziComment.setComment(comment);
        pereveziComment.setNid(nid);
        pereveziComment.setPid(pid);

        RxUseCase.callback(mCreateCommentUseCase, pereveziComment).register(new RxUseCase.AsyncCallback<PereveziComment>() {


            @Override
            public void onResultOk(PereveziComment comment) {
                mCommentUi.toggleDialogProgress(false);
                mCommentUi.navigateToCommentList(R.string.comment_success_msg);
            }

            @Override
            public void onError(Throwable throwable) {
                mCommentUi.toggleDialogProgress(false);
                mCommentUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }
}
