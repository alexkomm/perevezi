/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.lot;

import android.app.Activity;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.events.RefreshDataEvent;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.AccountInfoCard;
import ru.perevezi.app.ui.card.LotViewBidSettingsCard;
import ru.perevezi.app.ui.card.LotViewInfoCard;
import ru.perevezi.app.ui.card.LotViewRouteCard;
import ru.perevezi.app.ui.controllers.LotDetailsController;
import ru.perevezi.app.ui.views.LotViewUi;
import rx.Subscription;
import rx.android.app.AppObservable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class LotDetailsFragment extends BaseFragment implements LotViewUi {

    @Inject
    LotDetailsController mLotDetailsController;

    @Inject
    SettingsState mSettingsState;

    @Inject
    UserState mUserState;

    @Inject
    RxBus mBus;

    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectView(R.id.lot_view_group)
    ViewGroup mLotViewGroup;

    @InjectView(R.id.main_info)
    CardViewNative mCardMainInfo;

    @InjectView(R.id.route)
    CardViewNative mCardRoute;

    @InjectView(R.id.additional_info)
    CardViewNative mCardAdditionalInfo;

    @InjectView(R.id.customer_info)
    CardViewNative mCardCustomerInfo;

    private UiCallbacks mUiCallbacks;

    private int mLotId;

    private LotModel mLot;

    private LotHostListener mLotHostController;

    private CompositeSubscription mSubscription;

    public static LotDetailsFragment newInstance(int page, int lotId) {
        LotDetailsFragment fragment = new LotDetailsFragment();

        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putInt("lot_id", lotId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mLotId = args.getInt("lot_id");

        PereveziApplication.from(getActivity()).inject(this);

        if (savedInstanceState != null) {
            mLot = Parcels.unwrap(savedInstanceState.getParcelable("lot"));
        }

        mLotDetailsController.initialize(this);
        registerUiLifecycleObserver(mLotDetailsController);

        mSubscription = new CompositeSubscription();
        mSubscription.add(
                AppObservable.bindFragment(this, mBus.toObserverable()).subscribe(new Action1<Object>() {

                    @Override
                    public void call(Object o) {
                        if (o instanceof RefreshDataEvent) {
                            mUiCallbacks.onDataRefresh();
                        }
                    }
                })
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        mLotHostController.toggleLotActionView(true);

        setToolbarTitle(getResources().getString(R.string.lot_screen_title, mLotId));
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSwipeRefreshLayout!= null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onDestroy() {
        mSubscription.unsubscribe();

        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mUiCallbacks.onDataRefresh();
            }
        });

        if (mLot != null) {
            showLotInfo(mLot);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("lot", Parcels.wrap(mLot));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LotHostController");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public int getLotId() {
        return mLotId;
    }

    @Override
    public LotModel getLot() {
        return mLot;
    }

    @Override
    public void showLotInfo(LotModel lot) {
        mLot = lot;

        mLotHostController.onLotDataLoaded(lot);

        Card mainInfoCard = new LotViewInfoCard(getActivity(), lot);
        LotViewBidSettingsCard biddingCard = new LotViewBidSettingsCard(getActivity(), mSettingsState.getGeneralSettings(), mLot);
        AccountInfoCard accountInfoCard = new AccountInfoCard(getActivity(), mLot.getAuthor());

        List<Address> routePoints = mLot.getRoutePoints();
        LotViewRouteCard routeCard = new LotViewRouteCard(getActivity(), routePoints);

        if (mLotViewGroup.getVisibility() == View.GONE) {
            mCardMainInfo.setCard(mainInfoCard);
            mCardRoute.setCard(routeCard);
            mCardAdditionalInfo.setCard(biddingCard);
            mCardCustomerInfo.setCard(accountInfoCard);

            mLotViewGroup.setVisibility(View.VISIBLE);
        } else {
            mCardMainInfo.replaceCard(mainInfoCard);
            mCardRoute.replaceCard(routeCard);
            mCardAdditionalInfo.replaceCard(biddingCard);
            mCardCustomerInfo.replaceCard(accountInfoCard);
        }

        mSwipeRefreshLayout.setRefreshing(false);

    }


}
