/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.location.Address;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.LotRouteViewFragment;
import ru.perevezi.app.utils.LocationUtils;

public class LotViewRouteCard extends CardWithList {

    private List<Address> mRoutePoints;

    public LotViewRouteCard(Context context, List<Address> routePoints) {
        super(context, R.layout.card_lot_route);
        mRoutePoints = routePoints;

        init();
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        FloatingActionButton fab = (FloatingActionButton) parent.findViewById(R.id.route_view_fab);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) fab.getLayoutParams();
            params.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            fab.setLayoutParams(params);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();

                LotRouteViewFragment lotRouteViewFragment = LotRouteViewFragment.newInstance((ArrayList<Address>) mRoutePoints);

                fm.beginTransaction()
                        .replace(R.id.container, lotRouteViewFragment, LotRouteViewFragment.TAG)
                        .addToBackStack(LotRouteViewFragment.TAG)
                        .commit();
            }
        });
    }

    @Override
    protected CardHeader initCardHeader() {
        return null;
    }

    @Override
    protected void initCard() {

    }

    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> routePointList = new ArrayList<>();
        for (Address rp : mRoutePoints) {
            routePointList.add(new RoutePointListObject(this, rp));
        }

        return routePointList;
    }

    @Override
    public View setupChildView(int i, ListObject listObject, View view, ViewGroup viewGroup) {
        TextView text = (TextView)view.findViewById(android.R.id.text1);
        Address address = ((RoutePointListObject)listObject).getRoutePoint();

        text.setText(LocationUtils.formatAddress(address));

        return view;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.item_list_item_default;
    }


    public void setRoutePoints(List<Address> addressList) {
        mRoutePoints = addressList;

        ArrayList<ListObject> routePointList = new ArrayList<>();
        for (Address rp : addressList) {
            routePointList.add(new RoutePointListObject(this, rp));
        }

        getLinearListAdapter().addAll(routePointList);
    }


    public class RoutePointListObject extends CardWithList.DefaultListObject {
        private final Address mRoutePoint;

        public RoutePointListObject(Card parentCard, Address address) {
            super(parentCard);
            mRoutePoint = address;
        }

        public Address getRoutePoint() {
            return mRoutePoint;
        }
    }
}
