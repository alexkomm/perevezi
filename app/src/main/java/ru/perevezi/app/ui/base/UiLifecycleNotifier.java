/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.base;

import java.util.ArrayList;
import java.util.List;

public class UiLifecycleNotifier implements UiLifecycleObserver {

    private List<UiLifecycleObserver> mObserverList = new ArrayList<>();

    public void registerUiObserver(UiLifecycleObserver uiLifecycleObserver) {
        mObserverList.add(uiLifecycleObserver);
    }

    @Override
    public void onResume() {
        for (UiLifecycleObserver uiLifecycleObserver : mObserverList) {
            uiLifecycleObserver.onResume();
        }
    }

    @Override
    public void onPause() {
        for (UiLifecycleObserver uiLifecycleObserver : mObserverList) {
            uiLifecycleObserver.onPause();
        }
    }
}
