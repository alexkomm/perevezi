/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.lot;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.events.RefreshDataEvent;
import ru.perevezi.app.model.CommentModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.ui.card.LotCommentCard;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.controllers.LotCommentsController;
import ru.perevezi.app.ui.fragments.CommentFragment;
import ru.perevezi.app.ui.views.CommentListUi;
import ru.perevezi.domain.utils.Preconditions;
import rx.android.app.AppObservable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class LotCommentsFragment extends BaseFragment implements CommentListUi {

    @Inject
    LotCommentsController mLotCommentsController;

    @Inject
    RxBus mBus;

    @InjectView(R.id.btn_create_comment)
    Button mCreateCommentButton;

    @InjectView(R.id.no_comments_text)
    TextView mNoCommentsView;

    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int mLotId;
    private CardArrayRecyclerViewAdapter mCardArrayAdapter;
    private CardRecyclerView mRecyclerView;
    private List<CommentModel> mComments;
    private LotHostListener mLotHostController;
    private UiCallbacks mUiCallbacks;

    private CompositeSubscription mSubscription;

    public static LotCommentsFragment newInstance(int page, int lotId) {
        LotCommentsFragment fragment = new LotCommentsFragment();

        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putInt("lot_id", lotId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle args = getArguments();
        mLotId = args.getInt("lot_id");

        if (savedInstanceState != null) {
            mComments = Parcels.unwrap(savedInstanceState.getParcelable("comments"));
        }

        PereveziApplication.from(getActivity()).inject(this);

        mSubscription = new CompositeSubscription();
        mSubscription.add(
                AppObservable.bindFragment(this, mBus.toObserverable()).subscribe(new Action1<Object>() {

                    @Override
                    public void call(Object o) {
                        if (o instanceof RefreshDataEvent) {
                            mUiCallbacks.onDataRefresh();
                        }
                    }
                })
        );

        mLotCommentsController.initialize(this);
        registerUiLifecycleObserver(mLotCommentsController);
    }

    @Override
    public void onResume() {
        super.onResume();
        mLotHostController.toggleLotActionView(true);

        setToolbarTitle(getResources().getString(R.string.lot_screen_title, mLotId));
    }

    @Override
    public void onPause() {
        if (mSwipeRefreshLayout!= null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }

        super.onPause();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LotHostController");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onDestroy() {
        mSubscription.unsubscribe();

        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_comment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArrayRecyclerViewAdapter(getActivity(), cards);

        //Staggered grid view
        mRecyclerView = (CardRecyclerView) getActivity().findViewById(R.id.comment_list_recyclerview);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Set the empty view
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mCardArrayAdapter);
        }

        mCreateCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();

                CommentFragment commentFragment = CommentFragment.newInstance(mLotId, null);

                fm.beginTransaction().replace(R.id.container, commentFragment,
                        CommentFragment.TAG).addToBackStack(CommentFragment.TAG).commit();
            }
        });

        if (mComments != null) {
            updateCardAdapter(mComments);
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mUiCallbacks.onDataRefresh();
            }
        });
    }

    @Override
    public void toggleButtonsVisibility(boolean visible) {
        mCreateCommentButton.setVisibility(visible ? View.VISIBLE : View.GONE);

        for (int i = 0; i < mCardArrayAdapter.getItemCount(); i++) {
            LotCommentCard commentCard = (LotCommentCard) mCardArrayAdapter.getItem(i);
            commentCard.toogleReplyButton(visible);
        }
    }

    @Override
    public void showCommentForbiddenMessage(String message) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("comments", Parcels.wrap(mComments));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {

        mUiCallbacks = uiCallbacks;
    }

    @Override
    public int getLotId() {
        return mLotId;
    }

    @Override
    public List<CommentModel> getCommentsList() {
        return mComments;
    }

    private void updateCardAdapter(List<CommentModel> comments) {
        if (mComments.size() == 0) {
            mCardArrayAdapter.clear();
            mNoCommentsView.setVisibility(View.VISIBLE);
            return;
        } else {
            mNoCommentsView.setVisibility(View.GONE);
        }

        ArrayList<Card> cards = new ArrayList<>();

        for (CommentModel comment : comments) {
            LotCommentCard card = new LotCommentCard(getActivity(), comment, true);
            cards.add(card);
        }

        mCardArrayAdapter.setCards(cards);
        mCardArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void addCommentsToList(List<CommentModel> comments) {
        Preconditions.checkState(comments != null, "comments list cannot be null");

        mComments = comments;
        updateCardAdapter(comments);

        mSwipeRefreshLayout.setRefreshing(false);
    }
}
