/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.app.model.LotModel;

public interface LotViewUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    int getLotId();

    LotModel getLot();

    void showLotInfo(LotModel lot);

    interface UiCallbacks {
        void onDataRefresh();
    }
}
