/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.Constants;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotListCard;
import ru.perevezi.app.ui.components.DynamicLoadingListener;
import ru.perevezi.app.ui.controllers.UserLotsSectionController;
import ru.perevezi.app.ui.views.UserLotsSectionUi;

public class UserLotsSectionFragment extends BaseFragment implements UserLotsSectionUi {

    private static final String ARG_KEY_SECTION = "section";
    private static final String ARG_KEY_LOTS = "lots";

    @Inject
    UserLotsSectionController mUserLotsSectionController;

    @InjectView(R.id.card_list)
    CardRecyclerView mCardRecyclerView;

    private CardArrayRecyclerViewAdapter mCardArrayAdapter;
    private LinearLayoutManager mLayoutManager;

    private int mStatus;

    private List<LotModel> mLotList;
    private UiCallbacks mUiCallbacks;
    private DynamicLoadingListener mDynamicLoadingListener;

    public static UserLotsSectionFragment newInstance(int section) {
        UserLotsSectionFragment fragment = new UserLotsSectionFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_KEY_SECTION, section);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mStatus = args.getInt(ARG_KEY_SECTION);
        }

        if (savedInstanceState != null) {
            mLotList = Parcels.unwrap(savedInstanceState.getParcelable(ARG_KEY_LOTS));
        }

        mUserLotsSectionController.initialize(this);
        registerUiLifecycleObserver(mUserLotsSectionController);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(ARG_KEY_LOTS, Parcels.wrap(mLotList));
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pager_section, container, false);
    }

    private void updateCardAdapter(List<LotModel> list) {
        List<Card> cardList = new ArrayList<>();

        for (LotModel lot : mLotList) {
            cardList.add(new LotListCard(getActivity(), lot, true));
        }

        mCardArrayAdapter.addAll(cardList);

        mCardRecyclerView.clearOnScrollListeners();

        if (mCardArrayAdapter.getItemCount() >= Constants.PEREVEZI_MY_LOTS_LIST_LIMIT) {
            mCardRecyclerView.addOnScrollListener(mDynamicLoadingListener);
        }
    }

    @Override
    public void addItemsToList(List<LotModel> list) {
        if (mLotList == null) {
            mLotList = new ArrayList<>();
        }
        
        mLotList.addAll(list);

        updateCardAdapter(list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArrayRecyclerViewAdapter(getActivity(), cards);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mCardRecyclerView.setHasFixedSize(false);
        mCardRecyclerView.setLayoutManager(mLayoutManager);
        mCardRecyclerView.setAdapter(mCardArrayAdapter);

        mDynamicLoadingListener = new DynamicLoadingListener(mCardRecyclerView, mLayoutManager,
                new DynamicLoadingListener.OnThresholdReachedListener() {
            @Override
            public void onThresholdReached(int totalCount) {
                mUiCallbacks.onVisibleThresholdReached(totalCount);
            }
        });


        if (mLotList != null) {
            updateCardAdapter(mLotList);
        }
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public boolean isItemsFetched() {
        return mLotList != null;
    }

    @Override
    public int getSection() {
        return mStatus;
    }
}
