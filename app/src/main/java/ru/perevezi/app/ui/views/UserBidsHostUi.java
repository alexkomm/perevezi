/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.ArrayList;
import java.util.Map;

public interface UserBidsHostUi extends BaseUi {
    void setUiCallbacks(UiCallbacks uiCallbacks);

    void initViewPager(ArrayList<Map<String, Object>> bidStats);

    interface UiCallbacks {
        void fetchBidStats();
    }
}
