/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardExpand;
import it.gmariotti.cardslib.library.internal.ViewToClickToExpand;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.RefundFragment;
import ru.perevezi.app.ui.fragments.SiteViewFragment;
import ru.perevezi.app.utils.ViewUtils;
import ru.perevezi.domain.utils.Preconditions;



public class CommissionsListCard extends Card implements SiteViewFragment.SiteViewListener {

    private final Integer mLotId;
    private final Integer mPaymentId;


    @InjectView(R.id.commission_amount_text)
    TextView mCommissionAmountText;

    @InjectView(R.id.lot_id_text)
    TextView mLotIdText;

    @InjectView(R.id.lot_title_text)
    TextView mLotTitleText;

    @InjectView(R.id.pay_till_text)
    TextView mPayTillText;

    private final Map<String, String> mCommissionData;

    public CommissionsListCard(Context context, Map<String, String> commissionData) {
        this(context, R.layout.card_commissions_list, commissionData);
    }

    public CommissionsListCard(Context context, int innerLayout, Map<String, String> commissionData) {
        super(context, innerLayout);

        mCommissionData = commissionData;
        mLotId  = Integer.parseInt(mCommissionData.get("lotId"));
        mPaymentId = Integer.parseInt(mCommissionData.get("paymentId"));

        Preconditions.checkState(mCommissionData != null, "Commission data cannot be null");
        Preconditions.checkState(mLotId != null, "Lot id cannot be null");
        Preconditions.checkState(mPaymentId != null, "Amount cannot be null");


        init();
    }

    private void init() {

        CardExpand expand = new CommissionCardExpand(getContext());
        addCardExpand(expand);

        ViewToClickToExpand viewToClickToExpand = ViewToClickToExpand.builder().highlightView(false)
                .setupCardElement(ViewToClickToExpand.CardElementUI.CARD);

        setViewToClickToExpand(viewToClickToExpand);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ButterKnife.inject(this, parent);

        mLotIdText.setText("№" + mLotId);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( Long.parseLong(mCommissionData.get("till")) * 1000);

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        mPayTillText.setText(mContext.getString(R.string.commission_pay_till, sdf.format(calendar.getTime())));

        BigDecimal commissionAmount = new BigDecimal(mCommissionData.get("amount"));

        mCommissionAmountText.setText(ViewUtils.formatCurrency(commissionAmount.doubleValue()));

        mLotTitleText.setText(mCommissionData.get("title"));
    }

    @Override
    public void onFinalDialogClosed() {
        Toast.makeText(mContext, R.string.transaction_completed_msg, Toast.LENGTH_SHORT);
    }

    public class CommissionCardExpand extends CardExpand implements View.OnClickListener {

        @InjectView(R.id.refund_btn)
        Button mRefundButton;

        @InjectView(R.id.pay_btn)
        Button mPayBtn;

        public CommissionCardExpand(Context context) {
            super(context, R.layout.card_commission_expand);
        }

        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            super.setupInnerViewElements(parent, view);

            ButterKnife.inject(this, parent);

            mRefundButton.setOnClickListener(this);
            mPayBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            FragmentManager fm;

            switch (v.getId()) {
                case R.id.refund_btn:
                    fm = ((AppCompatActivity) mContext).getSupportFragmentManager();

                    RefundFragment refundFragment = RefundFragment.newInstance(mLotId);
                    fm.beginTransaction().replace(R.id.container, refundFragment, RefundFragment.TAG).addToBackStack(RefundFragment.TAG).commit();
                    break;
                case R.id.pay_btn:
                    Bundle actionArgs = new Bundle();
                    actionArgs.putInt("paymentId", mPaymentId);

                    fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                    SiteViewFragment siteViewFragment = SiteViewFragment.newInstance(SiteViewFragment.Action.PAYMENT_ID_PAGE, actionArgs);

                    siteViewFragment.setSiteViewListener(CommissionsListCard.this);

                    fm.beginTransaction().replace(R.id.container, siteViewFragment, SiteViewFragment.TAG).addToBackStack(SiteViewFragment.TAG).commit();
                    break;
                default:
                    break;
            }
        }
    }
}
