/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import javax.inject.Inject;

import ru.perevezi.app.model.mapper.AccountModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.BillingUi;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.impl.SystemConnectUseCase;

public class BillingController implements Controller<BillingUi>, BillingUi.UiCallbacks {

    private final SystemConnectUseCase mUseCase;
    private final AccountModelMapper mMapper;
    private final UserState mUserState;
    private BillingUi mBillingUi;

    @Inject
    public BillingController(SystemConnectUseCase useCase, AccountModelMapper mapper, UserState userState) {
        mUseCase = useCase;
        mMapper = mapper;
        mUserState = userState;
    }

    @Override
    public void initialize(BillingUi billingUi) {
        mBillingUi = billingUi;
        mBillingUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onUpdateCredentials() {
        mBillingUi.toggleToolbarProgress(true);

        RxUseCase.callback(mUseCase).register(new RxUseCase.AsyncCallback<PereveziAccount>() {
            @Override
            public void onResultOk(PereveziAccount result) {
                mBillingUi.toggleToolbarProgress(false);
                mUserState.setCurrentAccount(mMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                mBillingUi.toggleToolbarProgress(false);
            }
        });
    }
}
