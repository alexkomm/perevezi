/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.controllers.DeclineBidController;
import ru.perevezi.app.ui.views.DeclineBidUi;
import ru.perevezi.domain.utils.Preconditions;

public class DeclineBidFragment extends BaseFragment implements DeclineBidUi {

    public static final String TAG = "decline_bid_fragment";
    private static final String ARG_KEY_NID = "nid";
    private static final String ARG_KEY_ACCEPT_NEW = "new_bid";
    private SettingsModel mSettings;

    @Inject
    SettingsState mSettingsState;

    @Inject
    DeclineBidController mController;

    @InjectView(R.id.rg_reasons)
    RadioGroup mReasonsGroup;

    @InjectView(R.id.et_reason)
    EditText mReasonEditText;

    @InjectView(R.id.btn_decline_bid)
    Button mDeclineBidButton;
    private UiCallbacks mUiCallbacks;

    private HashMap<String, Object> mDeclineDataMap = new HashMap<>(4);
    private BidModel mBid;
    private boolean mAcceptNew;

    public static DeclineBidFragment newInstance(BidModel bidModel, boolean acceptNew) {
        DeclineBidFragment fragment = new DeclineBidFragment();

        Bundle args = new Bundle();

        args.putParcelable(ARG_KEY_NID, Parcels.wrap(bidModel));
        args.putBoolean(ARG_KEY_ACCEPT_NEW, acceptNew);

        fragment.setArguments(args);

        return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        if (savedInstanceState != null) {
            mDeclineDataMap = (HashMap<String, Object>) savedInstanceState.getSerializable("decline_data");
        }

        Bundle args = getArguments();
        if (args != null) {
            mBid = Parcels.unwrap(args.getParcelable(ARG_KEY_NID));
            mAcceptNew = args.getBoolean(ARG_KEY_ACCEPT_NEW);
        }

        mSettings = mSettingsState.getGeneralSettings();

        Preconditions.checkState(mBid != null, "bid cannot be null");
        Preconditions.checkState(mSettings != null, "settings cannot be null");

        mController.initialize(this);

        mDeclineDataMap.put("lot_id", mBid.getNid());
        mDeclineDataMap.put("bid_id", mBid.getBid());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources()
                .getString(R.string.decline_bid_fragment_title));

        List<Map<String, String>> reasonsList = mSettings.getDeclineBidReasons();

        for (int i = 0; i  < reasonsList.size(); i++) {
            Map<String, String> reasonMap = reasonsList.get(i);

            RadioButton rb = new RadioButton(getActivity());
            rb.setId(Integer.parseInt(reasonMap.get("id")));
            rb.setText(reasonMap.get("value"));

            mReasonsGroup.addView(rb);
        }

        mReasonsGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
            mDeclineDataMap.put("decline_options", checkedId);

            if (checkedId == 9) {
                mReasonEditText.setVisibility(View.VISIBLE);
            } else {
                mReasonEditText.setVisibility(View.GONE);
            }
            }
        });

        mDeclineBidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUiCallbacks.onDeclineButtonClicked(mDeclineDataMap, mAcceptNew);
            }
        });

        mReasonEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mDeclineDataMap.put("reason", s.toString());
            }
        });
    }

    @Override
    public void navigateToBidList() {
        Intent intent = new Intent(getActivity(), LotActivity.class);
        intent.putExtra(LotActivity.EXTRA_ID, mBid.getNid());
        intent.putExtra(LotActivity.EXTRA_INITIAL_PAGER_ITEM, LotActivity.PAGER_ITEM_BIDS);
        intent.putExtra(LotActivity.EXTRA_MESSAGE, getResources().getString(R.string.bid_decline_success_msg));

        startActivity(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_decline_bid, container, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("decline_data", mDeclineDataMap);
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }
}
