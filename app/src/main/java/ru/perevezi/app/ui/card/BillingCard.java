/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.ui.fragments.SiteViewFragment;
import ru.perevezi.app.utils.Strings;
import ru.perevezi.app.utils.ViewUtils;

public class BillingCard extends Card implements View.OnClickListener, SiteViewFragment.SiteViewListener {

    private final AccountModel mAccountModel;
    private final Type mType;
    private EditText mBalanceEditText;
    private TextView mBalanceTextView;


    public static enum Type {
        BALANCE, PRO, SMS
    }

    public BillingCard(Context context, AccountModel accountModel, Type type) {
        this(context, getInnerLayout(type), accountModel, type);

    }

    public BillingCard(Context context, int innerLayout, AccountModel accountModel, Type type) {
        super(context, innerLayout);
        mAccountModel = accountModel;
        mType = type;

        init();
    }

    private void init() {

    }


    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        switch (mType) {
            case BALANCE:
                mBalanceTextView = (TextView) parent.findViewById(R.id.balance_amount);
                mBalanceTextView.setText(ViewUtils.formatCurrency(mAccountModel.getBalance()
                        .doubleValue()));

                mBalanceEditText = (EditText) parent.findViewById(R.id.fillup_amount_et);

                break;
            case PRO:
                TextView proTextView = (TextView) parent.findViewById(R.id.billing_pro_remaining);
                TextView proSubtitle = (TextView) parent.findViewById(R.id.billing_pro_subtitle);

                if (mAccountModel.getProDaysLeft() == 0) {
                    proTextView.setVisibility(View.GONE);
                    proSubtitle.setVisibility(View.GONE);
                } else {
                    proTextView.setText(mContext.getResources().getQuantityString(R.plurals.days, mAccountModel.getProDaysLeft(), mAccountModel.getProDaysLeft()));
                }


            default:
                break;
        }

        Button billingButton = (Button) parent.findViewById(R.id.billing_button);
        billingButton.setOnClickListener(this);
    }

    public static int getInnerLayout(Type type) {
        switch (type) {
            case BALANCE:
                return R.layout.card_billing_balance;
            case PRO:
                return R.layout.card_billing_pro;
            case SMS:
                return R.layout.card_billing_sms;
            default:
                return 0;
        }
    }

    @Override
    public void onClick(View v) {
        Bundle actionArgs = null;

        if (mBalanceEditText != null && !Strings.isEmpty(mBalanceEditText.getText())) {
            actionArgs = new Bundle();
            actionArgs.putDouble("amount", Double.parseDouble(mBalanceEditText.getText().toString()));
        }

        FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
        SiteViewFragment siteViewFragment = SiteViewFragment.newInstance(SiteViewFragment.Action.PAYMENT_LIST_PAGE, actionArgs);

        siteViewFragment.setSiteViewListener(this);

        fm.beginTransaction().replace(R.id.container, siteViewFragment, SiteViewFragment.TAG).addToBackStack(SiteViewFragment.TAG).commit();
    }

    @Override
    public void onFinalDialogClosed() {
        Toast.makeText(mContext, R.string.transaction_completed_msg, Toast.LENGTH_SHORT);
    }

    public void setUserBalance(double balance) {
        if (mBalanceTextView != null) {
            mBalanceTextView.setText(ViewUtils.formatCurrency(balance));
        }
    }
}
