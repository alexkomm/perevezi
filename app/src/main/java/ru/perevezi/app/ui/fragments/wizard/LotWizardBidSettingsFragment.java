/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.wizard.BidSettingsPage;

public class LotWizardBidSettingsFragment extends AbstractWizardFragment {

    @InjectView(R.id.et_max_price)
    EditText mMaxPriceField;

    @InjectView(R.id.et_blitz_price)
    EditText mBlitzPriceField;

    @InjectView(R.id.ch_pro)
    CheckBox mProCheckBox;

    public static LotWizardBidSettingsFragment newInstance(String requestCode) {
        LotWizardBidSettingsFragment fragment = new LotWizardBidSettingsFragment();

        Bundle args = new Bundle();
        args.putString(ARG_KEY, requestCode);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_bid_settings, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mMaxPriceField.setTag(BidSettingsPage.KEY_MAX_PRICE);
        mMaxPriceField.addTextChangedListener(new SavingDataTextWatcher(mMaxPriceField, mPage));

        mBlitzPriceField.setTag(BidSettingsPage.KEY_BLITZ_PRICE);
        mBlitzPriceField.addTextChangedListener(new SavingDataTextWatcher(mBlitzPriceField, mPage));

        mProCheckBox.setTag(BidSettingsPage.KEY_PRO);
        mProCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPage.getData().putBoolean(BidSettingsPage.KEY_PRO, isChecked);
                mPage.notifyDataChanged();
            }
        });

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        });
    }

    private void loadData() {
        mMaxPriceField.setText(mPage.getData().getString(BidSettingsPage.KEY_MAX_PRICE));
    }

    @Override
    protected String getPageTitleResId() {
        return getResources().getString(R.string.lot_wizard_title_bid_settings);
    }
}
