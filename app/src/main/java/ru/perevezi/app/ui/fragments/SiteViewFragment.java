/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import javax.inject.Inject;

import ru.perevezi.app.BuildConfig;
import ru.perevezi.app.Constants;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.views.SiteViewUi;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.utils.PereveziPreferences;
import ru.perevezi.domain.utils.Preconditions;

public class SiteViewFragment extends BaseFragment implements SiteViewUi {

    public static final String TAG = "site_view_fragment";


    @Inject
    PereveziAccountManager mPereveziAccountManager;

    @Inject
    PereveziPreferences mPereveziPreferences;

    private String mUrl;

    private PereveziAccount mAccount;
    private Action mAction;
    private SiteViewListener mSiteViewListener;
    private Bundle mActionArgs;

    public static enum Action {
        PAYMENT_LIST_PAGE, PAYMENT_ID_PAGE
    }

    public static SiteViewFragment newInstance(Action action, Bundle actionArgs) {
        SiteViewFragment fragment = new SiteViewFragment();

        Bundle args = new Bundle();
        args.putBundle("arguments", actionArgs);
        args.putSerializable("action", action);

        fragment.setArguments(args);

        return fragment;
    }


    private WebView mWebView;
    private boolean mIsWebViewAvailable;

    public SiteViewFragment() {
    }


    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    /**
     * Called when the fragment is no longer resumed. Pauses the WebView.
     */
    @Override
    public void onResume() {
        mWebView.onResume();
        super.onResume();
    }

    /**
     * Called when the WebView has been detached from the fragment.
     * The WebView is no longer available after this time.
     */
    @Override
    public void onDestroyView() {
        mIsWebViewAvailable = false;
        super.onDestroyView();
    }

    /**
     * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
     */
    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    /**
     * Gets the WebView.
     */
    public WebView getWebView() {
        return mIsWebViewAvailable ? mWebView : null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mActionArgs = args.getBundle("arguments");
            mAction = (Action) args.getSerializable("action");
        }

        Preconditions.checkState(mAction != null, "Action must be passed to site view fragment");

        mUrl = BuildConfig.SITE_URL;

        mAccount = mPereveziAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        if (mWebView != null) {
            mWebView.destroy();
        }

        mWebView = new WebView(getActivity());

        mIsWebViewAvailable = true;

        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        cookieManager.setAcceptCookie(true);

        String cookie = mAccount.getAuthToken();

        cookieManager.setCookie(mUrl, cookie);
        CookieSyncManager.getInstance().sync();

        if (mWebView != null) {
            mWebView.getSettings().setLoadsImagesAutomatically(true);
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.addJavascriptInterface(new SiteViewJavaScriptProxy(getActivity()),
                    "androidAppProxy");
            mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            mWebView.setWebViewClient(new SiteViewWebViewClient() {
                @Override
                public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler,
                                                      String host, String realm) {
                    handler.proceed(BuildConfig.BASIC_AUTH_LOGIN, BuildConfig.BASIC_AUTH_PASSWORD);

                }
            });

            initAction();
        }

        return mWebView;
    }

    private void initAction() {
        switch (mAction) {
            case PAYMENT_LIST_PAGE:
                openPaymentListPage();
                break;
            case PAYMENT_ID_PAGE:
                openPaymentIdPage();
                break;
            default:
                break;
        }
    }

    @Override
    public void openPaymentIdPage() {
        int paymentId = 0;
        String url = mUrl + "/user/" + mAccount.getExtId() + "/payment";

        if (mActionArgs != null) {
            paymentId = mActionArgs.getInt("paymentId");
        }

        Preconditions.checkState(paymentId > 0, "Payment id not found");

        url += "?opid=" + paymentId;

        getWebView().loadUrl(url);
    }

    @Override
    public void openPaymentListPage() {
        double amount = 0;
        String url = mUrl + "/user/" + mAccount.getExtId() + "/account";

        if (mActionArgs != null) {
            amount = mActionArgs.getDouble("amount");
        }

        if (amount > 0) {
            url += "?sum=" + amount;
        }

        getWebView().loadUrl(url);
    }

    public boolean canGoBack() {
        return mWebView.canGoBack();
    }

    public void onBackPressed() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        }
    }

    public void setSiteViewListener(SiteViewListener siteViewListener) {
        mSiteViewListener = siteViewListener;
    }

    public interface SiteViewListener {

        void onFinalDialogClosed();

    }

    private class SiteViewWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    private class SiteViewJavaScriptProxy {

        private final Activity mActivity;

        private SiteViewJavaScriptProxy(Activity activity) {
            mActivity = activity;
        }

        @JavascriptInterface
        public void showMessage(String message) {
            Toast toast = Toast.makeText(mActivity.getApplicationContext(),
                    message,
                    Toast.LENGTH_SHORT);

            toast.show();
        }

        @JavascriptInterface
        public void showFinalDialog(String message) {
            new AlertDialog.Builder(getActivity())
                    .setMessage(message)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mSiteViewListener != null) {
                                mSiteViewListener.onFinalDialogClosed();
                            }
                        }
                    })
                    .setCancelable(false)
                    .show();

            getWebView().post(new Runnable() {
                @Override
                public void run() {
                    getWebView().destroy();
                }
            });
        }

        @JavascriptInterface
        public void navigateToLot(int lotId) {
            getRouterUi().navigateToLotScreen(lotId);
            mActivity.finish();
        }

        @JavascriptInterface
        public void navigateToBilling() {
            getRouterUi().navigateToBillingScreen();
            mActivity.finish();
        }
    }
}
