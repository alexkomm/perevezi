/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;


import ru.perevezi.app.ui.base.UiLifecycleObserver;

/**
 * Presenter interface with methods to support activity and fragment lifecycle
 */
public interface Controller<UI> extends UiLifecycleObserver {
    void initialize(UI ui);
}
