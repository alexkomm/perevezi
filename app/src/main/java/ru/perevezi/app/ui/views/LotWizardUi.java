/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.domain.entities.PereveziLot;

public interface LotWizardUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void toggleDialogProgress(boolean show);

    public interface UiCallbacks {
        void onLotCreateConfirm(PereveziLot lot);
    }

}
