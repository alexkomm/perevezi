/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.Constants;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.UserBidsSectionUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetUserBidsUseCase;

public class UserBidsSectionController implements Controller<UserBidsSectionUi>, UserBidsSectionUi.UiCallbacks {
    private final GetUserBidsUseCase mGetUserBidsUseCase;
    private final LotModelMapper mLotModelMapper;
    private final UserState mUserState;
    private UserBidsSectionUi mUi;

    @Inject
    public UserBidsSectionController(GetUserBidsUseCase getUserBidsUseCase,
                                     LotModelMapper lotModelMapper, UserState userState) {
        mGetUserBidsUseCase = getUserBidsUseCase;
        mLotModelMapper = lotModelMapper;
        mUserState = userState;
    }

    @Override
    public void initialize(UserBidsSectionUi userBidsSectionUi) {
        mUi = userBidsSectionUi;
        mUi.setUiCallbacks(this);
    }

    private void fetchBids(final int offset) {
        toggleProgress(true, offset);

        AccountModel accountModel = mUserState.getCurrentAccount();

        Map<String, Object> argumentMap = new HashMap<>();
        argumentMap.put("user", accountModel.getExtId());
        argumentMap.put("section", mUi.getSection());
        argumentMap.put("offset", offset);
        argumentMap.put("limit", Constants.PEREVEZI_MY_BIDS_LIST_LIMIT);

        RxUseCase.callback(mGetUserBidsUseCase, argumentMap).register(new RxUseCase.AsyncCallback<List<PereveziLot>>() {

            @Override
            public void onResultOk(List<PereveziLot> result) {
                toggleProgress(false, offset);
                mUi.addItemsToList((java.util.ArrayList) mLotModelMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                toggleProgress(false, offset);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {
        if (!mUi.isItemsFetched()) {
            fetchBids(0);
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onVisibleThresholdReached(int offset) {
        fetchBids(offset);
    }

    private void toggleProgress(boolean show, int offset) {
        if (offset == 0) {
            mUi.toggleOverlayProgress(show);
        } else {
            mUi.toggleToolbarProgress(show);
        }
    }
}
