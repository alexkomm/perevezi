/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.BaseRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import ru.perevezi.app.R;

public class CardArrayEmptyViewRecyclerViewAdapter extends CardArrayRecyclerViewAdapter {

    private static final int EMPTY_VIEW = -1;

    public CardArrayEmptyViewRecyclerViewAdapter(Context context, List<Card> cards) {
        super(context, cards);
    }


    @Override
    public int getItemViewType(int position) {
        if (mCards.isEmpty()) {
            return EMPTY_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        int itemCount = super.getItemCount();

        return itemCount > 0 ? itemCount : 1;
    }

    @Override
    public Card getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == EMPTY_VIEW) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new EmptyViewHolder(inflater.inflate(R.layout.view_empty_list, parent, false));
        }

        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int position) {
        if (!(cardViewHolder instanceof EmptyViewHolder)) {
            super.onBindViewHolder(cardViewHolder, position);
        }
    }

    public static class EmptyViewHolder extends CardViewHolder {

        public TextView text;

        public EmptyViewHolder(View view) {
            super(view);

            this.text = (TextView) view.findViewById(R.id.empty_list_text);
        }
    }
}
