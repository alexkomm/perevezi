/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface SiteViewUi extends BaseUi {

    void openPaymentListPage();

    void openPaymentIdPage();
}
