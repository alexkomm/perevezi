/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.app.utils.ViewUtils;
import ru.perevezi.domain.utils.Preconditions;

public class LotViewInfoCard extends Card {

    private final LotModel mLotModel;

    public LotViewInfoCard(Context context, LotModel lotModel) {
        this(context, R.layout.card_lot_main_info, lotModel);
    }

    public LotViewInfoCard(Context context, int innerLayout, LotModel lotModel) {
        super(context, innerLayout);
        Preconditions.checkState(lotModel != null, "Lot model cannot be null");

        mLotModel = lotModel;
        init();
    }

    private void init() {

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ViewUtils.inflateLotContactsAndBidView(mLotModel, parent, mContext);

        TextView titleView = (TextView)parent.findViewById(R.id.lot_title);
        titleView.setText(mLotModel.getTitle());

        TextView bodyView = (TextView)parent.findViewById(R.id.lot_body);
        bodyView.setText(mLotModel.getBody());

        TextView shipDateView = (TextView)parent.findViewById(R.id.lot_ship_date);

        String shippingInfo = mLotModel.formatShippingDate(mContext);

        if (shippingInfo == null) {
            shippingInfo = mContext.getResources().getString(R.string.lot_view_flexible_date);
        }

        shipDateView.setText(shippingInfo);

        if (mLotModel.getExpirationDate() != null) {
            long[] estimateData = ru.perevezi.app.utils.DateUtils.getIntervalData(mLotModel.getExpirationDate().getTimeInMillis(),System
                    .currentTimeMillis());

            boolean estimatedSet = false;

            for (int i = 0; i < estimateData.length; i++) {
                if (estimateData[i] > 0) {
                    estimatedSet = true;

                    ((TextView)parent.findViewById(R.id.lot_estimated_value)).setText(Long.toString(estimateData[i]));

                    String estimatedText = mContext.getResources().getQuantityString(StringManager.getTimeUnitStringResId(i), (int)estimateData[i]);
                    estimatedText += " " + mContext.getString(R.string.till_the_bidding_ends);

                    ((TextView)parent.findViewById(R.id.lot_estimated_text)).setText(estimatedText);
                    break;
                }
            }

            if (!estimatedSet) {
                parent.findViewById(R.id.lot_estimated_value).setVisibility(View.GONE);
                ((TextView)parent.findViewById(R.id.lot_estimated_text)).setText(R.string.lot_already_expired);
                parent.findViewById(R.id.lot_estimated_expired_icon).setVisibility(View.VISIBLE);
            }

        } else {
            parent.findViewById(R.id.lot_estimated_group).setVisibility(View.GONE);
        }
    }
}
