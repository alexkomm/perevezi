/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BasePagerFragment;
import ru.perevezi.app.ui.controllers.UserReviewsHostController;
import ru.perevezi.app.ui.fragments.UserReviewsSectionFragment;
import ru.perevezi.app.ui.views.UserReviewsHostUi;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.domain.utils.Preconditions;

public class UserReviewsActivity extends BaseActivity implements UserReviewsHostUi, BasePagerFragment.PagerFragmentListener {


    private static final String ARG_KEY_REVIEW_STATS = "review_stats";

    public static final String EXTRA_USER_ID = "user_id";

    @Inject
    UserReviewsHostController mController;

    private ArrayList<Map<String, Integer>> mReviewStats;
    private Fragment mPagerFragment;
    private UiCallbacks mUiCallbacks;

    private Integer mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mUserId = extras.getInt(EXTRA_USER_ID);
        }

        Preconditions.checkState(mUserId != null, "User id cannot be null");

        toggleBackArrow(true);

        mController.initialize(this);

        if (savedInstanceState == null) {
            mUiCallbacks.onGetReviewStats(mUserId);
        } else {
            mReviewStats = (ArrayList<Map<String, Integer>>) savedInstanceState.getSerializable(ARG_KEY_REVIEW_STATS);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARG_KEY_REVIEW_STATS, mReviewStats);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Fragment onGetItem(int position) {
        return UserReviewsSectionFragment.newInstance(mUserId, mReviewStats.get(position).get("section"));
    }

    @Override
    public CharSequence onGetPageTitle(int position) {
        int section = mReviewStats.get(position).get("section");
        int count  = mReviewStats.get(position).get("cnt");

        CharSequence pageTitle = getResources().getString(StringManager.getReviewSectionPagerTitleResId(section), count);

        // Generate title based on item position
        Drawable image = StringManager.getReviewSectionDrawable(section, this);

        if (image != null) {
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            // Replace blank spaces with image icon
            SpannableString sb = new SpannableString("   " + pageTitle);
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }

        return pageTitle;
    }

    @Override
    public int onGetCount() {
        return mReviewStats.size();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void initViewPager(ArrayList<Map<String, Integer>> lotStats) {
        mReviewStats = lotStats;

        if (mReviewStats.size() > 0) {

            FragmentManager fm = getSupportFragmentManager();

            mPagerFragment = fm.findFragmentByTag(BasePagerFragment.TAG);

            if (mPagerFragment == null) {
                mPagerFragment = BasePagerFragment.newInstance(0);
                fm.beginTransaction().replace(R.id.container, mPagerFragment, BasePagerFragment.TAG).commit();
            }

        } else {
            // @TODO Показываем текст и предложение разместить запрос
        }
    }
}
