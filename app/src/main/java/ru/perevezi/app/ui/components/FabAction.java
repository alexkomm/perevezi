/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;

public class FabAction extends RelativeLayout {
    private final Context mContext;
    private final Integer mActionCode;

    private LotActionViewListener mLotActionViewListener;
    private FloatingActionButton mStickyFab;

    public FabAction(Context context) {
        this(context, null, null);
    }

    public FabAction(Context context, Integer actionCode) {
        this(context, null, actionCode);
    }

    public FabAction(Context context, AttributeSet attrs, Integer actionCode) {
        this(context, attrs, 0, actionCode);
    }

    public FabAction(Context context, AttributeSet attrs, int defStyleAttr,
                     Integer actionCode) {
        super(context, attrs, defStyleAttr);

        mContext = context;
        mActionCode = actionCode;

        init();
    }

    public void setLotActionViewListener(LotActionViewListener lotActionViewListener) {
        mLotActionViewListener = lotActionViewListener;
    }

    private void init() {
        LayoutInflater.from(mContext).inflate(R.layout.view_sticky_action, this);

        mStickyFab = (FloatingActionButton) findViewById(R.id.sticky_fab);

        if (mActionCode != null) {
            switch (mActionCode) {
                case LotModel.LOT_ACTION_PROVIDER_MAKE_BIDS:

                    mStickyFab.setImageDrawable(getResources().getDrawable(R.drawable
                            .handshake_white_24));
                    mStickyFab.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mLotActionViewListener != null) {
                                mLotActionViewListener.onMakeBidButtonClicked();
                            }
                        }
                    });

                    break;
                case LotModel.LOT_ACTION_CLIENT_CONFIRM_COMPLETION:
                case LotModel.LOT_ACTION_PROVIDER_CONFIRM_COMPLETION:

                    mStickyFab.setImageDrawable(getResources().getDrawable(R.drawable
                            .thumbs_up_white_24));

                    mStickyFab.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mLotActionViewListener != null) {
                                mLotActionViewListener.onConfirmCompletionButtonClicked();
                            }
                        }
                    });

                    break;
                case LotModel.LOT_ACTION_PROVIDER_CONFIRM_WON:

                    mStickyFab.setImageDrawable(getResources().getDrawable(R.drawable.approval_white_24));

                    mStickyFab.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mLotActionViewListener != null) {
                                mLotActionViewListener.onConfirmWonButtonClicked();
                            }
                        }
                    });

                    break;
                case LotModel.LOT_ACTION_CLIENT_NEED_VOTE:

                    mStickyFab.setImageDrawable(getResources().getDrawable(R.drawable.star_white_24));

                    mStickyFab.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mLotActionViewListener != null) {
                                mLotActionViewListener.onNewReviewButtonClicked();
                            }
                        }
                    });

                    break;
                default:
                    removeAllViews();
                    return;
            }
        }

        setId(R.id.sticky_action_view);

        ViewGroup containerView = (ViewGroup) ((Activity) mContext).findViewById(R.id.root);

        if (containerView.findViewById(R.id.sticky_action_view) == null) {
            containerView.addView(this);
        }
    }


    public void setText(int stringResId) {
    }

    public void setFabDrawable(Drawable drawable) {
        mStickyFab.setImageDrawable(drawable);
    }

    public void setOnFabClickListener(OnClickListener onClickListener) {
        mStickyFab.setOnClickListener(onClickListener);
    }

    public interface LotActionViewListener {

        void onMakeBidButtonClicked();

        void onConfirmCompletionButtonClicked();

        void onConfirmWonButtonClicked();

        void onNewReviewButtonClicked();
    }
}
