/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.app.model.BidModel;

public interface BidViewUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    interface UiCallbacks {
        void onBidAcceptConfirmed(BidModel bidModel);
    }
}
