/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.Map;

public interface RefundUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    interface UiCallbacks {
        void onRefundRequest(Map<String, String> args);
    }
}
