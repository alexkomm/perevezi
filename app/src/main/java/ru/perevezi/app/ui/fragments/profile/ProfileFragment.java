/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.UserReviewsActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.components.FabAction;
import ru.perevezi.app.utils.RoundImageTransformation;
import ru.perevezi.app.utils.ViewUtils;
import ru.perevezi.domain.utils.Preconditions;

public class ProfileFragment extends BaseFragment {

    public static final String TAG = "profile_fragment";

    public static final String KEY_ACCOUNT = "account";

    private AccountModel mAccount;

    @Inject
    UserState mUserState;

    @InjectView(R.id.profile_image)
    ImageView mProfileImageView;

    @InjectView(R.id.profile_name)
    TextView mProfileNameTextView;

    @InjectView(R.id.profile_rating_bar)
    RatingBar mProfileRatingBar;

    @InjectView(R.id.profile_rating)
    TextView mProfileRatingTextView;

    @InjectView(R.id.profile_info_list)
    ListView mProfileInfoListView;

    @InjectView(R.id.profile_info)
    TextView mProfileInfoTextView;


    public static ProfileFragment newInstance(AccountModel user) {
        ProfileFragment fragment = new ProfileFragment();

        Bundle args = new Bundle();
        args.putParcelable(KEY_ACCOUNT, Parcels.wrap(user));

        fragment.setArguments(args);

        return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mAccount = Parcels.unwrap(args.getParcelable(KEY_ACCOUNT));
        }

        if (savedInstanceState != null) {
            mAccount = Parcels.unwrap(savedInstanceState.getParcelable(KEY_ACCOUNT));
        }

        Preconditions.checkState(mAccount != null, "Account cannot be null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        showProfileInfo(mAccount);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(KEY_ACCOUNT, Parcels.wrap(mAccount));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void showProfileInfo(AccountModel accountModel) {
        mAccount = accountModel;

        if (!Strings.isNullOrEmpty(mAccount.getProfileImageUrl())) {
            mProfileImageView.setVisibility(View.VISIBLE);

            Picasso picasso = Picasso.with(getActivity());
            picasso.load(mAccount.getProfileImageUrl()).transform(new
                    RoundImageTransformation()).into(mProfileImageView);
        }

        mProfileNameTextView.setText(mAccount.getFullName());

        if (mAccount.getRating() > 0) {
            mProfileRatingBar.setRating(mAccount.getRating());
            mProfileRatingTextView.setText(String.valueOf(mAccount.getRating()));
        } else {
            mProfileRatingBar.setVisibility(View.GONE);
            mProfileRatingTextView.setVisibility(View.GONE);
        }


        List<ProfileInfoItem> profileInfoItems = new ArrayList<>();

        SimpleDateFormat dateFormatter = new SimpleDateFormat(ru.perevezi.app.utils.DateUtils.DATE_FORMAT, Locale.getDefault());
        String registrationDate = dateFormatter.format(mAccount.getRegistrationDate()
                .getTimeInMillis());

        profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_registration_date), registrationDate));

        Calendar lastOnlineDate = mAccount.getLastAccessDate();

        if (!isOwnProfile() && lastOnlineDate != null) {
            final long now = System.currentTimeMillis();
            final long lastOnlineMillis = lastOnlineDate.getTimeInMillis();

            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_last_online), DateUtils.getRelativeTimeSpanString(lastOnlineMillis, now, 0, 0).toString()));
        }

        if (mAccount.isVendor()) {
            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_company), mAccount.getCompany()));

            if (mAccount.getHomeRegion() != null) {
                profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_region), mAccount.getHomeRegion()));
            }

            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_count_bids), String.valueOf(mAccount.getCountBids())));
            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_completed_bids), String.valueOf(mAccount.getCompletedBids())));
        }

        if (mAccount.isClient()) {
            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_count_lots), String.valueOf(mAccount.getCountLots())));
            profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_completed_lots), String.valueOf(mAccount.getCompletedLots())));
        }

        profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_positive_votes), String.valueOf(mAccount.getPositiveVotes())));
        profileInfoItems.add(new ProfileInfoItem(getResources().getString(R.string.profile_negative_votes), String.valueOf(mAccount.getNegativeVotes())));

        mProfileInfoListView.setAdapter(new ProfileInfoAdapter(getActivity(), profileInfoItems));
        ViewUtils.setListViewHeightBasedOnChildren(mProfileInfoListView);

        mProfileInfoTextView.setText(mAccount.getInfo());


        if (mAccount.isVendor()) {
            showRatingFab();
        }
    }

    private void showRatingFab() {
        FabAction fabAction = new FabAction(getActivity());
        fabAction.setText(R.string.see_user_reviews);
        fabAction.setFabDrawable(getResources().getDrawable(R.drawable.star_white_24));

        fabAction.setOnFabClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UserReviewsActivity.class);
                intent.putExtra(UserReviewsActivity.EXTRA_USER_ID, mAccount.getExtId());

                startActivity(intent);
            }
        });
    }


    private boolean isOwnProfile() {
        AccountModel currentAccount = mUserState.getCurrentAccount();
        return currentAccount.getExtId() == mAccount.getExtId();
    }


    private class ProfileInfoItem {
        String label;
        String value;

        private ProfileInfoItem(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    private class ProfileInfoAdapter extends ArrayAdapter<ProfileInfoItem> {

        private final LayoutInflater mLayoutInflater;
        private final List<ProfileInfoItem> mData;

        public ProfileInfoAdapter(Context context, List<ProfileInfoItem> data) {
            super(context, R.layout.item_list_item_def, data);
            mData     = data;
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_list_item_def, parent, false);
            }

            TextView label = (TextView) convertView.findViewById(android.R.id.text1);
            TextView value = (TextView) convertView.findViewById(android.R.id.text2);

            ProfileInfoItem profileInfoItem = mData.get(position);

            label.setText(profileInfoItem.label);
            value.setText(String.valueOf(profileInfoItem.value));

            return convertView;
        }
    }
}
