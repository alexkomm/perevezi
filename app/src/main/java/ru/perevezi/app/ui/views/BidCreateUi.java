/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.app.model.BidModel;

public interface BidCreateUi extends BaseUi {

    void setUiCallbacks(UiCallBacks uiCallBacks);

    int getLotId();

    void navigateToBidList(int msgResId);

    void showForbiddenMessage(String message);

    void showBidForm();

    interface UiCallBacks {
        void checkBidAccess();

        void onBidSubmitted(BidModel bidModel);
    }

}
