/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;


import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.
        AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.ui.fragments.BidFragment;
import ru.perevezi.app.utils.RoundImageTransformation;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.app.utils.ViewUtils;

public class LotBidCard extends Card {

    private final BidModel mBid;

    public LotBidCard(Context context, BidModel bid) {
        this(context, R.layout.card_bid_list, bid);
    }

    public LotBidCard(Context context, int innerLayout, BidModel bid) {
        super(context, innerLayout);
        mBid = bid;

        init();
    }

    private void init() {
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                BidFragment bidFragment = BidFragment.newInstance(mBid);

                fm.beginTransaction().replace(R.id.container, bidFragment,
                        BidFragment.TAG).addToBackStack(BidFragment.TAG).commit();
            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ImageView imageView = (ImageView) parent.findViewById(R.id.car_imgview);

        if (!Strings.isNullOrEmpty(mBid.getCarImagePath())) {
            Picasso.with(mContext).load(mBid.getCarImagePath()).transform(new RoundImageTransformation()).into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
        }

        TextView driverNameView = (TextView) parent.findViewById(R.id.driver_name_text);
        driverNameView.setText(mBid.getProvider().getFullName());

        RatingBar driverRatingBar = (RatingBar) parent.findViewById(R.id.driver_rating_rb);
        TextView driverRatingText = (TextView) parent.findViewById(R.id.driver_rating_text);

        if (mBid.getProvider().getRating() > 0) {

            driverRatingBar.setRating(mBid.getProvider().getRating());
            DecimalFormat df = new DecimalFormat("###.##");
            df.setMinimumFractionDigits(1);

            driverRatingText.setText(df.format(mBid.getProvider().getRating()));
        } else {
            driverRatingBar.setVisibility(View.GONE);
            driverRatingText.setVisibility(View.GONE);
        }


        AccountModel provider = mBid.getProvider();

        int voteTotal = provider.getPositiveVotes() + provider.getNeutralVotes() + provider.getNegativeVotes();

        TextView voteTotalText = (TextView) parent.findViewById(R.id.driver_votes_text);
        voteTotalText.setText(mContext.getResources().getQuantityString(R.plurals.votes_count, voteTotal, voteTotal));

        TextView bidAmountText = (TextView) parent.findViewById(R.id.bid_amount_text);
        bidAmountText.setText(ViewUtils.formatCurrencyWithSymbol(mContext, mBid.getAmount().doubleValue()));

        TextView expiryView = (TextView) parent.findViewById(R.id.bid_expiry_text);

        if (mBid.getExpirationDate() != null) {
            String shippingInfo = mContext.getResources().getString(R.string.lot_view_shipping_info);

            shippingInfo = String.format(shippingInfo, DateUtils.formatDateTime(mContext,
                    mBid.getExpirationDate().getTimeInMillis(), (DateUtils.FORMAT_SHOW_DATE |
                            DateUtils.FORMAT_SHOW_WEEKDAY)));

            expiryView.setText(shippingInfo);
        } else {
            expiryView.setText(mContext.getResources().getString(R.string.lot_view_flexible_date));
        }

        int bidStatusDrawable = StringManager.getBidStatusDrawableSmall(mBid.getStatus());

        if (bidStatusDrawable > 0) {
            bidAmountText.setCompoundDrawablesRelativeWithIntrinsicBounds(bidStatusDrawable, 0, 0, 0);
        }
    }
}
