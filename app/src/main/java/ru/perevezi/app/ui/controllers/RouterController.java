/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.mapper.AccountModelMapper;
import ru.perevezi.app.model.mapper.SettingsModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.ApplicationState;
import ru.perevezi.app.ui.views.RouterUi;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.entities.PereveziSettings;
import ru.perevezi.domain.interactor.impl.BindDeviceIdUseCase;
import ru.perevezi.domain.interactor.impl.GetGeneralSettingsUseCase;
import ru.perevezi.domain.interactor.impl.LoginUseCase;
import ru.perevezi.domain.interactor.impl.LogoutUseCase;
import ru.perevezi.domain.interactor.impl.SystemConnectUseCase;
import ru.perevezi.domain.interactor.impl.UnbindDeviceIdUseCase;
import ru.perevezi.domain.utils.PereveziPreferences;
import ru.perevezi.domain.utils.Preconditions;

public class RouterController implements RouterUi.UiCallbacks, Controller<RouterUi> {

    private final GetGeneralSettingsUseCase mGetGeneralSettingsUseCase;
    private final PereveziPreferences mPereveziPreferences;
    private final AccountModelMapper mAccountMapper;
    private final SettingsModelMapper mSettingsMapper;
    private final LogoutUseCase mLogoutUseCase;

    private RouterUi mUi;

    private ApplicationState mApplicationState;

    private PereveziAccountManager mAccountManager;

    private final SystemConnectUseCase mSystemConnectUseCase;
    private final LoginUseCase mLoginUseCase;


    @Inject
    public RouterController(
            AccountModelMapper accountMapper,
            SettingsModelMapper settingsMapper,
            PereveziAccountManager accountManager,
            ApplicationState applicationState,
            SystemConnectUseCase systemConnectUseCase,
            LoginUseCase loginUseCase,
            GetGeneralSettingsUseCase getGeneralSettingsUseCase,
            LogoutUseCase logoutUseCase,
            PereveziPreferences pereveziPreferences) {


        Preconditions.checkState(accountMapper != null, "accountMapper cannot be null");
        Preconditions.checkState(settingsMapper != null, "settingsMapper cannot be null");
        Preconditions.checkState(accountManager != null, "accountManager cannot be null");
        Preconditions.checkState(applicationState != null, "applicationState cannot be null");
        Preconditions.checkState(systemConnectUseCase != null, "systemConnectUseCase cannot be null");
        Preconditions.checkState(loginUseCase != null, "loginUseCase cannot be null");
        Preconditions.checkState(logoutUseCase != null, "logoutUseCase cannot be null");
        Preconditions.checkState(pereveziPreferences != null, "pereveziPreferences cannot be null");

        mAccountMapper = accountMapper;
        mSettingsMapper = settingsMapper;
        mApplicationState  = applicationState;
        mAccountManager    = accountManager;
        mSystemConnectUseCase = systemConnectUseCase;
        mLoginUseCase      = loginUseCase;
        mLogoutUseCase = logoutUseCase;
        mGetGeneralSettingsUseCase = getGeneralSettingsUseCase;
        mPereveziPreferences = pereveziPreferences;
    }

    @Override
    public void initialize(RouterUi mainUi) {
        mUi = mainUi;
        mUi.setInterfaceCallbacks(this);
    }

    @Override
    public void onInit() {
        mUi.toggleLogoProgress(true);
        fetchGeneralSettings();
    }

    public void initAccount() {
        List<PereveziAccount> accounts = mAccountManager.getAccounts();

        PereveziAccount currentAccount = null;

        if (accounts.size() == 0) {
            mUi.navigateToIntroScreen();
        } else if (accounts.size() > 1) {
            currentAccount = mAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());

            if (currentAccount == null) {
                if (!mPereveziPreferences.isLoggedOut()) {
                    mUi.showAccountPickerDialog();
                } else {
                    mUi.navigateToIntroScreen();
                }
            }
        } else {
            currentAccount = accounts.get(0);
        }

        if (currentAccount != null) {
            setCurrentAccountPreferences(currentAccount.getName(), currentAccount.getExtId());
            checkSystemAuth();
        }
    }

    @Override
    public void onAccountPicked(String name) {
        PereveziAccount currentAccount = mAccountManager.getAccount(name);
        setCurrentAccountPreferences(currentAccount.getName(), currentAccount.getExtId());

        checkSystemAuth();
    }

    @Override
    public void onAccountChanged() {
        AccountModel account = mApplicationState.getCurrentAccount();
        setCurrentAccountPreferences(account.getLogin(), account.getExtId());

        mUi.rebuildDrawerItems();
    }

    private void fetchGeneralSettings() {
        RxUseCase.callback(mGetGeneralSettingsUseCase).register(new RxUseCase
                .AsyncCallback<PereveziSettings>() {

            @Override
            public void onResultOk(PereveziSettings generalSettings) {
                mApplicationState.setGeneralSettings(mSettingsMapper.mapResponse(generalSettings));
                initAccount();
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.showMessage(R.string.api_error_msg);
            }
        });
    }

    private void checkSystemAuth() {
        RxUseCase.callback(mSystemConnectUseCase).register(new RxUseCase.AsyncCallback<PereveziAccount>() {


            @Override
            public void onResultOk(PereveziAccount account) {

                if (account.getExtId() == 0) {
                    if (!mPereveziPreferences.isLoggedOut()) {
                        attemptLogin();
                    } else {
                        mUi.navigateToIntroScreen();
                    }
                }
                else {
                    mPereveziPreferences.setLoggedOut(false);
                    mApplicationState.setCurrentAccount(mAccountMapper.mapResponse(account));
                }
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.showMessage(R.string.api_error_msg);
            }
        });
    }

    private void attemptLogin() {
        PereveziAccount pereveziAccount = mAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());

        Preconditions.checkState(pereveziAccount != null, "account not found");

        if (pereveziAccount.getPassword() == null) {
            purgeUserData();

            mUi.navigateToLoginScreen();
        } else {
            Map<String, String> loginArgs = new HashMap<>(2);
            loginArgs.put("login", pereveziAccount.getName());
            loginArgs.put("password", pereveziAccount.getPassword());

            mAccountManager.invalidateAuthToken(pereveziAccount);

            RxUseCase.callback(mLoginUseCase, loginArgs).register(new RxUseCase.AsyncCallback<PereveziAccount>() {
                @Override
                public void onResultOk(PereveziAccount result) {
                    mPereveziPreferences.setLoggedOut(false);

                    mAccountManager.updateCredentials(result);
                    mApplicationState.setCurrentAccount(mAccountMapper.mapResponse(result));
                }

                @Override
                public void onError(Throwable throwable) {
                    purgeUserData();

                    mUi.navigateToLoginScreen();
                }
            });
        }
    }


    @Override
    public void onUserLogout() {
        mUi.toggleDialogProgress(true);

        mPereveziPreferences.setLoggedOut(true);

        RxUseCase.callback(mLogoutUseCase).register(new RxUseCase.AsyncCallback<Boolean>() {
            @Override
            public void onResultOk(Boolean result) {
                mUi.toggleDialogProgress(false);
                purgeUserData();
                mUi.navigateToIntroScreen();
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    private void setCurrentAccountPreferences(String name, int extId) {
        mPereveziPreferences.setCurrentAccount(name);
        mPereveziPreferences.setCurrentAccountExtId(extId);
    }

    private void purgeUserData() {
        // Purging all user data
        mPereveziPreferences.clearAccountData();
        mApplicationState.setCurrentAccount(null);
    }


    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
