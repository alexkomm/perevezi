/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.card.LotListCard;
import ru.perevezi.app.utils.DateUtils;

public class CardArrayLotListRecyclerViewAdapter extends CardArrayRecyclerViewAdapter {

    private final int mControlsResId;
    private final List<Card> mCards;
    private final ControlsViewHolder.Callbacks mControlCallbacks;

    private static final int CONTROLS_TYPE = 0;

    public CardArrayLotListRecyclerViewAdapter(Context context, int controlsResId, List<Card>
            cards, ControlsViewHolder.Callbacks controlCallbacks) {
        super(context, cards);
        mControlsResId = controlsResId;
        mCards = cards;
        mControlCallbacks = controlCallbacks;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return isControlsPosition(position) ? CONTROLS_TYPE : super.getItemViewType(getCardPosition(position)) + 1;
    }

    private boolean isControlsPosition(int position) {
        return position == 0;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == CONTROLS_TYPE) {
            View controlsView = inflater.inflate(mControlsResId, parent, false);
            return new ControlsViewHolder(controlsView);
        }

        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int position) {
        if (cardViewHolder.getItemViewType() == CONTROLS_TYPE) {
            ControlsViewHolder vh = (ControlsViewHolder) cardViewHolder;
            configureControlsViewHolder(vh);
        } else {
            super.onBindViewHolder(cardViewHolder, getCardPosition(position));
        }
    }

    private void configureControlsViewHolder(ControlsViewHolder vh) {
        long cachedTimestamp = 0;

        if (mCards.size() > 0) {
            cachedTimestamp = ((LotListCard) mCards.get(0)).getCachedTimestamp();
        }

        if ((mControlCallbacks == null || mControlCallbacks.onCachedNotificationShow()) && cachedTimestamp > 0) {
            SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.DATE_WITH_TIME_FORMAT, Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cachedTimestamp);

            String cachedNotify = mContext.getResources().getString(R.string.lot_list_cached_notify,
                    formatter.format(calendar.getTime()));

            vh.getCachedNotifyText().setVisibility(View.VISIBLE);
            vh.getCachedNotifyText().setText(cachedNotify);
        } else {
            vh.getCachedNotifyText().setVisibility(View.GONE);
        }

        vh.getFilterButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mControlCallbacks != null) {
                    mControlCallbacks.onFilterButtonClick();
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        if (isControlsPosition(position)) {
            return Integer.MAX_VALUE;
        }

        return super.getItemId(getCardPosition(position));
    }

    private int getCardPosition(int position) {
        return position - 1;
    }

    public static class ControlsViewHolder extends CardViewHolder {
        private TextView cachedNotifyText;

        private Button filterButton;

        public ControlsViewHolder(View view) {
            super(view);

            cachedNotifyText = (TextView) view.findViewById(R.id.cached_notify_text);
            filterButton = (Button) view.findViewById(R.id.btn_lot_search_filter);
        }

        public TextView getCachedNotifyText() {
            return cachedNotifyText;
        }

        public void setCachedNotifyText(TextView cachedNotifyText) {
            this.cachedNotifyText = cachedNotifyText;
        }

        public Button getFilterButton() {
            return filterButton;
        }

        public void setFilterButton(Button filterButton) {
            this.filterButton = filterButton;
        }

        public interface Callbacks {

            boolean onCachedNotificationShow();

            void onFilterButtonClick();

        }
    }
}
