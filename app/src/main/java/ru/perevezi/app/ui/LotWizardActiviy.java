/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.ui.StepPagerStrip;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.app.model.wizard.LotWizard;
import ru.perevezi.app.model.wizard.PageCallbacks;
import ru.perevezi.app.model.wizard.ValidatingPage;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.controllers.LotWizardController;
import ru.perevezi.app.ui.fragments.LotCreateConfirmDialog;
import ru.perevezi.app.ui.fragments.wizard.LotWizardReviewFragment;
import ru.perevezi.app.ui.views.LotWizardUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.utils.Preconditions;

public class LotWizardActiviy extends BaseActivity implements ModelCallbacks, PageCallbacks,
        LotWizardReviewFragment.Callbacks, LotWizardUi, LotCreateConfirmDialog.OnDialogConfirmListener {

    @Inject
    SettingsState mSettingsState;

    @Inject
    LotWizardController mController;

    @InjectView(R.id.strip)
    StepPagerStrip mStrip;

    @InjectView(R.id.pager)
    ViewPager mPager;

    @InjectView(R.id.wizard_controls)
    ViewGroup mWizardControls;

    @InjectView(R.id.btn_prev)
    Button mButtonPrev;

    @InjectView(R.id.btn_next)
    Button mButtonNext;

    private WizardPagerAdapter mPagerAdapter;

    private LotWizard mLotWizard = new LotWizard(this);

    private boolean mConsumePageSelectedEvent;

    private List<Page> mCurrentPageSequence;

    private boolean mEditingAfterReview;

    private List<CategoryModel> mCategoryList;

    private SettingsModel mGeneralSettings;

    private UiCallbacks mUiCallbacks;

    private LotCreateConfirmDialog mLotCreateConfirmDialog;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGeneralSettings = mSettingsState.getGeneralSettings();
        mCategoryList = mGeneralSettings.getCategories();

        mController.initialize(this);
        registerUiLifecycleObserver(mController);

        Preconditions.checkState(mGeneralSettings != null, "General settings not found");
        Preconditions.checkState(mCategoryList != null, "Category settings not found");

        if (savedInstanceState != null) {
            final int catId = savedInstanceState.getInt("category");
            for (CategoryModel category : mCategoryList) {
                if (category.getId() == catId) {
                    mLotWizard.setSelectedCategory(category);
                    break;
                }
            }
        }

        initWizard();

        if (savedInstanceState != null) {
            mLotWizard.load(savedInstanceState.getBundle("model"));
        }

        mLotCreateConfirmDialog = new LotCreateConfirmDialog();
        mProgressDialog = new ProgressDialog(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLotWizard.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mLotWizard.save());

        CategoryModel selectedCategory = mLotWizard.getSelectedCategory();

        if (selectedCategory != null) {
            outState.putInt("category", mLotWizard.getSelectedCategory().getId());
        }
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_lot_wizard;
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mLotWizard;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mLotWizard.getCurrentPageSequence();
        recalculateCutOffPage();
        mStrip.setPageCount(mCurrentPageSequence.size() + 1);
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    @Override
    public Page onGetPage(String key) {
        return mLotWizard.findByKey(key);
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();

        if (position == mCurrentPageSequence.size()) {
            mButtonNext.setText(R.string.finish);
        } else {
            mButtonNext.setText(mEditingAfterReview && position > 0
                    ? R.string.review
                    : R.string.next);

            mButtonNext.setEnabled(position != mPagerAdapter.getCutOffPage());
        }


        mButtonPrev.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }


        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }


        return false;
    }


    private void initWizard() {
        mLotWizard.registerListener(this);

        mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager());

        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mStrip.setCurrentPage(position);


                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        onPageTreeChanged();

        mStrip.setVisibility(View.VISIBLE);

        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                    mLotCreateConfirmDialog.show(getFragmentManager(), "lot_create_confirm");
                } else {

                    if (showPageErrors()) {
                        return;
                    }

                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mButtonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });
    }

    private boolean showPageErrors() {
        Page page = mCurrentPageSequence.get(mPager.getCurrentItem());

        if (page instanceof ValidatingPage) {
            List<ValidatingPage.PageError> errors = ((ValidatingPage) page).getPageErrors();

            View contentView = findViewById(android.R.id.content);

            for (ValidatingPage.PageError error : errors) {
                TextView field = (TextView) contentView.findViewWithTag(error.fieldTag);

                if (field != null) {
                    field.setError(getResources().getString(error.msgResId));
                }
            }

            if (errors.size() > 0) {
                return true;
            }
        }


        return false;
    }

    @Override
    public List<CategoryModel> onGetCategoryList() {
        return mCategoryList;
    }

    @Override
    public SettingsModel onGetGeneralSettings() {
        return mGeneralSettings;
    }

    @Override
    public CategoryModel onGetSelectedCategory() {
        return mLotWizard.getSelectedCategory();
    }

    @Override
    public void onCategoryChanged(CategoryModel categoryModel) {
        if (mLotWizard.getSelectedCategory() == null || (categoryModel != null && !mLotWizard
                .getSelectedCategory().equals(categoryModel)))
        {
            mLotWizard.setSelectedCategory(categoryModel);
            mLotWizard.clear();

            onPageTreeChanged();
        }
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void onDialogConfirm() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final PereveziLot lot = mLotWizard.generateLotFromData();

                LotWizardActiviy.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mUiCallbacks.onLotCreateConfirm(lot);
                    }
                });
            }
        }).start();
    }

    @Override
    public void toggleDialogProgress(boolean show) {
        if (show) {
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public class WizardPagerAdapter extends FragmentStatePagerAdapter {

        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public WizardPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position >= mCurrentPageSequence.size()) {
                return new LotWizardReviewFragment();
            }

            return mCurrentPageSequence.get(position).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }


            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            if (mCurrentPageSequence == null) {
                return 0;
            }
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;

        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }


        public int getCutOffPage() {
            return mCutOffPage;
        }
    }
}
