/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BasePagerFragment;
import ru.perevezi.app.ui.controllers.UserLotsHostController;
import ru.perevezi.app.ui.fragments.UserLotsSectionFragment;
import ru.perevezi.app.ui.views.UserLotsHostUi;
import ru.perevezi.app.utils.StringManager;

public class UserLotsActivity extends BaseActivity implements UserLotsHostUi, BasePagerFragment.PagerFragmentListener {


    private static final String ARG_KEY_LOT_STATS = "lot_stats";

    @Inject
    UserLotsHostController mController;

    private ArrayList<Map<String, Integer>> mLotStats;
    private Fragment mPagerFragment;
    private UiCallbacks mUiCallbacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController.initialize(this);

        if (savedInstanceState == null) {
            mUiCallbacks.fetchLotStats();
        } else {
            mLotStats = (ArrayList<Map<String, Integer>>) savedInstanceState.getSerializable(ARG_KEY_LOT_STATS);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARG_KEY_LOT_STATS, mLotStats);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Fragment onGetItem(int position) {
        int section = mLotStats.get(position).get("section");

        return UserLotsSectionFragment.newInstance(section);
    }

    @Override
    public CharSequence onGetPageTitle(int position) {
        int section = mLotStats.get(position).get("section");
        int count  = mLotStats.get(position).get("cnt");

        String title = getResources().getString(StringManager.getLotStatusPagerTitleResId(section));

        if (count > 0) {
            title += " (" + count + ")";
        }

        return title;
    }

    @Override
    public int onGetCount() {
        return mLotStats.size();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void initViewPager(ArrayList<Map<String, Integer>> lotStats) {
        mLotStats = lotStats;

        if (mLotStats.size() > 0) {

            FragmentManager fm = getSupportFragmentManager();

            mPagerFragment = fm.findFragmentByTag(BasePagerFragment.TAG);

            if (mPagerFragment == null) {
                mPagerFragment = BasePagerFragment.newInstance(0);
                fm.beginTransaction().replace(R.id.container, mPagerFragment, BasePagerFragment.TAG).commit();
            }

        } else {
            // @TODO Показываем текст и предложение разместить запрос
        }
    }
}
