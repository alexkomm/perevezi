/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface LoginUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void finishLogin(String name, String authToken);

    interface UiCallbacks {
        void onLogin(String login, String password);
    }
}
