/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.UserReviewsHostUi;
import ru.perevezi.domain.interactor.impl.GetUserReviewsStatsUseCase;

public class UserReviewsHostController implements Controller<UserReviewsHostUi>, UserReviewsHostUi.UiCallbacks {


    private final GetUserReviewsStatsUseCase mUseCase;
    private UserReviewsHostUi mUi;

    @Inject
    public UserReviewsHostController(GetUserReviewsStatsUseCase useCase) {
        mUseCase = useCase;
    }


    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void initialize(UserReviewsHostUi userReviewsHostUi) {
        mUi = userReviewsHostUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onGetReviewStats(int userId) {
        mUi.toggleOverlayProgress(true);

        RxUseCase.callback(mUseCase, userId).register(new RxUseCase.AsyncCallback<ArrayList<Map<String, Integer>>>() {

            @Override
            public void onResultOk(ArrayList<Map<String, Integer>> result) {
                mUi.initViewPager(result);
                mUi.toggleOverlayProgress(false);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleOverlayProgress(false);
            }
        });
    }
}
