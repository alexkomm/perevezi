/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import org.parceler.Parcels;

import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.domain.utils.Preconditions;

public class ReviewDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String TAG = "review_dialog";

    public static final String ARG_KEY_LOT = "lot";

    public static final int REVIEW_DIALOG_REQUEST_CODE = 1;

    private LotModel mLotModel;

    public static ReviewDialog newInstance(LotModel lotModel) {
        ReviewDialog fragment = new ReviewDialog();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_LOT, Parcels.wrap(lotModel));

        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            mLotModel = Parcels.unwrap(args.getParcelable(ARG_KEY_LOT));
        }

        Preconditions.checkState(mLotModel != null, "LotModel cannot be null");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_review_title)
                .setMessage(getResources().getString(R.string.dialog_review_msg, mLotModel.getNid
                        ()))
                .setPositiveButton(R.string.dialog_review_btn, this);

        return adb.create();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                Fragment fragment = getTargetFragment();

                Intent intent = getActivity().getIntent();
                intent.putExtra(ARG_KEY_LOT, Parcels.wrap(mLotModel));

                if (fragment != null) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,
                            intent);
                } else {
                    ((BaseActivity) getActivity()).onActivityResult(REVIEW_DIALOG_REQUEST_CODE, Activity.RESULT_OK, intent);
                }

                break;
        }
    }
}
