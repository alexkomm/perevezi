/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.ArrayList;
import java.util.Map;

public interface UserReviewsHostUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void initViewPager(ArrayList<Map<String, Integer>> lotStats);

    interface UiCallbacks {
        void onGetReviewStats(int userId);
    }

}
