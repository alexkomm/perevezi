/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface ReviewListUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    interface UiCallbacks {
        void onGetReviewList(int userId);
    }

}
