/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.fragments.IntroScreenFragment;
import ru.perevezi.app.ui.fragments.RegisterFragment;

public class IntroActivity extends BaseActivity implements IntroScreenFragment.IntroScreenListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            IntroScreenFragment introScreenFragment = new IntroScreenFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,
                    introScreenFragment).commit();
        }
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_intro;
    }


    @Override
    public void onSignInButtonClicked() {
        getRouterUi().navigateToLoginScreen();
    }

    @Override
    public void onCreateLotButtonClicked() {
        RegisterFragment registerFragment = new RegisterFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,
                registerFragment).addToBackStack(null).commit();
    }
}
