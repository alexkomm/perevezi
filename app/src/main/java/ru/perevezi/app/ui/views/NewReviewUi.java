/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.Map;

public interface NewReviewUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void showReviewForm();

    void finishReviewCreation();


    interface UiCallbacks {
        void onNewReview(Map<String, String> reviewData);
    }

}
