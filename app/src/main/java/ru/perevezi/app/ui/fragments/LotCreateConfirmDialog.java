/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import ru.perevezi.app.R;
import ru.perevezi.app.model.wizard.PageCallbacks;

public class LotCreateConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {


    private OnDialogConfirmListener mCallbacks;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_lot_confirm_title)
                .setPositiveButton(R.string.yes, this)
                .setNegativeButton(R.string.no, this)
                .setMessage(R.string.dialog_lot_confirm_text);

        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                mCallbacks.onDialogConfirm();
                break;
            case DialogInterface.BUTTON_NEGATIVE:

                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof PageCallbacks)) {
            throw new ClassCastException("Activity must implement OnDialogConfirmListener");
        }
        mCallbacks = (OnDialogConfirmListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public interface OnDialogConfirmListener {
        void onDialogConfirm();
    }
}
