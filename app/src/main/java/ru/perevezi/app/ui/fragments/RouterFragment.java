/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Strings;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;

import org.parceler.Parcels;

import javax.inject.Inject;

import ru.perevezi.app.Constants;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.events.AccountChangedEvent;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.ApplicationState;
import ru.perevezi.app.ui.AboutActivity;
import ru.perevezi.app.ui.BillingActivity;
import ru.perevezi.app.ui.HomeActivity;
import ru.perevezi.app.ui.IntroActivity;
import ru.perevezi.app.ui.LoginActivity;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.LotWizardActiviy;
import ru.perevezi.app.ui.ProfileActivity;
import ru.perevezi.app.ui.SearchLotsActivity;
import ru.perevezi.app.ui.SupportActivity;
import ru.perevezi.app.ui.UserBidsActivity;
import ru.perevezi.app.ui.UserLotsActivity;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.components.SimpleFragmentConfirmDialog;
import ru.perevezi.app.ui.controllers.RouterController;
import ru.perevezi.app.ui.views.RouterUi;
import ru.perevezi.domain.utils.Preconditions;
import rx.android.app.AppObservable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Fragment for application routing
 */
public class RouterFragment extends BaseFragment implements RouterUi {

    @Inject
    RxBus mBus;

    @Inject
    RouterController mRouterController;

    @Inject
    ApplicationState mApplicationState;

    public static final String TAG = "router_fragment";

    private static final int REQUEST_CODE_CHOOSE_ACCOUNT = 1;
    private static final int REQUEST_CODE_LOGIN = 2;
    private static final int REQUEST_CODE_LOGOUT = 3;

    public static final String EXTRA_BUNDLE = "bundle";
    public static final String EXTRA_MENU_ID = "menu_id";
    public static final String EXTRA_LOT_ID = "lot_id";

    public static final int ID_MENU_ITEM_SEARCH_LOTS = 1;
    public static final int ID_MENU_ITEM_MY_LOTS = 2;
    public static final int ID_MENU_ITEM_MY_BIDS = 3;
    public static final int ID_MENU_ITEM_SUPPORT = 4;
    public static final int ID_MENU_ITEM_BILLING = 5;
    public static final int ID_MENU_ITEM_CREATE_LOT = 6;
    public static final int ID_MENU_ITEM_ABOUT = 7;
    public static final int ID_MENU_ITEM_LOGOUT = 8;
    public static final int ID_MENU_ITEM_PROFILE = 9;

    private UiCallbacks mUiCallbacks;

    private CompositeSubscription mCompositeSubscription;

    private Drawer mDrawer;
    private AccountHeader mDrawerHeader;

    private SimpleFragmentConfirmDialog mConfirmDialog;
    private Bundle mExtraBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        PereveziApplication.from(getActivity()).inject(this);

        registerUiLifecycleObserver(mRouterController);
        mRouterController.initialize(this);

        mCompositeSubscription = new CompositeSubscription();

        mCompositeSubscription.add(
                AppObservable.bindFragment(this, mBus.toObserverable()).subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object e) {
                        if (e instanceof AccountChangedEvent) {
                            mUiCallbacks.onAccountChanged();

                            if (getActivity() instanceof HomeActivity) {
                                setDefaultScreen();
                            }
                        }
                    }
                })
        );
    }

    @Override
    public void setDefaultScreen() {
        if (!(getActivity() instanceof HomeActivity)) {
            return;
        }

        AccountModel currentAccount = mApplicationState.getCurrentAccount();

        if (currentAccount == null) {
            return;
        }

        Intent intent = getActivity().getIntent();

        int targetMenuId = intent.getIntExtra(EXTRA_MENU_ID, 0);

        if (targetMenuId > 0) {
            getDrawer().setSelection(targetMenuId);
            return;
        }

        int lotId = intent.getIntExtra(EXTRA_LOT_ID, 0);
        if (lotId > 0) {
            navigateToLotScreen(lotId);
            return;
        }

        if (!currentAccount.isVendor()) {
            getDrawer().setSelection(ID_MENU_ITEM_CREATE_LOT, true);
        } else {
            getDrawer().setSelection(ID_MENU_ITEM_SEARCH_LOTS, true);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mDrawer = getDrawer();

        if (mDrawer != null) {
            mDrawerHeader = getDrawerHeader();
            mDrawer.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                @Override
                public boolean onItemClick(View view, int i, IDrawerItem iDrawerItem) {
                    if (iDrawerItem != null && iDrawerItem instanceof Nameable) {
                        mApplicationState.setDrawerCurrentSelectedIdenitifer(iDrawerItem.getIdentifier());

                        switch (iDrawerItem.getIdentifier()) {
                            case ID_MENU_ITEM_MY_LOTS:
                                navigateToMyLots();
                                break;
                            case ID_MENU_ITEM_SEARCH_LOTS:
                                navigateToSearchLots();
                                break;
                            case ID_MENU_ITEM_CREATE_LOT:
                                navigateToCreateLot();
                                break;
                            case ID_MENU_ITEM_MY_BIDS:
                                navigateToMyBids();
                                break;
                            case ID_MENU_ITEM_PROFILE:
                                AccountModel currentAccount = mApplicationState.getCurrentAccount();
                                navigateToProfileScreen(currentAccount);
                                break;
                            case ID_MENU_ITEM_LOGOUT:
                                logout();
                                break;
                            case ID_MENU_ITEM_BILLING:
                                navigateToBillingScreen();
                                break;
                            case ID_MENU_ITEM_SUPPORT:
                                navigateToSupportScreen();
                                break;
                            case ID_MENU_ITEM_ABOUT:
                                navigateToAboutScreen();
                                break;
                            default:
                                break;
                        }
                    }

                    return true;
                }
            });

            rebuildDrawerItems();
        }

        if (!(getActivity() instanceof HomeActivity)) {
            checkPhoneConfirmation();
        }
    }

    private void checkPhoneConfirmation() {
        AccountModel account = mApplicationState.getCurrentAccount();

        if (account != null && account.isVendor() && !mApplicationState.isPhoneConfirmDialogShown
                ()) {
            final int phoneConfirmStatus = account.getPhoneConfirmStatus();

            if (phoneConfirmStatus == AccountModel.PHONE_CONFIRM_STATUS_NOT_CONFIRMED
                    || phoneConfirmStatus == AccountModel.PHONE_CONFIRM_STATUS_PENDING) {
                showPhoneConfirmDialog(false);

                mApplicationState.setPhoneConfirmDialogShown(true);
            }
        }
    }

    private void logout() {
        mConfirmDialog = SimpleFragmentConfirmDialog.newInstance(R.string.dialog_logout_title, R
                .string.dialog_logout_msg, 0, 0);
        mConfirmDialog.setTargetFragment(this, REQUEST_CODE_LOGOUT);
        mConfirmDialog.show(getFragmentManager(), SimpleFragmentConfirmDialog.TAG);
    }

    @Override
    public void rebuildDrawerItems() {
        if (mDrawer == null) {
            return;
        }

        AccountModel currentAccount = mApplicationState.getCurrentAccount();

        if (currentAccount == null) {
            return;
        }

        mDrawerHeader.clear();
        mDrawer.removeAllItems();

        final ProfileDrawerItem profileItem = new ProfileDrawerItem()
                .withName(currentAccount.getFullName())
                .withEmail(currentAccount.getLogin() + " (id: " + currentAccount.getExtId() + ")");

        if (!Strings.isNullOrEmpty(currentAccount.getProfileImageUrl())) {
            profileItem.withIcon(currentAccount.getProfileImageUrl());
        }


        mDrawerHeader.addProfile(profileItem, 0);

        mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_search_lots)
                .withIdentifier(ID_MENU_ITEM_SEARCH_LOTS).withIcon(FontAwesome.Icon.faw_search));

        mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_profile).withIdentifier
                (ID_MENU_ITEM_PROFILE).withIcon(FontAwesome.Icon.faw_user));

        if (currentAccount.isVendor()) {
            mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_my_bids)
                    .withIdentifier(ID_MENU_ITEM_MY_BIDS).withIcon(FontAwesome.Icon.faw_gavel));
            mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_billing)
                    .withIdentifier(ID_MENU_ITEM_BILLING).withIcon(FontAwesome.Icon.faw_rub));
        }

        if (currentAccount.isClient()) {
            mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_new_lot)
                    .withIdentifier(ID_MENU_ITEM_CREATE_LOT).withIcon(FontAwesome.Icon.faw_plus_circle));
            mDrawer.addItem(new PrimaryDrawerItem().withName(R.string.menu_my_lots)
                    .withIdentifier(ID_MENU_ITEM_MY_LOTS).withIcon(FontAwesome.Icon.faw_list));
        }

        mDrawer.addItem(new DividerDrawerItem());

        mDrawer.addItem(new SecondaryDrawerItem().withName(R.string.menu_support).withIdentifier
                (ID_MENU_ITEM_SUPPORT).withIcon(FontAwesome.Icon.faw_question));
        mDrawer.addItem(new SecondaryDrawerItem().withName(R.string.menu_about).withIdentifier
                (ID_MENU_ITEM_ABOUT).withIcon(FontAwesome.Icon.faw_info));

        mDrawer.addItem(new DividerDrawerItem());

        mDrawer.addItem(new SecondaryDrawerItem().withName(R.string.menu_logout).withIdentifier
                (ID_MENU_ITEM_LOGOUT));

        mDrawer.setSelection(mApplicationState.getDrawerCurrentSelectedIdentifier(), false);
    }

    protected CompositeSubscription getSubscription() {
        return mCompositeSubscription;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toggleLogoProgress(false);

        switch (requestCode) {
            case REQUEST_CODE_CHOOSE_ACCOUNT:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallbacks.onAccountPicked(data.getStringExtra(AccountManager
                            .KEY_ACCOUNT_NAME));
                } else {
                    getActivity().finish();
                    return;
                }
                break;
            case REQUEST_CODE_LOGOUT:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallbacks.onUserLogout();
                }
                break;
            case REQUEST_CODE_LOGIN:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallbacks.onAccountChanged();
                    navigateToHomeScreen();
                }
            default:
                break;
        }
    }

    @Override
    public void navigateToHomeScreen() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mConfirmDialog != null) {
            mConfirmDialog.dismiss();
        }
    }

    @Override
    public void showAccountPickerDialog() {
        final Intent chooseAccountIntent = AccountManager.newChooseAccountIntent(null, null,
                new String[]{Constants.PEREVEZI_ACCOUNT_TYPE}, false, null, null, null, null);

        toggleLogoProgress(true);

        startActivityForResult(chooseAccountIntent, REQUEST_CODE_CHOOSE_ACCOUNT);
    }

    @Override
    public void navigateToLotScreen(int id) {
        Preconditions.checkState(id > 0, "lot id is invalid");
        final Intent intent = new Intent(getActivity(), LotActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);

    }

    @Override
    public void navigateToLoginScreen() {
        final Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivityForResult(intent, REQUEST_CODE_LOGIN);
    }

    @Override
    public void navigateToSearchLots() {
        final Intent intent = new Intent(getActivity(), SearchLotsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // Pass extras from intent
        Bundle extras = getActivity().getIntent().getExtras();
        intent.putExtra(BaseActivity.EXTRA_BUNDLE, extras);

        startActivity(intent);
    }

    @Override
    public void navigateToCreateLot() {
        final Intent intent = new Intent(getActivity(), LotWizardActiviy.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void navigateToIntroScreen() {
        final Intent intent = new Intent(getActivity(), IntroActivity.class);
        startActivity(intent);

        getActivity().finish();
    }

    @Override
    public void navigateToMyLots() {
        final Intent intent = new Intent(getActivity(), UserLotsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void navigateToMyBids() {
        final Intent intent = new Intent(getActivity(), UserBidsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public void navigateToProfileScreen(int userId) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(ProfileActivity.EXTRA_USER_ID, userId);

        startActivity(intent);
    }

    @Override
    public void navigateToProfileScreen(AccountModel account) {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra(ProfileActivity.EXTRA_ACCOUNT, Parcels.wrap(account));

        startActivity(intent);
    }

    @Override
    public void navigateToBillingScreen() {
        Intent intent = new Intent(getActivity(), BillingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void navigateToSupportScreen() {
        Intent intent = new Intent(getActivity(), SupportActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void navigateToAboutScreen() {
        Intent intent = new Intent(getActivity(), AboutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    @Override
    public void showPhoneConfirmDialog(boolean cancellable) {
        PhoneConfirmDialog phoneConfirmDialog = new PhoneConfirmDialog();
        phoneConfirmDialog.setCancelable(cancellable);
        phoneConfirmDialog.show(getFragmentManager(), PhoneConfirmDialog.TAG);
    }

    @Override
    public void init() {
        mUiCallbacks.onInit();
    }

    @Override
    public void setInterfaceCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }
}
