/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.LotHostUi;
import ru.perevezi.domain.interactor.impl.ConfirmCompletionUseCase;

public class LotHostController implements Controller<LotHostUi>, LotHostUi.UiCallbacks {


    private final ConfirmCompletionUseCase mConfirmCompletionUseCase;
    private LotHostUi mLotHostUi;

    @Inject
    public LotHostController(ConfirmCompletionUseCase confirmCompletionUseCase) {
        mConfirmCompletionUseCase = confirmCompletionUseCase;
    }

    @Override
    public void initialize(LotHostUi lotHostUi) {
        mLotHostUi = lotHostUi;
        mLotHostUi.setUiCallbacks(this);
    }

    @Override
    public void onLotCompletionConfirmed(int lotId) {
        confirmCompletion(lotId, true);
    }

    @Override
    public void onLotCompletionDeclined(int lotId) {
        confirmCompletion(lotId, false);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    private void confirmCompletion(int lotId, boolean completed) {
        mLotHostUi.toggleDialogProgress(true);

        Map<String, Object> args = new HashMap<>(2);

        args.put("lotId", lotId);
        args.put("completed", completed);

        RxUseCase.callback(mConfirmCompletionUseCase, args).register(new RxUseCase.AsyncCallback<Void>() {


            @Override
            public void onResultOk(Void result) {
                mLotHostUi.toggleDialogProgress(false);
                mLotHostUi.showMessage("ОК");
            }

            @Override
            public void onError(Throwable throwable) {
                mLotHostUi.toggleDialogProgress(false);
                mLotHostUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }
}
