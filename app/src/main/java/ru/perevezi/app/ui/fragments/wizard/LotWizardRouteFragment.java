/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.wizard.RoutePage;
import ru.perevezi.app.ui.components.DelayAutoСompleteTextView;
import ru.perevezi.app.ui.services.GeocoderIntentService;
import ru.perevezi.app.utils.LocationUtils;
import ru.perevezi.app.utils.Strings;
import ru.perevezi.domain.utils.Preconditions;

public class LotWizardRouteFragment extends AbstractWizardFragment implements View
        .OnClickListener, LocationListener {

    private GoogleMap mMap;

    private AddressResultReceiver mAddressResultReceiver;

    public static final String ACTION_SET_MY_POSITION = "SET_MY_POSITION";
    public static final String ACTION_SET_MARKER = "SET_MARKER";
    public static final String ACTION_GEOCODE_INPUT = "GEOCODE_INPUT";

    public static final String EXTRA_FIELD_ID = "EXTRA_FIELD_ID";

    @Inject
    LocationManager mLocationManager;

    @InjectView(R.id.et_from)
    DelayAutoСompleteTextView mFromField;

    @InjectView(R.id.et_to)
    DelayAutoСompleteTextView mToField;

    @InjectView(R.id.progress_from)
    ProgressBar mProgressFrom;

    @InjectView(R.id.progress_to)
    ProgressBar mProgressTo;

    @InjectView(R.id.map)
    MapView mMapView;

    @InjectView(R.id.button_from_gps)
    ImageButton mButtonFromGps;

    @InjectView(R.id.button_to_gps)
    ImageButton mButtonToGps;

    private String mProvider;

    private Location mLatestLocation;

    private Address mMyLocationAddress;

    private Map<Integer, Marker> mMarkerList = new LinkedHashMap<>();
    private boolean mIsVisibleToUser;

    public static LotWizardRouteFragment newInstance(String requestCode) {
        LotWizardRouteFragment fragment = new LotWizardRouteFragment();

        Bundle args = new Bundle();
        args.putString(ARG_KEY, requestCode);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_route, container, false);
    }

    @Override
    public void onViewCreated(View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFromField.setAdapter(new RouteAutocompleteAdapter(getActivity(), mFromField));
        mFromField.setThreshold(3);
        mFromField.setTag(RoutePage.KEY_FROM_TAG);
        mFromField.setOnItemClickListener(new LocationSelectedListener(mFromField));
        mFromField.setLoadingIndicator(mProgressFrom);

        mToField.setAdapter(new RouteAutocompleteAdapter(getActivity(), mToField));
        mToField.setThreshold(3);
        mToField.setTag(RoutePage.KEY_TO_TAG);
        mToField.setOnItemClickListener(new LocationSelectedListener(mToField));
        mToField.setLoadingIndicator(mProgressTo);
        mToField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ((AutoCompleteTextView)v).dismissDropDown();
                }

                return false;
            }
        });

        mMapView.onCreate(savedInstanceState);

        Criteria criteria = new Criteria();
        mProvider = mLocationManager.getBestProvider(criteria, true);

        initMap(savedInstanceState);

        mButtonFromGps.setOnClickListener(this);
        mButtonToGps.setOnClickListener(this);


        Handler addressResultHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                final Bundle data = msg.getData();

                if (data == null) {
                    return;
                }

                final int resultCode = data.getInt(GeocoderIntentService.RESULT_STATUS_KEY);
                final String message = data.getString(GeocoderIntentService.RESULT_MESSAGE_KEY);
                final String action = data.getString(GeocoderIntentService.RESULT_ACTION_KEY);
                Address address = data.getParcelable(GeocoderIntentService.RESULT_DATA_KEY);

                final Bundle intentExtras = data.getBundle(GeocoderIntentService
                        .RESULT_INTENT_EXTRAS);
                final int fieldId = intentExtras.getInt(EXTRA_FIELD_ID);


                if (resultCode == GeocoderIntentService.SUCCESS_RESULT) {

                    if (action.equals(ACTION_SET_MY_POSITION)) {
                        mMyLocationAddress = address;
                    }

                    updateAddressField(address, fieldId);
                    saveRouteData(address, fieldId);
                    setMapMarker(address, fieldId);
                } else {
                    final DelayAutoСompleteTextView routeField = (DelayAutoСompleteTextView) getView().findViewById
                            (fieldId);

                    if (!Strings.isEmpty(routeField.getText().toString())) {
                        address = new Address(Locale.getDefault());
                        address.setAddressLine(0, routeField.getText().toString());
                        saveRouteData(address, fieldId);
                    }
                }

                if (message != null) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }

            }
        };

        mAddressResultReceiver = new AddressResultReceiver(addressResultHandler);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        mIsVisibleToUser = isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void initMap(Bundle savedInstanceState) {
        mMap = mMapView.getMap();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                loadRouteData();
            }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                for (Map.Entry<Integer, Marker> entry : mMarkerList.entrySet()) {
                    Integer key = entry.getKey();
                    Marker currentMarker = entry.getValue();

                    if (!currentMarker.equals(marker)) {
                        continue;
                    }

                    DelayAutoСompleteTextView routeField = (DelayAutoСompleteTextView) getView().findViewById(key);

                    if (routeField == null) {
                        return;
                    }

                    Location location = new Location(mProvider);
                    location.setLatitude(marker.getPosition().latitude);
                    location.setLongitude(marker.getPosition().longitude);

                    startGeocodeService(ACTION_SET_MARKER, location, routeField.getId());
                }
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (mMarkerList.size() >= 2) {
                    return;
                }

                Location location = new Location(mProvider);
                location.setLatitude(latLng.latitude);
                location.setLongitude(latLng.longitude);

                startGeocodeService(ACTION_SET_MARKER, location, mMarkerList.get(R.id.et_from) == null ? R.id.et_from : R.id.et_to);
            }
        });

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(getActivity());

        mLatestLocation = LocationUtils.getLatestLocation(getActivity(), mProvider);

        // Initialize the location fields
        if (mLatestLocation != null && savedInstanceState == null) {
            onLocationChanged(mLatestLocation);
        }
    }

    private void setMapMarker(Address address, int fieldId) {
        Preconditions.checkState(mMap != null, "map cannot be null");

        final double lat = address.getLatitude();
        final double lng = address.getLongitude();

        BitmapDescriptor markerIcon = fieldId == R.id.et_to ? BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_RED) : BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_GREEN);

        final Marker newMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat,
                lng)).icon(markerIcon).title(LocationUtils.formatAddress(address)).draggable(true));

        if (mMarkerList.get(fieldId) != null) {
            mMarkerList.get(fieldId).remove();
        }

        mMarkerList.put(fieldId, newMarker);

        animateCameraToFitMarkers();
    }

    private void animateCameraToFitMarkers() {
        if (mMarkerList.size() == 0) {
            return;
        }

        if (mMarkerList.size() > 1) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : mMarkerList.values()) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cameraUpdate);
        } else {
            Marker marker = (Marker) mMarkerList.values().toArray()[0];
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15);

            mMap.animateCamera(cameraUpdate);
        }
    }

    private void updateAddressField(Address address, int fieldId) {
        final DelayAutoСompleteTextView routeField = (DelayAutoСompleteTextView) getView().findViewById
                (fieldId);

        if (routeField != null) {
            routeField.setFilterEnabled(false);
            routeField.setText(LocationUtils.formatAddress(address));
            routeField.setFilterEnabled(true);
        }
    }

    private void saveRouteData(Address address, int fieldId) {
        Preconditions.checkState(mPage != null, "page cannot be null");

        final DelayAutoСompleteTextView routeField = (DelayAutoСompleteTextView) getView().findViewById
                (fieldId);

        if (routeField != null) {
            Bundle routeData = mPage.getData().getBundle(RoutePage.KEY_ROUTE);

            if (routeData == null) {
                routeData = new Bundle();
            }

            routeData.putParcelable((String) routeField.getTag(), address);

            mPage.getData().putBundle(RoutePage.KEY_ROUTE, routeData);
            mPage.notifyDataChanged();
        }
    }

    private void loadRouteData() {
        Preconditions.checkState(mPage != null, "page cannot be null");

        Bundle routeData = mPage.getData().getBundle(RoutePage.KEY_ROUTE);

        if (routeData == null) {
            return;
        }

        for (String tag : routeData.keySet()) {
            DelayAutoСompleteTextView routeField = (DelayAutoСompleteTextView) getView().findViewWithTag(tag);
            Address address = routeData.getParcelable(tag);

            if (routeField == null || address == null) {
                continue;
            }

            updateAddressField(address, routeField.getId());
            setMapMarker(address, routeField.getId());
        }
    }

    private void startGeocodeService(String action, Object data, int resId) {
        Preconditions.checkState(mAddressResultReceiver != null, "result receiver cannot be null");

        Intent intent = new Intent(getActivity(), GeocoderIntentService.class);
        intent.putExtra(GeocoderIntentService.EXTRA_RECEIVER, mAddressResultReceiver);

        if (data instanceof Location) {
            intent.putExtra(GeocoderIntentService.EXTRA_LOCATION_DATA, (Location) data);
        } else {
            intent.putExtra(GeocoderIntentService.EXTRA_LOCATION_NAME, (String) data);
        }

        if (resId > 0) {
            intent.putExtra(EXTRA_FIELD_ID, resId);
        }

        intent.setAction(action);

        getActivity().startService(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        mLocationManager.requestSingleUpdate(mProvider, this, null);
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }



    @Override
    public void onClick(View gpsButton) {
        int fieldId;

        if (gpsButton.getId() == R.id.button_from_gps) {
            fieldId = R.id.et_from;
        } else {
            fieldId = R.id.et_to;
        }


        if (mMyLocationAddress != null) {
            updateAddressField(mMyLocationAddress, fieldId);
            saveRouteData(mMyLocationAddress,fieldId);
        } else {
            startGeocodeService(ACTION_SET_MY_POSITION, mLatestLocation, fieldId);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatestLocation = location;

        double lat = location.getLatitude();
        double lng = location.getLongitude();

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15);

        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getActivity(), "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getActivity(), "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    private class RouteAutocompleteAdapter extends BaseAdapter implements Filterable {

        private final Context mContext;
        private ArrayList<Map<String, String>> mResultList = new ArrayList<>();

        private View mRouteField;

        private RouteAutocompleteAdapter(Context context, View routeField) {
            mContext = context;
            mRouteField = routeField;
        }

        @Override
        public int getCount() {
            return mResultList.size();
        }

        @Override
        public Map<String, String> getItem(int position) {
            return mResultList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            }

            Map<String, String> item = getItem(position);
            ((TextView)convertView.findViewById(android.R.id.text1)).setText(item.get("description"));

            return convertView;
        }



        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (constraint != null && mIsVisibleToUser) {
                        if (!Strings.isEmpty(constraint)) {
                            // startGeocodeService(ACTION_GEOCODE_INPUT, constraint.toString(), mRouteField.getId());
                            mResultList = LocationUtils.autocomplete(constraint.toString(), null);
                        } else {
                            mRouteField.post(new Runnable() {
                                @Override
                                public void run() {
                                    clearRouteData(mRouteField);
                                }
                            });
                        }
                    }

                    filterResults.values = mResultList;
                    filterResults.count = mResultList.size();

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        mResultList = (ArrayList<Map<String, String>>) results.values;
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };

            return filter;
        }
    }

    private void clearRouteData(View routeField) {
        Marker marker = mMarkerList.get(routeField.getId());

        if (marker != null) {
            marker.remove();
            mMarkerList.remove(routeField.getId());
        }

        Bundle bundle = mPage.getData().getBundle(RoutePage.KEY_ROUTE);

        if (bundle != null) {
            mPage.getData().getBundle(RoutePage.KEY_ROUTE).remove((String)routeField.getTag());
            mPage.notifyDataChanged();
        }
    }

    private class AddressResultReceiver extends ResultReceiver {


        private final Handler mHandler;


        public AddressResultReceiver(Handler handler) {
            super(handler);
            mHandler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Message message = mHandler.obtainMessage();
            message.setData(resultData);
            mHandler.sendMessage(message);
        }
    }

    private class LocationSelectedListener implements AdapterView.OnItemClickListener {
        private DelayAutoСompleteTextView mRouteField;

        private LocationSelectedListener(DelayAutoСompleteTextView routeField) {
            mRouteField = routeField;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Map<String, String> locationItem = (Map<String, String>)parent.getItemAtPosition(position);

            mRouteField.setFilterEnabled(false);
            mRouteField.setText(locationItem.get("description"));
            mRouteField.setFilterEnabled(true);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Address address = LocationUtils.fetchPlaceDetails(locationItem.get("placeid"));
                    address.setAddressLine(0, locationItem.get("description"));
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setMapMarker(address, mRouteField.getId());
                            saveRouteData(address, mRouteField.getId());
                        }
                    });
                }
            }).start();
        }
    }

    @Override
    protected String getPageTitleResId() {
        return getResources().getString(R.string.lot_wizard_title_route);
    }
}
