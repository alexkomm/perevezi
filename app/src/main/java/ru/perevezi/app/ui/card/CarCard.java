/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CarModel;
import ru.perevezi.app.ui.components.HorizontalSpacingItemDecoration;

public class CarCard extends Card {
    private final CarModel mCar;

    @InjectView(R.id.tv_car_name)
    TextView mCarNameTextView;

    @InjectView(R.id.tv_car_details)
    TextView mCarDetailsTextView;

    @InjectView(R.id.rv_car_gallery)
    RecyclerView mCarGalleryRecyclerView;

    public CarCard(Context context, CarModel car) {
        this(context, R.layout.card_car, car);
    }

    public CarCard(Context context, int innerLayout, CarModel car) {
        super(context, innerLayout);
        mCar = car;

        init();
    }

    private void init() {

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ButterKnife.inject(this, parent);

        mCarNameTextView.setText(mCar.getName());
        mCarDetailsTextView.setText(getFormattedDetails());

        mCarGalleryRecyclerView.addItemDecoration(new HorizontalSpacingItemDecoration(16));
        mCarGalleryRecyclerView.setHasFixedSize(true);
        mCarGalleryRecyclerView.setAdapter(new CarGalleryAdapter(mCar.getImages(), R
                .layout.item_car_image));
        mCarGalleryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext,
                LinearLayoutManager.HORIZONTAL, false));

    }

    private String getFormattedDetails() {
        List<String> strList = new ArrayList<>(3);

        if (!Strings.isNullOrEmpty(mCar.getBody())) {
            strList.add(mCar.getBody());
        }

        if (!Strings.isNullOrEmpty(mCar.getCapacity())) {
            strList.add(mCar.getCapacity());
        }

        if (!Strings.isNullOrEmpty(mCar.getSizes()) && !mCar.getSizes().equals("0x0x0")) {
            strList.add(mCar.getSizes());
        }

        if (mCar.getSeats() > 0) {
            strList.add(mContext.getResources().getQuantityString(R.plurals.seats, mCar.getSeats(), mCar.getSeats()));
        }

        return ru.perevezi.app.utils.Strings.join(" - ", strList);
    }

    public static class CarGalleryAdapter extends RecyclerView.Adapter<CarGalleryAdapter.ViewHolder> {

        private final String[] mUrls;
        private final int mItemLayout;

        public CarGalleryAdapter(String[] urls, int itemLayout) {
            mUrls = urls;
            mItemLayout = itemLayout;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(mItemLayout, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            String image = mUrls[position];

            holder.image.setImageBitmap(null);
            Picasso.with(holder.image.getContext()).cancelRequest(holder.image);
            Picasso.with(holder.image.getContext()).load(image).fit().centerCrop().into(holder.image);

            holder.itemView.setTag(image);
        }

        @Override
        public int getItemCount() {
            return mUrls.length;
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            ImageView image;

            public ViewHolder(View itemView) {
                super(itemView);

                image = (ImageView) itemView.findViewById(R.id.image);
            }

        }

    }


}
