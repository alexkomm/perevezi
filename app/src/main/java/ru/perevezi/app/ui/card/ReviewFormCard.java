/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.views.NewReviewUi;
import ru.perevezi.app.utils.Strings;

public class ReviewFormCard extends Card {

    private final LotModel mLotModel;

    Map<String, String> mReviewData;
    private final NewReviewUi.UiCallbacks mUiCallbacks;

    @InjectView(R.id.review_save)
    Button mReviewSaveButton;

    @InjectView(R.id.review_vote_group)
    RadioGroup mReviewVoteRadioGroup;

    @InjectView(R.id.review_final_price)
    TextView mReviewFinalPriceTextView;

    @InjectView(R.id.review_comment)
    TextView mReviewCommentTextView;

    public ReviewFormCard(Context context, LotModel lotModel, Map<String, String> reviewData, NewReviewUi.UiCallbacks uiCallbacks) {
        this(context, R.layout.card_review_form, lotModel, reviewData, uiCallbacks);
    }

    public ReviewFormCard(Context context, int innerLayout, LotModel lotModel, Map<String, String> reviewData, NewReviewUi.UiCallbacks uiCallbacks) {
        super(context, innerLayout);
        mLotModel = lotModel;
        mReviewData = reviewData;
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ButterKnife.inject(this, parent);

        BidModel bidModel = mLotModel.getRelatedBid();

        if (bidModel.getStatus() == BidModel.BID_STATUS_COMPLETE) {
            mReviewFinalPriceTextView.setVisibility(View.VISIBLE);
        }

        mReviewVoteRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mReviewData.put("vote", String.valueOf(group.findViewById(checkedId).getTag()));
            }
        });

        if (mReviewData.get("vote") == null) {
            mReviewVoteRadioGroup.check(R.id.review_positive);
        }

        mReviewSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prepare()) {
                    mUiCallbacks.onNewReview(mReviewData);
                }
            }
        });
    }


    private boolean prepare() {

        boolean noErrors = true;

        if (mReviewFinalPriceTextView.getVisibility() == View.VISIBLE) {

            String finalPrice = mReviewFinalPriceTextView.getText().toString();

            if (finalPrice != null && !Strings.isEmpty(finalPrice)) {
                mReviewData.put("price", finalPrice);
            } else {
                mReviewFinalPriceTextView.setError(mContext.getResources().getString(R.string.error_field_required));
                noErrors = false;
            }
        }

        String comment = mReviewCommentTextView.getText().toString();

        if (comment != null && !Strings.isEmpty(comment)) {
            mReviewData.put("comment", comment);
        } else {
            mReviewCommentTextView.setError(mContext.getResources().getString(R.string.error_field_required));
            noErrors = false;
        }

        return noErrors;
    }

}
