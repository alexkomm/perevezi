/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.HashMap;

public interface CommissionsUi extends BaseUi {

    HashMap<String, Object> getCommissionsData();

    void addCommissionsToList(HashMap<String, Object> data);

}
