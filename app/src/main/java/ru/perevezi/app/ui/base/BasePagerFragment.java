/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.perevezi.app.R;

public class BasePagerFragment extends BaseFragment {

    public static final String TAG = "base_pager_fragment";

    private static final String ARG_KEY_POSITION = "position";

    private PagerFragmentListener mPagerFragmentListener;
    private SavedState mSavedState;
    private int mPosition;

    public static BasePagerFragment newInstance(int position) {
        BasePagerFragment fragment = new BasePagerFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_KEY_POSITION, position);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mPosition = args.getInt(ARG_KEY_POSITION);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mPagerFragmentListener = (PagerFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PagerFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPagerFragmentListener = null;
    }

    @Override
    public void onPause() {
        super.onPause();

        mSavedState = getFragmentManager().saveFragmentInstanceState(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.view_lot_view_pager, container, false);

        ViewPager viewPager = (ViewPager) result.findViewById(R.id.pager);
        viewPager.setAdapter(new BasePagerAdapter(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(3);

        TabLayout tabLayout = (TabLayout) result.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (mPosition > 0) {
            viewPager.setCurrentItem(mPosition);
        }

        return result;
    }

    public interface PagerFragmentListener {
        Fragment onGetItem(int position);
        CharSequence onGetPageTitle(int position);
        int onGetCount();
    }

    public class BasePagerAdapter extends FragmentStatePagerAdapter {

        public BasePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f = mPagerFragmentListener.onGetItem(position);
            return f;
        }

        @Override
        public int getCount() {
            return mPagerFragmentListener.onGetCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mPagerFragmentListener.onGetPageTitle(position);
        }
    }
}
