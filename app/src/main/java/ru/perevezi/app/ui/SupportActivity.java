/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;

public class SupportActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Button supportButton = (Button) findViewById(R.id.btn_support_call);
        supportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+78005551528"));
                startActivity(intent);
            }
        });

    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_support;

    }
}
