/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.Constants;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotListCard;
import ru.perevezi.app.ui.components.DynamicLoadingListener;
import ru.perevezi.app.ui.controllers.UserReviewsSectionController;
import ru.perevezi.app.ui.views.UserReviewsSectionUi;
import ru.perevezi.domain.utils.Preconditions;

public class UserReviewsSectionFragment extends BaseFragment implements UserReviewsSectionUi {

    private static final String ARG_KEY_SECTION = "section";
    private static final String ARG_KEY_REVIEWS = "reviews";
    private static final String ARG_KEY_USER_ID = "user_id";

    @Inject
    UserReviewsSectionController mUserReviewsSectionController;

    @InjectView(R.id.card_list)
    CardRecyclerView mCardRecyclerView;

    private CardArrayRecyclerViewAdapter mCardArrayAdapter;
    private LinearLayoutManager mLayoutManager;

    private Integer mVoteType;
    private Integer mUserId;

    private List<LotModel> mLotList;
    private UiCallbacks mUiCallbacks;
    private DynamicLoadingListener mDynamicLoadingListener;


    public static UserReviewsSectionFragment newInstance(int userId, int section) {
        UserReviewsSectionFragment fragment = new UserReviewsSectionFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_KEY_USER_ID, userId);
        args.putInt(ARG_KEY_SECTION, section);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mVoteType = args.getInt(ARG_KEY_SECTION);
            mUserId = args.getInt(ARG_KEY_USER_ID);
        }

        Preconditions.checkState(mVoteType != null, "Vote type cannot be null");
        Preconditions.checkState(mUserId != null, "User id cannot be null");

        if (savedInstanceState != null) {
            mLotList = Parcels.unwrap(savedInstanceState.getParcelable(ARG_KEY_REVIEWS));
        }

        mUserReviewsSectionController.initialize(this);
        registerUiLifecycleObserver(mUserReviewsSectionController);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(ARG_KEY_REVIEWS, Parcels.wrap(mLotList));
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pager_section, container, false);
    }

    @Override
    public void addReviewsToList(List<LotModel> list) {
        if (mLotList == null) {
            mLotList = new ArrayList<>();
        }

        mLotList.addAll(list);

        updateCardAdapter(list);
    }

    private void updateCardAdapter(List<LotModel> list) {
        List<Card> cardList = new ArrayList<>();

        for (LotModel lot : list) {
            cardList.add(new LotListCard(getActivity(), lot, true));
        }

        mCardArrayAdapter.addAll(cardList);

        mCardRecyclerView.clearOnScrollListeners();

        if (mCardArrayAdapter.getItemCount() >= Constants.PEREVEZI_REVIEWS_LIST_LIMIT) {
            mCardRecyclerView.addOnScrollListener(mDynamicLoadingListener);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArrayRecyclerViewAdapter(getActivity(), cards);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mCardRecyclerView.setHasFixedSize(false);
        mCardRecyclerView.setLayoutManager(mLayoutManager);
        mCardRecyclerView.setAdapter(mCardArrayAdapter);

        mDynamicLoadingListener = new DynamicLoadingListener(mCardRecyclerView,
                mLayoutManager, new DynamicLoadingListener.OnThresholdReachedListener() {
            @Override
            public void onThresholdReached(int totalCount) {
                mUiCallbacks.onVisibleThresholdReached(totalCount);
            }
        });

        if (mLotList != null) {
            updateCardAdapter(mLotList);
        }
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public int getUserId() {
        return mUserId;
    }

    @Override
    public boolean isItemsFetched() {
        return mLotList != null;
    }

    @Override
    public int getSection() {
        return mVoteType;
    }

}
