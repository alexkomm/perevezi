/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.mapper.BidModelMapper;
import ru.perevezi.app.ui.views.LotBidsUi;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.interactor.impl.GetLotBidsUseCase;

public class LotBidsController implements Controller<LotBidsUi>, LotBidsUi.UiCallbacks
{

    private final GetLotBidsUseCase mGetLotBidsUseCase;
    private final BidModelMapper mBidModelMapper;
    private LotBidsUi mLotBidsUi;
    private int mLotId;


    @Inject
    public LotBidsController(GetLotBidsUseCase getLotBidsUseCase, BidModelMapper bidModelMapper) {
        mGetLotBidsUseCase = getLotBidsUseCase;
        mBidModelMapper = bidModelMapper;
    }

    @Override
    public void initialize(LotBidsUi lotBidsUi) {
        mLotBidsUi = lotBidsUi;
        mLotBidsUi.setUiCallbacks(this);

        mLotId = mLotBidsUi.getLotId();
    }

    private void fetchLotBids() {
        RxUseCase.callback(mGetLotBidsUseCase, mLotId).register(new RxUseCase.AsyncCallback<List<PereveziBid>>() {
            @Override
            public void onResultOk(List<PereveziBid> result) {
                mLotBidsUi.toggleOverlayProgress(false);

                List<BidModel> bidModelList = (List<BidModel>) mBidModelMapper.mapResponse(result);
                mLotBidsUi.addBidsToList(bidModelList);
            }

            @Override
            public void onError(Throwable throwable) {
                mLotBidsUi.toggleOverlayProgress(false);
                mLotBidsUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {
        if (mLotBidsUi.getBidsList() == null) {
            mLotBidsUi.toggleOverlayProgress(true);
            fetchLotBids();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDataRefresh() {
        fetchLotBids();
    }
}
