/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.ui.LotWizardActiviy;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.controllers.ConfirmCodeController;
import ru.perevezi.app.ui.views.ConfirmCodeUi;
import ru.perevezi.domain.utils.Preconditions;

public class ConfirmCodeFragment extends BaseFragment implements ConfirmCodeUi {

    @InjectView(R.id.btn_retry)
    Button mRetryButton;

    @InjectView(R.id.et_sms_code)
    EditText mSmsCodeField;

    @Inject
    ConfirmCodeController mController;

    UiCallbacks mCallbacks;

    ViewGroup mContainer;

    private ProgressDialog mProgressDialog;
    
    private final static String REQUEST_ID_ARG = "request_id";

    private String mRequestId;

    public static ConfirmCodeFragment newInstance(String requestCode) {
        ConfirmCodeFragment fragment = new ConfirmCodeFragment();

        Bundle args = new Bundle();
        args.putString(REQUEST_ID_ARG, requestCode);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRequestId = getArguments().getString(REQUEST_ID_ARG);
        Preconditions.checkState(mRequestId != null, "Request id must be provided for confirmation");

        PereveziApplication.from(getActivity()).inject(this);

        mController.initialize(this);
        mProgressDialog = new ProgressDialog(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        mContainer = container;

        return inflater.inflate(R.layout.fragment_confirm_code, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Card card = new Card(getActivity(), R.layout.card_confirm_code);

        CardHeader cardHeader = new CardHeader(getActivity());
        cardHeader.setTitle(getResources().getString(R.string.header_sms_confirmation));

        card.addCardHeader(cardHeader);

        CardViewNative cardViewNative = (CardViewNative)view.findViewById(R.id.confirm_code);
        cardViewNative.setCard(card);

        super.onViewCreated(view, savedInstanceState);

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onRetry(mRequestId);
            }
        });

        mSmsCodeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 4) {
                    mCallbacks.onConfirmRequest(mRequestId, Integer.parseInt(s.toString()));
                }
            }
        });
    }

    @Override
    public void setInterfaceCallbacks(UiCallbacks callbacks) {
        mCallbacks = callbacks;
    }

    @Override
    public void toggleToolbarProgress(boolean show) {
        if (show) {
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toggleInvalid(boolean show) {
        if (show) {
            mSmsCodeField.setError(getResources().getString(R.string.error_invalid_sms_code));
            mSmsCodeField.requestFocus();
        } else {
            mSmsCodeField.setError(null);
        }
    }

    @Override
    public void onCodeRetry() {
        Toast.makeText(getActivity(), getResources().getString(R.string.toast_code_resent), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToCreateLotScreen(AccountModel accountModel) {
        Intent intent = new Intent(getActivity(), LotWizardActiviy.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public void navigateToQuickRegisterScreen() {
        RegisterFragment registerFragment = new RegisterFragment();

        getFragmentManager().beginTransaction().replace(mContainer.getId(), registerFragment).commit();
    }

}
