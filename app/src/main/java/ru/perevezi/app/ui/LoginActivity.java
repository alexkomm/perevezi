/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.Constants;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.controllers.LoginController;
import ru.perevezi.app.ui.views.LoginUi;

import static android.accounts.AccountManager.KEY_ACCOUNT_NAME;
import static android.accounts.AccountManager.KEY_ACCOUNT_TYPE;
import static android.accounts.AccountManager.KEY_AUTHTOKEN;

public class LoginActivity extends BaseActivity implements LoginUi {

    @Inject
    LoginController mLoginController;

    Button mSignInButton;
    EditText mLoginEditText;
    EditText mPasswordEditText;

    private AccountAuthenticatorResponse accountAuthenticatorResponse = null;
    private Bundle resultBundle = null;
    private UiCallbacks mUiCallbacks;

    /**
     * Set the result that is to be sent as the result of the request that caused this
     * Activity to be launched. If result is null or this method is never called then
     * the request will be canceled.
     *
     * @param result this is returned as the result of the AbstractAccountAuthenticator request
     */
    public final void setAccountAuthenticatorResult(Bundle result) {
        resultBundle = result;
    }

    /**
     * Retreives the AccountAuthenticatorResponse from either the intent of the icicle, if the
     * icicle is non-zero.
     *
     * @param icicle the save instance data of this Activity, may be null
     */
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        Card card = new Card(this, R.layout.card_login);

        CardHeader cardHeader = new CardHeader(this);
        cardHeader.setTitle(getResources().getString(R.string.action_sign_in_to_app));

        card.addCardHeader(cardHeader);

        CardViewNative cardViewNative = (CardViewNative) findViewById(R.id.card);
        cardViewNative.setCard(card);

        mLoginEditText = (EditText) findViewById(R.id.et_login);
        mPasswordEditText = (EditText) findViewById(R.id.et_pass);
        mSignInButton = (Button) findViewById(R.id.btn_sign_in);

        accountAuthenticatorResponse =
                getIntent().getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);

        if (accountAuthenticatorResponse != null) {
            accountAuthenticatorResponse.onRequestContinued();
        }

        mLoginController.initialize(this);

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = mLoginEditText.getText().toString();
                String password = mPasswordEditText.getText().toString();

                boolean hasErrors = false;

                if (login.isEmpty()) {
                    mLoginEditText.setError(getResources().getString(R.string.error_field_required));
                    hasErrors = true;
                }

                if (password.isEmpty()) {
                    mPasswordEditText.setError(getResources().getString(R.string.error_field_required));
                    hasErrors = true;
                }

                if (!hasErrors) {
                    mUiCallbacks.onLogin(login, password);
                }

            }
        });
    }

    @Override
    public void finishLogin(String name, String authToken) {
        final Intent intent = new Intent();
        intent.putExtra(KEY_ACCOUNT_NAME, name);
        intent.putExtra(KEY_ACCOUNT_TYPE, Constants.PEREVEZI_ACCOUNT_TYPE);
        intent.putExtra(KEY_AUTHTOKEN, authToken);

        resultBundle = intent.getExtras();
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Sends the result or a Constants.ERROR_CODE_CANCELED error if a result isn't present.
     */
    public void finish() {
        if (accountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (resultBundle != null) {
                accountAuthenticatorResponse.onResult(resultBundle);
            } else {
                accountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled");
            }
            accountAuthenticatorResponse = null;
        }

        super.finish();
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }
}
