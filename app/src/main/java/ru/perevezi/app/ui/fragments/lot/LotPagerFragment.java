/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.lot;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseFragment;

public class LotPagerFragment extends BaseFragment {

    public static final String TAG = "pager_fragment";

    private static final String ARG_KEY_LOT_ID = "lot_id";
    private static final String ARG_KEY_POSITION = "position";

    private int mLotId;

    private SavedState mSavedState;
    private int mPosition;

    public static LotPagerFragment newInstance(int lotId, int position) {
        LotPagerFragment fragment = new LotPagerFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_KEY_LOT_ID, lotId);
        args.putInt(ARG_KEY_POSITION, position);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mLotId = args.getInt(ARG_KEY_LOT_ID);
            mPosition = args.getInt(ARG_KEY_POSITION);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mSavedState = getFragmentManager().saveFragmentInstanceState(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.view_lot_view_pager, container, false);

        ViewPager viewPager = (ViewPager) result.findViewById(R.id.pager);
        viewPager.setAdapter(new LotFragmentPagerAdapter(getChildFragmentManager(), mSavedState));
        viewPager.setOffscreenPageLimit(3);

        TabLayout tabLayout = (TabLayout) result.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (mPosition > 0) {
            viewPager.setCurrentItem(mPosition);
        }

        return result;
    }


    public class LotFragmentPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 3;
        private final SavedState mSavedState;


        public LotFragmentPagerAdapter(FragmentManager fm, SavedState savedState) {
            super(fm);

            mSavedState = savedState;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment f;

            switch (position) {
                case 0:
                    f = LotDetailsFragment.newInstance(position, mLotId);
                    f.setInitialSavedState(mSavedState);

                    return f;
                case 1:
                    f = LotBidsFragment.newInstance(position, mLotId);
                    f.setInitialSavedState(mSavedState);

                    return f;
                case 2:
                    f = LotCommentsFragment.newInstance(position, mLotId);
                    f.setInitialSavedState(mSavedState);

                    return f;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.pager_lot_fragment);
                case 1:
                    return getString(R.string.pager_bid_list_fragment);
                case 2:
                    return getString(R.string.pager_comments_fragment);
                default:
                    return null;
            }
        }
    }
}
