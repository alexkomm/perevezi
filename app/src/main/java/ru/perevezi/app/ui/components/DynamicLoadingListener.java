/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class DynamicLoadingListener extends RecyclerView.OnScrollListener {

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private final OnThresholdReachedListener mListener;

    public DynamicLoadingListener(RecyclerView recyclerView, LinearLayoutManager layoutManager, OnThresholdReachedListener listener) {
        mRecyclerView = recyclerView;
        mLayoutManager = layoutManager;
        mListener = listener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = mRecyclerView.getChildCount();
        totalItemCount = mLayoutManager.getItemCount();
        firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        if (totalItemCount < previousTotal) {
            previousTotal = totalItemCount;
            if (totalItemCount == 0) { loading = true; }
        }

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {

            mListener.onThresholdReached(totalItemCount);

            loading = true;
        }
    }


    public interface OnThresholdReachedListener {
        void onThresholdReached(int totalCount);
    }

}
