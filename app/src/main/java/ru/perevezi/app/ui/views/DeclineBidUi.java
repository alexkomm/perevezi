/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.Map;

public interface DeclineBidUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void navigateToBidList();

    interface UiCallbacks {
        void onDeclineButtonClicked(Map<String, Object> declineData, boolean acceptNew);
    }

}
