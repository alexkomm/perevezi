/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface BillingUi extends BaseUi {
    void setUiCallbacks(UiCallbacks uiCallbacks);

    interface UiCallbacks {
        void onUpdateCredentials();
    }
}
