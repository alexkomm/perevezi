/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.LotWizardUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.CreateLotUseCase;

public class LotWizardController implements Controller<LotWizardUi>, LotWizardUi.UiCallbacks {

    private LotWizardUi mUi;

    private final CreateLotUseCase mUseCase;

    @Inject
    public LotWizardController(CreateLotUseCase useCase) {
        mUseCase = useCase;
    }

    @Override
    public void initialize(LotWizardUi lotWizardUi) {
        mUi = lotWizardUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onLotCreateConfirm(PereveziLot lot) {
        mUi.toggleDialogProgress(true);

        RxUseCase.callback(mUseCase, lot).register(new RxUseCase.AsyncCallback<PereveziLot>() {

            @Override
            public void onResultOk(PereveziLot result) {
                mUi.toggleDialogProgress(false);
                mUi.getRouterUi().navigateToLotScreen(result.getNid());
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }
}
