/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CommentModel;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotCommentCard;
import ru.perevezi.app.ui.controllers.CommentController;
import ru.perevezi.app.ui.views.CommentUi;
import ru.perevezi.domain.utils.Preconditions;

public class CommentFragment extends BaseFragment implements CommentUi {

    public static final String TAG = "comment_fragment";

    private static final String ARG_KEY_PARENT_COMMENT = "parent_comment";
    private static final String ARG_KEY_COMMENT = "comment";
    private static final String ARG_KEY_NID = "nid";
    private static final String ARG_KEY_FORBID_MSG = "forbid_msg";
    private static final String ARG_KEY_COMMENT_ACCESS = "comment_access";
    private String mForbidMessage;


    private enum CommentAccess {
        NOT_CHECKED,
        GRANTED,
        RESTRICTED
    }

    private CommentAccess mCommentAccess = CommentAccess.NOT_CHECKED;

    private CommentModel mParentComment;

    private int mNid;

    @Inject
    CommentController mCommentController;

    @InjectView(R.id.parent_comment)
    CardViewNative mParentCommentCard;

    @InjectView(R.id.et_comment)
    EditText mCommentTextView;

    @InjectView(R.id.btn_comment_submit)
    Button mCommentSubmit;

    @InjectView(R.id.comment_form_group)
    ViewGroup mCommentFormGroup;

    @InjectView(R.id.comment_forbid_msg)
    TextView mCommentForbidMessageView;

    private UiCallbacks mUiCallbacks;

    private CommentCreateConfirmDialog mCommentCreateConfirmDialog;

    private ProgressDialog mProgressDialog;

    public static CommentFragment newInstance(int nid, @Nullable CommentModel parentComment) {
        CommentFragment fragment = new CommentFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_PARENT_COMMENT, Parcels.wrap(parentComment));
        args.putInt(ARG_KEY_NID, nid);
        
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mForbidMessage = savedInstanceState.getString(ARG_KEY_FORBID_MSG);
            mCommentAccess = (CommentAccess) savedInstanceState.getSerializable(ARG_KEY_COMMENT_ACCESS);
        }

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();
        if (args != null) {
            mParentComment = Parcels.unwrap(args.getParcelable(ARG_KEY_PARENT_COMMENT));
            mNid = args.getInt(ARG_KEY_NID);
        }


        mCommentController.initialize(this);

        mProgressDialog = new ProgressDialog(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleBackArrow(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleBackArrow(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mCommentTextView != null) {
            outState.putString(ARG_KEY_COMMENT, mCommentTextView.getText().toString());
        }

        outState.putString(ARG_KEY_FORBID_MSG, mForbidMessage);
        outState.putSerializable(ARG_KEY_COMMENT_ACCESS, mCommentAccess);

        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mCommentAccess == CommentAccess.NOT_CHECKED) {
            mUiCallbacks.checkCommentAccess(mNid);
        } else if (mCommentAccess == CommentAccess.GRANTED) {
            showCommentForm();
        } else {
            showCommentForbiddenMessage(mForbidMessage);
        }

        if (mParentComment != null) {
            LotCommentCard commentCard = new LotCommentCard(getActivity(), mParentComment, false);

            mParentCommentCard.setVisibility(View.VISIBLE);
            mParentCommentCard.setCard(commentCard);

            commentCard.toogleReplyButton(false);

            mCommentSubmit.setText(getResources().getString(R.string.btn_comment_reply));
        }

        if (savedInstanceState != null) {
            String commentText = savedInstanceState.getString(ARG_KEY_COMMENT);
            mCommentTextView.setText(commentText);
        }

        mCommentSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pid = mParentComment != null ? mParentComment.getCid() : 0;
                String comment = mCommentTextView.getText().toString();

                if (Strings.isNullOrEmpty(comment)) {
                    mCommentTextView.setError(getResources().getString(R.string.error_field_required));
                    return;
                }

                mCommentCreateConfirmDialog = CommentCreateConfirmDialog.newInstance(comment, mNid, pid);
                mCommentCreateConfirmDialog.setUiCallbacks(mUiCallbacks);

                mCommentCreateConfirmDialog.show(getFragmentManager(), CommentCreateConfirmDialog.TAG);
            }
        });
    }

    @Override
    public void toggleDialogProgress(boolean show) {
        if (show) {
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(getResources().getString(R.string.progress_text));
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showCommentForbiddenMessage(String message) {
        mCommentFormGroup.setVisibility(View.GONE);

        mCommentForbidMessageView.setVisibility(View.VISIBLE);
        mCommentForbidMessageView.setText(Html.fromHtml(message).toString());

        mCommentAccess = CommentAccess.RESTRICTED;
        mForbidMessage = message;
    }

    @Override
    public void showCommentForm() {
        mCommentForbidMessageView.setVisibility(View.GONE);

        mCommentFormGroup.setVisibility(View.VISIBLE);

        mCommentAccess = CommentAccess.GRANTED;
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void navigateToCommentList(@Nullable Integer msgResID) {
        Preconditions.checkState(mNid > 0, "Lot id not found");

        Intent intent = new Intent(getActivity(), LotActivity.class);
        intent.putExtra(LotActivity.EXTRA_ID, mNid);
        intent.putExtra(LotActivity.EXTRA_INITIAL_PAGER_ITEM, 2);

        if (msgResID != null) {
            intent.putExtra(LotActivity.EXTRA_MESSAGE, getResources().getString(msgResID));
        }

        startActivity(intent);
    }

    public static class CommentCreateConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {

        private static final String ARG_KEY_PID = "pid";

        private static final String TAG = "comment_create_confirm";

        private UiCallbacks mUiCallbacks;
        private String mComment;
        private int mNid;
        private int mPid;

        public static CommentCreateConfirmDialog newInstance(String comment, int nid, int pid) {
            CommentCreateConfirmDialog fragment = new CommentCreateConfirmDialog();

            Bundle args = new Bundle();
            args.putString(ARG_KEY_COMMENT, comment);
            args.putInt(ARG_KEY_NID, nid);
            args.putInt(ARG_KEY_PID, pid);

            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Bundle args = getArguments();
            if (args != null) {
                mComment = args.getString(ARG_KEY_COMMENT);
                mNid = args.getInt(ARG_KEY_NID);
                mPid = args.getInt(ARG_KEY_PID);
            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.dialog_comment_create_title)
                    .setPositiveButton(R.string.yes, this)
                    .setNegativeButton(R.string.no, this)
                    .setMessage(R.string.dialog_comment_create_text);

            return adb.create();
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            Preconditions.checkState(mUiCallbacks != null, "Ui callbacks must be set");

            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    mUiCallbacks.onCommentSubmitted(mComment, mNid, mPid);
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }

        public void setUiCallbacks(UiCallbacks uiCallbacks) {
            Preconditions.checkState(uiCallbacks != null, "Ui callbacks cannot be null");

            mUiCallbacks = uiCallbacks;
        }
    }
}
