/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.controllers.RegisterController;
import ru.perevezi.app.ui.views.QuickRegisterUi;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.utils.Validation;

public class RegisterFragment extends BaseFragment implements QuickRegisterUi {

    @Inject
    RegisterController mQuickRegisterPresenter;

    @InjectView(R.id.btn_register)
    Button mButtonRegister;

    @InjectView(R.id.et_email)
    EditText mEmail;

    @InjectView(R.id.et_name)
    EditText mName;

    @InjectView(R.id.et_phone)
    EditText mPhone;

    @InjectView(R.id.form_container)
    LinearLayout mFormContainer;

    ViewGroup mContainer;

    private ProgressDialog mProgressDialog;

    private AlreadyRegisteredDialog mAlreadyRegisteredDialog;

    private UiCallbacks mUiCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PereveziApplication.from(getActivity()).inject(this);

        mQuickRegisterPresenter.initialize(this);

        mProgressDialog = new ProgressDialog(getActivity());
        mAlreadyRegisteredDialog = new AlreadyRegisteredDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
       mContainer = container;

       return inflater.inflate(R.layout.fragment_quick_register, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Card card = new Card(getActivity(), R.layout.card_quick_register);

        CardHeader cardHeader = new CardHeader(getActivity());
        cardHeader.setTitle(getResources().getString(R.string.header_quick_register));

        card.addCardHeader(cardHeader);

        CardViewNative cardViewNative = (CardViewNative)view.findViewById(R.id.quick_register);
        cardViewNative.setCard(card);

        super.onViewCreated(view, savedInstanceState);

        mEmail.setTag("mail");

        mPhone.setTag("profile_phone");
        mPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        mName.setTag("profile_name");


        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Map<String, String> errors = validateFields();

                if (errors.size() > 0) {
                    showErrors(errors);
                } else {
                    mUiCallbacks.onRegisterRequest(mName.getText().toString(), mEmail.getText()
                            .toString(), mPhone.getText().toString());
                }
            }
        });
    }

    @Override
    public void setInterfaceCallbacks(UiCallbacks callbacks) {
        mUiCallbacks = callbacks;
    }


    private Map<String, String> validateFields() {
        Map<String, String> errors = new HashMap<>();

        final String email = mEmail.getText().toString();
        final String phone = mPhone.getText().toString();
        final String name  = mName.getText().toString();

        if (Validation.isEmpty(email)) {
            errors.put((String)mEmail.getTag(), getResources().getString(R.string.error_field_required));
        }
        if (Validation.isEmpty(phone)) {
            errors.put((String)mPhone.getTag(), getResources().getString(R.string.error_field_required));
        }
        if (Validation.isEmpty(name)) {
            errors.put((String)mName.getTag(), getResources().getString(R.string.error_field_required));
        }

        if (!Validation.isValidEmail(email) && !errors.containsKey(mEmail.getTag())) {
            errors.put((String)mEmail.getTag(), getResources().getString(R.string.error_invalid_email));
        }

        if (!Validation.isValidPhone(phone) && !errors.containsKey(mPhone.getTag())) {
            errors.put((String)mPhone.getTag(), getResources().getString(R.string.error_invalid_field));
        }

        return errors;
    }

    @Override
    public void showErrors(Map<String, String> errors) {
        if (errors == null) {
            return;
        }

        mEmail.setError(null);
        mPhone.setError(null);
        mName.setError(null);

        View focusView = null;
        boolean error = false;


        for (int i = 0; i < mFormContainer.getChildCount(); i++) {
            View v = mFormContainer.getChildAt(i);
            if (v instanceof EditText && errors.containsKey(v.getTag())) {
                error = true;

                final String errorText = errors.get(v.getTag());

                ((EditText) v).setError(errorText);

                if (focusView == null) {
                    focusView = v;
                }
            }
        }

        if (error) {
            focusView.requestFocus();
        }
    }

    @Override
    public void navigateToConfirmScreen(String requestId) {
        ConfirmCodeFragment confirmCodeFragment = ConfirmCodeFragment.newInstance(requestId);

        getFragmentManager().beginTransaction().replace(mContainer.getId(), confirmCodeFragment).addToBackStack(null).commit();
    }

    @Override
    public void showAlreadyRegisteredDialog() {
        mAlreadyRegisteredDialog.show(getFragmentManager(), "already_registered");
    }
}