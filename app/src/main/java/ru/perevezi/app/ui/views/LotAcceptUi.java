/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.Map;

public interface LotAcceptUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void showLotAcceptForm(Map<String, Object> result);

    void openPaymentWebView();

    void finishLotConfirmation(boolean confirmed);

    interface UiCallbacks {
        void onCreate(int bidId);

        void onConfirm(int bidId);

        void onDecline(int bidId);

        void onPayLater(int bidId);

        void onPay();
    }

}
