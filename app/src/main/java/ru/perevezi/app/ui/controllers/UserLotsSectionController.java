/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.Constants;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.UserLotsSectionUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetUserLotsUseCase;

public class UserLotsSectionController implements Controller<UserLotsSectionUi>, UserLotsSectionUi.UiCallbacks {

    private final GetUserLotsUseCase mGetUserLotsUseCase;
    private final LotModelMapper mLotModelMapper;
    private final UserState mUserState;
    private UserLotsSectionUi mUi;

    @Inject
    public UserLotsSectionController(GetUserLotsUseCase getUserLotsUseCase,
                                     LotModelMapper lotModelMapper, UserState userState) {
        mGetUserLotsUseCase = getUserLotsUseCase;
        mLotModelMapper = lotModelMapper;
        mUserState = userState;
    }

    @Override
    public void initialize(UserLotsSectionUi myLotsUi) {
        mUi = myLotsUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {
        if (!mUi.isItemsFetched()) {
            fetchLots(0);
        }
    }

    @Override
    public void onPause() {

    }

    private void fetchLots(final int offset) {
        toggleProgress(true, offset);

        AccountModel accountModel = mUserState.getCurrentAccount();

        Map<String, Integer> argumentMap = new HashMap<>();
        argumentMap.put("user", accountModel.getExtId());
        argumentMap.put("status", mUi.getSection());
        argumentMap.put("limit", Constants.PEREVEZI_MY_LOTS_LIST_LIMIT);
        argumentMap.put("offset", offset);

        RxUseCase.callback(mGetUserLotsUseCase, argumentMap).register(new RxUseCase.AsyncCallback<List<PereveziLot>>() {


            @Override
            public void onResultOk(List<PereveziLot> result) {
                toggleProgress(false, offset);
                mUi.addItemsToList((List<LotModel>) mLotModelMapper.mapResponse(result));
            }

            @Override
            public void onError(Throwable throwable) {
                toggleProgress(false, offset);
                mUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onVisibleThresholdReached(int offset) {
        fetchLots(offset);
    }

    private void toggleProgress(boolean show, int offset) {
        if (offset == 0) {
            mUi.toggleOverlayProgress(show);
        } else {
            mUi.toggleToolbarProgress(show);
        }
    }
}
