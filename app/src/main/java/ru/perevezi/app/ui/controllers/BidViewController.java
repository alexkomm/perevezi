/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.BidViewUi;
import ru.perevezi.domain.interactor.impl.AcceptBidUseCase;

public class BidViewController implements Controller<BidViewUi>, BidViewUi.UiCallbacks {
    private final AcceptBidUseCase mUseCase;
    private BidViewUi mBidViewUi;

    @Inject
    public BidViewController(AcceptBidUseCase useCase) {
        mUseCase = useCase;
    }

    @Override
    public void initialize(BidViewUi bidViewUi) {
        mBidViewUi = bidViewUi;
        mBidViewUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onBidAcceptConfirmed(final BidModel bidModel) {
        mBidViewUi.toggleDialogProgress(true);

        Map<String, Object> acceptData = new HashMap<>();
        acceptData.put("bid_id", bidModel.getBid());
        acceptData.put("lot_id", bidModel.getNid());

        RxUseCase.callback(mUseCase, acceptData).register(new RxUseCase.AsyncCallback<Void>() {

            @Override
            public void onResultOk(Void result) {
                mBidViewUi.toggleDialogProgress(false);
                mBidViewUi.getRouterUi().navigateToLotScreen(bidModel.getNid());
            }

            @Override
            public void onError(Throwable throwable) {
                mBidViewUi.toggleDialogProgress(false);
                mBidViewUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }
}
