/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;

import javax.inject.Inject;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.state.UserState;

public class PhoneConfirmDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "phone_confirm_dialog";

    @Inject
    UserState mUserState;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setTitle(R.string.user_phone_confirm_title);

        final AccountModel account = mUserState.getCurrentAccount();

        if (account.getPhoneConfirmStatus() == AccountModel.PHONE_CONFIRM_STATUS_NOT_CONFIRMED) {
            adb.setMessage(R.string.user_phone_confirm_message);
        } else {
            adb.setMessage(R.string.user_phone_confirm_pending_message);
        }

        View contentView = getActivity().getLayoutInflater().inflate(R.layout
                .fragment_phone_confirm_dialog, null);

        Button button = (Button) contentView.findViewById(R.id.btn_goto_confirm);
        button.setOnClickListener(this);

        adb.setView(contentView);

        return adb.create();
    }

    @Override
    public void onClick(View v) {
        dismiss();

        FragmentManager fm = getFragmentManager();

        fm.beginTransaction().replace(R.id.container, UserPhoneConfirmFragment.newInstance(),
                UserPhoneConfirmFragment.TAG).addToBackStack(UserPhoneConfirmFragment.TAG).commit();
    }
}
