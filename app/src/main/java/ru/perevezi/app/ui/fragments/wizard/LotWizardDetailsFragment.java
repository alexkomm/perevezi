/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.anton46.collectionitempicker.CollectionPicker;
import com.anton46.collectionitempicker.Item;
import com.anton46.collectionitempicker.OnItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.app.model.wizard.DetailsPage;
import ru.perevezi.app.ui.components.NothingSelectedSpinnerAdapter;
import ru.perevezi.app.utils.DateUtils;
import ru.perevezi.app.utils.Strings;

public class LotWizardDetailsFragment extends AbstractWizardFragment {

    @InjectView(R.id.details_collection)
    CollectionPicker mDetailsPicker;

    @InjectView(R.id.sp_need_load)
    Spinner mLoadingOptionsSpinner;

    @InjectView(R.id.et_weight)
    EditText mWeightField;

    @InjectView(R.id.et_volume)
    EditText mVolumeField;

    @InjectView(R.id.et_comments)
    EditText mCommentsField;

    @InjectView(R.id.et_date)
    EditText mDateField;

    @InjectView(R.id.et_time)
    EditText mTimeField;

    private List<Map<String, String>> mLoadingOptions;
    private List<Item> mTagItems;

    private DatePickerDialog mDatePickerDialog;
    private TimePickerDialog mTimePickerDialog;

    private SimpleDateFormat mDateFormatter = new SimpleDateFormat(DateUtils.DATE_FORMAT, Locale.getDefault());
    private SimpleDateFormat mTimeFormatter = new SimpleDateFormat(DateUtils.TIME_FORMAT, Locale.getDefault());

    public static LotWizardDetailsFragment newInstance(String requestCode) {
        LotWizardDetailsFragment fragment = new LotWizardDetailsFragment();

        Bundle args = new Bundle();
        args.putString(ARG_KEY, requestCode);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final CategoryModel selectedCategory = mPageCallbacks.onGetSelectedCategory();
        final SettingsModel generalSettings = mPageCallbacks.onGetGeneralSettings();

        if (selectedCategory == null || generalSettings == null) {
            return;
        }

        mWeightField.setTag(DetailsPage.KEY_WEIGHT);
        mVolumeField.setTag(DetailsPage.KEY_VOLUME);
        mCommentsField.setTag(DetailsPage.KEY_COMMENTS);
        mDateField.setTag(DetailsPage.KEY_DATE);
        mTimeField.setTag(DetailsPage.KEY_TIME);

        final Map<String, String> detailsTags = selectedCategory.getSettings().getDetailsTags();
        mLoadingOptions = generalSettings.getLotLoadingOptions();
        final List<String> excludedFields = selectedCategory.getSettings().getExcludedFields();
        final List<String> requiredFields = selectedCategory.getSettings().getRequiredFields();
        boolean isTimeNeeded = selectedCategory.getSettings().isTimeNeeded();

        mDateField.setInputType(InputType.TYPE_NULL);
        mDateField.setOnClickListener(new DateAndTimeClickListener());
        mDateField.setFocusable(false);
        mDateField.setFocusableInTouchMode(false);

        mTimeField.setInputType(InputType.TYPE_NULL);
        mTimeField.setFocusable(false);
        mTimeField.setFocusableInTouchMode(false);
        mTimeField.setOnClickListener(new DateAndTimeClickListener());

        if (!isTimeNeeded) {
            mTimeField.setVisibility(View.GONE);
        }

        Calendar calendar = Calendar.getInstance();
        mDatePickerDialog = new DatePickerDialog(getActivity(), new ShippingDateOrTimeSetListener
                (), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        mTimePickerDialog = new TimePickerDialog(getActivity(), new ShippingDateOrTimeSetListener(), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);

        if (Strings.isEmpty(selectedCategory.getSettings().getTitleHint())) {
            mPage.getData().putBoolean(DetailsPage.KEY_COMMENTS_REQUIRED, true);
            mCommentsField.setHint(R.string.hint_comments_required);
        } else {
            mPage.getData().putBoolean(DetailsPage.KEY_COMMENTS_REQUIRED, false);
        }

        mPage.getData().putStringArrayList(DetailsPage.KEY_EXCLUDED_FIELDS, (ArrayList<String>) excludedFields);
        mPage.getData().putStringArrayList(DetailsPage.KEY_REQUIRED_FIELDS, (ArrayList<String>) requiredFields);

        if (detailsTags != null) {
            mTagItems = new ArrayList<>();

            ArrayList<String> savedTags = mPage.getData().getStringArrayList(DetailsPage.KEY_TAGS);

            for (Object o : detailsTags.entrySet()) {
                Map.Entry entry = (Map.Entry) o;
                boolean isSelected = savedTags != null && savedTags.contains(entry.getValue().toString());

                mTagItems.add(new Item(entry.getValue().toString(), entry.getKey().toString(), isSelected));
            }

            mDetailsPicker.setItems(mTagItems);
            mDetailsPicker.setVisibility(View.VISIBLE);
            mDetailsPicker.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onClick(Item item, int position) {
                    ArrayList<String> comments = mPage.getData().getStringArrayList(DetailsPage
                            .KEY_TAGS);

                    if (comments == null) {
                        comments = new ArrayList<>();
                    }

                    if (item.isSelected) {
                        comments.add(item.id);

                    } else {
                        comments.remove(item.id);
                    }

                    mPage.getData().putStringArrayList(DetailsPage.KEY_TAGS, comments);
                    mPage.notifyDataChanged();
                }
            });
        }

        if (mLoadingOptions != null) {

            SimpleAdapter loadingOptionsAdapter = new SimpleAdapter(getActivity(),
                    mLoadingOptions, android.R.layout.simple_spinner_dropdown_item,
                    new String[]{"lot"}, new int[]{android.R.id.text1});
            mLoadingOptionsSpinner.setTag(DetailsPage.KEY_NEED_LOAD);
            mLoadingOptionsSpinner.setAdapter(new NothingSelectedSpinnerAdapter(
                    loadingOptionsAdapter,
                    R.layout.spinner_row_nothing_selected_loading,
                    getActivity()
            ));
            mLoadingOptionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    Map<String, String> loadingItem = (Map<String, String>) parent.getItemAtPosition(position);

                    if (loadingItem != null) {
                        mPage.getData().putString(DetailsPage.KEY_NEED_LOAD_ID, loadingItem.get("id"));
                        mPage.getData().putString(DetailsPage.KEY_NEED_LOAD, loadingItem.get("value"));
                        mPage.notifyDataChanged();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    return;
                }
            });
        }

        for (String field : excludedFields) {
            final View excludedField = getView().findViewWithTag(field);

            if (excludedField != null) {
                excludedField.setVisibility(View.GONE);
            }
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mWeightField.addTextChangedListener(new SavingDataTextWatcher(mWeightField, mPage));
        mVolumeField.addTextChangedListener(new SavingDataTextWatcher(mVolumeField, mPage));
        mCommentsField.addTextChangedListener(new SavingDataTextWatcher(mCommentsField, mPage));
    }

    private void loadData() {
        Bundle data = mPage.getData();

        mWeightField.setText(data.getString(DetailsPage.KEY_WEIGHT));
        mVolumeField.setText(data.getString(DetailsPage.KEY_VOLUME));
        mCommentsField.setText(data.getString(DetailsPage.KEY_COMMENTS));

        String needLoadId = data.getString(DetailsPage.KEY_NEED_LOAD_ID);
        Calendar savedDate = (Calendar) data.getSerializable(DetailsPage.KEY_SHIP_DATE);

        if (mLoadingOptions != null) {
            if (needLoadId != null) {
                for (int i = 0; i < mLoadingOptions.size(); i++) {
                    Map<String, String> option = mLoadingOptions.get(i);

                    if (option.get("id").equals(needLoadId)) {
                        mLoadingOptionsSpinner.setSelection(i);
                        break;
                    }
                }
            } else {
                mLoadingOptionsSpinner.setSelection(0);
            }
        }

        if (savedDate != null) {
            mDateField.setText(mDateFormatter.format(savedDate.getTime()));

            boolean isTimeEntered = mPage.getData().getBoolean(DetailsPage.KEY_TIME_ENTERED, false);

            if (isTimeEntered) {
                mTimeField.setText(mTimeFormatter.format(savedDate.getTime()));
            } else {
                mTimeField.setText(null);
            }
        } else {
            mDateField.setText(null);
            mTimeField.setText(null);
        }
    }

    private class DateAndTimeClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (v == mDateField) {
                mDatePickerDialog.show();
            } else {
                mTimePickerDialog.show();
            }
        }
    }

    private class ShippingDateOrTimeSetListener implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = (Calendar)mPage.getData().getSerializable(DetailsPage.KEY_SHIP_DATE);

            if (calendar == null) {
                calendar = Calendar.getInstance();
                calendar.clear();
            }

            calendar.set(year, monthOfYear, dayOfMonth);

            mDateField.setText(mDateFormatter.format(calendar.getTime()));

            mPage.getData().putSerializable(DetailsPage.KEY_SHIP_DATE, calendar);
            mPage.notifyDataChanged();

        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calendar = (Calendar)mPage.getData().getSerializable(DetailsPage.KEY_SHIP_DATE);

            if (calendar == null) {
                calendar = Calendar.getInstance();
                calendar.clear();
            }

            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);

            mPage.getData().putSerializable(DetailsPage.KEY_SHIP_DATE, calendar);
            mPage.getData().putBoolean(DetailsPage.KEY_TIME_ENTERED, true);
            mPage.notifyDataChanged();

            mTimeField.setText(mTimeFormatter.format(calendar.getTime()));
        }
    }

    @Override
    protected String getPageTitleResId() {
        return getResources().getString(R.string.lot_wizard_title_details);
    }
}
