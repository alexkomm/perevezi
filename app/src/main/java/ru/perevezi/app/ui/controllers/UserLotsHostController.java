/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.UserLotsHostUi;
import ru.perevezi.domain.interactor.impl.GetUserLotsStatsUseCase;
import ru.perevezi.app.state.UserState;

public class UserLotsHostController implements Controller<UserLotsHostUi>, UserLotsHostUi.UiCallbacks {


    private final GetUserLotsStatsUseCase mUseCase;
    private final UserState mUserState;
    private UserLotsHostUi mUi;

    @Inject
    public UserLotsHostController(GetUserLotsStatsUseCase useCase, UserState userState) {
        mUseCase = useCase;
        mUserState = userState;
    }

    @Override
    public void initialize(UserLotsHostUi userLotsHostUi) {
        mUi = userLotsHostUi;
        mUi.setUiCallbacks(this);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void fetchLotStats() {
        mUi.toggleOverlayProgress(true);

        AccountModel accountModel = mUserState.getCurrentAccount();

        RxUseCase.callback(mUseCase, accountModel.getExtId()).register(new RxUseCase.AsyncCallback<ArrayList<Map<String, Integer>>>() {

            @Override
            public void onResultOk(ArrayList<Map<String, Integer>> result) {
                mUi.initViewPager(result);
                mUi.toggleOverlayProgress(false);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleOverlayProgress(false);
            }
        });
    }
}
