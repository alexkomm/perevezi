/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import org.parceler.Parcels;

import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;

public class LotCompletionConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String ARG_KEY_ACCOUNT = "account";
    private CompletionConfirmDialogListener mCallbacks;
    private AccountModel mProfileInfo;

    public static LotCompletionConfirmDialog newInstance(AccountModel accountModel) {
        LotCompletionConfirmDialog fragment = new LotCompletionConfirmDialog();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_ACCOUNT, Parcels.wrap(accountModel));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null) {
            mProfileInfo = Parcels.unwrap(arguments.getParcelable(ARG_KEY_ACCOUNT));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean isVendor = mProfileInfo.isVendor();

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.confirm_completion_label)
                .setPositiveButton(isVendor ? R.string.yes : R.string.confirm_completion_positive, this)
                .setNegativeButton(isVendor ? R.string.no : R.string.confirm_completion_negative, this)
                .setMessage(isVendor ? R.string.vendor_confirm_completion_text : R.string.confirm_completion_text);

        return adb.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallbacks = (CompletionConfirmDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CompletionConfirmDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mCallbacks = null;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                mCallbacks.onCompletionConfirmed();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                if (mProfileInfo.isVendor()) {
                    mCallbacks.onCompletionCanceled();
                } else {
                    mCallbacks.onCompletionDeclined();
                }
                break;
        }
    }

    public interface CompletionConfirmDialogListener {
        void onCompletionConfirmed();

        void onCompletionDeclined();

        void onCompletionCanceled();
    }
}
