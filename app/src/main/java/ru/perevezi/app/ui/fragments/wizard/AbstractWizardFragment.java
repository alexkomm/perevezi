/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.wizard;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.tech.freak.wizardpager.model.Page;

import java.util.List;

import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.wizard.PageCallbacks;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.domain.utils.Preconditions;

public abstract class AbstractWizardFragment extends BaseFragment {


    protected static final String ARG_KEY = "key";

    protected String mKey;
    protected Page mPage;
    protected PageCallbacks mPageCallbacks;
    protected List<CategoryModel> mCategoryList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);

        mPage = mPageCallbacks.onGetPage(mKey);

        Preconditions.checkState(mPage != null, "page cannot be null");

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCategoryList = mPageCallbacks.onGetCategoryList();
        Preconditions.checkState(mCategoryList != null, "category list cannot be null");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            setToolbarTitle(getPageTitleResId());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageCallbacks)) {
            throw new ClassCastException("Activity must implement PageCallbacks");
        }

        mPageCallbacks = (PageCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPageCallbacks = null;
    }

    protected abstract String getPageTitleResId();
}
