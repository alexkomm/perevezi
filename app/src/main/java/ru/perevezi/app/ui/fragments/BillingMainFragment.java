/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.events.AccountChangedEvent;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.BillingCard;
import ru.perevezi.app.ui.controllers.BillingController;
import ru.perevezi.app.ui.views.BillingUi;
import rx.android.app.AppObservable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class BillingMainFragment extends BaseFragment implements BillingUi {

    @Inject
    BillingController mBillingController;

    @Inject
    UserState mUserState;

    @Inject
    RxBus mBus;

    @InjectView(R.id.balance_card)
    CardViewNative mBalanceCard;

    @InjectView(R.id.pro_card)
    CardViewNative mProCard;

    @InjectView(R.id.sms_card)
    CardViewNative mSmsCard;

    CompositeSubscription mCompositeSubscription;
    private BillingCard mBalanceCardModel;
    private UiCallbacks mUiCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        mCompositeSubscription = new CompositeSubscription();

        mCompositeSubscription.add(
                AppObservable.bindFragment(this, mBus.toObserverable()).subscribe(new Action1<Object>() {

                    @Override
                    public void call(Object o) {
                        if (o instanceof AccountChangedEvent) {
                            if (mBalanceCardModel != null) {
                                AccountModel account = mUserState.getCurrentAccount();

                                mBalanceCardModel.setUserBalance(account.getBalance().doubleValue());
                            }
                        }
                    }
                })
        );

        mBillingController.initialize(this);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billing_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBalanceCardModel = new BillingCard(getActivity(), mUserState.getCurrentAccount(), BillingCard.Type.BALANCE);
        mBalanceCard.setCard(mBalanceCardModel);

        Card proCard = new BillingCard(getActivity(), mUserState.getCurrentAccount(), BillingCard
                .Type.PRO);
        mProCard.setCard(proCard);

        Card smsCard = new BillingCard(getActivity(), mUserState.getCurrentAccount(), BillingCard
                .Type.SMS);
        mSmsCard.setCard(smsCard);

        if (savedInstanceState == null) {
            mUiCallbacks.onUpdateCredentials();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        mCompositeSubscription.unsubscribe();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {

        mUiCallbacks = uiCallbacks;
    }
}
