/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.lot;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.events.RefreshDataEvent;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotBidCard;
import ru.perevezi.app.ui.components.CardArraySectionedRecyclerViewAdapter;
import ru.perevezi.app.ui.controllers.LotBidsController;
import ru.perevezi.app.ui.views.LotBidsUi;
import ru.perevezi.domain.utils.Preconditions;
import rx.android.app.AppObservable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class LotBidsFragment extends BaseFragment implements LotBidsUi {

    @Inject
    LotBidsController mLotBidsController;

    @Inject
    RxBus mBus;

    @InjectView(R.id.no_bids_text)
    TextView mNoBidsTextView;

    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private int mLotId;

    private BidModel mWonBid;

    private CardArraySectionedRecyclerViewAdapter mCardArrayAdapter;
    private CardRecyclerView mRecyclerView;
    private List<BidModel> mBids;
    private LotHostListener mLotHostController;

    private CompositeSubscription mSubscription;
    private UiCallbacks mUiCallbacks;

    public static LotBidsFragment newInstance(int page, int lotId) {
        LotBidsFragment fragment = new LotBidsFragment();

        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putInt("lot_id", lotId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle args = getArguments();
        mLotId = args.getInt("lot_id");

        if (savedInstanceState != null) {
            mBids = Parcels.unwrap(savedInstanceState.getParcelable("bids"));
        }

        PereveziApplication.from(getActivity()).inject(this);

        mSubscription = new CompositeSubscription();
        mSubscription.add(
                AppObservable.bindFragment(this, mBus.toObserverable()).subscribe(new Action1<Object>() {

                    @Override
                    public void call(Object o) {
                        if (o instanceof RefreshDataEvent) {
                            mUiCallbacks.onDataRefresh();
                        }
                    }
                })
        );

        mLotBidsController.initialize(this);
        registerUiLifecycleObserver(mLotBidsController);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_bid_list, container, false);
    }

    @Override
    public void onPause() {
        if (mSwipeRefreshLayout!= null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mLotHostController.toggleLotActionView(true);

        setToolbarTitle(getResources().getString(R.string.lot_screen_title, mLotId));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LotHostController");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onDestroy() {
        mSubscription.unsubscribe();

        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArraySectionedRecyclerViewAdapter(getActivity(), R.layout.view_recyclerview_section, R.id.section_text, cards);

        //Staggered grid view
        mRecyclerView = (CardRecyclerView) getActivity().findViewById(R.id.bid_list_recyclerview);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mCardArrayAdapter);
        }

        if (mBids != null) {
            updateCardAdapter(mBids);
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mUiCallbacks.onDataRefresh();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("bids", Parcels.wrap(mBids));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public int getLotId() {
        return mLotId;
    }

    @Override
    public List<BidModel> getBidsList() {
        return mBids;
    }

    private void updateCardAdapter(List<BidModel> bidModelList) {
        mLotHostController.onBidListLoaded(mBids);

        if (mBids == null || mBids.size() == 0) {
            mCardArrayAdapter.clear();
            mNoBidsTextView.setVisibility(View.VISIBLE);
            return;
        } else {
            mNoBidsTextView.setVisibility(View.GONE);
        }

        ArrayList<Card> cards = new ArrayList<>();
        ArrayList<Card> mismatchBidCards = new ArrayList<>();

        for (BidModel bid : bidModelList) {
            if (bid.getStatus() == BidModel.BID_STATUS_WON) {
                mLotHostController.setWonBid(true);
            }

            LotBidCard card = new LotBidCard(getActivity(), bid);

            if (isBidMatchRequirements(bid.getRequirementsInfo())) {
                cards.add(card);
            } else {
                mismatchBidCards.add(card);
            }
        }

        if (mismatchBidCards.size() > 0) {
            CardArraySectionedRecyclerViewAdapter.Section[] sections = {new
                    CardArraySectionedRecyclerViewAdapter.Section(cards.size(),
                    getString(R.string.mismatch_bids_label))};

            mCardArrayAdapter.setSections(sections);

            cards.addAll(mismatchBidCards);
        }

        mCardArrayAdapter.setCards(cards);
        mCardArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void addBidsToList(List<BidModel> bidModelList) {
        Preconditions.checkState(bidModelList != null, "bid model list cannot be null");
        mBids = bidModelList;

        updateCardAdapter(bidModelList);

        mSwipeRefreshLayout.setRefreshing(false);
    }

    private boolean isBidMatchRequirements(Map<String, Boolean> requirementsInfo) {
        for (Map.Entry<String, Boolean> entry : requirementsInfo.entrySet()) {
            if (!entry.getValue()) {
                return false;
            }
        }

        return true;
    }

}
