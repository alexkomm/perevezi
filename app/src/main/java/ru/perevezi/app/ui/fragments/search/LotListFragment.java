/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.search;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.LotListCard;
import ru.perevezi.app.ui.components.CardArrayEmptyViewRecyclerViewAdapter;
import ru.perevezi.app.ui.components.DynamicLoadingListener;
import ru.perevezi.app.ui.controllers.LotListController;
import ru.perevezi.app.ui.views.LotListUi;
import ru.perevezi.data.cache.LotCache;
import ru.perevezi.domain.utils.PereveziPreferences;
import ru.perevezi.domain.utils.Preconditions;

public class LotListFragment extends BaseFragment implements LotListUi {

    public static final String TAG = "lot_list_fragment";

    public static final String ARG_SEARCH_FILTERS = "options";

    public static final String ARG_LOT_IDS = "lot_ids";

    public static final String ARG_DYN_LOADING = "dyn_loading";

    private static final int SEARCH_DIALOG = 1;

    @Inject
    LotListController mLotListController;

    @Inject
    LotCache mLotCache;

    @Inject
    PereveziPreferences mPreferences;

    @InjectView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private UiCallbacks mUiCallbacks;
    private CardArrayRecyclerViewAdapter mCardArrayAdapter;
    private CardRecyclerView mRecyclerView;

    private List<LotModel> mLots;

    private HashMap<String, Object> mFilterOptions = new HashMap<>();
    private DynamicLoadingListener mDynamicLoadingListener;

    private DialogFragment mSearchDialog;

    private boolean mDynamicLoadingEnabled;

    public static LotListFragment newInstance(HashMap<String, Object> options) {
        LotListFragment fragment = new LotListFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_SEARCH_FILTERS, options);

        fragment.setArguments(args);

        return fragment;
    }

    public static LotListFragment newInstance(String[] lotIds) {
        LotListFragment fragment = new LotListFragment();

        Bundle args = new Bundle();
        args.putStringArray(ARG_LOT_IDS, lotIds);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();
        if (args != null) {

            if (args.containsKey(ARG_SEARCH_FILTERS)) {
                mFilterOptions = (HashMap<String, Object>) args.getSerializable(ARG_SEARCH_FILTERS);

                if (mFilterOptions != null) {
                    mFilterOptions.put("offset", "0");
                }

            }
        }

        if (mFilterOptions != null && mFilterOptions.isEmpty() && mLotCache.getFilter() != null) {
            mFilterOptions = mLotCache.getFilter();
        }

        if (savedInstanceState != null) {
            mLots = Parcels.unwrap(savedInstanceState.getParcelable("lots"));
            mDynamicLoadingEnabled = savedInstanceState.getBoolean(ARG_DYN_LOADING);
        }

        mSearchDialog = new SearchByIdFragment();
        mSearchDialog.setTargetFragment(this, SEARCH_DIALOG);

        mLotListController.initialize(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setToolbarTitle(R.string.lot_search_title);

        ArrayList<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArrayEmptyViewRecyclerViewAdapter(getContext(), cards);

        //Staggered grid view
        mRecyclerView = (CardRecyclerView) getView().findViewById(R.id.card_list);

        //Set the empty view
        if (mRecyclerView != null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

            mRecyclerView.setHasFixedSize(false);
            mRecyclerView.setLayoutManager(layoutManager);
            mRecyclerView.setAdapter(mCardArrayAdapter);

            mDynamicLoadingListener = new DynamicLoadingListener(mRecyclerView, layoutManager,
                    new DynamicLoadingListener.OnThresholdReachedListener() {
                        @Override
                        public void onThresholdReached(int totalCount) {
                            mUiCallbacks.onVisibleThresholdReached(totalCount);
                        }
                    });
        }

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mUiCallbacks.onRefresh(true);
            }
        });

        if (mLots == null) {
            Bundle args = getArguments();

            if (args != null && args.containsKey(ARG_LOT_IDS)) {
                final String[] lotIds = args.getStringArray(ARG_LOT_IDS);
                mUiCallbacks.onSearchLotsByIds(lotIds);
                mPreferences.setNewLots(null);
            } else {
                mUiCallbacks.onSearchLotsByFilters(false, false);
            }

        } else {
            setDynamicLoadingEnabled(mDynamicLoadingEnabled);
            addLotsToList(mLots, true);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SEARCH_DIALOG:
                if (resultCode == Activity.RESULT_OK) {
                    String lotId = data.getStringExtra(SearchByIdFragment.EXTRA_LOT_ID);
                    mUiCallbacks.onSearchLotsByIds(new String[] { lotId });
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSwipeRefreshLayout!= null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleBackArrow(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("lots", Parcels.wrap(mLots));
        outState.putBoolean(ARG_DYN_LOADING, mDynamicLoadingEnabled);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lot_search_activity, menu);

        MenuItem refreshItem = menu.findItem(R.id.action_refresh);

        if (refreshItem != null) {
            refreshItem.setIcon(new IconicsDrawable(getActivity()).color(Color.WHITE).actionBar()
                    .icon(FontAwesome.Icon.faw_refresh));
        }

        MenuItem filterItem = menu.findItem(R.id.action_filter);
        if (filterItem != null) {
            filterItem.setIcon(new IconicsDrawable(getActivity()).color(Color.WHITE).actionBar()
                    .icon(FontAwesome.Icon.faw_filter));
        }

        menu.removeItem(BaseActivity.MENU_SEARCH);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mUiCallbacks.onRefresh(false);
                return true;
            case R.id.action_filter:
                navigateToFilterScreen();
                return true;
            case R.id.action_search_by_id:
                mSearchDialog.show(getActivity().getSupportFragmentManager(), "search_dialog");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void navigateToFilterScreen() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        LotFilterFragment lotFilterFragment = LotFilterFragment.newInstance(mFilterOptions);
        fm.beginTransaction().replace(R.id.container, lotFilterFragment,
                LotFilterFragment.TAG).addToBackStack(LotFilterFragment.TAG).commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    private void updateCardAdapter(List<LotModel> lotModelList, boolean clear) {
        ArrayList<Card> cards = new ArrayList<>();
        long cachedUpdatedTimestamp = 0;


        for (LotModel lot : lotModelList) {
            // Получаем дату обновления кеша из любого заказа
            cachedUpdatedTimestamp = lot.getCacheUpdatedTimestamp();

            LotListCard card = new LotListCard(getActivity(), lot, true);
            card.setShowMarkStatus(true);
            cards.add(card);
        }

        if (cachedUpdatedTimestamp > 0) {
            setDynamicLoadingEnabled(false);
            mUiCallbacks.onRefresh(false);
        }

        if (clear) {
            mCardArrayAdapter.clear();
        }

        mCardArrayAdapter.addAll(cards);
    }

    @Override
    public void addLotsToList(List<LotModel> lotModelList, boolean clear) {
        Preconditions.checkState(lotModelList != null, "lot model list cannot be null");

        if (clear) {
            mLots = lotModelList;
        } else {
            if (mLots == null) {
                mLots = new ArrayList<>();
            }

            mLots.addAll(lotModelList);
        }

        updateCardAdapter(lotModelList, clear);

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setDynamicLoadingEnabled(boolean dynamicLoadingEnabled) {
        if (mRecyclerView == null || mDynamicLoadingListener == null && mDynamicLoadingEnabled == dynamicLoadingEnabled) {
            return;
        }

        mDynamicLoadingEnabled = dynamicLoadingEnabled;

        if (dynamicLoadingEnabled) {
            mRecyclerView.addOnScrollListener(mDynamicLoadingListener);
        } else {
            mRecyclerView.removeOnScrollListener(mDynamicLoadingListener);
        }
    }

    @Override
    public HashMap<String, Object> getFilterOptions() {
        return mFilterOptions;
    }
}
