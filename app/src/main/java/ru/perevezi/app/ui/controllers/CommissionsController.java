/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.views.CommissionsUi;
import ru.perevezi.domain.interactor.impl.GetUserCommissionsUseCase;

public class CommissionsController implements Controller<CommissionsUi> {

    private final GetUserCommissionsUseCase mUseCase;
    private final UserState mUserState;
    private CommissionsUi mCommissionsUi;

    @Inject
    public CommissionsController(GetUserCommissionsUseCase useCase, UserState userState) {
        mUseCase = useCase;
        mUserState = userState;
    }

    @Override
    public void initialize(CommissionsUi commissionsUi) {

        mCommissionsUi = commissionsUi;
    }

    private void fetchUserCommissions() {
        mCommissionsUi.toggleOverlayProgress(true);

        AccountModel account = mUserState.getCurrentAccount();

        RxUseCase.callback(mUseCase, account.getExtId()).register(new RxUseCase.AsyncCallback<Map<String, Object>>() {


            @Override
            public void onResultOk(Map<String, Object> data) {
                mCommissionsUi.toggleOverlayProgress(false);
                mCommissionsUi.addCommissionsToList((HashMap<String, Object>) data);
            }

            @Override
            public void onError(Throwable throwable) {
                mCommissionsUi.toggleOverlayProgress(false);
                mCommissionsUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {
        if (mCommissionsUi.getCommissionsData() == null) {
            fetchUserCommissions();
        }
    }

    @Override
    public void onPause() {

    }
}
