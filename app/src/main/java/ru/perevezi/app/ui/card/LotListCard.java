/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.fragments.LotRouteViewFragment;
import ru.perevezi.app.ui.fragments.NewReviewFragment;
import ru.perevezi.app.utils.LocationUtils;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.app.utils.ViewUtils;

public class LotListCard extends Card {
    private final LotModel mLot;
    private final boolean mWithActions;

    private boolean mShowMarkStatus = false;

    public LotListCard(Context context, LotModel lot, boolean withActions) {
        this(context, R.layout.card_lot_list, lot, withActions);
    }

    public LotListCard(Context context, int innerLayout, LotModel lot, boolean withActions) {
        super(context, innerLayout);
        mLot = lot;
        mWithActions = withActions;

        init();
    }

    private void init() {
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                Intent intent = new Intent(mContext, LotActivity.class);
                intent.putExtra(LotActivity.EXTRA_ID, mLot.getNid());
                mContext.startActivity(intent);
            }
        });
    }

    public boolean isShowMarkStatus() {
        return mShowMarkStatus;
    }

    public void setShowMarkStatus(boolean showMarkStatus) {
        mShowMarkStatus = showMarkStatus;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        TextView lotTitleText = (TextView) parent.findViewById(R.id.lot_title_text);
        lotTitleText.setText(mLot.getTitle());

        if (isShowMarkStatus() && mLot.getMark() != LotModel.LOT_MARK_NEW) {
            lotTitleText.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_text));
        } else {
            lotTitleText.setTextColor(ContextCompat.getColor(mContext, R.color.primary_text));
        }

        TextView lotChangedText = (TextView) parent.findViewById(R.id.lot_changed_text);

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

        if (fmt.format(mLot.getCreatedDate().getTimeInMillis
                ()).equals(fmt.format(System.currentTimeMillis()))) {
            fmt.applyPattern("kk:mm");
        } else {
            fmt.applyPattern("dd.MM");
        }

        lotChangedText.setText(fmt.format(mLot.getCreatedDate().getTimeInMillis()));

        TextView lotIdText = (TextView) parent.findViewById(R.id.lot_id_text);
        lotIdText.setText(Integer.toString(mLot.getNid()));

        CategoryModel categoryModel = mLot
                .getCategoryModel();

        if (categoryModel != null) {
            int iconRes = mContext.getResources().getIdentifier("pic_category_" + categoryModel.getDrawableResName() + "_32dp", "drawable", mContext
                    .getPackageName());

            if (iconRes > 0) {
                lotIdText.setCompoundDrawablesWithIntrinsicBounds(0, iconRes, 0, 0);
            }
        }

        TextView lotWeightText = (TextView) parent.findViewById(R.id.lot_weight_text);
        TextView lotVolumeText = (TextView) parent.findViewById(R.id.lot_volume_text);

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setDecimalSeparatorAlwaysShown(false);

        if (mLot.getWeight() > 0) {
            int weightUnitStringResId = StringManager.getWeightUnitStringResId(mLot.getWeightUnit
                    ());

            String weightUnit = mContext.getResources().getString(weightUnitStringResId);
            String weightStr = mContext.getResources().getString(R.string.lot_list_weight_label,
                    df.format(mLot.getWeight()), weightUnit);

            lotWeightText.setText(weightStr);
            lotWeightText.setVisibility(View.VISIBLE);
        } else {
            lotWeightText.setVisibility(View.GONE);
        }

        if (mLot.getVolume() > 0) {
            String volumeStr = mContext.getResources().getString(R.string.lot_list_volume_label,
                    df.format(mLot.getVolume()));

            lotVolumeText.setText(volumeStr);
            lotVolumeText.setVisibility(View.VISIBLE);
        } else {
            lotVolumeText.setVisibility(View.GONE);
        }

        TextView fromPointText = (TextView) parent.findViewById(R.id.from_point_text);
        TextView toPointText = (TextView) parent.findViewById(R.id.to_point_text);

        final ArrayList<Address> routePoints = (ArrayList<Address>) mLot.getRoutePoints();

        final List<CharSequence> formattedAddressList = LocationUtils.formatRouteItems(routePoints);

        if (formattedAddressList != null) {
            CharSequence fromPointString = formattedAddressList.get(0);

            CharSequence toPointString = formattedAddressList.get(formattedAddressList.size() - 1);

            fromPointText.setText(fromPointString);
            toPointText.setText(toPointString);
        }


        FloatingActionButton routeViewFab = (FloatingActionButton) parent.findViewById(R.id
                .route_view_fab);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) routeViewFab.getLayoutParams();
            params.setMargins(0, 0, 0, 0); // get rid of margins since shadow area is now the margin
            routeViewFab.setLayoutParams(params);
        }

        routeViewFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();

                LotRouteViewFragment lotRouteViewFragment = LotRouteViewFragment.newInstance
                        (routePoints);

                fm.beginTransaction()
                        .replace(R.id.container, lotRouteViewFragment, LotRouteViewFragment.TAG)
                        .addToBackStack(LotRouteViewFragment.TAG)
                        .commit();
            }
        });

        TextView shipDateView = (TextView) parent.findViewById(R.id.lot_ship_date_text);

        String shippingInfo = mLot.formatShippingDate(mContext);

        if (shippingInfo == null) {
            shippingInfo = mContext.getResources().getString(R.string.lot_view_flexible_date);
        }

        shipDateView.setText(shippingInfo);

        TextView lotBidCountText = (TextView) parent.findViewById(R.id.lot_bid_count_value);
        TextView lotStatusText = (TextView) parent.findViewById(R.id.lot_status_text);

        String lotStatusString = "";

        if (mLot.getCountBids() > 0) {
            lotBidCountText.setText(Integer.toString(mLot.getCountBids()));
            lotBidCountText.setVisibility(View.VISIBLE);

            lotStatusText.setVisibility(View.VISIBLE);
            lotStatusString = mContext.getResources().getQuantityString(R.plurals.bids_no_count,
                    mLot.getCountBids());

            lotStatusText.setText(lotStatusString);
        } else {
            lotBidCountText.setVisibility(View.GONE);
            lotStatusText.setVisibility(View.GONE);
        }

        if (!mLot.isPro()) {
            parent.findViewById(R.id.lot_list_pro_label).setVisibility(View.GONE);
        } else {
            parent.findViewById(R.id.lot_list_pro_label).setVisibility(View.VISIBLE);
        }

        if (mWithActions) {
            if (mLot.getActionCode() == LotModel.LOT_ACTION_PROVIDER_NEED_VOTE || mLot.getActionCode
                    () == LotModel.LOT_ACTION_CLIENT_NEED_VOTE) {

                Button lotVoteButton = (Button) parent.findViewById(R.id.lot_vote_btn);
                lotVoteButton.setVisibility(View.VISIBLE);

                lotVoteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fm = ((AppCompatActivity) mContext)
                                .getSupportFragmentManager();

                        NewReviewFragment newReviewFragment = NewReviewFragment.newInstance(mLot);

                        fm.beginTransaction()
                                .replace(R.id.container, newReviewFragment, NewReviewFragment.TAG)
                                .addToBackStack(NewReviewFragment.TAG)
                                .commit();
                    }
                });
            }
        }

        if (mLot.getReview() != null) {

            ViewStub reviewStub = (ViewStub) parent.findViewById(R.id.review_stub);

            if (reviewStub != null) {
                reviewStub.setVisibility(View.VISIBLE);
            }

            parent.findViewById(R.id.lot_bidding_group).setVisibility(View.GONE);
            parent.findViewById(R.id.lot_details_group).setVisibility(View.GONE);

            TextView reviewCommentTextView = (TextView) parent.findViewById(R.id.review_comment);
            TextView reviewAuthorTime = (TextView) parent.findViewById(R.id.review_author_time);

            long reviewTimeMillis = Long.parseLong(mLot.getReview().get("time")) * 1000;

            String reviewTimeFormatted = DateUtils.formatDateTime(mContext,
                    reviewTimeMillis, (DateUtils.FORMAT_SHOW_DATE |
                            DateUtils.FORMAT_SHOW_WEEKDAY));

            reviewCommentTextView.setText(mLot.getReview().get("comment"));

            String reviewAuthorTimeStr = "";

            if (!Strings.isNullOrEmpty(mLot.getReview().get("author"))) {
                reviewAuthorTimeStr = mLot.getReview().get("author") + " - ";
            }

            reviewAuthorTimeStr += reviewTimeFormatted;

            reviewAuthorTime.setText(reviewAuthorTimeStr);
        }

        ViewUtils.inflateLotContactsAndBidView(mLot, parent, mContext);
    }

    public long getCachedTimestamp() {
        return mLot.getCacheUpdatedTimestamp();
    }
}
