/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.services.GeocoderIntentService;
import ru.perevezi.app.utils.LocationUtils;
import ru.perevezi.app.utils.Strings;
import ru.perevezi.domain.utils.Preconditions;

public class LotRouteViewFragment extends BaseFragment implements RoutingListener {

    private static final String KEY_ROUTE_POINTS = "route_points";

    public static final String TAG = "lot_route_view_fragment";

    private static final String ACTION_GEOCODE_INPUT = "GEOCODE_INPUT";

    private static final String EXTRA_ROUTE_POINT_INDEX = "route_point_index";

    private GoogleMap mMap;

    @InjectView(R.id.map)
    MapView mMapView;

    @InjectView(R.id.et_routing_progress)
    TextView mRouteProgress;

    @InjectView(R.id.distance)
    TextView mDistanceText;

    @InjectView(R.id.time)
    TextView mTimeText;

    @InjectView(R.id.route_info_group)
    ViewGroup mRouteInfoGroup;

    private ArrayList<Address> mRoutePointsList;

    private GeocodeResultReceiver mGeocodeResultReceiver;

    private Map<Integer, Marker> mMarkerList = new LinkedHashMap<>();

    public static LotRouteViewFragment newInstance(ArrayList<Address> routePoints) {

        LotRouteViewFragment lotRouteViewFragment = new LotRouteViewFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(KEY_ROUTE_POINTS, routePoints);

        lotRouteViewFragment.setArguments(arguments);

        return lotRouteViewFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null) {
            mRoutePointsList = args.getParcelableArrayList(KEY_ROUTE_POINTS);
        }

        Preconditions.checkState(mRoutePointsList != null, "Route points list cannot be null");

        Handler geocodeHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                final Bundle data = msg.getData();

                if (data == null) {
                    return;
                }

                final int resultCode = data.getInt(GeocoderIntentService.RESULT_STATUS_KEY);
                final String message = data.getString(GeocoderIntentService.RESULT_MESSAGE_KEY);

                Address address = data.getParcelable(GeocoderIntentService.RESULT_DATA_KEY);

                final Bundle intentExtras = data.getBundle(GeocoderIntentService
                        .RESULT_INTENT_EXTRAS);

                if (resultCode == GeocoderIntentService.SUCCESS_RESULT) {

                    int routePointIndex = intentExtras.getInt(EXTRA_ROUTE_POINT_INDEX);

                    mRoutePointsList.get(routePointIndex).setLatitude(address.getLatitude());
                    mRoutePointsList.get(routePointIndex).setLongitude(address.getLongitude());
                } else {
                    toggleOverlayProgress(false);
                    onRoutingCancelled();
                }

                if (message != null) {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }

                if (isAllRoutePointsGeocoded()) {
                    renderRouteOnMap();
                }
            }
        };

        mGeocodeResultReceiver = new GeocodeResultReceiver(geocodeHandler);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_route_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setToolbarTitle(R.string.lot_route_view_screen_title);

        final List<CharSequence> formattedAddressList = LocationUtils.formatRouteItems(mRoutePointsList);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(Strings.join(" -> " +
                "", formattedAddressList));

        mMapView.onCreate(savedInstanceState);

        initMap(savedInstanceState);
    }

    private void initMap(Bundle savedInstanceState) {
        toggleOverlayProgress(true);

        mMap = mMapView.getMap();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(false);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                loadRoute();
            }
        });

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(getActivity());
    }

    private void loadRoute() {
        for (int i = 0; i < mRoutePointsList.size(); i++) {

            Address routePoint = mRoutePointsList.get(i);
            boolean withLatLng = routePoint.hasLatitude() && routePoint.hasLongitude();

            if (!withLatLng) {
                geocodeRoutePoint(routePoint, i);
            }
        }

        if (isAllRoutePointsGeocoded()) {
            renderRouteOnMap();
        }
    }

    private void renderRouteOnMap() {
        toggleOverlayProgress(false);

        List<LatLng> latLngs = new ArrayList<>(mRoutePointsList.size());

        for (Address routePoint : mRoutePointsList) {
            latLngs.add(new LatLng(routePoint.getLatitude(), routePoint.getLongitude()));
        }

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(latLngs)
                .build();

        routing.execute();

        setMapMarkers();
    }

    private void setMapMarkers() {
        Preconditions.checkState(mMap != null, "map cannot be null");

        for (int i = 0; i < mRoutePointsList.size(); i++) {

            Address routePoint = mRoutePointsList.get(i);
            float bitMapHue;

            if (i == 0) {
                bitMapHue = BitmapDescriptorFactory.HUE_GREEN;
            } else if (i == mRoutePointsList.size() - 1) {
                bitMapHue = BitmapDescriptorFactory.HUE_RED;
            } else {
                bitMapHue = BitmapDescriptorFactory.HUE_BLUE;
            }

            BitmapDescriptor markerIcon = BitmapDescriptorFactory
                    .defaultMarker(bitMapHue);

            final Marker newMarker = mMap.addMarker(new MarkerOptions().position(new LatLng
                    (routePoint.getLatitude(),
                    routePoint.getLongitude())).icon(markerIcon).title(LocationUtils.formatAddress(routePoint)).draggable(false));

            if (mMarkerList.get(i) != null) {
                mMarkerList.get(i).remove();
            }

            mMarkerList.put(i, newMarker);
        }

        animateCameraToFitMarkers();
    }

    private void animateCameraToFitMarkers() {
        if (mMarkerList.size() == 0) {
            return;
        }

        if (mMarkerList.size() > 1) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : mMarkerList.values()) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            mMap.moveCamera(cameraUpdate);
        } else {
            Marker marker = (Marker) mMarkerList.values().toArray()[0];
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 15);

            mMap.moveCamera(cameraUpdate);
        }
    }

    private boolean isAllRoutePointsGeocoded() {
        for (Address routePoint : mRoutePointsList) {
            if (!routePoint.hasLatitude() || !routePoint.hasLongitude()) {
                return false;
            }
        }

        return true;
    }

    private void geocodeRoutePoint(Address routePoint, int index) {
        Intent intent = new Intent(getActivity(), GeocoderIntentService.class);
        intent.putExtra(GeocoderIntentService.EXTRA_RECEIVER, mGeocodeResultReceiver);
        intent.putExtra(GeocoderIntentService.EXTRA_LOCATION_NAME, LocationUtils.formatAddress(routePoint));

        intent.putExtra(EXTRA_ROUTE_POINT_INDEX, index);

        intent.setAction(ACTION_GEOCODE_INPUT);

        getActivity().startService(intent);
    }

    @Override
    public void onRoutingFailure() {
        mRouteProgress.setVisibility(View.GONE);

        Toast.makeText(getActivity(), R.string.error_routing, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {
        toggleOverlayProgress(false);

        mRouteProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        mRouteProgress.setVisibility(View.GONE);

        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(10);
        polyoptions.addAll(mPolyOptions.getPoints());
        mMap.addPolyline(polyoptions);

        mDistanceText.setText(route.getDistanceText());
        mTimeText.setText(route.getDurationText());
        mRouteInfoGroup.setVisibility(View.VISIBLE);
    }


    @Override
    public void onRoutingCancelled() {
        mRouteProgress.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleBackArrow(true);
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(null);
        toggleOverlayProgress(false);
        toggleBackArrow(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    private class GeocodeResultReceiver extends ResultReceiver {


        private final Handler mHandler;


        public GeocodeResultReceiver(Handler handler) {
            super(handler);
            mHandler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Message message = mHandler.obtainMessage();
            message.setData(resultData);
            mHandler.sendMessage(message);
        }
    }
}
