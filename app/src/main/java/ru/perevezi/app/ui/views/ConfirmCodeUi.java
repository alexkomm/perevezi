/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.app.model.AccountModel;

public interface ConfirmCodeUi extends BaseUi {

    void setInterfaceCallbacks(UiCallbacks callbacks);

    void toggleInvalid(boolean show);

    void onCodeRetry();

    void navigateToCreateLotScreen(AccountModel accountModel);

    void navigateToQuickRegisterScreen();

    public interface UiCallbacks {
        void onConfirmRequest(String requestId, int confirmCode);

        void onRetry(String requestId);
    }

}
