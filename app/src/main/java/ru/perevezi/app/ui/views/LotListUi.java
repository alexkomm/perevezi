/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.HashMap;
import java.util.List;

import ru.perevezi.app.model.LotModel;

public interface LotListUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    HashMap<String, Object> getFilterOptions();

    void addLotsToList(List<LotModel> lotModelList, boolean clear);

    void setDynamicLoadingEnabled(boolean dynamicLoadingEnabled);

    interface UiCallbacks {
        void onVisibleThresholdReached(int offset);

        void onRefresh(boolean swipe);

        void onSearchLotsByIds(String[] ids);

        void onSearchLotsByFilters(boolean resetCache, boolean swipe);
    }
}
