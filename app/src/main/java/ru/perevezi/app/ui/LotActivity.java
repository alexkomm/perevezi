/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.events.RefreshDataEvent;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.components.FabAction;
import ru.perevezi.app.ui.controllers.LotHostController;
import ru.perevezi.app.ui.fragments.BidCreateFragment;
import ru.perevezi.app.ui.fragments.LotAcceptFragment;
import ru.perevezi.app.ui.fragments.LotCompletionConfirmDialog;
import ru.perevezi.app.ui.fragments.NewReviewFragment;
import ru.perevezi.app.ui.fragments.SiteViewFragment;
import ru.perevezi.app.ui.fragments.lot.LotHostListener;
import ru.perevezi.app.ui.fragments.lot.LotPagerFragment;
import ru.perevezi.app.ui.views.LotHostUi;
import ru.perevezi.domain.utils.Preconditions;

public class LotActivity extends BaseActivity implements LotHostUi, LotHostListener,
        FabAction.LotActionViewListener, LotCompletionConfirmDialog
                .CompletionConfirmDialogListener {

    @Inject
    UserState mUserState;

    @Inject
    LotHostController mLotHostController;

    @Inject
    RxBus mBus;

    public static final String EXTRA_INITIAL_PAGER_ITEM = "initial_pager_item";

    public static final String EXTRA_ID = "id";
    public static final String EXTRA_MESSAGE = "message";

    public static final int PAGER_ITEM_SUMMARY = 0;
    public static final int PAGER_ITEM_BIDS = 1;
    public static final int PAGER_ITEM_COMMENTS = 2;

    private int mLotId;

    private int mInitialPagerItem;

    private String mExtraMessage;

    private Fragment mPagerFragment;

    private boolean mHasWonBid;
    private LotModel mLotModel;

    private LotCompletionConfirmDialog mCompletionConfirmDialog;
    private UiCallbacks mUiCallbacks;
    private List<BidModel> mBidModelList;

    private BroadcastReceiver mGcmBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mLotId = extras.getInt(EXTRA_ID);
            mInitialPagerItem = extras.getInt(EXTRA_INITIAL_PAGER_ITEM);
            mExtraMessage = extras.getString(EXTRA_MESSAGE);
        }

        Preconditions.checkState(mLotId > 0, "Lot id cannot be found");

        FragmentManager fm = getSupportFragmentManager();

        mPagerFragment = fm.findFragmentByTag(LotPagerFragment.TAG);
        if (mPagerFragment == null) {
            mPagerFragment = LotPagerFragment.newInstance(mLotId, mInitialPagerItem);
            fm.beginTransaction().replace(R.id.container, mPagerFragment,
                    LotPagerFragment.TAG).commit();
        }

        setToolbarTitle(getResources().getString(R.string.lot_screen_title, mLotId));

        if (mExtraMessage != null) {
            Toast.makeText(this, mExtraMessage, Toast.LENGTH_LONG).show();
        }

        mGcmBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                String lotId = extras.getString("nid");

                if (lotId != null && mLotId == Integer.valueOf(lotId)) {
                    mBus.send(new RefreshDataEvent());
                }
            }
        };

        mLotHostController.initialize(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final IntentFilter intentFilter = new IntentFilter("com.google.android.c2dm.intent.RECEIVE");
        intentFilter.setPriority(1);

        registerReceiver(mGcmBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mGcmBroadcastReceiver);

        super.onPause();
    }


    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public boolean hasWonBid() {
        return mHasWonBid;
    }

    @Override
    public void setWonBid(boolean hasWonBid) {
        mHasWonBid = hasWonBid;
    }

    @Override
    public void onBidListLoaded(List<BidModel> bidModelList) {
        mBidModelList = bidModelList;
    }

    @Override
    public void toggleLotActionView(boolean show) {
        ViewGroup lotActionView = (ViewGroup) findViewById(R.id.sticky_action_view);

        if (lotActionView != null) {
            lotActionView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onLotDataLoaded(LotModel lotModel) {
        mLotModel = lotModel;

        int actionCode = lotModel.getActionCode();

        if (actionCode > 0) {
            if (actionCode == LotModel.LOT_ACTION_PROVIDER_MAKE_BIDS) {
                final AccountModel account = mUserState.getCurrentAccount();
                final int phoneConfirmStatus = account.getPhoneConfirmStatus();

                if (phoneConfirmStatus == AccountModel.PHONE_CONFIRM_STATUS_PENDING ||
                        phoneConfirmStatus == AccountModel.PHONE_CONFIRM_STATUS_NOT_CONFIRMED) {
                    actionCode = 0;
                }
            }

            FabAction fabAction = new FabAction(this, actionCode);
            fabAction.setLotActionViewListener(this);
        }
    }

    @Override
    public void onNewReviewButtonClicked() {
        FragmentManager fm = getSupportFragmentManager();
        NewReviewFragment newReviewFragment = NewReviewFragment.newInstance
                (mLotModel);
        fm.beginTransaction().replace(R.id.container, newReviewFragment,
                NewReviewFragment.TAG).addToBackStack(NewReviewFragment.TAG)
                .commit();
    }

    @Override
    public void onMakeBidButtonClicked() {
        FragmentManager fm = getSupportFragmentManager();
        BidCreateFragment bidCreateFragment = BidCreateFragment.newInstance
                (mLotModel);
        fm.beginTransaction().replace(R.id.container, bidCreateFragment,
                BidCreateFragment.TAG).addToBackStack(BidCreateFragment.TAG)
                .commit();
    }

    @Override
    public void onConfirmCompletionButtonClicked() {
        AccountModel profileInfo = mUserState.getCurrentAccount();

        if (mCompletionConfirmDialog == null) {
            mCompletionConfirmDialog = LotCompletionConfirmDialog.newInstance(profileInfo);
        }

        mCompletionConfirmDialog.show(getFragmentManager(), "completion_confirm");
    }

    @Override
    public void onConfirmWonButtonClicked() {
        FragmentManager fm = getSupportFragmentManager();
        LotAcceptFragment lotAcceptFragment = LotAcceptFragment.newInstance
                (mLotModel.getRelatedBid());
        fm.beginTransaction().replace(R.id.container, lotAcceptFragment,
                LotAcceptFragment.TAG).addToBackStack(LotAcceptFragment.TAG)
                .commit();
    }

    @Override
    public void onCompletionConfirmed() {
        mLotHostController.onLotCompletionConfirmed(mLotId);
    }

    @Override
    public void onCompletionDeclined() {
        mLotHostController.onLotCompletionDeclined(mLotId);
    }

    @Override
    public void onCompletionCanceled() { }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void onBackPressed() {
        SiteViewFragment siteViewFragment = (SiteViewFragment) getSupportFragmentManager().findFragmentByTag(SiteViewFragment.TAG);

        if (siteViewFragment != null && siteViewFragment.canGoBack()) {
            siteViewFragment.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
