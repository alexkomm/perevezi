/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.QuickRegisterUi;
import ru.perevezi.data.Constants;
import ru.perevezi.data.exception.NotAcceptableException;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.interactor.impl.RegisterUseCase;

public class RegisterController implements QuickRegisterUi.UiCallbacks, Controller<QuickRegisterUi> {

    private final RegisterUseCase mUseCase;
    private QuickRegisterUi mUi;

    @Inject
    public RegisterController(RegisterUseCase useCase) {
        mUseCase = useCase;
    }

    @Override
    public void initialize(QuickRegisterUi ui) {
        mUi = ui;
        mUi.setInterfaceCallbacks(this);
    }

    @Override
    public void onRegisterRequest(String name, String mail, String phone) {

        final PereveziAccount pereveziAccount = new PereveziAccount(name, null);

        pereveziAccount.setFullName(name);
        pereveziAccount.setEmail(mail);
        pereveziAccount.setPhone(phone);

        mUi.toggleDialogProgress(true);

        RxUseCase.callback(mUseCase, pereveziAccount).register(new RxUseCase.AsyncCallback<String>() {

            @Override
            public void onResultOk(String requestId) {
                mUi.toggleDialogProgress(false);
                mUi.navigateToConfirmScreen(requestId);
            }

            @Override
            public void onError(Throwable throwable) {
                mUi.toggleDialogProgress(false);

                if (throwable instanceof NotAcceptableException) {
                    final int errorCode = ((NotAcceptableException) throwable).getErrorCode();
                    final Map<String, String> formErrors = ((NotAcceptableException) throwable).getFormErrors();

                    switch (errorCode) {
                        case Constants.API_RESPONSE_ALREADY_REGISTERED_ERROR:
                            mUi.showAlreadyRegisteredDialog();
                            break;
                        case Constants.API_RESPONSE_SMS_CONFIRM_INVALID:
                            break;
                        case Constants.API_RESPONSE_SMS_CONFIRM_NOT_FOUND:
                            break;
                        default:
                            mUi.showErrors(formErrors);
                            break;
                    }
                } else {
                    mUi.showMessage(throwable.getMessage());
                }
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
