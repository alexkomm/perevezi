/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class HorizontalSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeight;

    public HorizontalSpacingItemDecoration(int mVerticalSpaceHeight) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {

        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.right = mVerticalSpaceHeight;
        }

        if (parent.getChildAdapterPosition(view) != 0) {
            outRect.left = mVerticalSpaceHeight;
        }
    }
}
