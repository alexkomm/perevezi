/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.model.SettingsModel;

public class LotViewBidSettingsCard extends Card {
    private final SettingsModel mSettings;
    private final LotModel mLot;

    public LotViewBidSettingsCard(Context context, SettingsModel settingsModel, LotModel lot) {
        this(context, R.layout.card_lot_bidding_info, settingsModel, lot);
    }

    public LotViewBidSettingsCard(Context context, int innerLayout, SettingsModel
            settingsModel, LotModel lot) {
        super(context, innerLayout);
        mSettings = settingsModel;
        mLot = lot;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setCurrency(Currency.getInstance(Locale.getDefault()));

        if (mLot.getMaxPrice() != null && mLot.getMaxPrice().compareTo(BigDecimal.ZERO) > 0) {
            ((TextView)parent.findViewById(R.id.max_price_value)).setText(nf.format(mLot
                    .getMaxPrice().doubleValue()));
        } else {
            ((TextView)parent.findViewById(R.id.max_price_value)).setText(mContext.getResources()
                    .getString(R.string.label_not_specified));
        }

        if (mLot.getBlitzPrice() != null && mLot.getBlitzPrice().compareTo(BigDecimal.ZERO) > 0) {
            ((TextView)parent.findViewById(R.id.blitz_price_value)).setText(nf.format(mLot
                    .getBlitzPrice().doubleValue()));
        } else {
            ((TextView)parent.findViewById(R.id.blitz_price_value)).setText(mContext.getResources
                    ().getString(R.string.label_not_specified));
        }

        String addOptions = null;

        for (Map<String, String> option : mSettings.getLotLoadingOptions()) {
            if (Integer.parseInt(option.get("id")) == mLot.getLoadingServiceId()) {
                addOptions = option.get("lot");
                break;
            }
        }

        if (addOptions != null) {
            ((TextView)parent.findViewById(R.id.add_info_text)).setText(addOptions);
        } else {
            parent.findViewById(R.id.add_info_group).setVisibility(View.GONE);
        }
    }
}
