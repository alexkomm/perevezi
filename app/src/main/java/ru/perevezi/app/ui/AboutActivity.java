/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;
import android.widget.TextView;

import ru.perevezi.app.BuildConfig;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView versionTextView = (TextView) findViewById(R.id.tv_app_version);
        versionTextView.setText(BuildConfig.VERSION_NAME);
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_about;
    }
}
