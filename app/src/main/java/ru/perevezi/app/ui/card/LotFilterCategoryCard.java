/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.utils.ViewUtils;

public class LotFilterCategoryCard extends Card {


    private final List<CategoryModel> mCategoriesList;
    private final HashMap<String, Object> mFilterOptions;

    private Set<Integer> mCheckedCategories = new HashSet<>();
    private CategoriesArrayAdapter mListAdapter;

    public LotFilterCategoryCard(Context context, List<CategoryModel> categoriesList, HashMap<String, Object> filterOptions) {
        this(context, R.layout.card_filter_category, categoriesList, filterOptions);
    }

    public LotFilterCategoryCard(Context context, int innerLayout, List<CategoryModel> categoriesList, HashMap<String, Object> filterOptions) {
        super(context, innerLayout);
        mCategoriesList = categoriesList;
        mFilterOptions = filterOptions;

        if (mFilterOptions.get("category") != null) {
            mCheckedCategories = (Set<Integer>) mFilterOptions.get("category");
        }
    }


    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        mListAdapter = new CategoriesArrayAdapter(mContext, mCategoriesList);

        ListView categoriesListView = (ListView) parent.findViewById(R.id.categories_list);
        categoriesListView.setAdapter(mListAdapter);
        ViewUtils.setListViewHeightBasedOnChildren(categoriesListView);

        categoriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CategoryModel category = mListAdapter.getItem(position);

                toggleCategoryChecked(category.getId());

                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) view.getTag();
                categoryViewHolder.getCheckBox().setChecked(isCategoryChecked(category.getId()));
            }
        });
    }

    private void toggleCategoryChecked(int id) {
        if (!isCategoryChecked(id)) {
            mCheckedCategories.add(id);
        } else {
            mCheckedCategories.remove(id);
        }

        mFilterOptions.put("category", mCheckedCategories);
    }

    private boolean isCategoryChecked(int id) {
        return mCheckedCategories.contains(id);
    }

    private class CategoriesArrayAdapter extends ArrayAdapter<CategoryModel> {
        private final LayoutInflater mLayoutInflater;

        public CategoriesArrayAdapter(Context context, List<CategoryModel> pereveziCategories) {
            super(context, R.layout.item_category_filter, R.id.category_name, pereveziCategories);

            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CategoryModel category = getItem(position);

            CheckBox checkBox;
            TextView textView;
            ImageView imageView;

            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_category_filter, null);

                textView = (TextView) convertView.findViewById(R.id.category_name);
                checkBox = (CheckBox) convertView.findViewById(R.id.category_checkbox);
                imageView = (ImageView) convertView.findViewById(R.id.category_image);

                convertView.setTag( new CategoryViewHolder(textView,checkBox, imageView) );

                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox ch = (CheckBox) v;
                        CategoryModel category = (CategoryModel) ch.getTag();

                        toggleCategoryChecked(category.getId());
                    }
                });
            } else {
                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) convertView.getTag();
                checkBox = categoryViewHolder.getCheckBox();
                textView = categoryViewHolder.getTextView();
                imageView = categoryViewHolder.getImageView();
            }

            checkBox.setTag(category);

            checkBox.setChecked(isCategoryChecked(category.getId()));
            textView.setText(category.getName());
            imageView.setImageResource(mContext.getResources().getIdentifier("pic_category_" + category
                    .getDrawableResName() + "_32dp", "drawable", mContext.getPackageName()));


            return super.getView(position, convertView, parent);
        }

    }


    private static class CategoryViewHolder {
        private CheckBox mCheckBox;
        private TextView mTextView;
        private ImageView mImageView;

        public CategoryViewHolder() {}

        public CategoryViewHolder(TextView textView, CheckBox checkBox, ImageView imageView) {
            mCheckBox = checkBox ;
            mTextView = textView ;
            mImageView = imageView;
        }
        public CheckBox getCheckBox() {
            return mCheckBox;
        }
        public void setCheckBox(CheckBox checkBox) {
            mCheckBox = checkBox;
        }
        public TextView getTextView() {
            return mTextView;
        }
        public void setTextView(TextView textView) {
            mTextView = textView;
        }

        public ImageView getImageView() {
            return mImageView;
        }

        public void setImageView(ImageView imageView) {
            mImageView = imageView;
        }
    }

}
