/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.perevezi.app.R;
import ru.perevezi.app.utils.Strings;
import ru.perevezi.domain.utils.Preconditions;

public class LotWizardTitleView extends LinearLayout {

    protected Context mContext;

    private Button mAddMoreButton;

    private ViewGroup mTitleContainer;

    private String mAmountHint;

    private ArrayAdapter<String> mTitleOptionsAdapter;

    private int mFieldCounter;

    private TitleCallbacks mTitleCallbacks;

    public LotWizardTitleView(Context context) {
        this(context, null);
    }

    public LotWizardTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LotWizardTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

        init();
    }

    public void setAddMoreLabel(String addMoreLabel) {
        Preconditions.checkState(addMoreLabel != null, "label cannot be null");
        Preconditions.checkState(mAddMoreButton != null, "view not inflated yet");

        mAddMoreButton.setVisibility(VISIBLE);
        mAddMoreButton.setText(addMoreLabel);
    }

    public void setAmountHint(String amountHint) {
        mAmountHint = amountHint;
        Preconditions.checkState(mAmountHint != null, "label cannot be null");

        onViewDataChanged();
    }

    private void init() {
        setOrientation(VERTICAL);

        LayoutInflater.from(mContext).inflate(R.layout.view_lot_wizard_cargo, this);

        mTitleContainer = (ViewGroup)findViewById(R.id.title_container);

        mAddMoreButton = (Button)findViewById(R.id.btn_add_more);
        mAddMoreButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addTitleGroup();

                if (mTitleCallbacks != null) {
                    mTitleCallbacks.onAddMoreClicked(getTitleGroupCount());
                }
            }
        });
    }

    public void addTitleGroup() {
        Preconditions.checkState(mTitleContainer != null, "view not inflated yet");

        final LinearLayout titleGroup = (LinearLayout)LayoutInflater.from(mContext).inflate(R.layout.item_lot_wizard_cargo, mTitleContainer, false);
        refresh(titleGroup);

        final AutoCompleteTextView titleField = (AutoCompleteTextView)titleGroup.findViewWithTag("title");
        final EditText quantityField = (EditText)titleGroup.findViewWithTag("quantity");

        titleField.setId(mFieldCounter++);
        quantityField.setId(mFieldCounter++);

        titleField.addTextChangedListener(new TitleTextWatcher());
        quantityField.addTextChangedListener(new TitleTextWatcher());

        mTitleContainer.addView(titleGroup);
    }

    private void refresh(ViewGroup titleGroup) {
        EditText quantity = (EditText)titleGroup.findViewWithTag("quantity");

        quantity.setVisibility((mAmountHint == null) ? GONE : VISIBLE);

        if (mAmountHint != null) {
            quantity.setHint(mAmountHint);
        }

        if (mTitleOptionsAdapter != null) {
            AutoCompleteTextView title = (AutoCompleteTextView)titleGroup.findViewWithTag("title");
            title.setAdapter(mTitleOptionsAdapter);
            title.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        ((AutoCompleteTextView) v).showDropDown();
                    }
                }
            });
        }
    }

    public void setTitleAutocomplete(String[] titleOptions) {
        Preconditions.checkState(titleOptions != null, "titleOptions cannot be null");

        mTitleOptionsAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item, titleOptions);

        onViewDataChanged();
    }

    public void loadTitleData(ArrayList<Map<String, String>> titleData) {
        Preconditions.checkState(mTitleContainer != null, "view not inflated yet");

        for (int i = 0; i < mTitleContainer.getChildCount(); ++i) {
            LinearLayout titleGroup = (LinearLayout) mTitleContainer.getChildAt(i);
            Map<String, String> titleMap = titleData.get(i);

            final AutoCompleteTextView titleField = (AutoCompleteTextView)titleGroup.findViewWithTag("title");
            final EditText quantityField = (EditText)titleGroup.findViewWithTag("quantity");

            titleField.setText(titleMap.get("title"));
            quantityField.setText(titleMap.get("amount"));
        }
    }

    private void onViewDataChanged() {
        Preconditions.checkState(mTitleContainer != null, "view not inflated yet");

        for (int i = 0; i < mTitleContainer.getChildCount(); ++i) {
            refresh((LinearLayout) mTitleContainer.getChildAt(i));
        }
    }

    private int getTitleGroupCount() {
        Preconditions.checkState(mTitleContainer != null, "view not inflated yet");

        return mTitleContainer.getChildCount();
    }

    private void saveInput() {
        Preconditions.checkState(mTitleContainer != null, "view not inflated yet");

        ArrayList<Map<String, String>> titleData = new ArrayList<>();

        for (int i = 0; i < mTitleContainer.getChildCount(); ++i) {
            LinearLayout titleGroup = (LinearLayout) mTitleContainer.getChildAt(i);
            Map<String, String> titleMap = new HashMap<>();

            final AutoCompleteTextView titleField = (AutoCompleteTextView)titleGroup.findViewWithTag("title");
            final EditText quantityField = (EditText)titleGroup.findViewWithTag("quantity");

            titleMap.put("title", titleField.getText().toString());

            if (mAmountHint != null) {
                final String quantity = quantityField.getText().toString();
                titleMap.put("amount", Strings.isEmpty(quantity) ? "1" : quantity);
                titleMap.put("amount_hint", mAmountHint);
            }


            titleData.add(titleMap);
        }

        if (mTitleCallbacks != null) {
            mTitleCallbacks.onSaveInput(titleData);
        }
    }

    public void setTitleCallbacks(TitleCallbacks titleCallbacks) {
        mTitleCallbacks = titleCallbacks;
    }


    public static interface TitleCallbacks {
        void onAddMoreClicked(int totalCount);

        void onSaveInput(ArrayList<Map<String, String>> titleData);
    }

    public class TitleTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            saveInput();
        }
    }
}
