/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.lot;

import java.util.List;

import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.LotModel;

public interface LotHostListener {

    boolean hasWonBid();

    void setWonBid(boolean hasWonBid);

    void toggleLotActionView(boolean show);

    void onLotDataLoaded(LotModel lotModel);

    void onBidListLoaded(List<BidModel> bidModelList);

}
