/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.recyclerview.internal.CardArrayRecyclerViewAdapter;
import it.gmariotti.cardslib.library.recyclerview.view.CardRecyclerView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.CommissionsListCard;
import ru.perevezi.app.ui.controllers.CommissionsController;
import ru.perevezi.app.ui.views.CommissionsUi;

public class BillingCommissionsFragment extends BaseFragment implements CommissionsUi{

    private static final String KEY_COMMISSIONS = "commissions";

    HashMap<String, Object> mCommissionsData;

    @Inject
    CommissionsController mController;
    private CardArrayRecyclerViewAdapter mCardArrayAdapter;
    private CardRecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        if (savedInstanceState != null) {
            mCommissionsData = (HashMap<String, Object>) savedInstanceState.getSerializable(KEY_COMMISSIONS);
        }

        mController.initialize(this);
        registerUiLifecycleObserver(mController);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        return inflater.inflate(R.layout.fragment_billing_commissions, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Card> cards = new ArrayList<>();

        mCardArrayAdapter = new CardArrayRecyclerViewAdapter(getActivity(), cards);

        //Staggered grid view
        mRecyclerView = (CardRecyclerView) getActivity().findViewById(R.id.card_list);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Set the empty view
        if (mRecyclerView != null) {
            mRecyclerView.setAdapter(mCardArrayAdapter);
        }

        if (mCommissionsData != null) {
            updateCardAdapter(mCommissionsData);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_COMMISSIONS, mCommissionsData);
    }

    @Override
    public HashMap<String, Object> getCommissionsData() {
        return mCommissionsData;
    }

    private void updateCardAdapter(HashMap<String, Object> data) {
        ArrayList<Card> cards = new ArrayList<>();

        List<Map<String, String>> commissionsList = (List<Map<String, String>>) data.get("commissions");

        for (Map<String, String> commission : commissionsList) {
            Card card = new CommissionsListCard(getActivity(), commission);
            cards.add(card);
        }

        mCardArrayAdapter.addAll(cards);
    }

    @Override
    public void addCommissionsToList(HashMap<String, Object> data) {
        if (mCommissionsData == null) {
            mCommissionsData = new HashMap<>();
        }

        mCommissionsData.putAll(data);

        updateCardAdapter(data);
    }
}
