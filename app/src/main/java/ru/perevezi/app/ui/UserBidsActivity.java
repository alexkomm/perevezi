/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BasePagerFragment;
import ru.perevezi.app.ui.controllers.UserBidsHostController;
import ru.perevezi.app.ui.fragments.UserBidsSectionFragment;
import ru.perevezi.app.ui.views.UserBidsHostUi;
import ru.perevezi.app.utils.StringManager;

public class UserBidsActivity extends BaseActivity implements UserBidsHostUi, BasePagerFragment.PagerFragmentListener {


    private static final String ARG_KEY_BID_STATS = "bid_stats";

    @Inject
    UserBidsHostController mController;

    private ArrayList<Map<String, Object>> mBidStats;
    private Fragment mPagerFragment;
    private UiCallbacks mUiCallbacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController.initialize(this);

        if (savedInstanceState == null) {
            mUiCallbacks.fetchBidStats();
        } else {
            mBidStats = (ArrayList<Map<String, Object>>) savedInstanceState.getSerializable(ARG_KEY_BID_STATS);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARG_KEY_BID_STATS, mBidStats);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Fragment onGetItem(int position) {
        String section = (String) mBidStats.get(position).get("section");

        return UserBidsSectionFragment.newInstance(section);
    }

    @Override
    public CharSequence onGetPageTitle(int position) {
        String section = (String) mBidStats.get(position).get("section");
        int count  = Integer.parseInt((String) mBidStats.get(position).get("count"));

        String title = getResources().getString(StringManager.getBidSectionTitleResId(section));

        if (count > 0) {
            title += " (" + count + ")";
        }

        return title;
    }

    @Override
    public int onGetCount() {
        return mBidStats.size();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void initViewPager(ArrayList<Map<String, Object>> bidStats) {
        mBidStats = bidStats;

        Collections.sort(mBidStats, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> lhs, Map<String, Object> rhs) {
                return Integer.parseInt((String) lhs.get("w")) - Integer.parseInt((String) rhs.get("w"));
            }
        });

        if (mBidStats.size() > 0) {

            FragmentManager fm = getSupportFragmentManager();

            mPagerFragment = fm.findFragmentByTag(BasePagerFragment.TAG);

            if (mPagerFragment == null) {
                mPagerFragment = BasePagerFragment.newInstance(0);
                fm.beginTransaction().replace(R.id.container, mPagerFragment, BasePagerFragment.TAG).commit();
            }

        } else {
            // @TODO Показываем текст и предложение разместить запрос
        }
    }
}
