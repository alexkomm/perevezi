/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.base;

public interface UiLifecycleObserver {

    void onResume();

    void onPause();
}
