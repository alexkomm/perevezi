/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

public interface CommentUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void toggleDialogProgress(boolean show);

    void navigateToCommentList(Integer msgResId);

    void showCommentForbiddenMessage(String message);

    void showCommentForm();

    interface UiCallbacks {
        void checkCommentAccess(int nid);

        void onCommentSubmitted(String comment, int nid, int pid);
    }
}
