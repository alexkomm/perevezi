/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.utils.DateUtils;
import ru.perevezi.app.utils.DistanceRangeFormatter;
import ru.perevezi.app.utils.RangeFormatter;
import ru.perevezi.app.utils.WeightRangeFormatter;

public class LotFilterSettingsCard extends Card {

    private static final int WEIGHT_MAX_VALUE = 30000;
    private static final int DISTANCE_MAX_VALUE = 10000;

    private final HashMap<String, Object> mFilterOptions;

    private DatePickerDialog mDatePickerDialog;

    private TextView mDateActiveTextView;
    private SimpleDateFormat mDateFormatter = new SimpleDateFormat(DateUtils.DATE_FORMAT, Locale.getDefault());

    public LotFilterSettingsCard(Context context, HashMap<String, Object> filterOptions) {
        this(context, R.layout.card_filter_settings, filterOptions);
    }

    public LotFilterSettingsCard(Context context, int innerLayout, HashMap<String, Object> filterOptions) {
        super(context, innerLayout);
        mFilterOptions = filterOptions;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        DistanceRangeFormatter distanceRangeFormatter = new DistanceRangeFormatter();
        WeightRangeFormatter weightRangeFormatter = new WeightRangeFormatter();

        RangeBar weightRangeBar = (RangeBar) parent.findViewById(R.id.weight_sb);
        TextView weightText = (TextView) parent.findViewById(R.id.weight_text);
        weightText.setTag("weight");


        weightRangeBar.setOnRangeBarChangeListener(new FilterRangeBarChangeListener(WEIGHT_MAX_VALUE, weightText, weightRangeFormatter));

        float leftPinValue;
        float rightPinValue;
        float[] rangeOptions = (float[]) mFilterOptions.get("weight");

        if (rangeOptions != null) {
            leftPinValue = rangeOptions[0];
            rightPinValue = rangeOptions[1];

            weightRangeBar.setRangePinsByValue(leftPinValue, rightPinValue > 0 ? rightPinValue : 30500);
            weightText.setText(weightRangeFormatter.format(leftPinValue, rightPinValue));
        }

        RangeBar distanceRangeBar = (RangeBar) parent.findViewById(R.id.dist_sb);
        TextView distanceText = (TextView) parent.findViewById(R.id.dist_text);
        distanceText.setTag("distance");

        distanceRangeBar.setOnRangeBarChangeListener(new FilterRangeBarChangeListener(DISTANCE_MAX_VALUE, distanceText, distanceRangeFormatter));

        rangeOptions = (float[]) mFilterOptions.get("distance");

        if (rangeOptions != null) {
            leftPinValue = rangeOptions[0];
            rightPinValue = rangeOptions[1];

            distanceRangeBar.setRangePinsByValue(leftPinValue, rightPinValue > 0 ? rightPinValue : 10100);
            distanceText.setText(distanceRangeFormatter.format(leftPinValue, rightPinValue));
        }

        Calendar calendar = Calendar.getInstance();
        mDatePickerDialog = new DatePickerDialog(mContext, new DateSetListener(), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        TextView dateFromText = (TextView) parent.findViewById(R.id.date_from);
        dateFromText.setTag("date_from");

        Calendar dateCalendar = Calendar.getInstance();

        if (mFilterOptions.get("date_from") != null) {
            dateCalendar.setTimeInMillis((long) mFilterOptions.get("date_from"));
            dateFromText.setText(mDateFormatter.format(dateCalendar.getTime()));
        }

        dateFromText.setFocusable(false);
        dateFromText.setInputType(InputType.TYPE_NULL);
        dateFromText.setFocusableInTouchMode(false);
        dateFromText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateActiveTextView = (TextView) v;

                mDatePickerDialog.show();
            }
        });

        TextView dateTillText = (TextView) parent.findViewById(R.id.date_till);
        dateTillText.setTag("date_till");



        if (mFilterOptions.get("date_from") != null) {
            dateCalendar.setTimeInMillis((long) mFilterOptions.get("date_till"));
            dateTillText.setText(mDateFormatter.format(dateCalendar.getTime()));
        }

        dateTillText.setFocusable(false);
        dateTillText.setInputType(InputType.TYPE_NULL);
        dateTillText.setFocusableInTouchMode(false);
        dateTillText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateActiveTextView = (TextView) v;

                mDatePickerDialog.show();
            }
        });

    }


    private class FilterRangeBarChangeListener implements RangeBar.OnRangeBarChangeListener {


        private final int mLimit;
        private final TextView mView;
        private final RangeFormatter mRangeFormatter;

        private FilterRangeBarChangeListener(int limit, TextView view, RangeFormatter
                rangeFormatter) {
            mLimit = limit;
            mView = view;
            mRangeFormatter = rangeFormatter;
        }


        @Override
        public void onRangeChangeListener(RangeBar rangeBar, int i, int i2, String s, String s2) {
            float val1 = Float.parseFloat(s);
            float val2 = Float.parseFloat(s2);

            if (val2 > mLimit) val2 = 0;

            mView.setText(mRangeFormatter.format(val1, val2));

            mFilterOptions.put(mView.getTag().toString(), new float[] {val1, val2});
        }
    }

    private class DateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (mDateActiveTextView == null) {
                return;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(year, monthOfYear, dayOfMonth);

            mDateActiveTextView.setText(mDateFormatter.format(calendar.getTime()));

            mFilterOptions.put(mDateActiveTextView.getTag().toString(), calendar.getTimeInMillis());

            mDateActiveTextView = null;
        }
    }
}
