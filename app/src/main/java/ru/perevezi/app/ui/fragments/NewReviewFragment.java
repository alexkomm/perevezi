/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.view.CardViewNative;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.card.AccountInfoCard;
import ru.perevezi.app.ui.card.LotListCard;
import ru.perevezi.app.ui.card.ReviewFormCard;
import ru.perevezi.app.ui.controllers.NewReviewController;
import ru.perevezi.app.ui.fragments.lot.LotHostListener;
import ru.perevezi.app.ui.views.NewReviewUi;
import ru.perevezi.app.utils.StringManager;
import ru.perevezi.domain.utils.Preconditions;

public class NewReviewFragment extends BaseFragment implements NewReviewUi {

    private static final String ARG_KEY_LOT = "lot";
    private static final String ARG_KEY_OF_PROVIDER = "for_provider";
    public static final String TAG = "new_review_fragment";
    private static final String KEY_REVIEW_DATA = "review_data";

    private UiCallbacks mUiCallbacks;

    private LotModel mLotModel;

    private boolean mOfProvider;

    private HashMap<String, String> mReviewData = new HashMap<>();

    @Inject
    NewReviewController mController;

    @Inject
    UserState mUserState;

    @InjectView(R.id.provider_info)
    CardViewNative mProviderInfoCard;

    @InjectView(R.id.lot_info)
    CardViewNative mLotInfoCard;

    @InjectView(R.id.review_form)
    CardViewNative mReviewCard;
    private LotHostListener mLotHostController;
    private AccountModel mTargetAccount;

    public static NewReviewFragment newInstance(LotModel lotModel) {

        NewReviewFragment fragment = new NewReviewFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_LOT, Parcels.wrap(lotModel));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            // Let this fragment work without LotHostListener
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mLotHostController != null) {
            mLotHostController.toggleLotActionView(false);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mLotModel = Parcels.unwrap(args.getParcelable(ARG_KEY_LOT));
        }

        if (savedInstanceState != null) {
            mReviewData = (HashMap<String, String>) savedInstanceState.getSerializable(KEY_REVIEW_DATA);
        }

        Preconditions.checkState(mLotModel != null, "BidModel cannot be null");

        AccountModel currentAccount = mUserState.getCurrentAccount();

        mOfProvider = currentAccount.getExtId() == mLotModel.getAuthor().getExtId();

        mTargetAccount = mOfProvider ? mLotModel.getRelatedBid().getProvider() : mLotModel.getAuthor();

        mReviewData.put("lotId", String.valueOf(mLotModel.getNid()));
        mReviewData.put("targetUserId", String.valueOf(mTargetAccount.getExtId()));

        mController.initialize(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_REVIEW_DATA, mReviewData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_review, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(StringManager.getReviewScreenTitleResId(mOfProvider));

        showReviewForm();
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void showReviewForm() {

        Card providerInfoCard;

        if (mOfProvider) {
            providerInfoCard = new AccountInfoCard(getActivity(), mTargetAccount);
        } else {
            providerInfoCard = new AccountInfoCard(getActivity(), mTargetAccount);
        }

        mProviderInfoCard.setCard(providerInfoCard);

        Card lotInfoCard = new LotListCard(getActivity(), mLotModel, false);

        mLotInfoCard.setCard(lotInfoCard);

        Card reviewFormCard = new ReviewFormCard(getActivity(), mLotModel, mReviewData, mUiCallbacks);

        mReviewCard.setCard(reviewFormCard);
    }

    @Override
    public void finishReviewCreation() {

        showMessage(getResources().getString(R.string.review_success_message));

        if (mOfProvider) {
            getRouterUi().navigateToCreateLot();
        } else {
            getRouterUi().navigateToSearchLots();
        }

        getActivity().finish();
    }
}
