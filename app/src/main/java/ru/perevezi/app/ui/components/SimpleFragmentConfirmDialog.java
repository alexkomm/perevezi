/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.components;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import ru.perevezi.app.R;

public class SimpleFragmentConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String TAG = "confirm_dialog";

    int mTitleResId;

    int mMessageResId;

    private int mYesResId;
    private int mNoResId;

    public static SimpleFragmentConfirmDialog newInstance(int titleResId, int msgResId, int yesResId, int noResId){

        SimpleFragmentConfirmDialog dialogFragment = new SimpleFragmentConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("title", titleResId);
        bundle.putInt("message", msgResId);
        bundle.putInt("yes", yesResId);
        bundle.putInt("no", noResId);
        dialogFragment.setArguments(bundle);

        return dialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null) {
            mTitleResId = arguments.getInt("title");
            mMessageResId = arguments.getInt("message");
            mYesResId = arguments.getInt("yes");
            mNoResId = arguments.getInt("no");
        }

        if (mTitleResId == 0) {
            mTitleResId = R.string.dialog_title_default;
        }

        if (mMessageResId == 0) {
            mMessageResId = R.string.dialog_message_default;
        }

        if (mYesResId == 0) {
            mYesResId = R.string.yes;
        }

        if (mNoResId == 0) {
            mNoResId = R.string.no;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(mTitleResId)
                .setPositiveButton(mYesResId, this)
                .setNegativeButton(mNoResId, this)
                .setMessage(mMessageResId);

        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
                break;
        }
    }
}
