/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.text.DecimalFormat;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.Constants;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.ui.ProfileActivity;
import ru.perevezi.app.utils.RoundImageTransformation;
import ru.perevezi.domain.utils.Preconditions;

public class AccountInfoCard extends Card {

    private final AccountModel mAccount;

    @InjectView(R.id.car_imgview)
    ImageView mCarImageView;

    @InjectView(R.id.provider_name_text)
    TextView mNameTextView;

    @InjectView(R.id.driver_rating_rb)
    RatingBar mRatingBar;

    @InjectView(R.id.driver_rating_text)
    TextView mRatingText;

    @InjectView(R.id.vote_total_text)
    TextView mVoteTotalTextView;

    @InjectView(R.id.user_online_tip)
    TextView mUserOnlineTip;

    public AccountInfoCard(Context context, AccountModel account) {
        this(context, R.layout.card_account_info, account);
    }

    public AccountInfoCard(Context context, int innerLayout, AccountModel account) {
        super(context, R.layout.card_account_info);
        mAccount = account;

        Preconditions.checkState(mAccount != null, "account object cannot be null");

        init();
    }

    private void init() {
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                Intent intent = new Intent(mContext, ProfileActivity.class);
                intent.putExtra(ProfileActivity.EXTRA_ACCOUNT, Parcels.wrap(mAccount));

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        ButterKnife.inject(this, parent);

        mNameTextView.setText(mAccount.getFullName());

        if (mAccount.isVendor() && mAccount.getRating() > 0) {
            mRatingBar.setRating(mAccount.getRating());
            DecimalFormat df = new DecimalFormat("###.##");
            df.setMinimumFractionDigits(1);

            mRatingText.setText(df.format(mAccount.getRating()));
        } else {
            mRatingBar.setVisibility(View.GONE);
            mRatingText.setVisibility(View.GONE);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, Constants.PEREVEZI_USER_ONLINE_ACTIVITY_INTERVAL_MINUTES * -1);

        if (!mAccount.getLastAccessDate().after(calendar)) {
            mUserOnlineTip.setVisibility(View.GONE);
        }

        int voteTotal = mAccount.getPositiveVotes() + mAccount.getNeutralVotes() + mAccount.getNegativeVotes();

        mVoteTotalTextView.setText(mContext.getResources().getQuantityString(R.plurals.votes_count,
                voteTotal, voteTotal));

        if (!Strings.isNullOrEmpty(mAccount.getProfileImageUrl())) {
            Picasso picasso = Picasso.with(mContext);
            picasso.load(mAccount.getProfileImageUrl()).transform(new
                    RoundImageTransformation()).into(mCarImageView);

        } else {
            mCarImageView.setVisibility(View.GONE);
        }
    }
}
