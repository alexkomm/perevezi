/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.List;

import ru.perevezi.app.model.LotModel;

public interface UserLotsSectionUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);
    
    boolean isItemsFetched();

    int getSection();

    void addItemsToList(List<LotModel> list);

    interface UiCallbacks {
        void onVisibleThresholdReached(int offset);
    }

}
