/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.controllers.RefundController;
import ru.perevezi.app.ui.views.RefundUi;
import ru.perevezi.domain.utils.Preconditions;

public class RefundFragment extends BaseFragment implements RefundUi {

    public static final String TAG = "tag";

    private static final String KEY_LOT_ID = "lot_id";

    private UiCallbacks mUiCallbacks;

    @Inject
    RefundController mRefundController;

    @InjectView(R.id.refund_title_text)
    TextView mRefundTitleText;

    @InjectView(R.id.refund_send_btn)
    Button mRefundSendButton;

    @InjectView(R.id.refund_reason_text)
    TextView mRefundReasonTextView;

    private Integer mLotId;

    public static RefundFragment newInstance(int lotId) {
        RefundFragment fragment = new RefundFragment();

        Bundle args = new Bundle();
        args.putInt(KEY_LOT_ID, lotId);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mLotId = args.getInt(KEY_LOT_ID);
        }

        Preconditions.checkState(mLotId != null, "Lot id cannot be null");

        mRefundController.initialize(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_refund, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRefundTitleText.setText(getString(R.string.refund_title, mLotId));

        mRefundSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> args = prepareArguments();

                if (args != null) {
                    mUiCallbacks.onRefundRequest(args);
                }
            }
        });
    }

    private Map<String, String> prepareArguments() {
        if (!validate()) {
            return null;
        }

        Map<String, String> args = new HashMap<>(2);
        args.put("lotId", String.valueOf(mLotId));
        args.put("reason", mRefundReasonTextView.getText().toString());
        return args;
    }

    private boolean validate() {
        boolean noErrors = true;

        if (Strings.isNullOrEmpty(mRefundReasonTextView.getText().toString())) {
            mRefundReasonTextView.setError(getString(R.string.error_field_required));
            noErrors = false;
        }

        return noErrors;
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }
}
