/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.common.base.Strings;

import org.parceler.Parcels;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.InjectViews;
import ru.perevezi.app.Constants;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.CarModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.state.UserState;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.components.SimpleFragmentConfirmDialog;
import ru.perevezi.app.ui.controllers.BidCreateController;
import ru.perevezi.app.ui.fragments.lot.LotHostListener;
import ru.perevezi.app.ui.views.BidCreateUi;
import ru.perevezi.app.utils.DateUtils;
import ru.perevezi.app.utils.ViewUtils;
import ru.perevezi.domain.utils.Preconditions;

public class BidCreateFragment extends BaseFragment implements BidCreateUi {

    public static final String KEY_BID = "bid";

    public static final String TAG = "bid_create_fragment";

    public static final int DIALOG_FRAGMENT = 1;
    private static final String ARG_KEY_LOT = "lot";
    private static final String KEY_ACCESS = "access";
    private static final String KEY_FORBID_MSG = "forbid";

    @Inject
    SettingsState mSettingsState;

    @Inject
    UserState mUserState;

    @Inject
    BidCreateController mBidCreateController;

    @InjectView(R.id.bid_form_group)
    ViewGroup mBidFormGroup;

    @InjectView(R.id.bid_form_forbid_message)
    TextView mBidFormForbidMessage;

    @InjectView(R.id.et_bid_amount)
    EditText mBidAmountEditText;

    @InjectViews({R.id.et_ship_date, R.id.et_active_interval_date})
    EditText[] mDateFields;

    @InjectViews({R.id.et_ship_time, R.id.et_active_interval_time})
    EditText[] mTimeFields;

    @InjectView(R.id.sp_loading_services)
    Spinner mLoadingOptionsSpinner;

    @InjectView(R.id.sp_car)
    Spinner mCarSpinner;

    @InjectView(R.id.btn_create_bid)
    Button mCreateBidButton;

    @InjectView(R.id.bid_car_group)
    ViewGroup mBidCarGroup;

    @InjectView(R.id.bid_loading_group)
    ViewGroup mBidLoadingGroup;

    @InjectView(R.id.rg_ship_type)
    RadioGroup mShipTypeGroup;

    private UiCallBacks mUiCallBacks;

    private List<Map<String, String>> mLoadingOptions;

    private BidModel mBid = new BidModel();

    private DatePickerDialog mDatePickerDialog;
    private TimePickerDialog mTimePickerDialog;

    private SimpleDateFormat mDateFormatter = new SimpleDateFormat(DateUtils.DATE_FORMAT,
            Locale.getDefault());
    private SimpleDateFormat mTimeFormatter = new SimpleDateFormat(DateUtils.TIME_FORMAT,
            Locale.getDefault());

    private AccountModel mAccount;

    private EditText mActiveDateField;
    private EditText mActiveTimeField;

    private SimpleFragmentConfirmDialog mConfirmDialog;
    private LotModel mLot;
    private String mForbiddenMessage;
    private LotHostListener mLotHostController;

    private List<Map<String, String>> mShipTypeOptions;

    private enum BidAccess {
        NOT_CHECKED,
        GRANTED,
        RESTRICTED
    }

    private BidAccess mBidAccess = BidAccess.NOT_CHECKED;

    public static BidCreateFragment newInstance(LotModel lotModel) {
        BidCreateFragment fragment = new BidCreateFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_LOT, Parcels.wrap(lotModel));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle arguments = getArguments();

        if (arguments != null) {
            mLot = Parcels.unwrap(arguments.getParcelable(ARG_KEY_LOT));
        }

        Preconditions.checkState(mLot != null, "Lot model cannot be null");

        mBid.setNid(mLot.getNid());

        mBidCreateController.initialize(this);

        mLoadingOptions = mSettingsState.getGeneralSettings().getLotLoadingOptions();



        if (mLot.getCategoryModel().getId() == Constants.PEREVEZI_CAR_MOVING_CATEGORY_ID) {
            mShipTypeOptions = mSettingsState.getGeneralSettings().getCarMovingOptions();
        } else {
            mShipTypeOptions = mSettingsState.getGeneralSettings().getLotShippingOptions();
        }

        mAccount = mUserState.getCurrentAccount();

        if (savedInstanceState != null) {
            mBid = Parcels.unwrap(savedInstanceState.getParcelable(KEY_BID));
            mBidAccess = (BidAccess)savedInstanceState.getSerializable(KEY_ACCESS);
            mForbiddenMessage = savedInstanceState.getString(KEY_FORBID_MSG);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setToolbarTitle(R.string.bid_create_title);
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleBackArrow(true);
        mLotHostController.toggleLotActionView(false);
    }

    @Override
    public void onPause() {
        mLotHostController.toggleLotActionView(true);
        toggleBackArrow(false);
        super.onPause();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LotHostController");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_BID, Parcels.wrap(mBid));
        outState.putSerializable(KEY_ACCESS, mBidAccess);
        outState.putString(KEY_FORBID_MSG, mForbiddenMessage);

        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bid_create, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mBidAccess == BidAccess.NOT_CHECKED) {
            mUiCallBacks.checkBidAccess();
        } else if (mBidAccess == BidAccess.RESTRICTED) {
            showForbiddenMessage(mForbiddenMessage);
        } else {
            showBidForm();
        }

        mBidAmountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                BigDecimal amount;

                try {
                    amount = new BigDecimal(s.toString());
                } catch (NumberFormatException e) {
                    amount = null;
                }

                mBid.setAmount(amount);
            }
        });


        for (Map<String, String> shipType : mShipTypeOptions) {
            RadioButton rb = new RadioButton(getActivity());

            rb.setTag(shipType.get("id"));
            rb.setText(shipType.get("bid"));

            String icon = shipType.get("icon");
            int iconRes = 0;

            if (icon != null) {
                iconRes = getActivity().getResources().getIdentifier("pic_" + icon + "_16dp",
                        "drawable", getActivity().getPackageName());
            }

            if (iconRes > 0) {
                rb.setCompoundDrawablesWithIntrinsicBounds(iconRes, 0, 0, 0);
                rb.setCompoundDrawablePadding(16);
            }

            mShipTypeGroup.addView(rb);
        }

        mShipTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ViewUtils.hideSoftKeyboard(getActivity(), mBidAmountEditText);

                mBid.setShipType(Integer.parseInt((String)group.findViewById(checkedId).getTag()));
            }
        });


        if (mLot.getCategoryModel().getId() != Constants.PEREVEZI_CAR_MOVING_CATEGORY_ID) {
            SimpleAdapter loadingOptionsAdapter = new SimpleAdapter(getActivity(),
                    mLoadingOptions, android.R.layout.simple_spinner_dropdown_item,
                    new String[]{"bid"}, new int[]{android.R.id.text1});

            mLoadingOptionsSpinner.setAdapter(loadingOptionsAdapter);
            mLoadingOptionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ViewUtils.hideSoftKeyboard(getActivity(), mBidAmountEditText);

                    Map<String, String> loadingOption = (Map<String,
                            String>) parent.getItemAtPosition(position);

                    mBid.setLoadingOptions(Integer.parseInt(loadingOption.get("id")));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            mBidLoadingGroup.setVisibility(View.GONE);
        }


        List<CarModel> cars = mAccount.getCars();
        if (cars != null && cars.size() > 0) {
            ArrayAdapter<CarModel> carsAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, cars);

            mCarSpinner.setAdapter(carsAdapter);
            mCarSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long
                        id) {
                    ViewUtils.hideSoftKeyboard(getActivity(), mBidAmountEditText);

                    CarModel car = (CarModel) parent.getItemAtPosition
                            (position);

                    mBid.setCarId(car.getExtId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            mBidCarGroup.setVisibility(View.GONE);
        }

        Calendar calendar = Calendar.getInstance();

        mDatePickerDialog = new DatePickerDialog(getActivity(), new DateTimePickerListener(),
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        mDatePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        mTimePickerDialog = new TimePickerDialog(getActivity(), new DateTimePickerListener(),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);


        for (int i = 0; i < mDateFields.length; i++) {

            final EditText dateField = mDateFields[i];
            final EditText timeField = mTimeFields[i];

            dateField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.hideSoftKeyboard(getActivity(), mBidAmountEditText);
                    mDatePickerDialog.show();

                    mActiveDateField = dateField;
                    mActiveTimeField = timeField;
                }
            });

            timeField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewUtils.hideSoftKeyboard(getActivity(), mBidAmountEditText);
                    mTimePickerDialog.show();

                    mActiveDateField = dateField;
                    mActiveTimeField = timeField;
                }
            });
        }

        mCreateBidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateForm()) {
                    showConfirmDialog();
                }
            }
        });
    }

    @Override
    public int getLotId() {
        return mLot.getNid();
    }

    @Override
    public void showForbiddenMessage(String message) {
        mBidFormGroup.setVisibility(View.GONE);

        mBidFormForbidMessage.setText(Html.fromHtml(message));
        mBidFormForbidMessage.setVisibility(View.VISIBLE);

        mForbiddenMessage = message;
        mBidAccess = BidAccess.RESTRICTED;
    }

    @Override
    public void showBidForm() {
        mBidFormGroup.setVisibility(View.VISIBLE);
        mBidFormForbidMessage.setVisibility(View.GONE);

        mBidAccess = BidAccess.GRANTED;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallBacks.onBidSubmitted(mBid);
                }
                break;
        }
    }

    private void showConfirmDialog() {

        mConfirmDialog = SimpleFragmentConfirmDialog.newInstance(R.string
                .dialog_bid_confirm_title, R.string.dialog_bid_confirm_text, 0, 0);
        mConfirmDialog.setTargetFragment(this, DIALOG_FRAGMENT);
        mConfirmDialog.show(getFragmentManager(), "dialog");
    }

    @Override
    public void navigateToBidList(int msgResId) {
        Preconditions.checkState(mLot != null, "Lot not found");

        Intent intent = new Intent(getActivity(), LotActivity.class);
        intent.putExtra(LotActivity.EXTRA_ID, mLot.getNid());
        intent.putExtra(LotActivity.EXTRA_INITIAL_PAGER_ITEM, 1);
        intent.putExtra(LotActivity.EXTRA_MESSAGE, getResources().getString(msgResId));

        startActivity(intent);
    }

    private boolean validateForm() {

        boolean hasErrors = false;

        if (Strings.isNullOrEmpty(mBidAmountEditText.getText().toString())) {
            mBidAmountEditText.setError(getString(R.string.error_field_required));
            hasErrors = true;
        }

        return !hasErrors;
    }

    @Override
    public void setUiCallbacks(UiCallBacks uiCallBacks) {
        mUiCallBacks = uiCallBacks;
    }

    public class DateTimePickerListener implements DatePickerDialog.OnDateSetListener,
            TimePickerDialog.OnTimeSetListener {


        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            if (!view.isShown() || mActiveDateField == null) {
                return;
            }

            Calendar calendar = null;

            switch (mActiveDateField.getId()) {
                case R.id.et_ship_date:
                    calendar = mBid.getExpirationDate();
                    break;
                case R.id.et_active_interval_date:
                    calendar = mBid.getActiveTill();
            }

            if (calendar == null) {
                calendar = Calendar.getInstance();
                calendar.clear();
            }

            calendar.set(year, monthOfYear, dayOfMonth);

            mActiveDateField.setText(mDateFormatter.format(calendar.getTime()));

            switch (mActiveDateField.getId()) {
                case R.id.et_ship_date:
                    mBid.setExpirationDate(calendar);
                    break;
                case R.id.et_active_interval_date:
                    mBid.setActiveTill(calendar);
                    break;
            }

            mActiveDateField = null;
            mActiveTimeField = null;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (!view.isShown() || mActiveDateField == null || mActiveTimeField == null) {
                return;
            }

            Calendar calendar = null;

            switch (mActiveTimeField.getId()) {
                case R.id.et_ship_time:
                    calendar = mBid.getExpirationDate();
                    break;
                case R.id.et_active_interval_time:
                    calendar = mBid.getActiveTill();
            }

            if (calendar == null) {
                calendar = Calendar.getInstance();
            }

            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);

            mActiveTimeField.setText(mTimeFormatter.format(calendar.getTime()));

            if (Strings.isNullOrEmpty(mActiveDateField.getText().toString())) {
                mActiveDateField.setText(mDateFormatter.format(calendar.getTime()));
            }

            switch (mActiveTimeField.getId()) {
                case R.id.et_ship_time:
                    mBid.setExpirationDate(calendar);
                    break;
                case R.id.et_active_interval_time:
                    mBid.setActiveTill(calendar);
            }

            mActiveDateField = null;
            mActiveTimeField = null;
        }
    }
}
