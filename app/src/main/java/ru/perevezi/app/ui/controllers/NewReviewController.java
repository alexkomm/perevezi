/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.NewReviewUi;
import ru.perevezi.domain.interactor.impl.CreateReviewUseCase;

public class NewReviewController implements Controller<NewReviewUi>, NewReviewUi.UiCallbacks {
    private final CreateReviewUseCase mCreateReviewUseCase;
    private NewReviewUi mNewReviewUi;

    @Inject
    public NewReviewController(CreateReviewUseCase createReviewUseCase) {
        mCreateReviewUseCase = createReviewUseCase;
    }

    @Override
    public void initialize(NewReviewUi newReviewUi) {
        mNewReviewUi = newReviewUi;
        mNewReviewUi.setUiCallbacks(this);
    }

    @Override
    public void onNewReview(Map<String, String> reviewData) {
        mNewReviewUi.toggleDialogProgress(true);

        RxUseCase.callback(mCreateReviewUseCase, reviewData).register(new RxUseCase.AsyncCallback<Void>() {


            @Override
            public void onResultOk(Void result) {
                mNewReviewUi.toggleDialogProgress(false);
                mNewReviewUi.finishReviewCreation();
            }

            @Override
            public void onError(Throwable throwable) {
                mNewReviewUi.toggleDialogProgress(false);
                mNewReviewUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
