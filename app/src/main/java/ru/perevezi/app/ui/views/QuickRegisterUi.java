/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import java.util.Map;

public interface QuickRegisterUi extends BaseUi  {

    void setInterfaceCallbacks(UiCallbacks callbacks);

    void showErrors(Map<String, String> errors);

    void navigateToConfirmScreen(String requestId);

    void showAlreadyRegisteredDialog();

    public interface UiCallbacks {
        void onRegisterRequest(String name, String mail, String phone);
    }
}
