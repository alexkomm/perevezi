/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.base.BasePagerFragment;
import ru.perevezi.app.ui.fragments.BillingCommissionsFragment;
import ru.perevezi.app.ui.fragments.BillingMainFragment;

public class BillingActivity extends BaseActivity implements BasePagerFragment.PagerFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();

        BasePagerFragment pagerFragment = (BasePagerFragment) fm.findFragmentByTag(BasePagerFragment.TAG);

        if (pagerFragment == null) {
            pagerFragment = BasePagerFragment.newInstance(0);
            fm.beginTransaction().replace(R.id.container, pagerFragment, BasePagerFragment.TAG).commit();
        }
    }


    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Fragment onGetItem(int position) {
        switch (position) {
            case 0:
                return new BillingMainFragment();
            case 1:
                return new BillingCommissionsFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence onGetPageTitle(int position) {
        switch (position) {
            case 0:
                return getResources().getString(R.string.billing_pager_main);
            case 1:
                return getResources().getString(R.string.billing_pager_commissions);
            default:
                return null;
        }
    }

    @Override
    public int onGetCount() {
        return 2;
    }
}
