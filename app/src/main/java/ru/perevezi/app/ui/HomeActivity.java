/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import javax.inject.Inject;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.base.BaseActivity;
import ru.perevezi.app.ui.services.RegistrationIntentService;
import ru.perevezi.app.utils.Ln;
import ru.perevezi.domain.utils.PereveziPreferences;

public class HomeActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Inject
    DB mDB;

    @Inject
    PereveziPreferences mPereveziPreferences;

    private static final String KEY_IS_LOADING = "is_loading";
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toggleLogoProgress(true);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = mPereveziPreferences.isTokenSent();

                if (sentToken) {
                    getRouterUi().init();
                }
            }
        };

        if (savedInstanceState == null) {
            initCache();
            updateAppVersion();

            if (checkPlayServices()) {
                registerPlayServices();
            }
        } else {
            if (savedInstanceState.getBoolean(KEY_IS_LOADING)) {
                getProgressBar().setVisibility(View.VISIBLE);
            }
        }
    }

    private void registerPlayServices() {
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void updateAppVersion() {
        int appVersion = PereveziApplication.getAppVersion();
        mPereveziPreferences.setAppVersion(appVersion);
    }

    private void initCache() {
        try {
            int registeredVersion = mPereveziPreferences.getAppVersion();
            int currentVersion = PereveziApplication.getAppVersion();

            if (registeredVersion != currentVersion) {
                mDB.destroy();
                mDB = DBFactory.open(getApplicationContext());

                Ln.i("Purging cache while update @ " + SystemClock.elapsedRealtime());
            }
        } catch (SnappydbException e) {
            Ln.e(e.getLocalizedMessage());
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Ln.i("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IS_LOADING, getProgressBar() != null && getProgressBar().getVisibility() == View.VISIBLE);
    }

    @Override
    protected int getContentViewLayoutId() {
        return R.layout.activity_main;
    }
}
