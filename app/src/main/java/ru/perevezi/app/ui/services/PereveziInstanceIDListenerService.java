/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.services;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class PereveziInstanceIDListenerService extends InstanceIDListenerService {


    private static final String TAG = "PereveziInstanceIDLS";


    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }

}
