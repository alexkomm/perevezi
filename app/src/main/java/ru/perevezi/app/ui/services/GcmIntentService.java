/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;

import dagger.ObjectGraph;
import ru.perevezi.app.Injector;
import ru.perevezi.app.R;
import ru.perevezi.app.di.ContextModule;
import ru.perevezi.app.di.ServiceModule;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.HomeActivity;
import ru.perevezi.app.ui.fragments.RouterFragment;
import ru.perevezi.app.ui.fragments.search.LotListFragment;
import ru.perevezi.app.utils.Ln;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetLotDetailsUseCase;
import ru.perevezi.domain.utils.PereveziPreferences;
import ru.perevezi.domain.utils.Preconditions;

public class GcmIntentService extends GcmListenerService implements Injector {

    public static final String KEY_NEW_LOTS = "new_lots";

    @Inject
    GetLotDetailsUseCase mGetLotDetailsUseCase;

    @Inject
    PereveziPreferences mPreferences;

    public static final int NOTIFICATION_ID = 1;

    public static final String NEW_LOT_NOTIFICATION = "lot_notify_newlot";

    private NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;

    private Intent mTargetIntent;
    private ObjectGraph mObjectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        mObjectGraph = ((Injector) getApplication()).getObjectGraph().plus(getModules().toArray());
        inject(this);

        mTargetIntent = new Intent(this, HomeActivity.class);

        mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_launcher))
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager
                                .TYPE_NOTIFICATION))
                        .setAutoCancel(true)
                        .setOnlyAlertOnce(true);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String extId = data.getString("extId");
        String type  = data.getString("type", "");

        if (type == null || extId == null || mPreferences.isLoggedOut() || mPreferences
                .getCurrentAccountExtId() != Integer.valueOf(extId)) {
            return;
        }

        switch (type) {
            case NEW_LOT_NOTIFICATION:
                Set<String> newLots = mPreferences.getNewLots();

                if (newLots == null) {
                    newLots = new TreeSet<>();
                }

                String nid = data.getString("nid");

                if (nid == null) {
                    break;
                }

                if (!newLots.contains(nid)) {
                    newLots.add(nid);
                }

                mPreferences.setNewLots(newLots);

                String[] lotIds = newLots.toArray(new String[newLots.size()]);

                if (newLots.size() == 1) {
                    Map<String, Object> args = new HashMap<>(2);
                    args.put("lotId", lotIds[0]);
                    args.put("reset", true);

                    RxUseCase.callback(mGetLotDetailsUseCase, args)
                            .register(new RxUseCase.AsyncCallback<PereveziLot>() {


                                @Override
                                public void onResultOk(PereveziLot result) {
                                    Set<String> newLots = mPreferences.getNewLots();

                                    if (newLots != null && newLots.size() == 1) {
                                        mTargetIntent.putExtra(RouterFragment.EXTRA_LOT_ID, result.getNid());

                                        mBuilder.setContentTitle(getResources().getString(R.string.new_lot_push_title))
                                                .setContentText(result.getTitleString());

                                        sendNotification(0);
                                    }
                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    Ln.e(throwable);
                                }
                            });
                } else {
                    mTargetIntent.putExtra(RouterFragment.EXTRA_MENU_ID, RouterFragment
                            .ID_MENU_ITEM_SEARCH_LOTS);

                    mTargetIntent.putExtra(LotListFragment.ARG_LOT_IDS, lotIds);

                    mBuilder.setContentTitle(getResources().getQuantityString(R.plurals.new_lots,
                            newLots.size(), newLots.size()))
                            .setContentText(getResources().getString(R.string.new_lots_push_msg));

                    sendNotification(0);
                }

                break;
            default:

                String title = data.getString("title");
                String msg   = data.getString("msg");

                String lotId = data.getString("nid");

                if (title == null && msg == null) {
                    break;
                }

                mBuilder.setContentTitle(title)
                        .setContentText(msg);

                if (lotId != null) {
                    mTargetIntent.putExtra(RouterFragment.EXTRA_LOT_ID, Integer.valueOf(lotId));
                }

                // Some kind of unique id
                int notificationId = (int)((new Date().getTime()) % Integer.MAX_VALUE);
                sendNotification(notificationId);

                break;
        }
    }

    private void sendNotification(int id) {
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, mTargetIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mTargetIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // LocalBroadcastManager.getInstance(this).sendBroadcast(mTargetIntent);

        mBuilder.setContentIntent(contentIntent);

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, mBuilder.build());
    }

    @Override
    public ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }

    @Override
    public void inject(Object target) {
        Preconditions.checkState(mObjectGraph != null, "Object graph must be assigned before " +
                "injecting");
        mObjectGraph.inject(target);
    }

    protected List<Object> getModules() {
        List<Object> result = new ArrayList<Object>();
        result.add(new ContextModule(getApplicationContext()));
        result.add(new ServiceModule(this, this));
        return result;
    }
}
