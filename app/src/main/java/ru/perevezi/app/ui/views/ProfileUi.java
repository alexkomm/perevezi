/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.views;

import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.LotModel;

public interface ProfileUi extends BaseUi {

    void setUiCallbacks(UiCallbacks uiCallbacks);

    void showProfileInfo(AccountModel accountModel);

    void showNewReviewDialog(LotModel lotModel);

    interface UiCallbacks {
        void onGetProfileInfo(int userId);

        void onCheckMissingVote(int userId);
    }

}
