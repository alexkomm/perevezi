/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.card;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Locale;

import it.gmariotti.cardslib.library.internal.Card;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CommentModel;
import ru.perevezi.app.ui.fragments.CommentFragment;
import ru.perevezi.app.utils.DateUtils;
import ru.perevezi.domain.utils.Preconditions;

public class LotCommentCard extends Card {

    public static final int TREE_MARGIN = 50;

    private final CommentModel mCommentModel;
    private final boolean mTreeItem;
    private Button mReplyButton;

    public LotCommentCard(Context context, CommentModel commentModel, boolean treeItem) {
        this(context, R.layout.card_comment, commentModel, treeItem);
    }

    public LotCommentCard(Context context, int innerLayout, CommentModel commentModel, boolean treeItem) {
        super(context, innerLayout);
        mCommentModel = commentModel;
        mTreeItem = treeItem;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        View cardView = (View) getCardView();
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) cardView.getLayoutParams();

        if (mTreeItem && mCommentModel.getPid() > 0) {
            layoutParams.leftMargin = TREE_MARGIN;
        } else {
            layoutParams.leftMargin = 0;
        }

        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setCurrency(Currency.getInstance(Locale.getDefault()));

        TextView authorNameText = (TextView) parent.findViewById(R.id.author_name_text);
        authorNameText.setText(mCommentModel.getAuthorName());

        TextView bidAmountText = ((TextView)parent.findViewById(R.id.bid_amount_text));

        if (mCommentModel.getBidAmount() != null && mCommentModel.getBidAmount().compareTo(BigDecimal.ZERO) > 0) {
            bidAmountText.setVisibility(View.VISIBLE);
            bidAmountText.setText(nf.format(mCommentModel
                    .getBidAmount().doubleValue()));
        }

        TextView commentText = (TextView) parent.findViewById(R.id.comment_text);
        commentText.setText(mCommentModel.getComment());

        TextView commentDate = (TextView) parent.findViewById(R.id.comment_date);

        SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtils.DATE_WITH_TIME_FORMAT, Locale.getDefault());
        String registrationDate = dateFormatter.format(mCommentModel.getTimestamp() * 1000);

        commentDate.setText(registrationDate);

        mReplyButton = (Button) parent.findViewById(R.id.comment_reply_btn);

        mReplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = ((AppCompatActivity) mContext).getSupportFragmentManager();
                CommentFragment commentFragment = CommentFragment.newInstance(mCommentModel.getNid(), mCommentModel);

                fm.beginTransaction().replace(R.id.container, commentFragment,
                        CommentFragment.TAG).addToBackStack(CommentFragment.TAG).commit();
            }
        });
    }

    public void toogleReplyButton(boolean show) {
        Preconditions.checkState(mReplyButton != null, "Card not inited yet");

        mReplyButton.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
