/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.LotAcceptUi;
import ru.perevezi.domain.interactor.impl.AcceptLotUseCase;
import ru.perevezi.domain.interactor.impl.GetBidConfirmInfoUseCase;

public class LotAcceptController implements Controller<LotAcceptUi>, LotAcceptUi.UiCallbacks {

    private final GetBidConfirmInfoUseCase mConfirmInfoUseCase;
    private final AcceptLotUseCase mAcceptLotUseCase;
    private LotAcceptUi mLotAcceptUi;

    @Inject
    public LotAcceptController(GetBidConfirmInfoUseCase confirmInfoUseCase, AcceptLotUseCase acceptLotUseCase) {
        mConfirmInfoUseCase = confirmInfoUseCase;
        mAcceptLotUseCase = acceptLotUseCase;
    }

    @Override
    public void initialize(LotAcceptUi lotAcceptUi) {
        mLotAcceptUi = lotAcceptUi;
        mLotAcceptUi.setUiCallbacks(this);
    }

    private void getConfirmInfo(int bidId) {
        mLotAcceptUi.toggleDialogProgress(true);

        RxUseCase.callback(mConfirmInfoUseCase, bidId).register(new RxUseCase.AsyncCallback<Map<String, Object>>() {

            @Override
            public void onResultOk(Map<String, Object> result) {
                mLotAcceptUi.toggleDialogProgress(false);
                mLotAcceptUi.showLotAcceptForm(result);
            }

            @Override
            public void onError(Throwable throwable) {
                mLotAcceptUi.toggleDialogProgress(false);
                mLotAcceptUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onCreate(int bidId) {
        getConfirmInfo(bidId);
    }


    @Override
    public void onConfirm(int lotId) {
        Map<String, Object> args = new HashMap<>(3);
        args.put("lotId", lotId);
        args.put("payLater", false);
        args.put("confirmed", true);

        setLotConfirmationStatus(args);
    }

    @Override
    public void onDecline(int lotId) {
        Map<String, Object> args = new HashMap<>(3);
        args.put("lotId", lotId);
        args.put("confirmed", false);

        setLotConfirmationStatus(args);
    }

    @Override
    public void onPayLater(int lotId) {
        Map<String, Object> args = new HashMap<>(3);
        args.put("lotId", lotId);
        args.put("payLater", true);
        args.put("confirmed", true);

        setLotConfirmationStatus(args);
    }

    private void setLotConfirmationStatus(final Map<String, Object> args) {
        mLotAcceptUi.toggleDialogProgress(true);

        RxUseCase.callback(mAcceptLotUseCase, args).register(new RxUseCase.AsyncCallback<Boolean>() {
            @Override
            public void onResultOk(Boolean confirmed) {
                mLotAcceptUi.toggleDialogProgress(false);
                mLotAcceptUi.finishLotConfirmation(confirmed);
            }

            @Override
            public void onError(Throwable throwable) {
                mLotAcceptUi.toggleDialogProgress(false);
                mLotAcceptUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onPay() {
        mLotAcceptUi.openPaymentWebView();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }
}
