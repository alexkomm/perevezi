/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments.search;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import ru.perevezi.app.R;

public class SearchByIdFragment extends DialogFragment implements View.OnClickListener {


    public static final String EXTRA_LOT_ID = "lot_id";

    EditText mLotIdEditText;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_search_by_id, null);

        mLotIdEditText = (EditText) view.findViewById(R.id.et_id);
        view.findViewById(R.id.btn_search).setOnClickListener(this);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setView(view);

        return adb.create();
    }


    @Override
    public void onClick(View v) {
        final String lotId = mLotIdEditText.getText().toString();

        if (lotId.isEmpty()) {
            mLotIdEditText.setError(getResources().getString(R.string.error_field_required));
            return;
        }

        Intent intent = new Intent();
        intent.putExtra(EXTRA_LOT_ID, lotId);

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismiss();
    }
}
