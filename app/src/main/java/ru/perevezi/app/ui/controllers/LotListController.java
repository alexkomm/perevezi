/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.controllers;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.Constants;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.model.mapper.LotModelMapper;
import ru.perevezi.app.rx.RxUseCase;
import ru.perevezi.app.ui.views.LotListUi;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.interactor.impl.GetLotsByIdUseCase;
import ru.perevezi.domain.interactor.impl.SearchLotsUseCase;

public class LotListController implements Controller<LotListUi>, LotListUi.UiCallbacks {

    private final SearchLotsUseCase mSearchLotsUseCase;
    private final GetLotsByIdUseCase mGetLotsByIdUseCase;
    private final LotModelMapper mLotModelMapper;
    private LotListUi mLotListUi;

    private HashMap<String, Object> mFilterOptions = new HashMap<>();

    @Inject
    public LotListController(SearchLotsUseCase searchLotsUseCase, GetLotsByIdUseCase getLotsByIdUseCase, LotModelMapper lotModelMapper) {
        mSearchLotsUseCase = searchLotsUseCase;
        mGetLotsByIdUseCase = getLotsByIdUseCase;
        mLotModelMapper = lotModelMapper;
    }

    @Override
    public void initialize(LotListUi lotListUi) {
        mLotListUi = lotListUi;
        mLotListUi.setUiCallbacks(this);

        mFilterOptions = mLotListUi.getFilterOptions();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onVisibleThresholdReached(int offset) {
        mFilterOptions.put("offset", Integer.toString(offset));
        mLotListUi.toggleToolbarProgress(true);
        onSearchLotsByFilters(true, false);
    }

    @Override
    public void onSearchLotsByIds(String[] lotIds) {
        mLotListUi.toggleToolbarProgress(true);

        RxUseCase.callback(mGetLotsByIdUseCase, lotIds).register(new RxUseCase
                .AsyncCallback<List<PereveziLot>>() {

            @Override
            public void onResultOk(List<PereveziLot> result) {
                mLotListUi.toggleToolbarProgress(false);
                mLotListUi.setDynamicLoadingEnabled(false);
                mLotListUi.addLotsToList((List<LotModel>) mLotModelMapper.mapResponse(result),
                        true);
            }

            @Override
            public void onError(Throwable throwable) {
                mLotListUi.toggleToolbarProgress(false);
                mLotListUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @Override
    public void onSearchLotsByFilters(boolean resetCache, boolean swipe) {
        if (!swipe) {
            mLotListUi.toggleToolbarProgress(true);
        }

        if (resetCache) {
            mFilterOptions.put("reset", true);
        } else {
            mFilterOptions.put("reset", false);
        }

        mFilterOptions.put("limit", Constants.PEREVEZI_SEARCH_LOTS_LIST_LIMIT);

        RxUseCase.callback(mSearchLotsUseCase, mFilterOptions).register(new RxUseCase
                .AsyncCallback<List<PereveziLot>>() {

            @Override
            public void onResultOk(List<PereveziLot> result) {
                mLotListUi.toggleToolbarProgress(false);
                mLotListUi.setDynamicLoadingEnabled(true);

                boolean clearList = mFilterOptions.get("offset") == null || mFilterOptions.get
                        ("offset").equals("0");

                mLotListUi.addLotsToList((List<LotModel>) mLotModelMapper.mapResponse(result),
                        clearList);
            }

            @Override
            public void onError(Throwable throwable) {
                mLotListUi.toggleToolbarProgress(false);
                mLotListUi.showMessage(throwable.getLocalizedMessage());
            }
        });
    }

    @NonNull
    private RxUseCase.AsyncCallback<List<PereveziLot>> getSearchResultCallback(final boolean clearList) {
        return new RxUseCase
                    .AsyncCallback<List<PereveziLot>>() {

                @Override
                public void onResultOk(List<PereveziLot> result) {
                    mLotListUi.toggleToolbarProgress(false);
                    mLotListUi.addLotsToList((List<LotModel>) mLotModelMapper.mapResponse(result),
                            clearList);
                }

                @Override
                public void onError(Throwable throwable) {
                    mLotListUi.toggleToolbarProgress(false);
                    mLotListUi.showMessage(throwable.getLocalizedMessage());
                }
            };
    }

    @Override
    public void onRefresh(boolean swipe) {
        mFilterOptions.put("offset", "0");
        onSearchLotsByFilters(true, swipe);
    }
}
