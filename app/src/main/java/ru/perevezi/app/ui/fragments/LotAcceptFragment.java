/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.parceler.Parcels;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.InjectView;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.ui.base.BaseFragment;
import ru.perevezi.app.ui.components.SimpleFragmentConfirmDialog;
import ru.perevezi.app.ui.controllers.LotAcceptController;
import ru.perevezi.app.ui.fragments.lot.LotHostListener;
import ru.perevezi.app.ui.views.LotAcceptUi;
import ru.perevezi.app.utils.MathUtils;
import ru.perevezi.app.utils.ViewUtils;

public class LotAcceptFragment extends BaseFragment implements LotAcceptUi, View.OnClickListener {

    public static final int CONFIRM_ALLOW = 0;
    public static final int CONFIRM_CREDIT = 1;
    public static final int CONFIRM_DENY = 2;
    public static final int CONFIRM_DENY_CREDIT = 3;

    private static final int DECLINE_DIALOG = 1;

    @Inject
    LotAcceptController mLotAcceptController;

    @Inject
    SettingsState mSettingsState;

    @InjectView(R.id.lot_accept_group)
    ViewGroup mLotAcceptViewGroup;

    @InjectView(R.id.confirm)
    Button mConfirmButton;

    @InjectView(R.id.pay_later_restrict_comment)
    TextView mPayLaterRestrictComment;

    @InjectView(R.id.lot_accept_pay_later_btn)
    Button mPayLaterButton;

    @InjectView(R.id.lot_accept_decline_btn)
    Button mDeclineButton;

    @InjectView(R.id.lot_accept_text)
    TextView mLotAcceptText;

    SimpleFragmentConfirmDialog mDeclineDialog;

    private static final String ARG_KEY_BID = "lot";
    public static final String TAG = "lot_accept";

    private UiCallbacks mUiCallbacks;
    private HashMap<String, Object> mConfirmInfo;
    private BidModel mBidModel;
    private LotHostListener mLotHostController;

    public static LotAcceptFragment newInstance(BidModel bidModel) {
        LotAcceptFragment fragment = new LotAcceptFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_KEY_BID, Parcels.wrap(bidModel));

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PereveziApplication.from(getActivity()).inject(this);

        Bundle args = getArguments();

        if (args != null) {
            mBidModel = Parcels.unwrap(args.getParcelable(ARG_KEY_BID));
        }

        mLotAcceptController.initialize(this);

        if (savedInstanceState != null) {
            mConfirmInfo = (HashMap<String, Object>) savedInstanceState.getSerializable("confirm_info");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mLotHostController.toggleLotActionView(false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mLotHostController = (LotHostListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement LotHostController");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mLotHostController = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.lot_accept_title);

        if (mConfirmInfo == null) {
            mLotAcceptController.onCreate(mBidModel.getBid());
        } else {
            showLotAcceptForm(mConfirmInfo);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("confirm_info", mConfirmInfo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case DECLINE_DIALOG:
                if (resultCode == Activity.RESULT_OK) {
                    mUiCallbacks.onDecline(mBidModel.getNid());
                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lot_accept, container, false);
    }

    @Override
    public void setUiCallbacks(UiCallbacks uiCallbacks) {
        mUiCallbacks = uiCallbacks;
    }

    @Override
    public void showLotAcceptForm(Map<String, Object> result) {
        mConfirmInfo = (HashMap<String, Object>) result;

        mLotAcceptViewGroup.setVisibility(View.VISIBLE);

        BigDecimal balance    = MathUtils.getBigDecimal(mConfirmInfo.get("balance"));
        BigDecimal commission = MathUtils.getBigDecimal(mConfirmInfo.get("commission"));
        BigDecimal amount     = MathUtils.getBigDecimal(mConfirmInfo.get("amount"));

        mLotAcceptText.setText(getString(R.string.lot_accept_text, ViewUtils.formatCurrency
                (amount.doubleValue()), ViewUtils.formatCurrency(commission.doubleValue()),
                ViewUtils.formatCurrency(balance.doubleValue())));

        int confirmStatus = (int) mConfirmInfo.get("confirm_status");

        switch (confirmStatus) {
            case CONFIRM_ALLOW:
                mConfirmButton.setText(R.string.lot_accept_confirm_btn);
                mConfirmButton.setId(R.id.lot_accept_confirm_btn);
                break;
            case CONFIRM_CREDIT:
                mPayLaterButton.setVisibility(View.VISIBLE);
                break;
            case CONFIRM_DENY_CREDIT:
                BigDecimal paylaterDeposit = mSettingsState.getGeneralSettings()
                        .getPayLaterDeposit();
                mPayLaterRestrictComment.setText(getString(R.string
                        .lot_accept_pay_later_restrict, ViewUtils.formatCurrency(paylaterDeposit
                        .doubleValue())));
                mPayLaterRestrictComment.setVisibility(View.VISIBLE);
                break;
            case CONFIRM_DENY:
                mConfirmButton.setId(R.id.lot_accept_pay_btn);
                mConfirmButton.setText(R.string.lot_accept_pay_btn);
                break;
        }

        mConfirmButton.setOnClickListener(this);
        mPayLaterButton.setOnClickListener(this);
        mDeclineButton.setOnClickListener(this);
    }

    @Override
    public void openPaymentWebView() {
        BigDecimal commission = MathUtils.getBigDecimal(mConfirmInfo.get("commission"));

        Bundle actionArgs = new Bundle();
        actionArgs.putDouble("amount", commission.doubleValue());

        SiteViewFragment siteViewFragment = SiteViewFragment.newInstance(SiteViewFragment
                .Action.PAYMENT_LIST_PAGE, null);

        siteViewFragment.setSiteViewListener(new SiteViewFragment.SiteViewListener() {
            @Override
            public void onFinalDialogClosed() {
                getRouterUi().navigateToLotScreen(mBidModel.getNid());
                getActivity().finish();
            }
        });

        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, siteViewFragment, SiteViewFragment.TAG).addToBackStack(SiteViewFragment.TAG).commit();
    }

    @Override
    public void finishLotConfirmation(boolean confirmed) {
        showMessage(getResources().getString(confirmed ? R.string.lot_confirmed_success : R.string.lot_declined_success));

        getRouterUi().navigateToLotScreen(mBidModel.getNid());
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lot_accept_confirm_btn:
                mUiCallbacks.onConfirm(mBidModel.getNid());
                break;
            case R.id.lot_accept_decline_btn:
                if (mDeclineDialog == null) {
                    mDeclineDialog = SimpleFragmentConfirmDialog.newInstance(R.string.lot_decline_dialog_title, R.string.lot_decline_dialog_message, 0, 0);
                    mDeclineDialog.setTargetFragment(this, DECLINE_DIALOG);
                }
                
                FragmentManager fm = getActivity().getSupportFragmentManager();
                
                mDeclineDialog.show(fm, "dialog");
                break;
            case R.id.lot_accept_pay_btn:
                mUiCallbacks.onPay();
                break;
            case R.id.lot_accept_pay_later_btn:
                mUiCallbacks.onPayLater(mBidModel.getNid());
                break;
        }


    }


}
