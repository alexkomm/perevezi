/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.state;

import ru.perevezi.app.model.SettingsModel;

public interface SettingsState {

    SettingsModel getGeneralSettings();

    void setGeneralSettings(SettingsModel settings);
}
