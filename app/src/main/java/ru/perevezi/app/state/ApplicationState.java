/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.state;

import android.os.Bundle;

import org.parceler.Parcels;

import ru.perevezi.app.events.AccountChangedEvent;
import ru.perevezi.app.events.SettingsChangedEvent;
import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.domain.utils.Preconditions;

public class ApplicationState implements UserState, SettingsState, Saveable {

    private AccountModel mAccount;

    private RxBus mBus;

    private SettingsModel mGeneralSettings;

    private boolean mConfirmDialogShown = false;

    private int mDrawerCurrentSelectionIdentifier = -1;

    public ApplicationState(RxBus bus) {
        Preconditions.checkState(bus != null, "event bus cannot be null");

        mBus = bus;
    }

    ////////////////////////////////////////////////////////////////////////
    // User State
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void setCurrentAccount(AccountModel account) {
        if (account != null && (mAccount == null || !mAccount.equals(account))) {
            mAccount = account;
            mBus.send(new AccountChangedEvent());
        }
    }

    @Override
    public AccountModel getCurrentAccount() {
        return mAccount;
    }

    @Override
    public boolean isPhoneConfirmDialogShown() {
        return mConfirmDialogShown;
    }

    @Override
    public void setPhoneConfirmDialogShown(boolean shown) {
        mConfirmDialogShown = shown;
    }

    @Override
    public void setDrawerCurrentSelectedIdenitifer(int idenitifer) {
        mDrawerCurrentSelectionIdentifier = idenitifer;
    }

    @Override
    public int getDrawerCurrentSelectedIdentifier() {
        return mDrawerCurrentSelectionIdentifier;
    }

    ////////////////////////////////////////////////////////////////////////
    // Settings State
    ////////////////////////////////////////////////////////////////////////

    @Override
    public SettingsModel getGeneralSettings() {
        return mGeneralSettings;
    }

    @Override
    public void setGeneralSettings(SettingsModel settings) {
        if (settings != null && (mGeneralSettings == null || mGeneralSettings.equals(settings))) {
            mGeneralSettings = settings;
            mBus.send(new SettingsChangedEvent());
        }
    }


    @Override
    public void saveInstanceState(Bundle bundle) {
        bundle.putParcelable("settings", Parcels.wrap(mGeneralSettings));
        bundle.putParcelable("account", Parcels.wrap(mAccount));
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        mGeneralSettings = Parcels.unwrap(bundle.getParcelable("settings"));
        mAccount = Parcels.unwrap(bundle.getParcelable("account"));
    }
}
