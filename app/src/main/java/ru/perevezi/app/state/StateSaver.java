/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.state;

import android.os.Bundle;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class StateSaver implements Saveable {

    private final List<? extends Saveable> mSaveables;

    @Inject
    public StateSaver(ApplicationState applicationState) {
        mSaveables = (Arrays.asList(
                applicationState
        ));
    }

    @Override
    public void saveInstanceState(Bundle bundle) {
        Bundle instanceState = new Bundle();
        for (Saveable saveable : mSaveables) {
            saveable.saveInstanceState(instanceState);
        }
        bundle.putBundle("global-state", instanceState);
    }

    @Override
    public void restoreInstanceState(Bundle bundle) {
        if (bundle == null) {
            return;
        }

        Bundle instanceState = bundle.getBundle("global-state");
        if (instanceState == null) {
            return;
        }

        for (Saveable saveable : mSaveables) {
            saveable.restoreInstanceState(instanceState);
        }
    }
}
