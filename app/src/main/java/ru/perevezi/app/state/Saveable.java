/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.state;

import android.os.Bundle;

public interface Saveable {

    void saveInstanceState(Bundle bundle);

    void restoreInstanceState(Bundle bundle);

}
