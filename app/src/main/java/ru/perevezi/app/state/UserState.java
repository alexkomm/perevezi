/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.state;

import ru.perevezi.app.model.AccountModel;

public interface UserState {

    boolean isPhoneConfirmDialogShown();

    void setPhoneConfirmDialogShown(boolean shown);

    AccountModel getCurrentAccount();

    void setCurrentAccount(AccountModel accountModel);

    void setDrawerCurrentSelectedIdenitifer(int idenitifer);

    int getDrawerCurrentSelectedIdentifier();
}
