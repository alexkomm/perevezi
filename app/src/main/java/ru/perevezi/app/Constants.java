/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app;

public class Constants {

    public static final String PEREVEZI_ACCOUNT_TYPE = BuildConfig.APPLICATION_ID;

    public static final String PEREVEZI_AUTH_TOKEN_TYPE = PEREVEZI_ACCOUNT_TYPE;

    public static final int PEREVEZI_SEARCH_LOTS_LIST_LIMIT = 10;

    public static final int PEREVEZI_MY_LOTS_LIST_LIMIT = 10;

    public static final int PEREVEZI_MY_BIDS_LIST_LIMIT = 10;

    public static final int PEREVEZI_REVIEWS_LIST_LIMIT = 10;

    public static final int PEREVEZI_CAR_MOVING_CATEGORY_ID = 3;

    public static final int PEREVEZI_USER_ONLINE_ACTIVITY_INTERVAL_MINUTES = 5;
}
