/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app;


import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;
import io.fabric.sdk.android.Fabric;
import ru.perevezi.app.di.ApplicationModule;
import ru.perevezi.app.di.ContextModule;
import ru.perevezi.domain.utils.Preconditions;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Our application class with dependency management
 */
public class PereveziApplication extends Application implements Injector {

    private static Context mContext;

    public static PereveziApplication from(Context context) {
        return (PereveziApplication) context.getApplicationContext();
    }

    private ObjectGraph mObjectGraph;

    /**
     * Injects this Application using the created graph.
     * <p/>
     * Sets strict mode if needed.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        Fabric.with(this, crashlyticsKit);
        mContext = this;

        mObjectGraph = ObjectGraph.create(getModules().toArray());
        inject(this);

        if ((getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
            mObjectGraph.validate();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    public static int getAppVersion() {
        try {
            PackageInfo packageInfo = mContext.getPackageManager()
                    .getPackageInfo(mContext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    /**
     * Return this Application's object graph
     *
     * @return Application's object graph
     */
    @Override
    public ObjectGraph getObjectGraph() {
        return mObjectGraph;
    }


    /**
     * Injects the target object using this Application's object graph
     *
     * @param target the target object
     */
    @Override
    public void inject(Object target) {
        Preconditions.checkState(mObjectGraph != null, "Object graph should be initialized before injecting");
        mObjectGraph.inject(target);
    }


    /**
     * Returns the list of dagger modules to be included in this Application's object graph.  Subclasses that override
     * this method should add to the list returned by super.getModules().
     *
     * @return the list of modules
     */
    protected List<Object> getModules() {
        List<Object> result = new ArrayList<>();
        result.add(new ContextModule(this));
        result.add(new ApplicationModule(this, this));
        return result;
    }


    public static Context getContext() {
        return mContext;
    }
}
