/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

public class DateUtils {

    public static final String DATE_FORMAT = "dd.MM.yyyy";

    public static final String DATE_WITH_TIME_FORMAT = "dd.MM.yyyy HH:mm";

    public static final String TIME_FORMAT = "kk:mm";

    public static long[] getIntervalData(long start, long end){

        //milliseconds
        long different = start - end;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return new long[]{elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds};
    }
}
