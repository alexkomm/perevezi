/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.LotModel;

public class StringManager {

    public static int getTimeUnitStringResId(int pos) {
        switch (pos) {
            case 0:
                return R.plurals.day_no_count;
            case 1:
                return R.plurals.hour_no_count;
            case 2:
                return R.plurals.minute_no_count;
            case 3:
                return R.plurals.second_no_count;
            default:
                return 0;
        }
    }

    public static int getWeightUnitStringResId(String wu) {
        switch (wu) {
            case "kg":
                return R.string.kg;
            case "ton":
                return R.string.ton;
            default:
                return 0;
        }
    }

    public static int getLotStatusPagerTitleResId(Object status) {

        try {
            int intStatus = (int) status;

            switch (intStatus) {
                case LotModel.LOT_STATUS_ACTIVE:
                    return R.string.lot_status_page_active;
                case LotModel.LOT_STATUS_CANCELED:
                    return R.string.lot_status_page_canceled;
                case LotModel.LOT_STATUS_COMPLETED:
                    return R.string.lot_status_page_completed;
                case LotModel.LOT_STATUS_EXPIRED:
                    return R.string.lot_status_page_expired;
                case LotModel.LOT_STATUS_IN_PROCESS:
                    return R.string.lot_status_page_in_process;
                case LotModel.LOT_STATUS_NEW:
                    return R.string.lot_status_page_new;
                case LotModel.LOT_STATUS_WON:
                    return R.string.lot_status_page_won;
                default:
                    return 0;
            }

        } catch (NumberFormatException e) {

            if (status.equals("FAVORITES")) {
                return R.string.lot_status_page_favorites;
            }
        }

        return 0;
    }

    public static int getReviewSectionPagerTitleResId(int section) {

        switch (section) {
            case -1:
                return R.string.review_section_negative;
            case 0:
                return R.string.review_section_neutral;
            case 1:
                return R.string.review_section_positive;
            default:
                return R.string.review_section_all;
        }
    }

    public static Drawable getReviewSectionDrawable(int section, Context context) {

        switch (section) {
            case -1:
                return new IconicsDrawable(context)
                        .icon(FontAwesome.Icon.faw_minus_circle)
                        .color(ContextCompat.getColor(context, R.color.error))
                        .sizeDp(16);
            case 1:
                return new IconicsDrawable(context)
                        .icon(FontAwesome.Icon.faw_plus_circle)
                        .color(ContextCompat.getColor(context, R.color.accent))
                        .sizeDp(16);
            case 0:
                return new IconicsDrawable(context)
                        .icon(FontAwesome.Icon.faw_circle_o)
                        .color(ContextCompat.getColor(context, R.color.primary_dark))
                        .sizeDp(16);
            default:
                return null;
        }

    }

    public static int getLotStatusStrResId(int status) {
        switch (status) {
            case LotModel.LOT_STATUS_ACTIVE:
                return R.string.lot_status_active;
            case LotModel.LOT_STATUS_CANCELED:
                return R.string.lot_status_canceled;
            case LotModel.LOT_STATUS_COMPLETED:
                return R.string.lot_status_completed;
            case LotModel.LOT_STATUS_EXPIRED:
                return R.string.lot_status_expired;
            case LotModel.LOT_STATUS_IN_PROCESS:
                return R.string.lot_status_in_process;
            case LotModel.LOT_STATUS_NEW:
                return R.string.lot_status_new;
            case LotModel.LOT_STATUS_WON:
                return R.string.lot_status_won;
            default:
                return 0;
        }
    }


    public static int getBidSectionTitleResId(String section) {
        switch (section) {
            case "ACTIVE":
                return R.string.bid_section_active;
            case "WON":
                return R.string.bid_section_won;
            case "INCOMPLETE":
                return R.string.bid_section_incomplete;
            case "COMPLETE":
                return R.string.bid_section_complete;
            case "DELETED":
                return R.string.bid_section_deleted;
            case "FINISHED":
                return R.string.bid_section_finished;
            case "FAVORITES":
                return R.string.bid_section_favorites;
            default:
                return 0;

        }
    }

    public static int getBidStatusDrawableSmall(int bidStatus) {
        switch (bidStatus) {
            case BidModel.BID_STATUS_NEW:
                return R.drawable.pic_active_16dp;
            case BidModel.BID_STATUS_WON:
                return R.drawable.pic_win_16dp;
            case BidModel.BID_STATUS_ACCEPTED:
                return R.drawable.pic_ontheway_16dp;
            case BidModel.BID_STATUS_COMPLETE:
                return R.drawable.pic_complete_16dp;
            case BidModel.BID_STATUS_CANCELED:
                return R.drawable.pic_canceled_16dp;
            case BidModel.BID_STATUS_INCOMPLETE:
            case BidModel.BID_STATUS_DECLINED:
                return R.drawable.pic_uncomplete_16dp;
            default:
                return 0;
        }
    }

    public static int getBidStatusStrResId(int bidStatus) {
        switch (bidStatus) {
            case BidModel.BID_STATUS_DECLINED:
                return R.string.bid_status_declined;
            case BidModel.BID_STATUS_CANCELED:
                return R.string.bid_status_canceled;
            case BidModel.BID_STATUS_WON:
                return R.string.bid_status_won;
            case BidModel.BID_STATUS_NEW:
                return R.string.bid_status_new;
            case BidModel.BID_STATUS_ACCEPTED:
                return R.string.bid_status_accepted;
            case BidModel.BID_STATUS_COMPLETE:
                return R.string.bid_status_complete;
            case BidModel.BID_STATUS_DELETED:
                return R.string.bid_status_deleted;
            case BidModel.BID_STATUS_EXPIRED:
                return R.string.bid_status_expired;
            case BidModel.BID_STATUS_INCOMPLETE:
                return R.string.bid_status_incomplete;
            case BidModel.BID_STATUS_OBSOLETE:
                return R.string.bid_status_obsolete;
            case BidModel.BID_STATUS_REJECTED:
                return R.string.bid_status_rejected;
            default:
                return 0;
        }
    }


    public static int getReviewScreenTitleResId(boolean forProvider) {
        return forProvider ? R.string.review_for_provider_title : R.string.review_for_client_title;
    }

    public static int getReviewScreenLabelResId(boolean forProvider) {
        return forProvider ? R.string.review_label_for_provider : R.string.review_label_for_customer;
    }
}
