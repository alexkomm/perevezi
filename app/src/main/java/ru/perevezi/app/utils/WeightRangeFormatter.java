/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.content.res.Resources;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;

public class WeightRangeFormatter implements RangeFormatter<Float> {

    private final Resources mResources;

    public WeightRangeFormatter() {
        mResources = PereveziApplication.getContext().getResources();
    }

    @Override
    public String format(Float val1, Float val2) {

        String result = "";

        if (val1 > 0) {
            result += mResources.getString(R.string.range_from_value, getFormattedSingleValue(val1)) + " ";
        }

        if (val2 > 0) {
            result += mResources.getString(R.string.range_to_value, getFormattedSingleValue(val2));
        }

        if (result.length() == 0) {
            result = mResources.getString(R.string.range_no_limit);
        }

        return result.trim();
    }

    private String getFormattedSingleValue(Float value) {
        return value > 999 ? mResources.getString(R.string.weight_ton_value, Math.round(value / 1000)) : mResources.getString(R.string.weight_kg_value, Math.round(value));
    }
}
