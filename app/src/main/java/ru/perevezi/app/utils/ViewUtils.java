/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import ru.perevezi.app.R;
import ru.perevezi.app.model.BidModel;
import ru.perevezi.app.model.LotModel;

public class ViewUtils {

    public static void hideSoftKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec
                .UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();

        if (listView.getDivider() != null) {
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        } else {
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        }

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String formatCurrency(double value) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setCurrency(Currency.getInstance(new Locale("ru", "RU")));
        nf.setGroupingUsed(false);
        nf.setMaximumFractionDigits(0);

        return nf.format(value);
    }

    public static CharSequence formatCurrencyWithSymbol(Context context, double value) {
        NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
        nf.setGroupingUsed(false);
        nf.setMaximumFractionDigits(0);

        CharSequence amount = nf.format(value) + "  ";

        SpannableString sp = new SpannableString(amount);
        IconicsDrawable d = new IconicsDrawable(context, FontAwesome.Icon.faw_rub).color(ContextCompat.getColor(context, R.color.primary_text)).sizeDp(12);

        sp.setSpan(new ImageSpan(d, DynamicDrawableSpan.ALIGN_BASELINE), amount.length() - 1,
                amount.length(), Spannable
                        .SPAN_EXCLUSIVE_EXCLUSIVE);

        return sp;
    }


    public static void inflateLotContactsAndBidView(final LotModel lotModel, View parent, final Context context) {
        BidModel myBid = lotModel.getRelatedBid();

        View contactsGroup = parent.findViewById(R.id.view_lot_contacts_group);
        View myBidGroup = parent.findViewById(R.id.view_lot_my_bid_group);
        View declineReasonGroup = parent.findViewById(R.id.bid_decline_reason_group);

        if (contactsGroup != null) {
            contactsGroup.setVisibility(View.GONE);
        }

        if (myBidGroup != null) {
            myBidGroup.setVisibility(View.GONE);
        }

        if (declineReasonGroup != null) {
            declineReasonGroup.setVisibility(View.GONE);
        }

        if (myBid != null) {
            if (lotModel.getAuthorPhone() != null) {

                ViewStub contactsStub = (ViewStub) parent.findViewById(R.id.contacts_stub);

                if (contactsStub != null) {
                    contactsStub.setVisibility(View.VISIBLE);
                } else if (contactsGroup != null) {
                    contactsGroup.setVisibility(View.VISIBLE);
                }

                TextView bidAmountText = (TextView) parent.findViewById(R.id.bid_amount_text);
                bidAmountText.setText(ViewUtils.formatCurrency(myBid.getAmount().doubleValue()));

                TextView bidAuthorNameText = (TextView) parent.findViewById(R.id.bid_author_name_text);
                bidAuthorNameText.setText(lotModel.getAuthor().getFullName());

                TextView bidAuthorPhoneText = (TextView) parent.findViewById(R.id.bid_author_phone_text);
                bidAuthorPhoneText.setText(lotModel.getAuthorPhone());

                Button callToCustomerButton = (Button) parent.findViewById(R.id.call_to_customer_btn);
                callToCustomerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String uri = "tel:" + lotModel.getAuthorPhone();
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse(uri));
                        context.startActivity(intent);
                    }
                });

            } else {

                ViewStub myBidStub = (ViewStub) parent.findViewById(R.id.my_bid_stub);

                if (myBidStub != null) {
                    myBidStub.setVisibility(View.VISIBLE);
                } else if (myBidGroup != null) {
                    myBidGroup.setVisibility(View.VISIBLE);
                }

                TextView bidAmountText = (TextView) parent.findViewById(R.id.my_bid_text);
                bidAmountText.setText(ViewUtils.formatCurrency(myBid.getAmount().doubleValue()));

                if (myBid.getStatus() == BidModel.BID_STATUS_NEW && lotModel.getMinBid().doubleValue() > 0) {
                    int color;

                    if (myBid.getAmount().doubleValue() <=  lotModel.getMinBid().doubleValue()) {
                        color = context.getResources().getColor(R.color.accent);
                    } else {
                        color = context.getResources().getColor(R.color.error);
                    }

                    bidAmountText.setTextColor(color);
                }


                int bidDrawableResId = StringManager.getBidStatusDrawableSmall(myBid.getStatus());
                ((ImageView)parent.findViewById(R.id.bid_status_drawable)).setImageResource(bidDrawableResId);

                if (myBid.getStatus() == BidModel.BID_STATUS_DECLINED && !com.google.common.base.Strings.isNullOrEmpty
                        (myBid.getDeclineReason())) {
                    ViewStub declineReasonStub = (ViewStub) parent.findViewById(R.id.my_bid_decline_reason_stub);

                    if (declineReasonStub != null) {
                        declineReasonStub.setVisibility(View.VISIBLE);
                    } else if (declineReasonGroup != null) {
                        declineReasonGroup.setVisibility(View.VISIBLE);
                    }

                    String declineRasonString = context.getString(R.string.reason_label) + " " + myBid.getDeclineReason();

                    ((TextView)parent.findViewById(R.id.my_bid_decline_reason_text)).setText(declineRasonString);
                }
            }
        }
    }

}
