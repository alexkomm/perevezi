/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

public interface RangeFormatter<T> {
    String format(T val1, T val2);
}
