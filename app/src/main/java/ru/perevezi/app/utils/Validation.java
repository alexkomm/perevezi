/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.util.Patterns;

public class Validation {

    public static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        }

        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPhone(String phone) {
        if (phone == null) {
            return false;
        }

        return Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isEmpty(String field) {
        if (field == null) {
            return true;
        }

        return Strings.isEmpty(field);
    }

}
