/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.content.res.Resources;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;

public class DistanceRangeFormatter implements RangeFormatter<Float> {

    private final Resources mResources;

    public DistanceRangeFormatter() {
        mResources = PereveziApplication.getContext().getResources();
    }

    @Override
    public String format(Float val1, Float val2) {
        String result = "";
        String distanceValue;


        if (val1 > 0) {
            distanceValue = mResources.getString(R.string.distance_value, Math.round(val1));
            result += mResources.getString(R.string.range_from_value, distanceValue) + " ";
        }

        if (val2 > 0) {
            distanceValue = mResources.getString(R.string.distance_value, Math.round(val2));
            result += mResources.getString(R.string.range_to_value, distanceValue);
        }

        if (result.length() == 0) {
            result = mResources.getString(R.string.range_no_limit);
        }

        return result.trim();
    }
}
