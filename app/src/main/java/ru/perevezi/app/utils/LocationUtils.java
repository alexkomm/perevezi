/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.utils;

import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;
import static android.location.Criteria.ACCURACY_FINE;
import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;
import static android.location.LocationManager.PASSIVE_PROVIDER;

/**
 * Utilities for dealing with the location service
 */
public class LocationUtils {

    private static final String GEOCODE_API_BASE = "https://maps.googleapis.com/maps/api/geocode";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAILS = "/details";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyAci_Ssksq1B5WRFOiWb9wzIcZDquiucYk";


    /**
     * Get the location with the later date
     *
     * @param location1
     * @param location2
     * @return location
     */
    private static Location getLatest(final Location location1,
                                      final Location location2) {
        if (location1 == null)
            return location2;

        if (location2 == null)
            return location1;

        if (location2.getTime() > location1.getTime())
            return location2;
        else
            return location1;
    }

    /**
     * Get the latest location trying multiple providers
     * <p/>
     * Calling this method requires that your application's manifest contains the
     * {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission
     *
     * @param context
     * @return latest location set or null if none
     */
    public static Location getLatestLocation(final Context context, String provider) {
        LocationManager manager = (LocationManager) context
                .getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(ACCURACY_FINE);
        if (provider == null) {
            provider = manager.getBestProvider(criteria, true);
        }
        Location bestLocation;
        if (provider != null)
            bestLocation = manager.getLastKnownLocation(provider);
        else
            bestLocation = null;
        Location latestLocation = getLatest(bestLocation,
                manager.getLastKnownLocation(GPS_PROVIDER));
        latestLocation = getLatest(latestLocation,
                manager.getLastKnownLocation(NETWORK_PROVIDER));
        latestLocation = getLatest(latestLocation,
                manager.getLastKnownLocation(PASSIVE_PROVIDER));
        return latestLocation;
    }


    public static List<CharSequence> formatRouteItems(List<Address> addressList) {
        if (addressList == null) {
            return null;
        }

        List<CharSequence> result = new ArrayList<>(addressList.size());

        boolean showLocality = false;

        CharSequence prevLocality = null;


        for (Address address : addressList) {
            final CharSequence locality = address.getLocality();

            if (locality == null) {
                continue;
            }

            if (prevLocality != null && !locality.equals(prevLocality)) {
                showLocality = true;
            }

            prevLocality = address.getLocality();
        }

        for (Address address : addressList) {
            final CharSequence locality = address.getLocality();

            result.add(showLocality && locality != null ? locality : formatAddress(address));
        }

        return result;
    }

    public static String formatAddress(Address address) {
        if (address == null) {
            return "";
        }

        if (address.getMaxAddressLineIndex() > -1) {
            return formatAddressFromAddressLines(address);
        } else {
            List<CharSequence> addressFragments = new ArrayList<>();

            String addressStr = "";

            if (address.getThoroughfare() != null) {
                addressStr += address.getThoroughfare();
            }

            if (address.getSubThoroughfare() != null) {
                addressStr += " " + address.getSubThoroughfare();
            }

            if (!addressStr.isEmpty()) {
                addressFragments.add(addressStr);
            }

            final CharSequence locality = address.getLocality();

            if (locality != null) {
                addressFragments.add(locality);
            }

            if (address.getAdminArea() != null) {
                addressFragments.add(address.getAdminArea());
            }

            return Strings.join(", ", addressFragments);
        }
    }

    private static String formatAddressFromAddressLines(Address address) {
        if (address == null) {
            return "";
        }

        ArrayList<String> addressFragments = new ArrayList<>();

        // Fetch the address lines using getAddressLine,
        // join them, and send them to the thread.
        for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
            if (com.google.common.base.Strings.isNullOrEmpty(address.getAddressLine(i))) {
                continue;
            }

            addressFragments.add(address.getAddressLine(i));
        }

        return Strings.join(", ", addressFragments);
    }

    public static Address geocodeAddress(String locationName) {
        List<Address> addressList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            URL url = new URL(GEOCODE_API_BASE + OUT_JSON + "?key=" + API_KEY + "&language=ru" +
                    "&address=" + URLEncoder.encode(locationName, "utf8"));
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Ln.e("Error processing Geocode API URL", e);
            return null;
        } catch (IOException e) {
            Ln.e("Error connecting to Geocode API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray resultsJsonArray = jsonObj.getJSONArray("results");

            addressList = new ArrayList<>(resultsJsonArray.length());

            for (int i = 0; i < resultsJsonArray.length(); i++) {
                addressList.add(parseAddressComponent(resultsJsonArray.getJSONObject(i)));
            }

        } catch (JSONException e) {
            Ln.e("Cannot process JSON results", e);
        }

        if (addressList.size() > 0) {
            return addressList.get(0);
        }

        return null;
    }



    public static Address reverseGeocodeAddress(Location location) {
        List<Address> addressList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            URL url = new URL(GEOCODE_API_BASE + OUT_JSON + "?key=" + API_KEY + "&language=ru" +
                    "&latlng=" + Strings.join(",", location.getLatitude(),
                    location.getLongitude()));
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Ln.e("Error processing Geocode API URL", e);
            return null;
        } catch (IOException e) {
            Ln.e("Error connecting to Geocode API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray resultsJsonArray = jsonObj.getJSONArray("results");

            addressList = new ArrayList<>(resultsJsonArray.length());

            for (int i = 0; i < resultsJsonArray.length(); i++) {
                addressList.add(parseAddressComponent(resultsJsonArray.getJSONObject(i)));
            }

        } catch (JSONException e) {
            Ln.e("Cannot process JSON results", e);
        }


        return addressList.get(0);
    }


    public static ArrayList<Map<String, String>> autocomplete(String input, List<String> filterTypes) {
        ArrayList<Map<String, String>> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            URL url = new URL(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON +
                    "?sensor=false&key=" + API_KEY + "&language=ru" + "&types=geocode" +
                    "&input=" + URLEncoder.encode(input, "utf8"));
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Ln.e("Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Ln.e("Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                Map<String, String> map = new HashMap<>();

                map.put("placeid", predsJsonArray.getJSONObject(i).getString("place_id"));
                map.put("description", predsJsonArray.getJSONObject(i).getString("description"));

                if (filterTypes != null) {
                    JSONArray compTypesJsonArray = predsJsonArray.getJSONObject(i).getJSONArray("types");

                    for (int x = 0; x < compTypesJsonArray.length(); x++) {
                        if (filterTypes.contains(compTypesJsonArray.getString(x))) {
                            resultList.add(map);
                            break;
                        }
                    }
                } else {
                    resultList.add(map);
                }
            }
        } catch (JSONException e) {
            Ln.e("Cannot process JSON results", e);
        }

        return resultList;
    }

    public static Address fetchPlaceDetails(String placeId) {
        Address address = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {

            URL url = new URL(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON + "?key=" + API_KEY +
                    "&placeid=" + placeId + "&language=ru");
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Ln.e("Error processing Places API URL", e);
            return address;
        } catch (IOException e) {
            Ln.e("Error connecting to Places API", e);
            return address;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONObject resultJsonObj = jsonObj.getJSONObject("result");

            address = parseAddressComponent(resultJsonObj);

        } catch (JSONException e) {
            Ln.e("Cannot process JSON results", e);
        }

        return address;
    }

    private static Address parseAddressComponent(JSONObject resultJsonObj) throws JSONException {
        Address address;
        address = new Address(Locale.getDefault());
        address.setLatitude(resultJsonObj.getJSONObject("geometry").getJSONObject("location").getDouble("lat"));
        address.setLongitude(resultJsonObj.getJSONObject("geometry").getJSONObject("location").getDouble("lng"));

        JSONArray addressCompJsonArray = resultJsonObj.getJSONArray("address_components");
        for (int i = 0; i < addressCompJsonArray.length(); i++) {
            JSONArray compTypesJsonArray = addressCompJsonArray.getJSONObject(i).getJSONArray("types");

            for (int x = 0; x < compTypesJsonArray.length(); x++) {

                if (compTypesJsonArray.getString(x).equals("locality")) {
                    address.setLocality(addressCompJsonArray.getJSONObject(i).getString("long_name"));
                    break;
                }

                if (compTypesJsonArray.getString(x).equals("administrative_area_level_1")) {
                    address.setAdminArea(addressCompJsonArray.getJSONObject(i).getString
                            ("long_name"));
                    break;
                }

                if (compTypesJsonArray.getString(x).equals("country")) {
                    address.setCountryName(addressCompJsonArray.getJSONObject(i).getString
                            ("long_name"));
                    break;
                }

                if (compTypesJsonArray.getString(x).equals("street_address")) {
                    address.setThoroughfare(addressCompJsonArray.getJSONObject(i).getString
                            ("long_name"));
                    break;
                }

                if (compTypesJsonArray.getString(x).equals("route") && address.getThoroughfare() == null) {
                    address.setThoroughfare(addressCompJsonArray.getJSONObject(i).getString
                            ("long_name"));
                }

                if (compTypesJsonArray.getString(x).equals("street_number")) {
                    address.setSubThoroughfare(addressCompJsonArray.getJSONObject(i).getString
                            ("long_name"));
                    break;
                }
            }
        }
        return address;
    }
}
