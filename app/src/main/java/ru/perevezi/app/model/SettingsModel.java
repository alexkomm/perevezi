/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;

import org.parceler.Parcel;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Parcel
public class SettingsModel {

    List<Map<String, String>> mDeclineBidReasons;

    List<Map<String, String>> mLotLoadingOptions;

    List<Map<String, String>> mCarMovingOptions;

    List<Map<String, String>> mLotShippingOptions;

    List<CategoryModel> mCategories;

    BigDecimal mPayLaterDeposit;

    public SettingsModel() {
    }

    /**
     *
     * @return
     * The lotLoadingOptions
     */
    public List<Map<String, String>> getLotLoadingOptions() {
        return mLotLoadingOptions;
    }

    /**
     *
     * @param lotLoadingOptions
     * The lot_loading_options
     */
    public void setLotLoadingOptions(List<Map<String, String>> lotLoadingOptions) {
        mLotLoadingOptions = lotLoadingOptions;
    }

    public List<Map<String, String>> getCarMovingOptions() {
        return mCarMovingOptions;
    }

    public void setCarMovingOptions(List<Map<String, String>> carMovingOptions) {
        mCarMovingOptions = carMovingOptions;
    }

    public List<Map<String, String>> getLotShippingOptions() {
        return mLotShippingOptions;
    }

    public void setLotShippingOptions(List<Map<String, String>> lotShippingOptions) {
        mLotShippingOptions = lotShippingOptions;
    }

    public List<CategoryModel> getCategories() {
        return mCategories;
    }

    public void setCategories(List<CategoryModel> categories) {
        mCategories = categories;
    }

    public List<Map<String, String>> getDeclineBidReasons() {
        return mDeclineBidReasons;
    }

    public void setDeclineBidReasons(List<Map<String, String>> declineBidReasons) {
        mDeclineBidReasons = declineBidReasons;
    }

    public BigDecimal getPayLaterDeposit() {
        return mPayLaterDeposit;
    }

    public void setPayLaterDeposit(BigDecimal payLaterDeposit) {
        mPayLaterDeposit = payLaterDeposit;
    }
}
