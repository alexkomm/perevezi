/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;

import android.content.Context;
import android.location.Address;
import android.text.format.DateUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import ru.perevezi.data.entities.Lot;

@org.parceler.Parcel
public class LotModel {

    public static final int LOT_ACTION_CLIENT_VOTE_PLACED = 1;
    public static final int LOT_ACTION_CLIENT_NEED_VOTE = 2;
    public static final int LOT_ACTION_CANCELED = 3;
    public static final int LOT_ACTION_CLIENT_EXPIRED_WITH_BIDS = 4;
    public static final int LOT_ACTION_CLIENT_EXPIRED_NO_BIDS = 5;
    public static final int LOT_ACTION_CLIENT_CONFIRM_COMPLETION = 6;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_COMPLETION = 7;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_WON = 8;
    public static final int LOT_ACTION_CLIENT_WAIT_CONFIRMATION = 9;
    public static final int LOT_ACTION_CLIENT_REVIEW_BIDS = 10;
    public static final int LOT_ACTION_CLIENT_WAIT_BIDS = 11;
    public static final int LOT_ACTION_PROVIDER_MAKE_BIDS = 12;
    public static final int LOT_ACTION_PROVIDER_VOTE_PLACED = 13;
    public static final int LOT_ACTION_PROVIDER_NEED_VOTE = 14;

    public static final int LOT_STATUS_NEW = 0;
    public static final int LOT_STATUS_ACTIVE = 1;
    public static final int LOT_STATUS_WON = 2;
    public static final int LOT_STATUS_IN_PROCESS = 3;
    public static final int LOT_STATUS_COMPLETED = 4;
    public static final int LOT_STATUS_EXPIRED = 5;
    public static final int LOT_STATUS_CANCELED = 6;

    public static final int LOT_MARK_NEW = Lot.LOT_MARK_NEW;
    public static final int LOT_MARK_READ = Lot.LOT_MARK_READ;
    public static final int LOT_MARK_UPDATED = Lot.LOT_MARK_UPDATED;

    AccountModel mAuthor;

    int mNid;

    Calendar mCreatedDate;

    CategoryModel mCategoryModel;

    int mStatus;

    String mTitle;

    String mBody;

    float mWeight;

    String mWeightUnit;

    float mVolume;

    Calendar mShipDate;

    boolean mWithShipTime;

    Calendar mExpirationDate;

    List<Address> mRoutePoints;

    BigDecimal mMaxPrice;

    BigDecimal mBlitzPrice;

    int mLoadingServiceId;

    int mCountBids;

    boolean mPro;

    BidModel mRelatedBid;

    BigDecimal mMinBid;

    String mAuthorPhone;

    int mActionCode;

    Map<String, String> mReview;

    int mMark;

    long mCacheUpdatedTimestamp;

    public LotModel() {

    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public Calendar getShipDate() {
        return mShipDate;
    }

    public void setShipDate(Calendar shipDate) {
        mShipDate = shipDate;
    }

    public boolean isWithShipTime() {
        return mWithShipTime;
    }

    public void setWithShipTime(boolean withShipTime) {
        mWithShipTime = withShipTime;
    }

    public List<Address> getRoutePoints() {
        return mRoutePoints;
    }

    public void setRoutePoints(List<Address> routePoints) {
        mRoutePoints = routePoints;
    }

    public Calendar getExpirationDate() {
        return mExpirationDate;
    }

    public void setExpirationDate(Calendar expirationDate) {
        mExpirationDate = expirationDate;
    }

    public BigDecimal getMaxPrice() {
        return mMaxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        mMaxPrice = maxPrice;
    }

    public BigDecimal getBlitzPrice() {
        return mBlitzPrice;
    }

    public void setBlitzPrice(BigDecimal blitzPrice) {
        mBlitzPrice = blitzPrice;
    }

    public int getLoadingServiceId() {
        return mLoadingServiceId;
    }

    public void setLoadingServiceId(int loadingServiceId) {
        mLoadingServiceId = loadingServiceId;
    }

    public int getNid() {
        return mNid;
    }

    public void setNid(int nid) {
        mNid = nid;
    }

    public Calendar getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        mCreatedDate = createdDate;
    }

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        mWeight = weight;
    }

    public String getWeightUnit() {
        return mWeightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        mWeightUnit = weightUnit;
    }

    public float getVolume() {
        return mVolume;
    }

    public void setVolume(float volume) {
        mVolume = volume;
    }

    public int getCountBids() {
        return mCountBids;
    }

    public void setCountBids(int countBids) {
        mCountBids = countBids;
    }

    public boolean isPro() {
        return mPro;
    }

    public void setPro(boolean pro) {
        mPro = pro;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public BidModel getRelatedBid() {
        return mRelatedBid;
    }

    public void setRelatedBid(BidModel relatedBid) {
        mRelatedBid = relatedBid;
    }

    public BigDecimal getMinBid() {
        return mMinBid;
    }

    public void setMinBid(BigDecimal minBid) {
        mMinBid = minBid;
    }

    public String getAuthorPhone() {
        return mAuthorPhone;
    }

    public void setAuthorPhone(String authorPhone) {
        mAuthorPhone = authorPhone;
    }

    public int getActionCode() {
        return mActionCode;
    }

    public void setActionCode(int actionCode) {
        mActionCode = actionCode;
    }

    public long getCacheUpdatedTimestamp() {
        return mCacheUpdatedTimestamp;
    }

    public void setCacheUpdatedTimestamp(long cacheUpdatedTimestamp) {
        mCacheUpdatedTimestamp = cacheUpdatedTimestamp;
    }

    public AccountModel getAuthor() {
        return mAuthor;
    }

    public void setAuthor(AccountModel author) {
        mAuthor = author;
    }

    public Map<String, String> getReview() {
        return mReview;
    }

    public void setReview(Map<String, String> review) {
        mReview = review;
    }

    public int getMark() {
        return mMark;
    }

    public void setMark(int mark) {
        mMark = mark;
    }

    public CategoryModel getCategoryModel() {
        return mCategoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        mCategoryModel = categoryModel;
    }

    public String formatShippingDate(final Context context) {
        if (mShipDate == null) {
            return null;
        }

        int dateFormat = DateUtils.FORMAT_SHOW_DATE |
                DateUtils.FORMAT_SHOW_WEEKDAY;

        if (isWithShipTime()) {
            dateFormat = dateFormat | DateUtils.FORMAT_SHOW_TIME;
        }

        return DateUtils.formatDateTime(context, mShipDate.getTimeInMillis(), dateFormat);
    }
}
