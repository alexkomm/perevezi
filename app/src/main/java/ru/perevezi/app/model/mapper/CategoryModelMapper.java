/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.domain.entities.PereveziCategory;
import ru.perevezi.domain.mapper.ResponseMapper;

public class CategoryModelMapper implements ResponseMapper<PereveziCategory, CategoryModel> {

    @Inject
    public CategoryModelMapper() {
    }

    @Override
    public CategoryModel mapResponse(PereveziCategory category) {
        CategoryModel categoryModel = new CategoryModel();

        categoryModel.setName(category.getName());
        categoryModel.setId(category.getId());
        categoryModel.setDrawableResName(category.getDrawableResName());

        CategoryModel.Settings settings = new CategoryModel.Settings();

        settings.setTitleHint(category.getSettings().getTitleHint());

        settings.setAddMoreLabel(category.getSettings().getAddMoreLabel());
        settings.setQuantityHint(category.getSettings().getQuantityHint());
        settings.setTitleAutocomplete(category.getSettings().getTitleAutocomplete());
        settings.setDetailsTags(category.getSettings().getDetailsTags());
        settings.setExcludedFields(category.getSettings().getExcludedFields());
        settings.setRequiredFields(category.getSettings().getRequiredFields());
        settings.setTimeNeeded(category.getSettings().isTimeNeeded());

        categoryModel.setSettings(settings);

        return categoryModel;
    }

    @Override
    public Collection<CategoryModel> mapResponse(Collection<PereveziCategory> entityCollection) {
        List<CategoryModel> categoryList = new ArrayList<>(entityCollection.size());
        CategoryModel categoryModel;

        for (PereveziCategory categoryEntity : entityCollection) {
            categoryModel = mapResponse(categoryEntity);
            categoryList.add(categoryModel);
        }

        return categoryList;
    }
}
