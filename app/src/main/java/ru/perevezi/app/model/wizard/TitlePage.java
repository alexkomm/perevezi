/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.wizard.LotWizardTitleFragment;
import ru.perevezi.app.utils.Strings;

public class TitlePage extends Page {

    public static final String KEY_TITLE_DATA = "TITLE_DATA";

    public static final String TAG = "title";

    public TitlePage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return LotWizardTitleFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> reviewItems) {
        Resources resources = PereveziApplication.getContext().getResources();
        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_title), getFormattedReviewDisplay(), getKey()));
    }

    private String getFormattedReviewDisplay() {
        List<Map<String, String>> titleData = (List<Map<String, String>>) mData.getSerializable(KEY_TITLE_DATA);
        String output = "";

        if (titleData == null) {
            return output;
        }

        for (Map<String, String> titleItem : titleData) {
            String title = titleItem.get("title");
            String amount = titleItem.get("amount");
            String amountHint = titleItem.get("amount_hint");

            if (Strings.isEmpty(title)) {
                continue;
            }

            output += title;

            if (!Strings.isEmpty(amount)) {
                output += " - " + amount + " " + amountHint;
            }

            output += System.getProperty("line.separator");
        }

        return output;
    }

    @Override
    public boolean isCompleted() {
        List<Map<String, String>> titleData = (List<Map<String, String>>) mData.getSerializable(KEY_TITLE_DATA);

        return titleData != null && !Strings.isEmpty(titleData.get(0).get("title"));
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
