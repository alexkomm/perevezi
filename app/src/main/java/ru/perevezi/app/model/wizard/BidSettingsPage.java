/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.google.common.base.Strings;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;
import java.util.List;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.wizard.LotWizardBidSettingsFragment;

public class BidSettingsPage extends ValidatingPage {

    public static final String TAG = "bid_settings";

    public static final String KEY_MAX_PRICE = "max_price";
    public static final String KEY_BLITZ_PRICE = "blitz_price";
    public static final String KEY_PRO = "pro";

    public BidSettingsPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @NonNull
    @Override
    public List<PageError> getPageErrors() {

        List<PageError> pageErrorList = new ArrayList<>();

        int maxPrice;
        int blitzPrice;

        try {
            maxPrice   = Integer.parseInt(mData.getString(KEY_MAX_PRICE));
            blitzPrice = Integer.parseInt(mData.getString(KEY_BLITZ_PRICE));

        } catch (Exception e) {
            return pageErrorList;
        }

        if (blitzPrice >= maxPrice) {
            pageErrorList.add(new PageError(KEY_BLITZ_PRICE, R.string.error_blitz_price));
        }

        return pageErrorList;
    }

    @Override
    public Fragment createFragment() {
        return LotWizardBidSettingsFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> reviewItems) {
        Resources resources = PereveziApplication.getContext().getResources();
        String emptyLabel = resources.getString(R.string.label_not_specified);

        String maxPrice   = mData.getString(KEY_MAX_PRICE);
        String blitzPrice = mData.getString(KEY_BLITZ_PRICE);
        String forPro     = mData.getBoolean(KEY_PRO) ? resources.getString(R.string.yes) : resources.getString(R.string.no);

        maxPrice = Strings.isNullOrEmpty(maxPrice) ? emptyLabel : maxPrice + " " + resources.getString(R.string.rub);
        blitzPrice = Strings.isNullOrEmpty(blitzPrice) ? emptyLabel : blitzPrice + " " + resources.getString(R.string.rub);

        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_max_price), maxPrice, getKey()));
        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_blitz_price), blitzPrice, getKey()));
        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_for_pro), forPro, getKey()));
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
