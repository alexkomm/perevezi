/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.model.BidModel;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.mapper.BidirectionalMapper;

public class BidModelMapper implements BidirectionalMapper<PereveziBid, BidModel> {

    private final AccountModelMapper mAccountModelMapper;

    @Inject
    public BidModelMapper(AccountModelMapper accountModelMapper) {
        mAccountModelMapper = accountModelMapper;
    }


    @Override
    public BidModel mapResponse(PereveziBid pereveziBid) {
        BidModel bidModel = new BidModel();

        bidModel.setProvider(mAccountModelMapper.mapResponse(pereveziBid.getProvider()));
        bidModel.setBid(pereveziBid.getBid());
        bidModel.setNid(pereveziBid.getNid());
        bidModel.setCreatedDate(pereveziBid.getCreatedDate());
        bidModel.setUpdatedDate(pereveziBid.getUpdatedDate());
        bidModel.setActiveTill(pereveziBid.getActiveTill());
        bidModel.setWonDate(pereveziBid.getWonDate());
        bidModel.setAcceptedDate(pereveziBid.getAcceptedDate());
        bidModel.setAmount(pereveziBid.getAmount());
        bidModel.setShipType(pereveziBid.getShipType());
        bidModel.setLoadingOptions(pereveziBid.getLoadingOptions());
        bidModel.setExpirationDate(pereveziBid.getExpirationDate());
        bidModel.setStatus(pereveziBid.getStatus());
        bidModel.setCarId(pereveziBid.getCarId());
        bidModel.setCarImagePath(pereveziBid.getCarImagePath());
        bidModel.setCarName(pereveziBid.getCarName());
        bidModel.setCarSizes(pereveziBid.getCarSizes());
        bidModel.setCarCapacity(pereveziBid.getCarCapacity());
        bidModel.setCarBody(pereveziBid.getCarBody());
        bidModel.setCarAddPhotos(pereveziBid.getCarAddPhotos());
        bidModel.setAcceptable(pereveziBid.isAcceptable());
        bidModel.setDeclinable(pereveziBid.isDeclinable());
        bidModel.setCancellable(pereveziBid.isCancellable());
        bidModel.setRequirementsInfo(pereveziBid.getRequirementsInfo());
        bidModel.setDeclineReason(pereveziBid.getDeclineReason());

        return bidModel;
    }

    @Override
    public Collection<BidModel> mapResponse(Collection<PereveziBid> pereveziBids) {
        List<BidModel> bidList = new ArrayList<>();
        BidModel bidModel;

        for (PereveziBid bidEntity : pereveziBids) {
            bidModel = mapResponse(bidEntity);
            bidList.add(bidModel);
        }

        return bidList;
    }

    @Override
    public PereveziBid mapRequest(BidModel bidModel) {
        PereveziBid pereveziBid = new PereveziBid();

        pereveziBid.setBid(bidModel.getBid());
        pereveziBid.setNid(bidModel.getNid());
        pereveziBid.setCreatedDate(bidModel.getCreatedDate());
        pereveziBid.setUpdatedDate(bidModel.getUpdatedDate());
        pereveziBid.setActiveTill(bidModel.getActiveTill());
        pereveziBid.setWonDate(bidModel.getWonDate());
        pereveziBid.setAcceptedDate(bidModel.getAcceptedDate());
        pereveziBid.setAmount(bidModel.getAmount());
        pereveziBid.setShipType(bidModel.getShipType());
        pereveziBid.setLoadingOptions(bidModel.getLoadingOptions());
        pereveziBid.setExpirationDate(bidModel.getExpirationDate());
        pereveziBid.setStatus(bidModel.getStatus());
        pereveziBid.setCarId(bidModel.getCarId());
        pereveziBid.setCarImagePath(bidModel.getCarImagePath());
        pereveziBid.setCarName(bidModel.getCarName());
        pereveziBid.setCarSizes(bidModel.getCarSizes());
        pereveziBid.setCarCapacity(bidModel.getCarCapacity());
        pereveziBid.setCarBody(bidModel.getCarBody());
        pereveziBid.setCarAddPhotos(bidModel.getCarAddPhotos());

        return pereveziBid;
    }

    @Override
    public Collection<PereveziBid> mapRequest(Collection<BidModel> entityCollection) {
        return null;
    }
}
