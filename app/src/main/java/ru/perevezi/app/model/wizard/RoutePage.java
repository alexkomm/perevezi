/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.res.Resources;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.wizard.LotWizardRouteFragment;
import ru.perevezi.app.utils.LocationUtils;

public class RoutePage extends Page {

    public static final String TAG = "route";

    public static final String KEY_ROUTE = "route";

    public static final String KEY_FROM_TAG = "from";
    public static final String KEY_TO_TAG = "to";


    public RoutePage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return LotWizardRouteFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> reviewItems) {
        Resources resources = PereveziApplication.getContext().getResources();

        Bundle routeData = mData.getBundle(KEY_ROUTE);

        Address fromAddress = routeData.getParcelable(KEY_FROM_TAG);
        Address toAddress = routeData.getParcelable(KEY_TO_TAG);

        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_from), LocationUtils.formatAddress(fromAddress), getKey()));
        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_to), LocationUtils.formatAddress(toAddress), getKey()));
    }

    @Override
    public boolean isCompleted() {
        Bundle routeData = mData.getBundle(KEY_ROUTE);

        if (routeData == null || routeData.keySet().size() < 2) {
            return false;
        }

        for (String key : routeData.keySet()) {
            Address address = routeData.getParcelable(key);

            if (address.getMaxAddressLineIndex() == -1 && (address.getLocality() == null ||
                    address.getAdminArea() == null)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
