/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;

import org.parceler.Parcel;

import java.util.List;
import java.util.Map;

@Parcel
public class CategoryModel {

    int mId;

    String mName;

    String mDrawableResName;

    Settings mSettings;

    public CategoryModel() {
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Settings getSettings() {
        return mSettings;
    }

    public void setSettings(Settings settings) {
        mSettings = settings;
    }

    public String getDrawableResName() {
        return mDrawableResName;
    }

    public void setDrawableResName(String drawableResName) {
        mDrawableResName = drawableResName;
    }

    @Override
    public String toString() {
        return mName;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    @Parcel
    public static class Settings {

        String mTitleHint;

        String mAddMoreLabel;

        String mQuantityHint;

        String[] mTitleAutocomplete;

        List<String> mExcludedFields;

        List<String> mRequiredFields;

        Map<String, String> mDetailsTags;

        boolean isTimeNeeded;

        public String getQuantityHint() {
            return mQuantityHint;
        }

        public void setQuantityHint(String quantityHint) {
            mQuantityHint = quantityHint;
        }

        public String getTitleHint() {
            return mTitleHint;
        }

        public void setTitleHint(String titleHint) {
            mTitleHint = titleHint;
        }

        public String getAddMoreLabel() {
            return mAddMoreLabel;
        }

        public void setAddMoreLabel(String addMoreLabel) {
            mAddMoreLabel = addMoreLabel;
        }

        public String[] getTitleAutocomplete() {
            return mTitleAutocomplete;
        }

        public void setTitleAutocomplete(String[] titleAutocomplete) {
            mTitleAutocomplete = titleAutocomplete;
        }

        public Map<String, String> getDetailsTags() {
            return mDetailsTags;
        }

        public void setDetailsTags(Map<String, String> detailsTags) {
            mDetailsTags = detailsTags;
        }

        public List<String> getExcludedFields() {
            return mExcludedFields;
        }

        public void setExcludedFields(List<String> excludedFields) {
            mExcludedFields = excludedFields;
        }

        public List<String> getRequiredFields() {
            return mRequiredFields;
        }

        public void setRequiredFields(List<String> requiredFields) {
            mRequiredFields = requiredFields;
        }

        public boolean isTimeNeeded() {
            return isTimeNeeded;
        }

        public void setTimeNeeded(boolean isTimeNeeded) {
            this.isTimeNeeded = isTimeNeeded;
        }
    }
    
}
