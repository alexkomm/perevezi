/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.util.List;

import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.SettingsModel;

public interface PageCallbacks extends PageFragmentCallbacks {

    List<ru.perevezi.app.model.CategoryModel> onGetCategoryList();

    SettingsModel onGetGeneralSettings();

    CategoryModel onGetSelectedCategory();

    void onCategoryChanged(CategoryModel categoryModel);
}
