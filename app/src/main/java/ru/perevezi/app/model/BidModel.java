/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

@org.parceler.Parcel
public class BidModel {

    public static final int BID_STATUS_NEW = 0;
    public static final int BID_STATUS_WON = 1;
    public static final int BID_STATUS_ACCEPTED = 2;
    public static final int BID_STATUS_COMPLETE = 3;
    public static final int BID_STATUS_DECLINED = 4;
    public static final int BID_STATUS_REJECTED = 5;
    public static final int BID_STATUS_INCOMPLETE = 6;
    public static final int BID_STATUS_DELETED = 7;
    public static final int BID_STATUS_CANCELED = 8;
    public static final int BID_STATUS_OBSOLETE = 9;
    public static final int BID_STATUS_EXPIRED = 10;

    AccountModel provider;

    int bid;
    int nid;
    int uid;
    Calendar createdDate;
    Calendar updatedDate;
    Calendar wonDate;
    Calendar acceptedDate;
    BigDecimal amount;
    int shipType;
    int loadingOptions;
    Calendar expirationDate;
    int status;

    int carId;

    String carImagePath;
    String carName;
    String carSizes;
    String carBody;
    String carCapacity;
    String[] carAddPhotos;

    Calendar activeTill;

    boolean acceptable;
    boolean declinable;
    boolean cancellable;
    Map<String, Boolean> requirementsInfo;
    String declineReason;

    public BidModel() {
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public int getNid() {
        return nid;
    }

    public void setNid(int nid) {
        this.nid = nid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Calendar getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    public Calendar getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Calendar updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Calendar getWonDate() {
        return wonDate;
    }

    public void setWonDate(Calendar wonDate) {
        this.wonDate = wonDate;
    }

    public Calendar getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Calendar acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }

    public int getLoadingOptions() {
        return loadingOptions;
    }

    public void setLoadingOptions(int loadingOptions) {
        this.loadingOptions = loadingOptions;
    }

    public Calendar getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCarImagePath() {
        return carImagePath;
    }

    public void setCarImagePath(String carImagePath) {
        this.carImagePath = carImagePath;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarSizes() {
        return carSizes;
    }

    public void setCarSizes(String carSizes) {
        this.carSizes = carSizes;
    }

    public String getCarBody() {
        return carBody;
    }

    public void setCarBody(String carBody) {
        this.carBody = carBody;
    }

    public String getCarCapacity() {
        return carCapacity;
    }

    public void setCarCapacity(String carCapacity) {
        this.carCapacity = carCapacity;
    }

    public String[] getCarAddPhotos() {
        return carAddPhotos;
    }

    public void setCarAddPhotos(String[] carAddPhotos) {
        this.carAddPhotos = carAddPhotos;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public Calendar getActiveTill() {
        return activeTill;
    }

    public void setActiveTill(Calendar activeTill) {
        this.activeTill = activeTill;
    }

    public boolean isAcceptable() {
        return acceptable;
    }

    public void setAcceptable(boolean acceptable) {
        this.acceptable = acceptable;
    }

    public boolean isDeclinable() {
        return declinable;
    }

    public void setDeclinable(boolean declinable) {
        this.declinable = declinable;
    }

    public boolean isCancellable() {
        return cancellable;
    }

    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }

    public Map<String, Boolean> getRequirementsInfo() {
        return requirementsInfo;
    }

    public void setRequirementsInfo(Map<String, Boolean> requirementsInfo) {
        this.requirementsInfo = requirementsInfo;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }

    public AccountModel getProvider() {
        return provider;
    }

    public void setProvider(AccountModel provider) {
        this.provider = provider;
    }
}