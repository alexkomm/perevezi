/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.perevezi.app.model.CommentModel;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.mapper.ResponseMapper;

public class CommentModelMapper implements ResponseMapper<PereveziComment, CommentModel> {
    @Override
    public CommentModel mapResponse(PereveziComment pereveziComment) {
        CommentModel commentModel = new CommentModel();

        commentModel.setAuthorName(pereveziComment.getAuthorName());
        commentModel.setBidAmount(pereveziComment.getBidAmount());
        commentModel.setCid(pereveziComment.getCid());
        commentModel.setMail(pereveziComment.getMail());
        commentModel.setComment(pereveziComment.getComment());
        commentModel.setName(pereveziComment.getName());
        commentModel.setPid(pereveziComment.getPid());
        commentModel.setStatus(pereveziComment.getStatus());
        commentModel.setNid(pereveziComment.getNid());
        commentModel.setSubject(pereveziComment.getSubject());
        commentModel.setThread(pereveziComment.getThread());
        commentModel.setTimestamp(pereveziComment.getTimestamp());
        commentModel.setUid(pereveziComment.getUid());

        return commentModel;
    }

    @Override
    public Collection<CommentModel> mapResponse(Collection<PereveziComment> entityCollection) {
        List<CommentModel> comments = new ArrayList<>();
        CommentModel commentModel;

        for (PereveziComment commentEntity : entityCollection) {
            commentModel = mapResponse(commentEntity);
            comments.add(commentModel);
        }

        return comments;
    }
}
