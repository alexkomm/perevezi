/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.Context;
import android.content.res.Resources;
import android.location.Address;
import android.os.Bundle;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.utils.LocationUtils;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.utils.Preconditions;

public class LotWizard extends AbstractWizardModel implements ModelCallbacks {


    private CategoryModel mCurrentCategory;

    public LotWizard(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        Resources resources = PereveziApplication.getContext().getResources();
        return new PageList(
                new CategoryPage(this, resources.getString(R.string.title_category_page)).setRequired(true),
                new TitlePage(this, resources.getString(R.string.title_title_page)).setRequired(true),
                new DetailsPage(this, resources.getString(R.string.title_details_page)).setRequired(true),
                new RoutePage(this, resources.getString(R.string.title_route_page)).setRequired(true),
                new BidSettingsPage(this, resources.getString(R.string.title_bid_settings_page))
        );
    }

    public CategoryModel getSelectedCategory() {
        return mCurrentCategory;
    }

    public void setSelectedCategory(CategoryModel currentCategory) {
        Preconditions.checkState(currentCategory != null, "selected category cannot be null");
        mCurrentCategory = currentCategory;
    }

    @Override
    public List<Page> getCurrentPageSequence() {
        List<Page> flattened = super.getCurrentPageSequence();

        return filteredFlattenedPages(flattened);
    }

    private List<Page> filteredFlattenedPages(List<Page> flattened) {
        Predicate<Page> categoryPredicate = new Predicate<Page>() {
            public boolean apply(Page page) {
                if (mCurrentCategory == null) {
                    return true;
                }

                if (page instanceof TitlePage && mCurrentCategory.getSettings().getTitleHint() == null) {
                    return false;
                }

                return true;
            }
        };

        Iterable<Page> pageIterable = Iterables.filter(flattened, categoryPredicate);

        return Lists.newArrayList(pageIterable);
    }

    public void clear() {
        for (Page page : getCurrentPageSequence()) {
            page.resetData(new Bundle());
        }
    }

    public PereveziLot generateLotFromData() {
        PereveziLot lot = new PereveziLot();

        for (Page page : getCurrentPageSequence()) {
            Bundle pageData  = page.getData();

            switch (page.getKey()) {
                case CategoryPage.TAG:
                    lot.setCategoryId(pageData.getInt(CategoryPage.KEY_CATEGORY_ID));
                    break;
                case TitlePage.TAG:
                    ArrayList<Map<String, String>> titleData = (ArrayList<Map<String, String>>) pageData.getSerializable(TitlePage.KEY_TITLE_DATA);
                    List<PereveziLot.Cargo> cargos = new ArrayList<>();

                    if (titleData == null) {
                        break;
                    }

                    for (Map<String, String> title : titleData) {
                        PereveziLot.Cargo cargo = new PereveziLot.Cargo();

                        cargo.title  = title.get("title");

                        if (title.get("amount") != null) {
                            cargo.amount = Integer.parseInt(title.get("amount"));
                        }

                        cargos.add(cargo);
                    }

                    lot.setCargos(cargos.toArray(new PereveziLot.Cargo[cargos.size()]));
                    break;
                case DetailsPage.TAG:
                    if (!Strings.isNullOrEmpty(pageData.getString(DetailsPage.KEY_WEIGHT))) {
                        lot.setWeight(Float.parseFloat(pageData.getString(DetailsPage.KEY_WEIGHT)));
                    }

                    if (!Strings.isNullOrEmpty(pageData.getString(DetailsPage.KEY_VOLUME))) {
                        lot.setVolume(Float.parseFloat(pageData.getString(DetailsPage.KEY_VOLUME)));
                    }

                    if (pageData.getString(DetailsPage.KEY_NEED_LOAD_ID) != null) {
                        lot.setLoadingOptions(Integer.parseInt(pageData.getString(DetailsPage.KEY_NEED_LOAD_ID)));
                    }

                    if (pageData.getSerializable(DetailsPage.KEY_SHIP_DATE) != null) {
                        lot.setShipDate((java.util.Calendar) pageData.getSerializable(DetailsPage.KEY_SHIP_DATE));
                    }

                    String comments = pageData.getString(DetailsPage.KEY_COMMENTS);

                    if (pageData.getStringArrayList(DetailsPage.KEY_TAGS) != null) {
                        String tagsStringified = "";
                        for (String tag : pageData.getStringArrayList(DetailsPage.KEY_TAGS)) {
                            tagsStringified += tag + System.getProperty("line.separator");
                        }

                        comments = Strings.isNullOrEmpty(comments) ? tagsStringified : tagsStringified + comments;
                    }

                    lot.setDetails(comments);

                    break;
                case RoutePage.TAG:
                    Bundle routeData = pageData.getBundle(RoutePage.KEY_ROUTE);

                    List<PereveziLot.RoutePoint> routePoints = new ArrayList<>();

                    routePoints.add(createRoutePoint((Address) routeData.get("from")));
                    routePoints.add(createRoutePoint((Address) routeData.get("to")));

                    lot.setRoutePoints(routePoints);

                    break;
                case BidSettingsPage.TAG:

                    if (!Strings.isNullOrEmpty(pageData.getString(BidSettingsPage.KEY_MAX_PRICE))) {
                        lot.setMaxPrice(new BigDecimal(pageData.getString(BidSettingsPage.KEY_MAX_PRICE)));
                    }

                    if (!Strings.isNullOrEmpty(pageData.getString(BidSettingsPage.KEY_BLITZ_PRICE))) {
                        lot.setBlitzPrice(new BigDecimal(pageData.getString(BidSettingsPage.KEY_BLITZ_PRICE)));
                    }

                    lot.setForPro(pageData.getBoolean(BidSettingsPage.KEY_PRO, false));

                    break;
            }
        }

        return lot;
    }

    private PereveziLot.RoutePoint createRoutePoint(Address address) {
        PereveziLot.RoutePoint routePoint = new PereveziLot.RoutePoint();

        if (Strings.isNullOrEmpty(address.getLocality()) || Strings.isNullOrEmpty(address.getAdminArea())) {
            routePoint.locality = LocationUtils.formatAddress(address);
        } else {
            routePoint.country  = address.getCountryName();
            routePoint.locality = address.getLocality();
            routePoint.region = address.getAdminArea();
        }

        if (address.getThoroughfare() != null) {
            routePoint.address = address.getThoroughfare();

            if (address.getSubThoroughfare() != null) {
                routePoint.address += " " + address.getSubThoroughfare();
            }
        }

        return routePoint;
    }
}
