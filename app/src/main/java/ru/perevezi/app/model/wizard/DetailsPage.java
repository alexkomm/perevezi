/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

import com.google.common.base.Strings;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.wizard.LotWizardDetailsFragment;
import ru.perevezi.app.utils.DateUtils;


public class DetailsPage extends Page {

    public static final String KEY_WEIGHT = "weight";
    public static final String KEY_VOLUME = "volume";
    public static final String KEY_COMMENTS = "body";

    public static final String KEY_TAGS = "tags";

    public static final String KEY_NEED_LOAD    = "need_load";
    public static final String KEY_NEED_LOAD_ID = "need_load_id";

    public static final String KEY_REQUIRED_FIELDS = "required_fields";
    public static final String KEY_COMMENTS_REQUIRED = "comment_required";
    public static final String KEY_EXCLUDED_FIELDS = "excluded_fields";
    public static final String KEY_DATE = "date";
    public static final String KEY_TIME = "time";
    public static final String KEY_SHIP_DATE = "ship_date";
    public static final String KEY_TIME_ENTERED = "time_entered";

    public static final String TAG = "details";

    public DetailsPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return LotWizardDetailsFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> reviewItems) {
        String weight   = mData.getString(KEY_WEIGHT);
        String volume   = mData.getString(KEY_VOLUME);
        String comments = mData.getString(KEY_COMMENTS);
        
        ArrayList<String> tags = mData.getStringArrayList(KEY_TAGS);
        String loadingOptions  = mData.getString(KEY_NEED_LOAD);

        ArrayList<String> excludedFields = mData.getStringArrayList(KEY_EXCLUDED_FIELDS);

        if (excludedFields == null) {
            excludedFields = new ArrayList<>();
        }

        Resources resources = PereveziApplication.getContext().getResources();
        String emptyLabel = resources.getString(R.string.label_not_specified);

        Calendar savedDate = (Calendar) mData.getSerializable(KEY_SHIP_DATE);
        boolean isTimeEntered = mData.getBoolean(KEY_TIME_ENTERED, false);
        String dateString;

        if (savedDate == null) {
            dateString = emptyLabel;
        } else {
            String format = isTimeEntered ? DateUtils.DATE_WITH_TIME_FORMAT: DateUtils.DATE_FORMAT;

            SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());
            dateString = dateFormatter.format(savedDate.getTime());
        }

        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_date), dateString, getKey()));

        if (!excludedFields.contains(KEY_WEIGHT)) {
            weight = Strings.isNullOrEmpty(weight) ? emptyLabel : weight + " " + resources.getString(R.string.kg);
            reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_weight), weight, getKey()));
        }

        if (!excludedFields.contains(KEY_VOLUME)) {
            volume = Strings.isNullOrEmpty(volume) ? emptyLabel : volume + " " + resources.getString(R.string.m3);
            reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_volume), volume, getKey()));
        }

        if (!excludedFields.contains(KEY_NEED_LOAD)) {
            loadingOptions = Strings.isNullOrEmpty(loadingOptions) ? emptyLabel : loadingOptions;
            reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_loading), loadingOptions, getKey()));
        }

        if (tags != null) {
            String tagsStringified = "";
            for (String tag : tags) {
                tagsStringified += tag + System.getProperty("line.separator");
            }

            comments = Strings.isNullOrEmpty(comments) ? tagsStringified : tagsStringified + comments;
        }

        if (!Strings.isNullOrEmpty(comments)) {
            reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_comments), comments, getKey()));
        }
    }

    @Override
    public boolean isCompleted() {
        boolean completed = true;
        ArrayList<String> requiredFields = mData.getStringArrayList(KEY_REQUIRED_FIELDS);

        if (requiredFields != null) {
            for (String field : requiredFields) {
                if (Strings.isNullOrEmpty(mData.getString(field))) {
                    completed = false;
                }
            }
        }

        ArrayList<String> tags = mData.getStringArrayList(KEY_TAGS);

        boolean commentsProvided = !Strings.isNullOrEmpty(mData.getString(KEY_COMMENTS)) || (tags != null && tags.size() > 0);

        if (mData.getBoolean(KEY_COMMENTS_REQUIRED) && !commentsProvided) {
            completed = false;
        }

        return completed;
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
