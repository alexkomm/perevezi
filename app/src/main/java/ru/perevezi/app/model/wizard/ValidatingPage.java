/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.support.annotation.NonNull;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;

import java.util.List;

public abstract class ValidatingPage extends Page {

    protected ValidatingPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @NonNull
    public abstract List<PageError> getPageErrors();


    public static class PageError {

        public Object fieldTag;

        public int msgResId;

        public PageError(Object fieldTag, int msgResId) {
            this.fieldTag = fieldTag;
            this.msgResId = msgResId;
        }
    }
}
