/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.wizard;

import android.content.res.Resources;
import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.R;
import ru.perevezi.app.ui.fragments.wizard.LotWizardCategoryFragment;

public class CategoryPage extends Page {

    public static final String TAG = "category";

    public final static String KEY_CATEGORY_NAME = "CATEGORY_NAME";
    public final static String KEY_CATEGORY_ID = "CATEGORY_ID";

    public CategoryPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return LotWizardCategoryFragment.newInstance(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> reviewItems) {

        Resources resources = PereveziApplication.getContext().getResources();

        reviewItems.add(new ReviewItem(resources.getString(R.string.review_label_category), mData.getString(KEY_CATEGORY_NAME), getKey()));
    }

    @Override
    public boolean isCompleted() {
        return mData.getInt(KEY_CATEGORY_ID) > 0;
    }


    @Override
    public String getKey() {
        return TAG;
    }
}
