/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;

import com.google.common.base.Strings;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import ru.perevezi.domain.utils.Preconditions;

@Parcel
public class CarModel {

    int mExtId;

    String mName;

    String mBody;

    String mSizes;

    String mCapacity;

    int mSeats;

    String[] mImages;

    public CarModel() {

    }

    public CarModel(int extId, String name) {
        mExtId = extId;
        mName = name;

        Preconditions.checkState(mName != null, "Car name cannot be null");
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public String getSizes() {
        return mSizes;
    }

    public void setSizes(String sizes) {
        mSizes = sizes;
    }

    public String getCapacity() {
        return mCapacity;
    }

    public void setCapacity(String capacity) {
        mCapacity = capacity;
    }

    public int getSeats() {
        return mSeats;
    }

    public void setSeats(int seats) {
        mSeats = seats;
    }

    public String[] getImages() {
        return mImages;
    }

    public void setImages(String[] images) {
        mImages = images;
    }

    public int getExtId() {
        return mExtId;
    }

    public void setExtId(int extId) {
        mExtId = extId;
    }

    @Override
    public String toString() {
        return mName;
    }
}
