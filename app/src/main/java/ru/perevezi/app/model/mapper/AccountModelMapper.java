/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.perevezi.app.model.AccountModel;
import ru.perevezi.app.model.CarModel;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.mapper.ResponseMapper;

public class AccountModelMapper implements ResponseMapper<PereveziAccount, AccountModel> {

    @Inject
    public AccountModelMapper() {
    }

    @Override
    public AccountModel mapResponse(PereveziAccount account) {
        final AccountModel am = new AccountModel();

        am.setLogin(account.getName());
        am.setExtId(account.getExtId());
        am.setEmail(account.getEmail());
        am.setFullName(account.getFullName());
        am.setPhone(account.getPhone());
        am.setVendor(account.isVendor());
        am.setClient(account.isClient());
        am.setCountLots(account.getCountLots());
        am.setCompletedLots(account.getCompletedLots());

        am.setCountBids(account.getCountBids());
        am.setCompletedBids(account.getCompletedBids());

        List<Map<String, Object>> carList = account.getCars();

        if (carList != null) {
            List<CarModel> carModelList = new ArrayList<>(carList.size());

            for (Map<String, Object> car : carList) {
                CarModel carModel = new CarModel(Integer.valueOf((String) car.get("cid")), (String) car.get("name"));
                carModel.setBody((String) car.get("bodytype"));
                carModel.setSizes((String) car.get("sizes"));
                carModel.setCapacity((String) car.get("capacity"));
                carModel.setSeats(Integer.valueOf((String) car.get("seats")));

                ArrayList<String> images = (ArrayList<String>) car.get("img");
                String[] imagesArr = new String[images.size()];

                carModel.setImages(images.toArray(imagesArr));

                carModelList.add(carModel);
            }

            am.setCars(carModelList);
        }

        am.setRegistrationDate(account.getRegistrationDate());
        am.setLastAccessDate(account.getLastAccessDate());
        am.setCompany(account.getCompany());
        am.setHomeRegion(account.getHomeRegion());
        am.setPositiveVotes(account.getPositiveVotes());
        am.setNeutralVotes(account.getNeutralVotes());
        am.setNegativeVotes(account.getNegativeVotes());
        am.setInfo(account.getInfo());
        am.setProfileImageUrl(account.getProfileImageUrl());
        am.setRating(account.getRating());

        am.setBalance(account.getBalance());
        am.setProDaysLeft(account.getProDaysLeft());
        am.setSmsLeft(account.getSmsLeft());

        am.setPhoneConfirmStatus(account.getPhoneConfirmStatus());

        return am;
    }

    @Override
    public Collection<AccountModel> mapResponse(Collection<PereveziAccount> entityCollection) {
        throw new UnsupportedOperationException();
    }
}
