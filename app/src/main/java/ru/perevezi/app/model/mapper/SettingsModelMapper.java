/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.SettingsModel;
import ru.perevezi.domain.entities.PereveziSettings;
import ru.perevezi.domain.mapper.ResponseMapper;

public class SettingsModelMapper implements ResponseMapper<PereveziSettings, SettingsModel> {

    private final CategoryModelMapper mCategoryModelMapper;

    @Inject
    public SettingsModelMapper(CategoryModelMapper categoryModelMapper) {
        mCategoryModelMapper = categoryModelMapper;
    }

    @Override
    public SettingsModel mapResponse(PereveziSettings settings) {
        SettingsModel settingsModel = new SettingsModel();

        settingsModel.setLotLoadingOptions(settings.getLotLoadingOptions());
        settingsModel.setCarMovingOptions(settings.getCarMovingOptions());
        settingsModel.setLotShippingOptions(settings.getLotShippingOptions());
        settingsModel.setCategories((List<CategoryModel>) mCategoryModelMapper.mapResponse(settings.getCategories()));
        settingsModel.setDeclineBidReasons(settings.getDeclineBidReasons());
        settingsModel.setPayLaterDeposit(settings.getPayLaterDeposit());

        return settingsModel;
    }

    @Override
    public Collection<SettingsModel> mapResponse(Collection<PereveziSettings> entityCollection) {
        throw new UnsupportedOperationException();
    }
}
