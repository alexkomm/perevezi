/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model.mapper;

import android.location.Address;

import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import ru.perevezi.app.model.CategoryModel;
import ru.perevezi.app.model.LotModel;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.mapper.ResponseMapper;

public class LotModelMapper implements ResponseMapper<PereveziLot, LotModel> {

    private final BidModelMapper mBidModelMapper;
    private final AccountModelMapper mAccountModelMapper;
    private final SettingsState mSettingsState;

    @Inject
    public LotModelMapper(BidModelMapper bidModelMapper, AccountModelMapper accountModelMapper, SettingsState settingsState) {
        mBidModelMapper = bidModelMapper;
        mAccountModelMapper = accountModelMapper;
        mSettingsState = settingsState;
    }

    @Override
    public LotModel mapResponse(PereveziLot lot) {

        List<Address> addressList = new ArrayList<>();
        for (PereveziLot.RoutePoint rp : lot.getRoutePoints()) {
            Address address = new Address(Locale.getDefault());

            if (!Strings.isNullOrEmpty(rp.country)) {
                address.setCountryName(rp.country);
            }

            if (!Strings.isNullOrEmpty(rp.region)) {
                address.setAdminArea(rp.region);
            }

            if (!Strings.isNullOrEmpty(rp.locality)) {
                address.setLocality(rp.locality);
            }

            if (!Strings.isNullOrEmpty(rp.address)) {
                address.setThoroughfare(rp.address);
            }

            addressList.add(address);
        }

        LotModel lotModel = new LotModel();

        lotModel.setAuthor(mAccountModelMapper.mapResponse(lot.getAuthor()));
        lotModel.setNid(lot.getNid());
        lotModel.setCreatedDate(lot.getCreatedDate());
        lotModel.setStatus(lot.getStatus());

        final List<CategoryModel> categoryList = mSettingsState.getGeneralSettings().getCategories();

        for (CategoryModel category : categoryList) {
            if (category.getId() == lot.getCategoryId()) {
                lotModel.setCategoryModel(category);
                break;
            }
        }

        lotModel.setTitle(lot.getTitleString());
        lotModel.setBody(lot.getDetails());
        lotModel.setWeight(lot.getWeight());
        lotModel.setWeightUnit(lot.getWeightUnit());
        lotModel.setVolume(lot.getVolume());
        lotModel.setWithShipTime(lot.isWithShipTime());
        lotModel.setShipDate(lot.getShipDate());
        lotModel.setExpirationDate(lot.getExpirationDate());
        lotModel.setRoutePoints(addressList);
        lotModel.setMaxPrice(lot.getMaxPrice());
        lotModel.setBlitzPrice(lot.getBlitzPrice());
        lotModel.setLoadingServiceId(lot.getLoadingOptions());
        lotModel.setCountBids(lot.getCountBids());

        if (lot.getRelatedBid() != null) {
            lotModel.setRelatedBid(mBidModelMapper.mapResponse(lot.getRelatedBid()));
        }

        lotModel.setMinBid(lot.getMinBid());
        lotModel.setAuthorPhone(lot.getAuthorPhone());
        lotModel.setPro(lot.isForPro());

        lotModel.setActionCode(lot.getActionCode());
        lotModel.setReview(lot.getReview());
        lotModel.setMark(lot.getMark());
        lotModel.setCacheUpdatedTimestamp(lot.getCacheUpdatedTimestamp());

        return lotModel;
    }

    @Override
    public Collection<LotModel> mapResponse(Collection<PereveziLot> lotCollection) {
        List<LotModel> lotModelList = new ArrayList<>();
        LotModel lotModel;

        for (PereveziLot pereveziLot : lotCollection) {
            lotModel = mapResponse(pereveziLot);
            lotModelList.add(lotModel);
        }

        return lotModelList;
    }
}
