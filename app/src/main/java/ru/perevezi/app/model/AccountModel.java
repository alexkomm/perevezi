/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.model;


import org.parceler.Parcel;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Parcel
public class AccountModel {

    public static final int PHONE_CONFIRM_STATUS_NOT_CONFIRMED = 0;
    public static final int PHONE_CONFIRM_STATUS_PENDING = 1;
    public static final int PHONE_CONFIRM_STATUS_CONFIRMED = 2;

    int mExtId;

    String mLogin;
    String mFullName;
    String mEmail;
    String mPhone;

    boolean mVendor;

    boolean mClient;

    List<CarModel> mCars;

    Calendar mRegistrationDate;

    Calendar mLastAccessDate;

    String mCompany;

    String mHomeRegion;

    int mCountLots;

    int mCompletedLots;

    int mCountBids;

    int mCompletedBids;

    int mPositiveVotes;

    int mNeutralVotes;

    int mNegativeVotes;

    String mInfo;

    String mProfileImageUrl;

    float mRating;

    BigDecimal mBalance;

    int mProDaysLeft;

    int mSmsLeft;

    int mPhoneConfirmStatus;

    public AccountModel() {
        
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public int getExtId() {
        return mExtId;
    }

    public void setExtId(int extId) {
        mExtId = extId;
    }

    public boolean isVendor() {
        return mVendor;
    }

    public void setVendor(boolean isVendor) {
        mVendor = isVendor;
    }

    public boolean isClient() {
        return mClient;
    }

    public void setClient(boolean client) {
        mClient = client;
    }

    public int getCountLots() {
        return mCountLots;
    }

    public void setCountLots(int countLots) {
        mCountLots = countLots;
    }

    public int getCountBids() {
        return mCountBids;
    }

    public void setCountBids(int countBids) {
        mCountBids = countBids;
    }

    public int getCompletedBids() {
        return mCompletedBids;
    }

    public void setCompletedBids(int completedBids) {
        mCompletedBids = completedBids;
    }

    public List<CarModel> getCars() {
        return mCars;
    }

    public void setCars(List<CarModel> cars) {
        mCars = cars;
    }

    public Calendar getRegistrationDate() {
        return mRegistrationDate;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        mRegistrationDate = registrationDate;
    }

    public Calendar getLastAccessDate() {
        return mLastAccessDate;
    }

    public void setLastAccessDate(Calendar lastAccessDate) {
        mLastAccessDate = lastAccessDate;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getHomeRegion() {
        return mHomeRegion;
    }

    public void setHomeRegion(String homeRegion) {
        mHomeRegion = homeRegion;
    }

    public int getCompletedLots() {
        return mCompletedLots;
    }

    public void setCompletedLots(int completedLots) {
        mCompletedLots = completedLots;
    }

    public int getPositiveVotes() {
        return mPositiveVotes;
    }

    public void setPositiveVotes(int positiveVotes) {
        mPositiveVotes = positiveVotes;
    }

    public int getNeutralVotes() {
        return mNeutralVotes;
    }

    public void setNeutralVotes(int neutralVotes) {
        mNeutralVotes = neutralVotes;
    }

    public int getNegativeVotes() {
        return mNegativeVotes;
    }

    public void setNegativeVotes(int negativeVotes) {
        mNegativeVotes = negativeVotes;
    }

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    public String getProfileImageUrl() {
        return mProfileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        mProfileImageUrl = profileImageUrl;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        mRating = rating;
    }

    public BigDecimal getBalance() {
        return mBalance;
    }

    public void setBalance(BigDecimal balance) {
        this.mBalance = balance;
    }

    public int getProDaysLeft() {
        return mProDaysLeft;
    }

    public void setProDaysLeft(int proDaysLeft) {
        this.mProDaysLeft = proDaysLeft;
    }

    public int getSmsLeft() {
        return mSmsLeft;
    }

    public void setSmsLeft(int smsLeft) {
        this.mSmsLeft = smsLeft;
    }

    public int getPhoneConfirmStatus() {
        return mPhoneConfirmStatus;
    }

    public void setPhoneConfirmStatus(int phoneConfirmStatus) {
        mPhoneConfirmStatus = phoneConfirmStatus;
    }

    public int getTotalVotesCount() {
        return mNegativeVotes + mPositiveVotes + mNeutralVotes;
    }
}
