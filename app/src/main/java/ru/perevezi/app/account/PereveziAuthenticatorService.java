/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class PereveziAuthenticatorService extends Service {

    private PereveziAccountAuthenticator mAccountAuthenticator;

    @Override
    public void onCreate() {
        super.onCreate();
        mAccountAuthenticator = new PereveziAccountAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAccountAuthenticator.getIBinder();
    }
}
