/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.account;

import android.accounts.Account;
import android.accounts.AccountManager;

import java.util.ArrayList;
import java.util.List;

import ru.perevezi.app.Constants;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.utils.Preconditions;

public class AndroidAccountManager implements PereveziAccountManager {

    private AccountManager mAccountManager;

    public AndroidAccountManager(AccountManager accountManager) {
        mAccountManager = accountManager;
    }

    @Override
    public List<PereveziAccount> getAccounts() {
        final android.accounts.Account[] accounts = mAccountManager.getAccountsByType(Constants.PEREVEZI_ACCOUNT_TYPE);
        ArrayList<PereveziAccount> pereveziPereveziAccounts = new ArrayList<>(accounts.length);

        for (Account account : accounts) {
            pereveziPereveziAccounts.add(getAccount(account.name));
        }

        return pereveziPereveziAccounts;
    }

    @Override
    public void addAccount(PereveziAccount pereveziAccount) {
        final Account account = new Account(pereveziAccount.getName(), Constants.PEREVEZI_ACCOUNT_TYPE);

        mAccountManager.addAccountExplicitly(account, null, null);
        mAccountManager.setPassword(account, pereveziAccount.getPassword());
        mAccountManager.setAuthToken(account, Constants.PEREVEZI_AUTH_TOKEN_TYPE, pereveziAccount.getAuthToken());
        mAccountManager.setUserData(account, "extId", String.valueOf(pereveziAccount.getExtId()));
    }

    @Override
    public void removeAccount(PereveziAccount pereveziAccount) {

    }

    @Override
    public boolean accountExists(PereveziAccount pereveziAccount) {
        Preconditions.checkState(pereveziAccount != null, "perevezi account cannot be null");

        final Account[] accounts = mAccountManager.getAccountsByType(Constants.PEREVEZI_ACCOUNT_TYPE);
        for (int i = 0; i < accounts.length ; i++) {
            final Account account = accounts[i];


            if (pereveziAccount.getName() != null && account.name.equals(pereveziAccount.getName())) {
                return true;
            }
        }

        return false;
    }


    @Override
    public PereveziAccount getAccount(String name) {
        Preconditions.checkState(name != null, "name cannot be null");

        final Account[] accounts = mAccountManager.getAccountsByType(Constants.PEREVEZI_ACCOUNT_TYPE);
        for (int i = 0; i < accounts.length ; i++) {
            final Account account = accounts[i];


            if (account.name.equals(name)) {
                String password = mAccountManager.getPassword(account);

                PereveziAccount pereveziAccount = new PereveziAccount(account.name, password);
                pereveziAccount.setEmail(account.name);
                pereveziAccount.setAuthToken(mAccountManager.peekAuthToken(account, Constants.PEREVEZI_AUTH_TOKEN_TYPE));
                pereveziAccount.setExtId(Integer.valueOf(mAccountManager.getUserData(account, "extId")));

                return pereveziAccount;
            }
        }

        return null;
    }

    @Override
    public void invalidateAuthToken(PereveziAccount pereveziAccount) {
        mAccountManager.invalidateAuthToken(Constants.PEREVEZI_ACCOUNT_TYPE, pereveziAccount.getAuthToken());
    }

    @Override
    public void updateCredentials(PereveziAccount pereveziAccount) {
        Preconditions.checkState(pereveziAccount != null, "perevezi account cannot be null");

        final Account[] accounts = mAccountManager.getAccountsByType(Constants.PEREVEZI_ACCOUNT_TYPE);
        for (int i = 0; i < accounts.length ; i++) {
            final Account account = accounts[i];


            if (pereveziAccount.getName() != null && account.name.equals(pereveziAccount.getName())) {
                mAccountManager.setPassword(account, pereveziAccount.getPassword());
                mAccountManager.setAuthToken(account, Constants.PEREVEZI_AUTH_TOKEN_TYPE, pereveziAccount.getAuthToken());
                return;
            }
        }
    }
}