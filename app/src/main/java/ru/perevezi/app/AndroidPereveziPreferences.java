/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app;

import android.content.SharedPreferences;

import java.util.Set;
import java.util.TreeSet;

import ru.perevezi.domain.utils.PereveziPreferences;

public class AndroidPereveziPreferences implements PereveziPreferences {

    private static final String KEY_LAST_UPDATED_TIME = "last_updated_time";
    private static final String KEY_REGISTRATION_ID = "registration_id";
    private static final String KEY_APP_VERSION = "app_version";
    private static final String KEY_CURRENT_ACCOUNT = "current_account";
    private static final String KEY_CURRENT_ACCOUNT_EXT_ID = "ext_id";
    private static final String KEY_LOGGED_OUT = "logged_out";
    private static final String KEY_TOKEN_SENT = "token_sent";
    private static final String KEY_NEW_LOTS = "new_lots";


    private final SharedPreferences mPreferences;

    public AndroidPereveziPreferences(SharedPreferences preferences) {
        mPreferences = preferences;
    }

    @Override
    public String getCurrentAccount() {
        return mPreferences.getString(KEY_CURRENT_ACCOUNT, "");
    }

    @Override
    public void setCurrentAccount(String accountName) {
        if (accountName != null) {
            mPreferences.edit().putString(KEY_CURRENT_ACCOUNT, accountName).apply();
        } else {
            mPreferences.edit().remove(KEY_CURRENT_ACCOUNT).apply();
        }

    }

    @Override
    public int getCurrentAccountExtId() {
        return mPreferences.getInt(KEY_CURRENT_ACCOUNT_EXT_ID, 0);
    }

    @Override
    public void setCurrentAccountExtId(Integer extId) {
        if (extId != null) {
            mPreferences.edit().putInt(KEY_CURRENT_ACCOUNT_EXT_ID, extId).apply();
        } else {
            mPreferences.edit().remove(KEY_CURRENT_ACCOUNT_EXT_ID).apply();
        }

    }

    @Override
    public long getLastUpdatedTime() {
        return mPreferences.getLong(KEY_LAST_UPDATED_TIME, 0);
    }

    @Override
    public void setLastUpdatedTime(long updatedTime) {
        mPreferences.edit().putLong(KEY_LAST_UPDATED_TIME, updatedTime).apply();
    }

    @Override
    public int getAppVersion() {
        return mPreferences.getInt(KEY_APP_VERSION, Integer.MIN_VALUE);
    }

    @Override
    public void setAppVersion(int appVersion) {
        mPreferences.edit().putInt(KEY_APP_VERSION, appVersion).apply();
    }

    @Override
    public String getRegistrationId() {
        return mPreferences.getString(KEY_REGISTRATION_ID, "");
    }

    @Override
    public void setRegistrationId(String registrationId) {
        mPreferences.edit().putString(KEY_REGISTRATION_ID, registrationId).apply();
    }

    @Override
    public void setLoggedOut(boolean loggedOut) {
        mPreferences.edit().putBoolean(KEY_LOGGED_OUT, loggedOut).apply();
    }

    @Override
    public boolean isLoggedOut() {
        return mPreferences.getBoolean(KEY_LOGGED_OUT, true);
    }

    @Override
    public void setTokenSent(boolean sent) {
        mPreferences.edit().putBoolean(KEY_TOKEN_SENT, sent).apply();
    }

    @Override
    public boolean isTokenSent() {
        return mPreferences.getBoolean(KEY_TOKEN_SENT, false);
    }

    @Override
    public void clearAll() {
        mPreferences.edit().clear().apply();
    }

    @Override
    public void clearAccountData() {
        setCurrentAccount(null);
        setCurrentAccountExtId(null);
    }

    @Override
    public void setNewLots(Set<String> newLots) {
        mPreferences.edit().putStringSet(KEY_NEW_LOTS, newLots).apply();
    }

    @Override
    public Set<String> getNewLots() {
        return mPreferences.getStringSet(KEY_NEW_LOTS, new TreeSet<String>());
    }
}
