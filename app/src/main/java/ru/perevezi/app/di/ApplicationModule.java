/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.Injector;
import ru.perevezi.app.PereveziApplication;
import ru.perevezi.app.di.qualifiers.ApplicationContext;
import ru.perevezi.app.ui.AboutActivity;
import ru.perevezi.app.ui.BillingActivity;
import ru.perevezi.app.ui.HomeActivity;
import ru.perevezi.app.ui.IntroActivity;
import ru.perevezi.app.ui.LoginActivity;
import ru.perevezi.app.ui.LotActivity;
import ru.perevezi.app.ui.LotWizardActiviy;
import ru.perevezi.app.ui.ProfileActivity;
import ru.perevezi.app.ui.SupportActivity;
import ru.perevezi.app.ui.UserBidsActivity;
import ru.perevezi.app.ui.UserLotsActivity;
import ru.perevezi.app.ui.SearchLotsActivity;
import ru.perevezi.app.ui.UserReviewsActivity;
import ru.perevezi.app.ui.fragments.BidCreateFragment;
import ru.perevezi.app.ui.fragments.BidFragment;
import ru.perevezi.app.ui.fragments.BillingCommissionsFragment;
import ru.perevezi.app.ui.fragments.BillingMainFragment;
import ru.perevezi.app.ui.fragments.CommentFragment;
import ru.perevezi.app.ui.fragments.ConfirmCodeFragment;
import ru.perevezi.app.ui.fragments.DeclineBidFragment;
import ru.perevezi.app.ui.fragments.LotAcceptFragment;
import ru.perevezi.app.ui.fragments.NewReviewFragment;
import ru.perevezi.app.ui.fragments.PhoneConfirmDialog;
import ru.perevezi.app.ui.fragments.RefundFragment;
import ru.perevezi.app.ui.fragments.SiteViewFragment;
import ru.perevezi.app.ui.fragments.UserBidsSectionFragment;
import ru.perevezi.app.ui.fragments.UserLotsSectionFragment;
import ru.perevezi.app.ui.fragments.RegisterFragment;
import ru.perevezi.app.ui.fragments.RouterFragment;
import ru.perevezi.app.ui.fragments.UserPhoneConfirmFragment;
import ru.perevezi.app.ui.fragments.UserReviewsSectionFragment;
import ru.perevezi.app.ui.fragments.lot.LotBidsFragment;
import ru.perevezi.app.ui.fragments.lot.LotCommentsFragment;
import ru.perevezi.app.ui.fragments.lot.LotDetailsFragment;
import ru.perevezi.app.ui.fragments.profile.ProfileFragment;
import ru.perevezi.app.ui.fragments.search.LotFilterFragment;
import ru.perevezi.app.ui.fragments.search.LotListFragment;
import ru.perevezi.app.ui.fragments.wizard.LotWizardCategoryFragment;
import ru.perevezi.app.ui.fragments.wizard.LotWizardRouteFragment;

/**
 * The dagger module associated for {@link ru.perevezi.app.PereveziApplication}
 */
@Module(
        library = true,
        includes = {
                PersistenceModule.class,
                NetworkModule.class,
                UtilsModule.class,
                RepositoryModule.class
        },
        injects = {
                AboutActivity.class,
                SupportActivity.class,
                HomeActivity.class,
                IntroActivity.class,
                LoginActivity.class,
                LotWizardActiviy.class,
                LotActivity.class,
                ProfileActivity.class,
                SearchLotsActivity.class,
                UserBidsActivity.class,
                UserLotsActivity.class,
                UserReviewsActivity.class,
                BillingActivity.class,
                PereveziApplication.class,
                RouterFragment.class,
                RegisterFragment.class,
                ConfirmCodeFragment.class,
                LotWizardCategoryFragment.class,
                LotWizardRouteFragment.class,
                LotDetailsFragment.class,
                LotBidsFragment.class,
                LotCommentsFragment.class,
                LotListFragment.class,
                LotFilterFragment.class,
                BidFragment.class,
                CommentFragment.class,
                BidCreateFragment.class,
                UserBidsSectionFragment.class,
                UserLotsSectionFragment.class,
                UserReviewsSectionFragment.class,
                DeclineBidFragment.class,
                LotAcceptFragment.class,
                SiteViewFragment.class,
                ProfileFragment.class,
                NewReviewFragment.class,
                BillingMainFragment.class,
                BillingCommissionsFragment.class,
                RefundFragment.class,
                PhoneConfirmDialog.class,
                UserPhoneConfirmFragment.class
        }
)
public class ApplicationModule {

    private Application mApplication;
    private Injector mInjector;

    public ApplicationModule(Application application, Injector injector) {
        mApplication = application;
        mInjector    = injector;
    }

    @Provides
    @Singleton
    @ApplicationContext
    public Injector provideMainInjector() {
        return mInjector;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return mApplication;
    }



}
