/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.Injector;
import ru.perevezi.app.di.qualifiers.ActivityContext;

@Module(
        library = true
)
public class ActivityModule {

    private Activity mActivity;
    private Injector mInjector;

    public ActivityModule(final AppCompatActivity activity, final Injector injector) {
        mActivity = activity;
        mInjector = injector;
    }

    @Provides
    @Singleton
    @ActivityContext
    public Context provideActivityContext() {
        return (Context) mActivity;
    }

    @Provides
    @Singleton
    public Activity provideActivity() {
        return mActivity;
    }

    @Provides
    @Singleton
    @ActivityContext
    public Injector provideInjector() {
        return mInjector;
    }

    @Provides
    @Singleton
    public FragmentManager provideFragmentManager(Activity activity) {
        return activity.getFragmentManager();
    }
}
