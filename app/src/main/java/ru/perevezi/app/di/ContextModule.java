/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.accounts.AccountManager;
import android.content.Context;
import android.location.LocationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.di.qualifiers.ApplicationContext;
import ru.perevezi.domain.utils.Preconditions;

@Module(
        library = true
)
public class ContextModule {

    private final Context mApplicationContext;

    public ContextModule(Context context) {
        Preconditions.checkState(context != null, "context cannot be null");

        mApplicationContext = context;
    }

    @Provides
    @Singleton
    @ApplicationContext
    public Context provideApplicationContext() {
        return mApplicationContext;
    }

    // ==========================================================================================================================
    // Android Services
    // ==========================================================================================================================

    @Provides @Singleton
    public AccountManager provideAccountManager() {
        return (AccountManager) mApplicationContext.getSystemService(Context.ACCOUNT_SERVICE);
    }

    @Provides @Singleton
    public LocationManager provideLocationManager() {
        return (LocationManager) mApplicationContext.getSystemService(Context.LOCATION_SERVICE);
    }

}

