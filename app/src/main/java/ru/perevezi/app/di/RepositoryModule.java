/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.content.Context;

import com.snappydb.DB;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import ru.perevezi.app.di.qualifiers.ApplicationContext;
import ru.perevezi.data.entities.mapper.BidMapper;
import ru.perevezi.data.entities.mapper.CommentMapper;
import ru.perevezi.data.entities.mapper.LotMapper;
import ru.perevezi.data.entities.mapper.UserMapper;
import ru.perevezi.data.repository.BidDataRepository;
import ru.perevezi.data.repository.CommentDataRepository;
import ru.perevezi.data.repository.LotDataRepository;
import ru.perevezi.data.repository.SettingsDataRepository;
import ru.perevezi.data.repository.UserDataRepository;
import ru.perevezi.data.repository.datasource.LotDataSourceFactory;
import ru.perevezi.data.repository.datasource.SettingsDataSourceFactory;
import ru.perevezi.data.entities.mapper.SettingsMapper;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.repository.BidRepository;
import ru.perevezi.domain.repository.CommentRepository;
import ru.perevezi.domain.repository.LotRepository;
import ru.perevezi.domain.repository.SettingsRepository;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.PereveziPreferences;

@Module(library = true, complete = false)
public class RepositoryModule {

    @Provides
    public UserRepository provideUserRepository(RestAdapter restAdapter, UserMapper userMapper,
                                                LotMapper lotMapper,
                                                PereveziPreferences pereveziPreferences, DB db,
                                                PereveziAccountManager accountManager,
                                                @ApplicationContext Context context) {
        return new UserDataRepository(restAdapter, userMapper, lotMapper, pereveziPreferences,
                db, accountManager, context);
    }

    @Provides
    public LotRepository provideLotRepository(LotDataSourceFactory lotDataSourceFactory, RestAdapter restAdapter, LotMapper lotMapper, BidMapper bidMapper, CommentMapper commentMapper) {
        return new LotDataRepository(lotDataSourceFactory, restAdapter, lotMapper, bidMapper, commentMapper);
    }


    @Provides
    SettingsRepository provideSettingsRepository(SettingsDataSourceFactory settingsDataSourceFactory, SettingsMapper settingsMapper) {
        return new SettingsDataRepository(settingsDataSourceFactory, settingsMapper);
    }

    @Provides
    CommentRepository provideCommentRepository(RestAdapter restAdapter, CommentMapper commentMapper) {
        return new CommentDataRepository(restAdapter, commentMapper);
    }

    @Provides
    BidRepository provideBidRepository(RestAdapter restAdapter, BidMapper bidMapper) {
        return new BidDataRepository(restAdapter, bidMapper);
    }
}
