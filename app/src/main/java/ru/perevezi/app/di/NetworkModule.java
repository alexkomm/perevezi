/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import ru.perevezi.data.Constants;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.data.ApiErrorHandler;
import ru.perevezi.data.ApiRequestInterceptor;
import ru.perevezi.domain.utils.PereveziPreferences;

@Module(library = true, complete = false)
public class NetworkModule {

    @Provides
    public ErrorHandler provideRestApiErrorHandler() {
        return new ApiErrorHandler();
    }

    @Provides
    public RequestInterceptor provideRestApiRequestInterceptor(PereveziAccountManager accountManager, PereveziPreferences pereveziPreferences) {
        return new ApiRequestInterceptor(accountManager, pereveziPreferences);
    }

    @Provides
    Gson provideGson() {
        /**
         * GSON instance to use for all request  with date format set up for proper parsing.
         * <p/>
         * You can also configure GSON with different naming policies for your API.
         * Maybe your API is Rails API and all json values are lower case with an underscore,
         * like this "first_name" instead of "firstName".
         * You can configure GSON as such below.
         * <p/>
         *
         * public static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd")
         *         .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create();
         */

        final String[] DATE_FORMATS = new String[]{
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd HH:mm:ss",
        };

        return new GsonBuilder()
//           .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//            @Override
//            public Date deserialize(JsonElement json, Type typeOfT,
//                                    JsonDeserializationContext context) throws JsonParseException {
//                for (String format : DATE_FORMATS) {
//                    try {
//                        return new SimpleDateFormat(format, Locale.getDefault()).parse(json.getAsString());
//                    } catch (ParseException e) {
//
//                    }
//                }
//                throw new JsonParseException("Unparseable date: \"" + json.getAsString()
//                        + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));
//                }
//            })
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    }

    @Provides
    OkClient provideOkClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(Constants.CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        client.setReadTimeout(Constants.READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

        return new OkClient(client);
    }

    @Provides
    public RestAdapter provideRestAdapter(OkClient okClient, ErrorHandler errorHandler, RequestInterceptor requestInterceptor, Gson gson) {
      return new RestAdapter.Builder()
                .setEndpoint(Constants.API_URL)
                .setClient(okClient)
                .setErrorHandler(errorHandler)
                .setRequestInterceptor(requestInterceptor)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(Constants.API_DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();
    }

}
