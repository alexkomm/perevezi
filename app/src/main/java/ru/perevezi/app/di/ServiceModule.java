/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.content.Context;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.Injector;
import ru.perevezi.app.ui.services.GcmIntentService;
import ru.perevezi.app.ui.services.RegistrationIntentService;

@Module(
        library = true,
        includes = {
                PersistenceModule.class,
                NetworkModule.class,
                RepositoryModule.class,
        },
        injects = {
                GcmIntentService.class,
                RegistrationIntentService.class,
        }
)
public class ServiceModule {

    private final android.app.Service mService;
    private final Injector mInjector;

    /**
     * Class constructor.
     *
     * @param service the Service with which this module is associated.
     */
    public ServiceModule(android.app.Service service, Injector injector) {
        mService = service;
        mInjector = injector;
    }


    /**
     * Provides the Service Context
     *
     * @return the Service Context
     */
    @Provides
    @Singleton
    @Service
    public Context provideServiceContext() {
        return mService;
    }


    @Provides
    @Singleton
    public android.app.Service provideService() {
        return mService;
    }


    @Provides
    @Singleton
    @Service
    public Injector provideServiceInjector() {
        return mInjector;
    }


    @Qualifier
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Service {
    }

}
