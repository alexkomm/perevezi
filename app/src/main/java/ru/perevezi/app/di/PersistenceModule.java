/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.AndroidPereveziPreferences;
import ru.perevezi.app.di.qualifiers.ApplicationContext;
import ru.perevezi.app.rx.RxBus;
import ru.perevezi.app.state.ApplicationState;
import ru.perevezi.app.state.SettingsState;
import ru.perevezi.app.state.UserState;
import ru.perevezi.data.cache.LotCache;
import ru.perevezi.data.cache.LotCacheImpl;
import ru.perevezi.data.cache.SettingsCache;
import ru.perevezi.data.cache.SettingsCacheImpl;
import ru.perevezi.domain.utils.PereveziPreferences;

@Module(
        library = true,
        includes = {
                ContextModule.class,
                UtilsModule.class
        }
)
public class PersistenceModule {

    @Provides @Singleton
    public PereveziPreferences providePereveziPreferences(@ApplicationContext Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        return new AndroidPereveziPreferences(prefs);
    }


    @Provides @Singleton
    DB provideSnappyDb(@ApplicationContext Context context) {
        try {
            return DBFactory.open(context);
        } catch (SnappydbException e) {
            e.printStackTrace();
            throw new IllegalStateException("Cannot create database");
        }
    }

    @Provides
    SettingsCache provideSettingsCache(DB db) {
        return new SettingsCacheImpl(db);
    }

    @Provides
    LotCache provideLotCache(DB db) {
        return new LotCacheImpl(db);
    }

    // ==========================================================================================================================
    // Application state
    // ==========================================================================================================================

    @Provides @Singleton
    public ApplicationState provideApplicationState(RxBus bus) {
        return new ApplicationState(bus);
    }


    @Provides @Singleton
    public UserState provideUserState(ApplicationState applicationState) {
        return applicationState;
    }

    @Provides @Singleton
    public SettingsState provideCategoryState(ApplicationState applicationState) {
        return applicationState;
    }
}
