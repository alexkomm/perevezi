/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.app.di;

import android.accounts.AccountManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.perevezi.app.account.AndroidAccountManager;
import ru.perevezi.data.Constants;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.app.rx.RxBus;

@Module(library = true, complete = false)
public class UtilsModule {

    @Provides @Singleton
    public RxBus provideEventBus() {
        return new RxBus();
    }

    @Provides @Singleton
    public Constants provideApiConstants() {
        return new Constants();
    }

    @Provides @Singleton
    public PereveziAccountManager provideAndroidAccountManager(AccountManager accountManager) {
       return new AndroidAccountManager(accountManager);
    }
}
