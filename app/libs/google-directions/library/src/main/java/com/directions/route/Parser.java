/*
 * Copyright (c) 2015. Перевези.рф
 */

package com.directions.route;

import java.util.List;

//. by Haseem Saheed
public interface Parser {
    public List<Route> parse();
}