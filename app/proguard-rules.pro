# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/komm/Android SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-keep class * extends java.util.ListResourceBundle {
#    protected Object[][] getContents();
#}
#
#-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
#    public static final *** NULL;
#}
#
#-keepnames @com.google.android.gms.common.annotation.KeepName class *
#-keepclassmembernames class * {
#    @com.google.android.gms.common.annotation.KeepName *;
#}
#
#-keepnames class * implements android.os.Parcelable {
#    public static final ** CREATOR;
#}
#
## Dagger
#-dontwarn dagger.internal.codegen.**
#-keepclassmembers,allowobfuscation class * {
#    @javax.inject.* *;
#    @dagger.* *;
#    <init>();
#}
#-keep class dagger.* { *; }
#-keep class javax.inject.* { *; }
#-keep class * extends dagger.internal.Binding
#-keep class * extends dagger.internal.ModuleAdapter
#-keep class * extends dagger.internal.StaticInjection
#
## Retrofit
#-keep class com.squareup.okhttp.** { *; }
#-keep class retrofit.** { *; }
#-keep interface com.squareup.okhttp.** { *; }
#
#-dontwarn com.squareup.okhttp.**
#-dontwarn okio.**
#-dontwarn retrofit.**
#-dontwarn rx.**
#-dontwarn com.squareup.javawriter.**
#
#-keepclasseswithmembers class * {
#    @retrofit.http.* <methods>;
#}
#
## If in your rest service interface you use methods with Callback argument.
#-keepattributes Exceptions
#
## If your rest service methods throw custom exceptions, because you've defined an ErrorHandler.
#-keepattributes Signature
#
## Also you must note that if you are using GSON for conversion from JSON to POJO representation, you must ignore those POJO classes from being obfuscated.
## Here include the POJO's that have you have created for mapping JSON response to POJO for example.
#
## ButterKnife 6
#
#-keep class butterknife.** { *; }
#-dontwarn butterknife.internal.**
#-keep class **$$ViewInjector { *; }
#-keepclasseswithmembernames class * {
#    @butterknife.* <fields>;
#}
#
#-keepclasseswithmembernames class * {
#    @butterknife.* <methods>;
#}
#
## Cryo
#-dontwarn sun.nio.ch.**
#-dontwarn sun.misc.**
#
#
## Nullable
#-dontwarn javax.annotation.**
#-dontwarn sun.misc.Unsafe
#-dontwarn java.beans.**