/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository;

import retrofit.RestAdapter;
import ru.perevezi.data.api.CommentApi;
import ru.perevezi.data.entities.Comment;
import ru.perevezi.data.entities.mapper.CommentMapper;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.repository.CommentRepository;
import ru.perevezi.domain.utils.Preconditions;

public class CommentDataRepository implements CommentRepository {

    private final CommentApi mCommentApi;
    private final CommentMapper mCommentMapper;

    public CommentDataRepository(RestAdapter restAdapter, CommentMapper commentMapper) {
        Preconditions.checkState(restAdapter != null && commentMapper != null, "constructor arguments cannot be null");

        mCommentApi = restAdapter.create(CommentApi.class);
        mCommentMapper = commentMapper;
    }

    @Override
    public PereveziComment createComment(PereveziComment pereveziComment) {
        Comment comment = mCommentApi.create(mCommentMapper.mapRequest(pereveziComment));

        return mCommentMapper.mapResponse(comment);
    }
}
