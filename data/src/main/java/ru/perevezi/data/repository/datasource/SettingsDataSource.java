/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import ru.perevezi.data.entities.Settings;

public interface SettingsDataSource {

    Settings getSettingsEntity();
}
