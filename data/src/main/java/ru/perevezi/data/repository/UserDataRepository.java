/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository;

import android.content.Context;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;
import ru.perevezi.data.api.UserApi;
import ru.perevezi.data.entities.Lot;
import ru.perevezi.data.entities.Session;
import ru.perevezi.data.entities.mapper.LotMapper;
import ru.perevezi.data.entities.mapper.UserMapper;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.repository.UserRepository;
import ru.perevezi.domain.utils.PereveziPreferences;

public class UserDataRepository implements UserRepository {

    private final UserMapper mUserMapper;
    private final LotMapper mLotMapper;
    private final PereveziPreferences mPereveziPreferences;
    private DB mDb;
    private final PereveziAccountManager mAccountManager;
    private final Context mContext;
    private UserApi mUserApi;

    public UserDataRepository(RestAdapter restAdapter, UserMapper userMapper,
                              LotMapper lotMapper,
                              PereveziPreferences pereveziPreferences, DB db,
                              PereveziAccountManager accountManager,
                              Context context) {
        mUserMapper = userMapper;
        mLotMapper = lotMapper;
        mPereveziPreferences = pereveziPreferences;
        mDb = db;
        mAccountManager = accountManager;
        mContext = context;
        mUserApi = restAdapter.create(UserApi.class);
    }

    @Override
    public PereveziAccount getUserById(int userId) {
        return mUserMapper.mapResponse(mUserApi.retrieve(userId));
    }

    @Override
    public void addUserPhone(int userId, String userPhone) {
        Map<String, Object> arguments = new HashMap<>(1);
        arguments.put("number", userPhone);

        mUserApi.addUserPhone(userId, arguments);
    }

    @Override
    public void confirmUserPhone(int userId, int code) {
        Map<String, Object> arguments = new HashMap<>(1);
        arguments.put("code", code);

        mUserApi.confirmUserPhone(userId, arguments);
    }

    @Override
    public void resendUserPhoneCode(int userId) {
        mUserApi.resendUserPhoneCode(userId);
    }

    @Override
    public String register(PereveziAccount pereveziAccount) {
       final RequestIdReponse res = mUserApi.smsRegister(pereveziAccount.getEmail(),
               pereveziAccount.getFullName(), pereveziAccount.getPhone(), true);

       return res.getRequestId();
    }

    @Override
    public PereveziAccount confirm(String requestId, int confirmCode) {
        Session session = mUserApi.smsRegisterConfirm(requestId, confirmCode);

        PereveziAccount pereveziAccount = mUserMapper.mapResponse(session.getUser());
        pereveziAccount.setAuthToken(session.getSessionName() + "=" + session.getSessid());

        return pereveziAccount;
    }

    @Override
    public String retrySms(String requestId) {
        final RequestIdReponse res = mUserApi.smsRegisterRetry(requestId);

        return res.getRequestId();
    }

    @Override
    public PereveziAccount getInfo() {
        Session session = mUserApi.getInfo();

        PereveziAccount pereveziAccount = mUserMapper.mapResponse(session.getUser());

        return pereveziAccount;
    }

    @Override
    public PereveziAccount login(String login, String password) {
        Session session = mUserApi.login(login, password);

        PereveziAccount pereveziAccount = mUserMapper.mapResponse(session.getUser());

        pereveziAccount.setPassword(password);
        pereveziAccount.setAuthToken(session.getSessionName() + "=" + session.getSessid());

        return pereveziAccount;
    }

    @Override
    public Boolean logout() {
        boolean result = mUserApi.logout()[0];

        try {
            mDb.destroy();
            mDb = DBFactory.open(mContext);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        PereveziAccount account = mAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());

        if (account != null) {
            mAccountManager.invalidateAuthToken(account);
            mPereveziPreferences.setCurrentAccount("");
        }

        return result;
    }

    @Override
    public List<PereveziLot> getUserLots(Map<String, Integer> argumentMap) {
        int userId = argumentMap.get("user");
        int status = argumentMap.get("status");

        Map<String, Integer> queryArgs = new HashMap<>(2);
        queryArgs.put("offset", argumentMap.get("offset"));
        queryArgs.put("limit", argumentMap.get("limit"));

        List<Lot> lots = mUserApi.lots(userId, status,
                queryArgs);

        return (List<PereveziLot>) mLotMapper.mapResponse(lots);
    }

    @Override
    public List<PereveziLot> getUserBids(Map<String, Object> argumentsMap) {
        int userId = (int) argumentsMap.get("user");
        String status = (String) argumentsMap.get("section");

        Map<String, Integer> queryArgs = new HashMap<>(2);
        queryArgs.put("offset", (Integer) argumentsMap.get("offset"));
        queryArgs.put("limit", (Integer) argumentsMap.get("limit"));

        List<Lot> lots = mUserApi.bids(userId,
                status,
                queryArgs);

        return (List<PereveziLot>) mLotMapper.mapResponse(lots);
    }

    @Override
    public List<PereveziLot> getUserReviews(int userId, int vote, Map<String, Integer>
            queryArguments) {
        return (List<PereveziLot>) mLotMapper.mapResponse(mUserApi.reviews(userId, vote, queryArguments));
    }

    @Override
    public Map<String, Object> getUserCommissions(int userId) {
        Map<String, Object> commissionsData = mUserApi.commissions(userId);

        HashMap<String, Object> result = new HashMap<>(commissionsData.size());
        result.put("count", commissionsData.get("cnt"));

        ArrayList<HashMap<String, String>> commissions = new ArrayList<>();

        if (commissionsData.get("payments") != null) {
            List<Map<String, String>> paymentsList = (List<Map<String, String>>) commissionsData.get("payments");

            for (Map<String, String> payment : paymentsList) {
                HashMap<String, String> commission = new HashMap<>(payment.size());

                commission.put("lotId", payment.get("nid"));
                commission.put("amount", payment.get("amount"));
                commission.put("paymentId", payment.get("opid"));
                commission.put("till", payment.get("till"));
                commission.put("title", payment.get("title"));

                commissions.add(commission);
            }
        }

        result.put("commissions", commissions);

        result.put("amount", commissionsData.get("sum"));

        return result;
    }

    @Override
    public ArrayList<Map<String, Object>> getUserBidsStats(int userId) {
        return mUserApi.bidStats(userId);
    }

    @Override
    public ArrayList<Map<String, Integer>> getUserLotsStats(int userId) {
        return mUserApi.lotStats(userId);
    }

    @Override
    public ArrayList<Map<String, Integer>> getUserReviewsStats(int userId) {
        return mUserApi.reviewStats(userId);
    }

    @Override
    public Integer[] getNewLotsCount(int userId) {
        return mUserApi.getNewLotsCount(userId);
    }

    @Override
    public String bindDeviceId(String registrationId) {

        return mUserApi.saveDeviceId(registrationId)[0];
    }

    @Override
    public String unbindDeviceId(String registrationId) {

        return mUserApi.removeDeviceId(registrationId)[0];
    }

    @Override
    public void addReview(Map<String, String> reviewData) {
        Map<String, String> args = new HashMap<>();

        args.put("nid", reviewData.get("lotId"));
        args.put("vote", reviewData.get("vote"));
        args.put("comment", reviewData.get("comment"));
        args.put("price", reviewData.get("price"));

        mUserApi.addReview(Integer.parseInt(reviewData.get("targetUserId")), args);
    }

    @Override
    public PereveziLot checkMissingVote(int userId) {
        return mLotMapper.mapResponse(mUserApi.getUnvotedDeal(userId));
    }

    public static class RequestIdReponse {
        private String requestId;

        public String getRequestId() {
            return requestId;
        }
    }
}
