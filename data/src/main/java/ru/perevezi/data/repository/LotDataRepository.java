/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;
import ru.perevezi.data.api.LotApi;
import ru.perevezi.data.entities.Bid;
import ru.perevezi.data.entities.Comment;
import ru.perevezi.data.entities.Lot;
import ru.perevezi.data.entities.mapper.BidMapper;
import ru.perevezi.data.entities.mapper.CommentMapper;
import ru.perevezi.data.entities.mapper.LotMapper;
import ru.perevezi.data.repository.datasource.LotCacheDataSource;
import ru.perevezi.data.repository.datasource.LotDataSource;
import ru.perevezi.data.repository.datasource.LotDataSourceFactory;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.repository.LotRepository;

public class LotDataRepository implements LotRepository {

    private final LotMapper mLotMapper;
    private final BidMapper mBidMapper;
    private final CommentMapper mCommentMapper;
    private final LotDataSourceFactory mLotDataSourceFactory;
    private final LotApi mLotApi;

    public LotDataRepository(LotDataSourceFactory lotDataSourceFactory, RestAdapter restAdapter,
                             LotMapper lotMapper, BidMapper bidMapper,
                             CommentMapper commentMapper) {

        mLotDataSourceFactory = lotDataSourceFactory;
        mLotApi = restAdapter.create(LotApi.class);
        mLotMapper = lotMapper;
        mBidMapper = bidMapper;
        mCommentMapper = commentMapper;
    }

    @Override
    public List<PereveziLot> getLotList(HashMap<String, Object> options) {
        LotDataSource lotDataSource;

        // При указании флага сброса кеша всегда используем апи
        if (options == null || !options.containsKey("reset") || !(boolean) options.get("reset")) {
            lotDataSource = mLotDataSourceFactory.create();
        } else {
            lotDataSource = mLotDataSourceFactory.createApiDataSource();
        }

        List<Lot> lotList = lotDataSource.getLotEntityList(options);

        return (List<PereveziLot>) mLotMapper.mapResponse(lotList);
    }

    @Override
    public PereveziLot getLotById(int nid, boolean resetCache) {
        LotDataSource lotDataSource;

        if (resetCache) {
            lotDataSource = mLotDataSourceFactory.createApiDataSource();
        } else {
            lotDataSource = mLotDataSourceFactory.create(nid);
        }

        return mLotMapper.mapResponse(lotDataSource.getLotEntityDetails(nid));
    }

    @Override
    public List<PereveziLot> getLotList(String[] nids) {
        LotDataSource lotDataSource = mLotDataSourceFactory.createApiDataSource();

        return (List<PereveziLot>) mLotMapper.mapResponse(lotDataSource.getLotEntityList(nids));
    }

    @Override
    public PereveziLot createLot(PereveziLot pereveziLot) {
        Lot lot = mLotMapper.mapRequest(pereveziLot);
        Lot createdLot = mLotApi.create(lot);

        return mLotMapper.mapResponse(createdLot);
    }

    @Override
    public List<PereveziBid> getLotBids(int nid) {
        List<Bid> bid = mLotApi.getBids(nid);

        return (List<PereveziBid>) mBidMapper.mapResponse(bid);
    }

    @Override
    public List<PereveziComment> getLotComments(int nid) {
        List<Comment> comments = mLotApi.getComments(nid);

        return (List<PereveziComment>) mCommentMapper.mapResponse(comments);
    }

    @Override
    public String checkBidAccess(int nid) {
        return mLotApi.bidAccess(nid)[0];
    }

    @Override
    public String checkCommentAccess(int nid) {
        return mLotApi.commentAccess(nid)[0];
    }

    @Override
    public void confirmCompletion(Map<String, Object> args) {
        int id = (int) args.get("lotId");

        Map<String, Object> apiArgs = new HashMap<>(1);
        apiArgs.put("completed", args.get("completed"));

        mLotApi.confirmCompletion(id, apiArgs);
    }

    @Override
    public boolean acceptLot(Map<String, Object> args) {
        int id = (int) args.get("lotId");

        Map<String, Object> apiArgs = new HashMap<>(args.size() - 1);
        apiArgs.put("confirmed", args.get("confirmed"));
        apiArgs.put("pay_later", args.get("payLater"));


        return mLotApi.accept(id, apiArgs)[0];
    }

    @Override
    public String refundRequest(Map<String, String> args) {
        int id = Integer.parseInt(args.get("lotId"));

        Map<String, String> apiArgs = new HashMap<>(1);
        apiArgs.put("reason", args.get("reason"));

        return mLotApi.moneyback(id, apiArgs)[0];
    }

    @Override
    public void markAsViewed(int nid) {
        LotDataSource lotDataSource = mLotDataSourceFactory.create(nid);

        if (lotDataSource instanceof LotCacheDataSource) {
            Lot lot = lotDataSource.getLotEntityDetails(nid);
            lot.setMark(Lot.LOT_MARK_READ);

            lotDataSource.updateLotEntityDetails(lot);
        }

        mLotApi.markAsViewed(nid);
    }
}
