/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository;

import ru.perevezi.data.entities.Settings;
import ru.perevezi.data.entities.mapper.SettingsMapper;
import ru.perevezi.data.repository.datasource.SettingsDataSource;
import ru.perevezi.data.repository.datasource.SettingsDataSourceFactory;
import ru.perevezi.domain.entities.PereveziSettings;
import ru.perevezi.domain.repository.SettingsRepository;

public class SettingsDataRepository implements SettingsRepository {

    private final SettingsMapper mSettingsMapper;
    private final SettingsDataSource mSettingsDataSource;

    public SettingsDataRepository(SettingsDataSourceFactory settingsDataSourceFactory, SettingsMapper settingsMapper) {
        mSettingsMapper = settingsMapper;
        mSettingsDataSource = settingsDataSourceFactory.create();
    }

    @Override
    public PereveziSettings getGeneralSettings() {
        Settings settings = mSettingsDataSource.getSettingsEntity();

        return mSettingsMapper.mapResponse(settings);
    }
}
