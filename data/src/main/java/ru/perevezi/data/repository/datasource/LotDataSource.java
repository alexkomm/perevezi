/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import java.util.HashMap;
import java.util.List;

import ru.perevezi.data.entities.Lot;

public interface LotDataSource {

    Lot getLotEntityDetails(int id);

    List<Lot> getLotEntityList(HashMap<String, Object> options);

    List<Lot> getLotEntityList(String[] ids);

    void updateLotEntityDetails(Lot lot);
}
