/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import ru.perevezi.data.cache.SettingsCache;
import ru.perevezi.data.entities.Settings;

public class SettingsCacheDataSource implements SettingsDataSource {

    private final SettingsCache mSettingsCache;

    public SettingsCacheDataSource(SettingsCache settingsCache) {
        mSettingsCache = settingsCache;
    }

    @Override
    public Settings getSettingsEntity() {
        return mSettingsCache.get();
    }
}
