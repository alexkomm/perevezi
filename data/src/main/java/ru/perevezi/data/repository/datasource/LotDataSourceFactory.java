/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import javax.inject.Inject;

import retrofit.RestAdapter;
import ru.perevezi.data.api.LotApi;
import ru.perevezi.data.cache.LotCache;
import ru.perevezi.data.entities.mapper.SearchFilterMapper;

public class LotDataSourceFactory {

    private final LotCache mLotCache;
    private final RestAdapter mRestAdapter;
    private final SearchFilterMapper mSearchFilterMapper;

    @Inject
    public LotDataSourceFactory(RestAdapter restAdapter, LotCache lotCache, SearchFilterMapper searchFilterMapper) {
        if (lotCache == null || restAdapter == null || searchFilterMapper == null) {
            throw new IllegalArgumentException("constructor arguments cannot be null");
        }
        mRestAdapter = restAdapter;
        mLotCache = lotCache;
        mSearchFilterMapper = searchFilterMapper;

    }

    public LotDataSource create(int id) {
        if (mLotCache.isCached(id) && !mLotCache.isExpired(id)) {
            return createCacheDataSource();
        } else {
            return createApiDataSource();
        }
    }

    public LotDataSource create() {
        if (mLotCache.isCached()) {
            return createCacheDataSource();
        } else {
            return createApiDataSource();
        }
    }

    public LotDataSource createCacheDataSource() {
        return new LotCacheDataSource(mLotCache);
    }

    public LotDataSource createApiDataSource() {
        LotApi lotApi = mRestAdapter.create(LotApi.class);
        return new LotApiDataSource(lotApi, mLotCache, mSearchFilterMapper);
    }

}
