/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import java.util.HashMap;
import java.util.List;

import ru.perevezi.data.api.LotApi;
import ru.perevezi.data.cache.LotCache;
import ru.perevezi.data.entities.Lot;
import ru.perevezi.data.entities.mapper.SearchFilterMapper;

public class LotApiDataSource implements LotDataSource {
    private final LotApi mLotApi;
    private final LotCache mLotCache;
    private final SearchFilterMapper mSearchFilterMapper;

    public LotApiDataSource(LotApi lotApi, LotCache lotCache, SearchFilterMapper searchFilterMapper) {
        mLotApi = lotApi;
        mLotCache = lotCache;
        mSearchFilterMapper = searchFilterMapper;
    }

    @Override
    public Lot getLotEntityDetails(int id) {
        Lot lot = mLotApi.get(id);

        putLotInCache(lot);
        lot.setCacheUpdatedTimestamp(0);

        return lot;
    }

    @Override
    public List<Lot> getLotEntityList(HashMap<String, Object> options) {

        List<Lot> lotList;

        lotList = mLotApi.index(mSearchFilterMapper.mapRequest(options));

        boolean evictCache = options.get("offset") == null || options.get("offset").equals("0");

        if (evictCache) {
            mLotCache.evictAll();
        }

        for (Lot lot : lotList) {
            putLotInCache(lot);
            lot.setCacheUpdatedTimestamp(0);
        }

        mLotCache.putFilter(options);

        return lotList;
    }

    @Override
    public List<Lot> getLotEntityList(String[] ids) {
        return mLotApi.index(ids, "all");
    }

    @Override
    public void updateLotEntityDetails(Lot lot) {
        throw new UnsupportedOperationException("Not supported in api data source");
    }

    private void putLotInCache(Lot lot) {
        mLotCache.put(lot);
    }

}
