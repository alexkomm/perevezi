/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import ru.perevezi.data.api.SettingsApi;
import ru.perevezi.data.cache.SettingsCache;
import ru.perevezi.data.entities.Settings;

public class SettingsApiDataSource implements SettingsDataSource {

    private final SettingsApi mSettingsApi;
    private final SettingsCache mSettingsCache;

    public SettingsApiDataSource(SettingsApi settingsApi, SettingsCache settingsCache) {
        mSettingsApi = settingsApi;
        mSettingsCache = settingsCache;
    }

    @Override
    public Settings getSettingsEntity() {
        Settings settings = mSettingsApi.getGeneralSettings();
        putGeneralSettingsInCache(settings);

        return settings;
    }

    private void putGeneralSettingsInCache(Settings settings) {
        if (settings != null) {
            mSettingsCache.put(settings);
        }
    }


}
