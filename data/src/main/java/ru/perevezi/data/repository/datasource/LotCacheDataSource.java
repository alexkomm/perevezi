/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import java.util.HashMap;
import java.util.List;

import ru.perevezi.data.cache.LotCache;
import ru.perevezi.data.entities.Lot;

public class LotCacheDataSource implements LotDataSource {

    private final LotCache mLotCache;

    public LotCacheDataSource(LotCache lotCache) {
        mLotCache = lotCache;
    }

    @Override
    public Lot getLotEntityDetails(int id) {
        return mLotCache.get(id);
    }

    @Override
    public List<Lot> getLotEntityList(HashMap<String, Object> options) {
        return mLotCache.getAll();
    }

    @Override
    public List<Lot> getLotEntityList(String[] ids) {
        throw new UnsupportedOperationException("Not supported method");
    }

    @Override
    public void updateLotEntityDetails(Lot lot) {
        mLotCache.put(lot);
    }
}
