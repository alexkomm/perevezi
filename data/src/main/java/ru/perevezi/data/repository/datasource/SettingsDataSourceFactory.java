/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository.datasource;

import javax.inject.Inject;

import retrofit.RestAdapter;
import ru.perevezi.data.api.SettingsApi;
import ru.perevezi.data.cache.SettingsCache;

public class SettingsDataSourceFactory {

    private final SettingsCache mSettingsCache;
    private final RestAdapter mRestAdapter;

    @Inject
    public SettingsDataSourceFactory(RestAdapter restAdapter, SettingsCache settingsCache) {
        if (settingsCache == null || restAdapter == null) {
            throw new IllegalArgumentException("constructor arguments cannot be null");
        }

        mSettingsCache = settingsCache;
        mRestAdapter = restAdapter;
    }

    public SettingsDataSource create() {
        SettingsDataSource settingsDataSource;

        try {
            if (!mSettingsCache.isExpired() && mSettingsCache.isCached()) {
                settingsDataSource = new SettingsCacheDataSource(mSettingsCache);
            } else {
                settingsDataSource = createApiDataSource();
            }

        } catch (Exception e) { // In case of something goes wrong we are using the api data source
            e.printStackTrace();
            settingsDataSource = createApiDataSource();
        }

        return settingsDataSource;
    }

    public SettingsDataSource createApiDataSource() {
        SettingsApi settingsApi = mRestAdapter.create(SettingsApi.class);

        return new SettingsApiDataSource(settingsApi, mSettingsCache);
    }
}
