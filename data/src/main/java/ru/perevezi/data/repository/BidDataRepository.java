/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.repository;

import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;
import ru.perevezi.data.api.BidApi;
import ru.perevezi.data.entities.Bid;
import ru.perevezi.data.entities.PaymentInfo;
import ru.perevezi.data.entities.mapper.BidMapper;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.repository.BidRepository;

public class BidDataRepository implements BidRepository {

    private final BidMapper mBidMapper;
    private final BidApi mBidApi;

    public BidDataRepository(RestAdapter restAdapter, BidMapper bidMapper) {
        mBidApi = restAdapter.create(BidApi.class);
        mBidMapper = bidMapper;
    }

    @Override
    public PereveziBid createBid(PereveziBid pereveziBid) {
        Bid bid = mBidMapper.mapRequest(pereveziBid);

        bid = mBidApi.create(bid);

        return mBidMapper.mapResponse(bid);
    }

    @Override
    public void declineBid(Map<String, Object> declineData) {
        int id = (int) declineData.get("bid_id");

        Map<String, Object> apiArgs = new HashMap<>(3);
        apiArgs.put("nid", declineData.get("lot_id"));
        apiArgs.put("decline_options", declineData.get("decline_options"));
        apiArgs.put("reason", declineData.get("reason"));

        mBidApi.decline(id, apiArgs);
    }

    @Override
    public void acceptBid(Map<String, Object> acceptData) {
        int id = (int) acceptData.get("bid_id");

        Map<String, Object> apiArgs = new HashMap<>(3);
        apiArgs.put("nid", acceptData.get("lot_id"));
        apiArgs.put("decline_options", acceptData.get("decline_options"));
        apiArgs.put("reason", acceptData.get("reason"));

        mBidApi.accept(id, apiArgs);
    }

    @Override
    public Map<String, Object> getConfirmationInfo(int id) {
        PaymentInfo paymentInfo = mBidApi.payment(id);

        HashMap<String, Object> result = new HashMap<>(5);
        result.put("bid", paymentInfo.bid);
        result.put("amount", paymentInfo.amount);
        result.put("commission", paymentInfo.commission);
        result.put("balance", paymentInfo.balance);
        result.put("confirm_status", paymentInfo.confirmStatus);

        return result;
    }



}
