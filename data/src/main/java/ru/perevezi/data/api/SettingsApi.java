/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.api;

import java.util.List;

import retrofit.http.POST;
import ru.perevezi.data.entities.Category;
import ru.perevezi.data.entities.Settings;

public interface SettingsApi {

    @POST("/settings/categories")
    List<Category> getCategorySettings();

    @POST("/settings/general")
    Settings getGeneralSettings();
}
