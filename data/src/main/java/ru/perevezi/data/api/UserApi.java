/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.QueryMap;
import ru.perevezi.data.entities.Lot;
import ru.perevezi.data.entities.Session;
import ru.perevezi.data.entities.User;
import ru.perevezi.data.repository.UserDataRepository;

public interface UserApi {

    @FormUrlEncoded
    @POST("/user/login")
    Session login(@Field("username") String login, @Field("password") String password);

    @POST("/user/logout")
    Boolean[] logout();

    @POST("/system/connect")
    Session getInfo();

    @GET("/user/retrieve/{id}")
    User retrieve(int id);

    @FormUrlEncoded
    @POST("/user/smsRegister")
    UserDataRepository.RequestIdReponse smsRegister(
            @Field("mail") String mail,
            @Field("profile_name") String name,
            @Field("profile_phone") String phone,
            @Field("legal_accept") boolean legalAccept
    );

    @FormUrlEncoded
    @POST("/user/smsRegisterConfirm")
    Session smsRegisterConfirm(
            @Field("requestId") String requestId,
            @Field("confirmCode") int confirmCode
    );

    @FormUrlEncoded
    @POST("/user/smsRegisterRetry")
    UserDataRepository.RequestIdReponse smsRegisterRetry(
            @Field("requestId") String requestId
    );

    @FormUrlEncoded
    @POST("/user/saveDeviceId")
    String[] saveDeviceId(@Field("deviceId") String registrationId);

    @FormUrlEncoded
    @POST("/user/removeDeviceId")
    String[] removeDeviceId(@Field("deviceId") String registrationId);

    @GET("/user/{id}/lots/{status}")
    List<Lot> lots(@Path("id") int id, @Path("status") int status, @QueryMap Map<String, Integer> queryArgs);


    @GET("/user/{id}/bids/{section}")
    List<Lot> bids(@Path("id") int id, @Path("section") String section, @QueryMap Map<String, Integer> queryArgs);

    @GET("/user/{id}/reviews/{type}")
    List<Lot> reviews(@Path("id") int id, @Path("type") int type, @QueryMap Map<String, Integer> queryArgs);

    @GET("/user/{id}/payments")
    Map<String, Object> commissions(@Path("id") int id);

    @GET("/user/{id}/bidStats")
    ArrayList<Map<String, Object>> bidStats(@Path("id") int id);

    @GET("/user/{id}/lotStats")
    ArrayList<Map<String, Integer>> lotStats(@Path("id") int id);

    @GET("/user/{id}/reviewStats")
    ArrayList<Map<String, Integer>> reviewStats(@Path("id") int id);

    @GET("/user/{id}/newLotsCount")
    Integer[] getNewLotsCount(@Path("id") int id);

    @GET("/user/{id}/unvotedDeal")
    Lot getUnvotedDeal(@Path("id") int id);

    @POST("/user/{id}/review")
    Object[] addReview(@Path("id") int id, @Body Map<String, String> reviewData);

    @POST("/user/{id}/smsUserAdd")
    Object[] addUserPhone(@Path("id") int id, @Body Map<String, Object> arguments);

    @POST("/user/{id}/smsUserConfirm")
    Object[] confirmUserPhone(@Path("id") int id, @Body Map<String, Object> arguments);

    @POST("/user/{id}/smsUserResend")
    Object[] resendUserPhoneCode(@Path("id") int id);
}
