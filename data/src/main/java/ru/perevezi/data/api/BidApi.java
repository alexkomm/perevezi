/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.api;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import ru.perevezi.data.entities.Bid;
import ru.perevezi.data.entities.PaymentInfo;

public interface BidApi {

    @POST("/bid")
    Bid create(@Body Bid bid);

    @POST("/bid/{id}/decline")
    boolean[] decline(@Path("id") int id, @Body Map<String, Object> declineData);

    @POST("/bid/{id}/accept")
    boolean[] accept(@Path("id") int id, @Body Map<String, Object> acceptData);

    @GET("/bid/{id}/payment")
    PaymentInfo payment(@Path("id") int id);
}
