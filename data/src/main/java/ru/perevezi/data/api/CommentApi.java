/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.api;

import retrofit.http.Body;
import retrofit.http.POST;
import ru.perevezi.data.entities.Comment;

public interface CommentApi {

    @POST("/comment")
    Comment create(@Body Comment lot);
}
