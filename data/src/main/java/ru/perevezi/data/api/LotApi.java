/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.api;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import ru.perevezi.data.entities.Bid;
import ru.perevezi.data.entities.Comment;
import ru.perevezi.data.entities.Lot;

public interface LotApi {

    @GET("/lot")
    List<Lot> index(@QueryMap Map<String, Object> options);

    @GET("/lot")
    List<Lot> index(@Query("nids[]") String[] nids, @Query("statuses[]") int[] statuses);

    @GET("/lot")
    List<Lot> index(@Query("nids[]") String[] nids, @Query("statuses") String statuses);

    @POST("/lot")
    Lot create(@Body Lot lot);

    @GET("/lot/{id}")
    Lot get(@Path("id") int id);

    @GET("/lot/{id}/bids")
    List<Bid> getBids(@Path("id") int id);

    @GET("/lot/{id}/comments")
    List<Comment> getComments(@Path("id") int id);

    @GET("/lot/{id}/bidAccess")
    String[] bidAccess(@Path("id") int id);

    @GET("/lot/{id}/commentAccess")
    String[] commentAccess(@Path("id") int id);

    @POST("/lot/{id}/confirmCompletion")
    Object[] confirmCompletion(@Path("id") int id, @Body Map<String, Object> args);

    @POST("/lot/{id}/accept")
    boolean[] accept(@Path("id") int id, @Body Map<String, Object> args);

    @POST("/lot/{id}/moneyback")
    String[] moneyback(@Path("id") int id, @Body Map<String, String> args);

    @POST("/lot/{id}/markAsViewed")
    Object[] markAsViewed(@Path("id") int id);
}
