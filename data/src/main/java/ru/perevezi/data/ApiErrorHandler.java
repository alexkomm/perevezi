/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ru.perevezi.data.exception.NotAcceptableException;
import ru.perevezi.data.exception.NotAuthorizedException;

public class ApiErrorHandler implements ErrorHandler {

    private static final int STATUS_NOT_ACCEPTABLE = 406;
    private static final int STATUS_NOT_AUTHORIZED = 401;

    @Override public Throwable handleError(RetrofitError cause) {
        Response r = cause.getResponse();

        if (r != null) {
            switch (r.getStatus()) {
                case STATUS_NOT_ACCEPTABLE:
                    try {
                        ApiErrorResponse apiErrorResponse = (ApiErrorResponse) cause.getBodyAs(ApiErrorResponse.class);

                        return new NotAcceptableException(r.getReason(), apiErrorResponse.getFormErrors(), apiErrorResponse.getCode());
                    } catch (Exception e) {
                        return new NotAcceptableException(r.getReason(), null, 0);
                    }
                case STATUS_NOT_AUTHORIZED:
                    return new NotAuthorizedException(r.getReason());
                default:
                    return cause;
            }


        }

        return cause;
    }

    public static class ApiErrorResponse {
        private int code;

        @SerializedName("form_errors")
        private Map<String, String> formErrors;

        public int getCode() {
            return code;
        }

        public Map<String, String> getFormErrors() {
            return formErrors;
        }
    }
}
