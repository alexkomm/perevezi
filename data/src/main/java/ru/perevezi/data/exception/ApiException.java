/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.exception;

import ru.perevezi.data.ApiErrorHandler;

public class ApiException extends RuntimeException {

    private ApiErrorHandler.ApiErrorResponse mError;

    public ApiException(Throwable throwable) {
        super(throwable);
    }

    public ApiException(Throwable throwable, ApiErrorHandler.ApiErrorResponse error) {
        super(throwable);
        mError = error;
    }

    public ApiErrorHandler.ApiErrorResponse getError() {
        return mError;
    }
}
