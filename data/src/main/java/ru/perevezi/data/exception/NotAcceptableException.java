/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.exception;

import java.util.Map;

public class NotAcceptableException extends RuntimeException {

    private final Map<String, String> mFormErrors;

    private final int mErrorCode;

    public NotAcceptableException(String detailMessage, Map<String, String> formErrors, int errorCode) {
        super(detailMessage);

        mFormErrors = formErrors;
        mErrorCode = errorCode;
    }

    public Map<String, String> getFormErrors() {
        return mFormErrors;
    }

    public int getErrorCode() {
        return mErrorCode;
    }
}
