/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.exception;

public class NotAuthorizedException extends RuntimeException {

    public NotAuthorizedException(String detailMessage) {
        super(detailMessage);
    }

}
