/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.cache;

import android.util.Log;

import com.snappydb.DB;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import ru.perevezi.data.entities.Lot;

public class LotCacheImpl implements LotCache {

    private static final String LOT_KEY = "lot";
    public static final String FILTER_KEY = "filter";

    private static final long EXPIRATION_TIME = 300000;
    private static final String TAG = "LotCache";

    private final DB mDb;

    public LotCacheImpl(DB db) {
        mDb = db;
    }

    @Override
    public Lot get(int id) {
        synchronized (mDb) {
            try {
                return mDb.getObject(LOT_KEY + ":" + id, Lot.class);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());

                return null;
            }
        }
    }

    @Override
    public List<Lot> getAll() {
        synchronized (mDb) {
            List<Lot> lotList = new ArrayList<>();
            try {
                for (String key : mDb.findKeys(LOT_KEY)) {
                    lotList.add(mDb.getObject(key, Lot.class));
                }
            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            Collections.reverse(lotList);

            return lotList;
        }
    }

    @Override
    public void put(Lot lot) {
        synchronized (mDb) {
            try {
                lot.setCacheUpdatedTimestamp(System.currentTimeMillis());

                mDb.put(LOT_KEY + ":" + lot.getNid(), lot);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }

    @Override
    public HashMap<String, Object> getFilter() {
        synchronized (mDb) {
            try {
                return mDb.get(FILTER_KEY, HashMap.class);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
                return null;
            }
        }
    }

    @Override
    public void putFilter(HashMap<String, Object> options) {
        synchronized (mDb) {
            try {
                mDb.put(FILTER_KEY, options);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }


    @Override
    public boolean isExpired(int id) {
        synchronized (mDb) {
            Lot lot = get(id);

            long currentTime = System.currentTimeMillis();
            long lastUpdateTime = lot.getCacheUpdatedTimestamp();

            boolean expired = ((currentTime - lastUpdateTime) > EXPIRATION_TIME);

            if (expired) {
                this.evictAll();
            }

            return expired;
        }
    }

    @Override
    public boolean isCached(int id) {
        synchronized (mDb) {
            try {
                return mDb.exists(LOT_KEY + ":" + id);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());

                return false;
            }
        }

    }

    @Override
    public boolean isCached() {
        synchronized (mDb) {
            try {
                return mDb.countKeys(LOT_KEY) > 0;
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());

                return false;
            }
        }
    }

    @Override
    public void evictAll() {
        synchronized (mDb) {
            try {
                for (String key : mDb.findKeys(LOT_KEY)) {
                    mDb.del(key);
                }

                mDb.del(FILTER_KEY);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }
}
