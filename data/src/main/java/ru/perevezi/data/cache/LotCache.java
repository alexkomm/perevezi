/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.cache;

import java.util.HashMap;
import java.util.List;

import ru.perevezi.data.entities.Lot;

public interface LotCache {

    Lot get(final int id);

    List<Lot> getAll();

    void put(final Lot lot);

    HashMap<String, Object> getFilter();

    void putFilter(final HashMap<String, Object> options);

    boolean isExpired(final int id);

    boolean isCached();

    boolean isCached(final int id);

    void evictAll();
}
