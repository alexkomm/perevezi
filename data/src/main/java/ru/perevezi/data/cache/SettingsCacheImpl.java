/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.cache;

import android.util.Log;

import com.snappydb.DB;
import com.snappydb.SnappydbException;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.perevezi.data.entities.Settings;

@Singleton
public class SettingsCacheImpl implements SettingsCache {

    private static final String SETTINGS_KEY_GENERAL_SETTINGS = "general_settings";
    private static final String SETTINGS_KEY_LAST_CACHE_UPDATE = "settings_last_cache_update";

    private static final long EXPIRATION_TIME = 60 * 60 * 24 * 1000;
    private static final String TAG = "SettingsCache";

    private final DB mDb;

    @Inject
    public SettingsCacheImpl(DB db) {
        mDb = db;
    }

    @Override
    public Settings get() {
        synchronized (mDb) {
            try {
                return mDb.getObject(SETTINGS_KEY_GENERAL_SETTINGS, Settings.class);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
                return null;
            }
        }
    }

    @Override
    public void put(Settings settings) {
        synchronized (mDb) {
            try {
                mDb.put(SETTINGS_KEY_GENERAL_SETTINGS, settings);
                mDb.putLong(SETTINGS_KEY_LAST_CACHE_UPDATE, System.currentTimeMillis());
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }

    @Override
    public boolean isExpired() {
        synchronized (mDb) {
            try {
                long currentTime = System.currentTimeMillis();
                long lastUpdateTime = mDb.getLong(SETTINGS_KEY_LAST_CACHE_UPDATE);

                boolean expired = ((currentTime - lastUpdateTime) > EXPIRATION_TIME);

                if (expired) {
                    evict();
                }

                return expired;
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
                return true;
            }
        }
    }

    @Override
    public boolean isCached() {
        synchronized (mDb) {
            try {
                return mDb.findKeys(SETTINGS_KEY_GENERAL_SETTINGS).length > 0;
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
                return false;
            }
        }
    }

    @Override
    public void evict() {
        synchronized (mDb) {
            try {
                mDb.del(SETTINGS_KEY_GENERAL_SETTINGS);
                mDb.del(SETTINGS_KEY_LAST_CACHE_UPDATE);
            } catch (SnappydbException e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    }
}
