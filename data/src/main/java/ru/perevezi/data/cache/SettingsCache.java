/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.cache;

import ru.perevezi.data.entities.Settings;

public interface SettingsCache {

    Settings get();

    void put(Settings settings);

    boolean isExpired();

    boolean isCached();

    void evict();
}
