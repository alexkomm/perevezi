/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import ru.perevezi.domain.mapper.RequestMapper;

public class SearchFilterMapper implements RequestMapper<HashMap<String, Object>, Map<String, Object>> {

    @Inject
    public SearchFilterMapper() {
    }

    @Override
    public HashMap<String, Object> mapRequest(Map<String, Object> entity) {
        if (entity == null) {
            return null;
        }

        HashMap<String, Object> resultMap = new HashMap<>();

        for (String key : entity.keySet()) {
            if (entity.get(key) == null) {
                continue;
            }

            switch (key) {
                case "date_from":
                    long d1 = (long) entity.get(key);

                    if (d1 > 0) {
                        resultMap.put("d1", Long.toString(d1 / 1000));
                    }

                    break;
                case "date_till":
                    long d2 = (long) entity.get(key);

                    if (d2 > 0) {
                        resultMap.put("d2", Long.toString(d2 / 1000));
                    }

                    break;
                case "distance":
                    float[] distanceArr = (float[]) entity.get(key);

                    float di1 = distanceArr[0];
                    float di2 = distanceArr[1];

                    resultMap.put("di1", Float.toString(di1));

                    if (di2 > 0) {
                        resultMap.put("di2", Float.toString(di2));
                    }

                    break;
                case "weight":
                    float[] weightArr = (float[]) entity.get(key);

                    float w1 = weightArr[0];
                    float w2 = weightArr[1];

                    resultMap.put("w1", Float.toString(w1));

                    if (w2 > 0) {
                        resultMap.put("w2", Float.toString(w2));
                    }

                    break;
                case "category":
                    Set<Integer> categorySet = (Set<Integer>) entity.get(key);

                    StringBuilder result = new StringBuilder();
                    String separator = "_";

                    for (Integer catId : categorySet) {
                        result.append(separator).append(catId);
                    }

                    resultMap.put("category", result.substring(separator.length()));

                    break;
                case "region":
                    String[] regionArr = ((String) entity.get(key)).split(", ");

                    if (regionArr.length > 1) {
                        resultMap.put("region", regionArr[0]);
                        resultMap.put("country", regionArr[1]);
                    } else {
                        resultMap.put("country", regionArr[0]);
                    }

                    break;
                default:
                    resultMap.put(key, String.valueOf(entity.get(key)));
                    break;
            }
        }

        return resultMap;
    }

    @Override
    public Collection<HashMap<String, Object>> mapRequest(Collection<Map<String,
            Object>> entityCollection) {
        return null;
    }
}
