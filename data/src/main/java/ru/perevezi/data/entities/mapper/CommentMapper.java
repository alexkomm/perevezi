/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.data.entities.Comment;
import ru.perevezi.domain.entities.PereveziComment;
import ru.perevezi.domain.mapper.BidirectionalMapper;

public class CommentMapper implements BidirectionalMapper<Comment, PereveziComment> {

    @Inject
    public CommentMapper() {
    }

    @Override
    public PereveziComment mapResponse(Comment comment) {
        PereveziComment pereveziComment = new PereveziComment();

        pereveziComment.setAuthorName(comment.getAuthorName());
        pereveziComment.setBidAmount(comment.getBidAmount());
        pereveziComment.setCid(comment.getCid());
        pereveziComment.setMail(comment.getMail());
        pereveziComment.setComment(comment.getComment());
        pereveziComment.setName(comment.getName());
        pereveziComment.setPid(comment.getPid());
        pereveziComment.setStatus(comment.getStatus());
        pereveziComment.setNid(comment.getNid());
        pereveziComment.setSubject(comment.getSubject());
        pereveziComment.setThread(comment.getThread());
        pereveziComment.setTimestamp(comment.getTimestamp());
        pereveziComment.setUid(comment.getUid());
        
        return pereveziComment;
    }

    @Override
    public Collection<PereveziComment> mapResponse(Collection<Comment> entityCollection) {
        List<PereveziComment> comments = new ArrayList<>();
        PereveziComment pereveziComment;

        for (Comment commentEntity : entityCollection) {
            pereveziComment = mapResponse(commentEntity);
            comments.add(pereveziComment);
        }

        return comments;
    }

    @Override
    public Comment mapRequest(PereveziComment pereveziComment) {
        Comment comment = new Comment();

        comment.setComment(pereveziComment.getComment());
        comment.setNid(pereveziComment.getNid());
        comment.setPid(pereveziComment.getPid());

        return  comment;
    }

    @Override
    public Collection<Comment> mapRequest(Collection<PereveziComment> entityCollection) {
        return null;
    }
}
