/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.SerializedName;

public class Session {

    private String sessid;

    @SerializedName("session_name")
    private String sessionName;

    private User user;

    public String getSessid() {
        return sessid;
    }

    public void setSessid(String sessid) {
        this.sessid = sessid;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
