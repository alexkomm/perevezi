/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Lot {

    public static final int LOT_STATUS_NEW = 0;
    public static final int LOT_STATUS_ACTIVE = 1;
    public static final int LOT_STATUS_WON = 2;
    public static final int LOT_STATUS_IN_PROCESS = 3;
    public static final int LOT_STATUS_COMPLETED = 4;
    public static final int LOT_STATUS_EXPIRED = 5;
    public static final int LOT_STATUS_CANCELED = 6;

    public static final int LOT_ACTION_CLIENT_VOTE_PLACED = 1;
    public static final int LOT_ACTION_CLIENT_NEED_VOTE = 2;
    public static final int LOT_ACTION_CANCELED = 3;
    public static final int LOT_ACTION_CLIENT_EXPIRED_WITH_BIDS = 4;
    public static final int LOT_ACTION_CLIENT_EXPIRED_NO_BIDS = 5;
    public static final int LOT_ACTION_CLIENT_CONFIRM_COMPLETION = 6;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_COMPLETION = 7;
    public static final int LOT_ACTION_PROVIDER_CONFIRM_WON = 8;
    public static final int LOT_ACTION_CLIENT_WAIT_CONFIRMATION = 9;
    public static final int LOT_ACTION_CLIENT_REVIEW_BIDS = 10;
    public static final int LOT_ACTION_CLIENT_WAIT_BIDS = 11;
    public static final int LOT_ACTION_PROVIDER_MAKE_BIDS = 12;

    public static final int LOT_MARK_READ = 0;
    public static final int LOT_MARK_NEW = 1;
    public static final int LOT_MARK_UPDATED = 2;

    @Expose
    private User author;

    @Expose
    private int nid;
    @Expose
    private long created;
    @Expose
    private String type = "lot";
    @Expose
    private String uid;
    @Expose
    private String title;
    @SerializedName("item")
    @Expose
    private List<TitleItem> titleItems;
    @Expose
    private String body;
    @Expose
    private long expiry;
    @SerializedName("ship_date")
    @Expose
    private long shipDate;
    @SerializedName("with_ship_time")
    @Expose
    private boolean withShipTime;
    @SerializedName("need_load")
    @Expose
    private int needLoad;
    @SerializedName("start_price")
    @Expose
    private BigDecimal startPrice;
    @Expose
    private BigDecimal price;
    @Expose
    private Weight weight;
    @Expose
    private Float volume;
    @Expose
    private int pro;
    @Expose
    private String distance;
    @SerializedName("recipient_country")
    @Expose
    private String recipientCountry;
    @SerializedName("recipient_region")
    @Expose
    private String recipientRegion;
    @SerializedName("custom_recipient_city")
    @Expose
    private String customRecipientCity;
    @SerializedName("recipient_city")
    @Expose
    private String recipientCity;
    @SerializedName("recipient_address")
    @Expose
    private String recipientAddress;
    @SerializedName("recipient_metro")
    @Expose
    private String recipientMetro;
    @SerializedName("sender_country")
    @Expose
    private String senderCountry;
    @SerializedName("sender_region")
    @Expose
    private String senderRegion;
    @SerializedName("custom_sender_city")
    @Expose
    private String customSenderCity;
    @SerializedName("sender_city")
    @Expose
    private String senderCity;
    @SerializedName("sender_address")
    @Expose
    private String senderAddress;
    @SerializedName("sender_metro")
    @Expose
    private String senderMetro;
    @SerializedName("count_bids")
    @Expose
    private int countBids;
    @SerializedName("count_active_bids")
    @Expose
    private int countActiveBids;
    @SerializedName("lot_status")
    @Expose
    private int lotStatus;
    @Expose
    private int category;
    @SerializedName("route_points")
    @Expose
    private List<RoutePoint> routePoints = new ArrayList<RoutePoint>();

    @SerializedName("related_bid")
    @Expose
    private Bid relatedBid;

    @SerializedName("min_bid")
    @Expose
    private BigDecimal minBid;

    @SerializedName("author_phone")
    @Expose
    private String authorPhone;

    @SerializedName("action_code")
    @Expose
    private int actionCode;

    private Map<String, String> review;

    private int mark;

    // CACHE FIELDS

    private long cacheUpdatedTimestamp;

    /**
     * @return The routePoints
     */
    public List<RoutePoint> getRoutePoints() {
        return routePoints;
    }

    /**
     * @param routePoints The route_points
     */
    public void setRoutePoints(List<RoutePoint> routePoints) {
        this.routePoints = routePoints;
    }

    /**
     * @return The nid
     */
    public int getNid() {
        return nid;
    }

    /**
     * @param nid The nid
     */
    public void setNid(int nid) {
        this.nid = nid;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public List<TitleItem> getTitleItems() {
        return titleItems;
    }

    public void setTitleItems(List<TitleItem> titleItems) {
        this.titleItems = titleItems;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The expiry
     */
    public long getExpiry() {
        return expiry;
    }

    /**
     * @param expiry The expiry
     */
    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    /**
     * @return The shipDate
     */
    public long getShipDate() {
        return shipDate;
    }

    /**
     * @param shipDate The ship_date
     */
    public void setShipDate(long shipDate) {
        this.shipDate = shipDate;
    }


    public boolean isWithShipTime() {
        return withShipTime;
    }

    public void setWithShipTime(boolean withShipTime) {
        this.withShipTime = withShipTime;
    }

    /**
     * @return The needLoad
     */
    public int getNeedLoad() {
        return needLoad;
    }

    /**
     * @param needLoad The need_load
     */
    public void setNeedLoad(int needLoad) {
        this.needLoad = needLoad;
    }

    /**
     * @return The startPrice
     */
    public BigDecimal getStartPrice() {
        return startPrice;
    }

    /**
     * @param startPrice The start_price
     */
    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    /**
     * @return The price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return The weight
     */
    public Weight getWeight() {
        return weight;
    }

    /**
     * @param weight The weight
     */
    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    /**
     * @return The volume
     */
    public float getVolume() {
        return volume;
    }

    /**
     * @param volume The volume
     */
    public void setVolume(float volume) {
        this.volume = volume;
    }

    /**
     * @return The pro
     */
    public int getPro() {
        return pro;
    }

    /**
     * @param pro The pro
     */
    public void setPro(int pro) {
        this.pro = pro;
    }

    /**
     * @return The distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }


    /**
     * @return The recipientCountry
     */
    public String getRecipientCountry() {
        return recipientCountry;
    }

    /**
     * @param recipientCountry The recipient_country
     */
    public void setRecipientCountry(String recipientCountry) {
        this.recipientCountry = recipientCountry;
    }

    /**
     * @return The recipientRegion
     */
    public String getRecipientRegion() {
        return recipientRegion;
    }

    /**
     * @param recipientRegion The recipient_region
     */
    public void setRecipientRegion(String recipientRegion) {
        this.recipientRegion = recipientRegion;
    }

    /**
     * @return The customRecipientCity
     */
    public String getCustomRecipientCity() {
        return customRecipientCity;
    }

    /**
     * @param customRecipientCity The custom_recipient_city
     */
    public void setCustomRecipientCity(String customRecipientCity) {
        this.customRecipientCity = customRecipientCity;
    }

    /**
     * @return The recipientCity
     */
    public String getRecipientCity() {
        return recipientCity;
    }

    /**
     * @param recipientCity The recipient_city
     */
    public void setRecipientCity(String recipientCity) {
        this.recipientCity = recipientCity;
    }

    /**
     * @return The recipientAddress
     */
    public String getRecipientAddress() {
        return recipientAddress;
    }

    /**
     * @param recipientAddress The recipient_address
     */
    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    /**
     * @return The recipientMetro
     */
    public String getRecipientMetro() {
        return recipientMetro;
    }

    /**
     * @param recipientMetro The recipient_metro
     */
    public void setRecipientMetro(String recipientMetro) {
        this.recipientMetro = recipientMetro;
    }

    /**
     * @return The senderCountry
     */
    public String getSenderCountry() {
        return senderCountry;
    }

    /**
     * @param senderCountry The sender_country
     */
    public void setSenderCountry(String senderCountry) {
        this.senderCountry = senderCountry;
    }

    /**
     * @return The senderRegion
     */
    public String getSenderRegion() {
        return senderRegion;
    }

    /**
     * @param senderRegion The sender_region
     */
    public void setSenderRegion(String senderRegion) {
        this.senderRegion = senderRegion;
    }

    /**
     * @return The customSenderCity
     */
    public String getCustomSenderCity() {
        return customSenderCity;
    }

    /**
     * @param customSenderCity The custom_sender_city
     */
    public void setCustomSenderCity(String customSenderCity) {
        this.customSenderCity = customSenderCity;
    }

    /**
     * @return The senderCity
     */
    public String getSenderCity() {
        return senderCity;
    }

    /**
     * @param senderCity The sender_city
     */
    public void setSenderCity(String senderCity) {
        this.senderCity = senderCity;
    }

    /**
     * @return The senderAddress
     */
    public String getSenderAddress() {
        return senderAddress;
    }

    /**
     * @param senderAddress The sender_address
     */
    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    /**
     * @return The senderMetro
     */
    public String getSenderMetro() {
        return senderMetro;
    }

    /**
     * @param senderMetro The sender_metro
     */
    public void setSenderMetro(String senderMetro) {
        this.senderMetro = senderMetro;
    }

    /**
     * @return The countBids
     */
    public int getCountBids() {
        return countBids;
    }

    /**
     * @param countBids The count_bids
     */
    public void setCountBids(int countBids) {
        this.countBids = countBids;
    }

    /**
     * @return The countActiveBids
     */
    public int getCountActiveBids() {
        return countActiveBids;
    }

    /**
     * @param countActiveBids The count_active_bids
     */
    public void setCountActiveBids(int countActiveBids) {
        this.countActiveBids = countActiveBids;
    }

    /**
     * @return The lotStatus
     */
    public int getLotStatus() {
        return lotStatus;
    }

    /**
     * @param lotStatus The lot_status
     */
    public void setLotStatus(int lotStatus) {
        this.lotStatus = lotStatus;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public Bid getRelatedBid() {
        return relatedBid;
    }

    public void setRelatedBid(Bid relatedBid) {
        this.relatedBid = relatedBid;
    }

    public String getAuthorPhone() {
        return authorPhone;
    }

    public void setAuthorPhone(String authorPhone) {
        this.authorPhone = authorPhone;
    }

    public BigDecimal getMinBid() {
        return minBid;
    }

    public void setMinBid(BigDecimal minBid) {
        this.minBid = minBid;
    }

    public static class TitleItem {

        @Expose
        private String title;

        @Expose
        private int quantity;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }

    public static class Weight {

        @Expose
        private float amount;

        @Expose
        private String unit = "kg";

        public float getAmount() {
            return amount;
        }

        public void setAmount(float amount) {
            this.amount = amount;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    public static class RoutePoint {

        @Expose
        private String city;
        @Expose
        private String region;
        @Expose
        private String address;
        @Expose
        private String metro;
        @Expose
        private String country;

        /**
         * @return The city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city The city
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return The region
         */
        public String getRegion() {
            return region;
        }

        /**
         * @param region The region
         */
        public void setRegion(String region) {
            this.region = region;
        }

        /**
         * @return The address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         * @return The metro
         */
        public String getMetro() {
            return metro;
        }

        /**
         * @param metro The metro
         */
        public void setMetro(String metro) {
            this.metro = metro;
        }

        /**
         * @return The country
         */
        public String getCountry() {
            return country;
        }

        /**
         * @param country The country
         */
        public void setCountry(String country) {
            this.country = country;
        }
    }

    public int getActionCode() {
        return actionCode;
    }

    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }

    public long getCacheUpdatedTimestamp() {
        return cacheUpdatedTimestamp;
    }

    public void setCacheUpdatedTimestamp(long cacheUpdatedTimestamp) {
        this.cacheUpdatedTimestamp = cacheUpdatedTimestamp;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Map<String, String> getReview() {
        return review;
    }

    public void setReview(Map<String, String> review) {
        this.review = review;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
