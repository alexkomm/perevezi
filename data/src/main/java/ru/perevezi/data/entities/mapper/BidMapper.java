/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.data.entities.Bid;
import ru.perevezi.domain.entities.PereveziBid;
import ru.perevezi.domain.mapper.BidirectionalMapper;

public class BidMapper implements BidirectionalMapper<Bid, PereveziBid> {

    private final UserMapper mUserMapper;

    @Inject
    public BidMapper(UserMapper userMapper) {
        mUserMapper = userMapper;
    }

    @Override
    public Bid mapRequest(PereveziBid pereveziBid) {
        Bid bid = new Bid();

        bid.setNid(pereveziBid.getNid());
        bid.setAmount(pereveziBid.getAmount());
        bid.setShipType(pereveziBid.getShipType());
        if (pereveziBid.getExpirationDate() != null) {
            bid.setExpiry(pereveziBid.getExpirationDate().getTimeInMillis() / 1000);
        }

        if (pereveziBid.getActiveTill() != null) {
            bid.setActiveTill(pereveziBid.getActiveTill().getTimeInMillis() / 1000);
        }

        bid.setLoading(pereveziBid.getLoadingOptions());
        bid.setCarId(pereveziBid.getCarId());

        return bid;
    }

    @Override
    public Collection<Bid> mapRequest(Collection<PereveziBid> entityCollection) {
        return null;
    }

    @Override
    public PereveziBid mapResponse(Bid bid) {
        PereveziBid pereveziBid = new PereveziBid();

        pereveziBid.setProvider(mUserMapper.mapResponse(bid.getProvider()));

        pereveziBid.setAmount(bid.getAmount());
        pereveziBid.setBid(bid.getBid());
        pereveziBid.setNid(bid.getNid());

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(bid.getTime() * 1000);

        pereveziBid.setCreatedDate(calendar);

        calendar.setTimeInMillis(bid.getUpdated() * 1000);

        pereveziBid.setUpdatedDate(calendar);

        calendar.setTimeInMillis(bid.getWon());

        pereveziBid.setWonDate(calendar);

        calendar.setTimeInMillis(bid.getAccepted() * 1000);

        pereveziBid.setAcceptedDate(calendar);

        pereveziBid.setAmount(bid.getAmount());
        pereveziBid.setShipType(bid.getShipType());
        pereveziBid.setLoadingOptions(bid.getLoading());

        if (bid.getExpiry() > 0) {
            calendar.setTimeInMillis(bid.getExpiry() * 1000);
            pereveziBid.setExpirationDate(calendar);
        }

        calendar.setTimeInMillis(bid.getActiveTill() * 1000);
        pereveziBid.setActiveTill(calendar);
        pereveziBid.setStatus(bid.getStatusIndex());
        pereveziBid.setCarImagePath(bid.getCarPath());
        pereveziBid.setCarName(bid.getCarName());
        pereveziBid.setCarSizes(bid.getCarSizes());
        pereveziBid.setCarCapacity(bid.getCarCapacity());
        pereveziBid.setCarBody(bid.getCarBody());
        pereveziBid.setCarAddPhotos(bid.getCarAddPhotos());

        pereveziBid.setAcceptable(bid.isAcceptable());
        pereveziBid.setDeclinable(bid.isDeclinable());
        pereveziBid.setCancellable(bid.isCancellable());

        pereveziBid.setRequirementsInfo(bid.getRequirementsInfo());

        pereveziBid.setDeclineReason(bid.getReason());

        return pereveziBid;
    }

    @Override
    public Collection<PereveziBid> mapResponse(Collection<Bid> entityCollection) {
        List<PereveziBid> bidList = new ArrayList<>();
        PereveziBid pereveziBid;

        for (Bid bidEntity : entityCollection) {
            pereveziBid = mapResponse(bidEntity);
            bidList.add(pereveziBid);
        }

        return bidList;
    }
}
