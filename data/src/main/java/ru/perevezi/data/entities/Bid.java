/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Map;

public class Bid {

    public static final String[] STATUSES = new String[]{
            "NEW",
            "WON",
            "ACCEPTED",
            "COMPLETE",
            "DECLINED",
            "REJECTED",
            "INCOMPLETE",
            "DELETED",
            "CANCELED",
            "OBSOLETE",
            "EXPIRED"
    };

    @Expose
    private User provider;

    @Expose
    private int bid;
    @Expose
    private int nid;
    @Expose
    private int uid;
    @Expose
    private long time;
    @Expose
    private long updated;
    @SerializedName("won_ts")
    @Expose
    private long won;
    @SerializedName("accepted_ts")
    @Expose
    private long accepted;
    @Expose
    private BigDecimal amount;
    @Expose
    private int shipType;
    @Expose
    private int loading;
    @Expose
    private long expiry;
    @Expose
    private String status;
    @Expose
    private String reason;
    @SerializedName("car_id")
    @Expose
    private int carId;
    @SerializedName("car_name")
    @Expose
    private String carName;
    @SerializedName("active_till")
    @Expose
    private long activeTill;
    @SerializedName("car_path")
    @Expose
    private String carPath;
    @SerializedName("car_sizes")
    @Expose
    private String carSizes;
    @SerializedName("car_body")
    @Expose
    private String carBody;
    @SerializedName("car_capacity")
    @Expose
    private String carCapacity;
    @SerializedName("car_add_photos")
    @Expose
    private String[] carAddPhotos;

    @Expose
    private boolean acceptable;

    @Expose
    private boolean declinable;

    @Expose
    private boolean cancellable;

    @SerializedName("requirements_info")
    @Expose
    private Map<String, Boolean> requirementsInfo;

    /**
     *
     * @return
     * The bid
     */
    public int getBid() {
        return bid;
    }

    /**
     *
     * @param bid
     * The bid
     */
    public void setBid(int bid) {
        this.bid = bid;
    }

    /**
     *
     * @return
     * The nid
     */
    public int getNid() {
        return nid;
    }

    /**
     *
     * @param nid
     * The nid
     */
    public void setNid(int nid) {
        this.nid = nid;
    }

    /**
     *
     * @return
     * The uid
     */
    public int getUid() {
        return uid;
    }

    /**
     *
     * @param uid
     * The uid
     */
    public void setUid(int uid) {
        this.uid = uid;
    }

    /**
     *
     * @return
     * The time
     */
    public long getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The updated
     */
    public long getUpdated() {
        return updated;
    }

    /**
     *
     * @param updated
     * The updated
     */
    public void setUpdated(long updated) {
        this.updated = updated;
    }

    /**
     *
     * @return
     * The wonTs
     */
    public long getWon() {
        return won;
    }

    /**
     *
     * @param won
     * The won_ts
     */
    public void setWon(long won) {
        this.won = won;
    }

    /**
     *
     * @return
     * The acceptedTs
     */
    public long getAccepted() {
        return accepted;
    }

    /**
     *
     * @param accepted
     */
    public void setAccepted(long accepted) {
        this.accepted = accepted;
    }

    /**
     *
     * @return
     * The amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     *
     * @param amount
     * The amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getShipType() {
        return shipType;
    }

    public void setShipType(int shipType) {
        this.shipType = shipType;
    }

    /**
     *
     * @return
     * The loading
     */
    public int getLoading() {
        return loading;
    }

    /**
     *
     * @param loading
     * The loading
     */
    public void setLoading(int loading) {
        this.loading = loading;
    }

    /**
     *
     * @return
     * The expiry
     */
    public long getExpiry() {
        return expiry;
    }

    /**
     *
     * @param expiry
     * The expiry
     */
    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The reason
     */
    public String getReason() {
        return reason;
    }

    /**
     *
     * @param reason
     * The reason
     */
    public void setReason(String reason) {
        this.reason = reason;
    }


    /**
     *
     * @return
     * The carId
     */
    public int getCarId() {
        return carId;
    }

    /**
     *
     * @param carId
     * The car_id
     */
    public void setCarId(int carId) {
        this.carId = carId;
    }

    /**
     *
     * @return
     * The activeTill
     */
    public long getActiveTill() {
        return activeTill;
    }

    /**
     *
     * @param activeTill
     * The active_till
     */
    public void setActiveTill(long activeTill) {
        this.activeTill = activeTill;
    }

    /**
     *
     * @return
     * The carPath
     */
    public String getCarPath() {
        return carPath;
    }

    /**
     *
     * @param carPath
     * The car_path
     */
    public void setCarPath(String carPath) {
        this.carPath = carPath;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarSizes() {
        return carSizes;
    }

    public void setCarSizes(String carSizes) {
        this.carSizes = carSizes;
    }

    public String getCarBody() {
        return carBody;
    }

    public void setCarBody(String carBody) {
        this.carBody = carBody;
    }

    public String getCarCapacity() {
        return carCapacity;
    }

    public void setCarCapacity(String carCapacity) {
        this.carCapacity = carCapacity;
    }

    public String[] getCarAddPhotos() {
        return carAddPhotos;
    }

    public void setCarAddPhotos(String[] carAddPhotos) {
        this.carAddPhotos = carAddPhotos;
    }

    public int getStatusIndex() {
        for (int i = 0; i < STATUSES.length; i++) {
            if (STATUSES[i].equals(status)) {
                return i;
            }
        }

        return -1;
    }

    public static int getStatusIndex(String status) {
        for (int i = 0; i < STATUSES.length; i++) {
            if (STATUSES[i].equals(status)) {
                return i;
            }
        }

        return -1;
    }

    public boolean isAcceptable() {
        return acceptable;
    }

    public void setAcceptable(boolean acceptable) {
        this.acceptable = acceptable;
    }

    public boolean isDeclinable() {
        return declinable;
    }

    public void setDeclinable(boolean declinable) {
        this.declinable = declinable;
    }

    public boolean isCancellable() {
        return cancellable;
    }

    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }

    public Map<String, Boolean> getRequirementsInfo() {
        return requirementsInfo;
    }

    public void setRequirementsInfo(Map<String, Boolean> requirementsInfo) {
        this.requirementsInfo = requirementsInfo;
    }


    public User getProvider() {
        return provider;
    }

    public void setProvider(User provider) {
        this.provider = provider;
    }
}
