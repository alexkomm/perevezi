/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.Collection;

import javax.inject.Inject;

import ru.perevezi.data.entities.Settings;
import ru.perevezi.domain.entities.PereveziSettings;
import ru.perevezi.domain.mapper.ResponseMapper;

public class SettingsMapper implements ResponseMapper<Settings, PereveziSettings> {

    private final CategoryMapper mCategoryMapper;

    @Inject
    public SettingsMapper(CategoryMapper categoryMapper) {
        mCategoryMapper = categoryMapper;
    }

    @Override
    public PereveziSettings mapResponse(Settings settings) {
        PereveziSettings pereveziSettings = new PereveziSettings();

        pereveziSettings.setLotLoadingOptions(settings.getLotLoadingOptions());
        pereveziSettings.setCarMovingOptions(settings.getLotCarMovingOptions());
        pereveziSettings.setCategories(mCategoryMapper.mapResponse(settings.getCategories()));
        pereveziSettings.setDeclineBidReasons(settings.getDeclineBidReasons());
        pereveziSettings.setPayLaterDeposit(settings.getPayLaterDeposit());
        pereveziSettings.setLotShippingOptions(settings.getLotShippingOptions());

        return pereveziSettings;
    }

    @Override
    public Collection<PereveziSettings> mapResponse(Collection<Settings> settingsCollection) {
        throw new UnsupportedOperationException();
    }
}
