
/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Category {

    @Expose
    private int tid;
    @Expose
    private String name;
    @Expose
    private String path;
    @Expose
    private Settings settings;

    /**
     * 
     * @return
     *     The tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * 
     * @param tid
     *     The tid
     */
    public void setTid(int tid) {
        this.tid = tid;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     *
     * @return
     *     The path
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     *     The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 
     * @return
     *     The settings
     */
    public Settings getSettings() {
        return settings;
    }

    /**
     * 
     * @param settings
     *     The settings
     */
    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public static class Settings implements Serializable {

        @SerializedName("2pages")
        @Expose
        private String _2pages;
        @SerializedName("add_more")
        @Expose
        private String addMore;
        @SerializedName("avg_price")
        @Expose
        private String avgPrice;
        @SerializedName("block_title")
        @Expose
        private String blockTitle;
        @SerializedName("body_hint")
        @Expose
        private String bodyHint;
        @SerializedName("body_label")
        @Expose
        private String bodyLabel;
        @SerializedName("body_placeholder")
        @Expose
        private String bodyPlaceholder;
        @SerializedName("body_to_add_info")
        @Expose
        private String bodyToAddInfo;
        @SerializedName("body_tpl")
        @Expose
        private Map<String, String> bodyTpl;
        @SerializedName("btn_text")
        @Expose
        private String btnText;
        @SerializedName("collapsed_form")
        @Expose
        private String collapsedForm;
        @SerializedName("descriptor_token")
        @Expose
        private String descriptorToken;
        @SerializedName("details_form")
        @Expose
        private String detailsForm;
        @Expose
        private List<String> exclude;
        @SerializedName("fast_icon")
        @Expose
        private String fastIcon;
        @Expose
        private String fields;
        @SerializedName("highlighted_image")
        @Expose
        private String highlightedImage;
        @SerializedName("main_question")
        @Expose
        private String mainQuestion;
        @SerializedName("need_load")
        @Expose
        private String needLoad;
        @Expose
        private String[] options;
        @SerializedName("quantity_field")
        @Expose
        private String quantityField;
        @Expose
        private List<String> required;
        @SerializedName("ship_opts")
        @Expose
        private String shipOpts;
        @SerializedName("ship_time")
        @Expose
        private boolean shipTime;
        @SerializedName("show_body")
        @Expose
        private String showBody;
        @SerializedName("title_format")
        @Expose
        private String titleFormat;
        @SerializedName("title_hint")
        @Expose
        private String titleHint;
        @SerializedName("title_label")
        @Expose
        private String titleLabel;
        @SerializedName("title_multi")
        @Expose
        private String titleMulti;
        @SerializedName("title_placeholder")
        @Expose
        private String titlePlaceholder;
        @SerializedName("title_type")
        @Expose
        private String titleType;
        @SerializedName("weight_unit")
        @Expose
        private String weightUnit;
        @Expose
        private String yaisland;

        /**
         *
         * @return
         *     The _2pages
         */
        public String get2pages() {
            return _2pages;
        }

        /**
         *
         * @param _2pages
         *     The 2pages
         */
        public void set2pages(String _2pages) {
            this._2pages = _2pages;
        }

        /**
         *
         * @return
         *     The addMore
         */
        public String getAddMore() {
            return addMore;
        }

        /**
         *
         * @param addMore
         *     The add_more
         */
        public void setAddMore(String addMore) {
            this.addMore = addMore;
        }

        /**
         *
         * @return
         *     The avgPrice
         */
        public String getAvgPrice() {
            return avgPrice;
        }

        /**
         *
         * @param avgPrice
         *     The avg_price
         */
        public void setAvgPrice(String avgPrice) {
            this.avgPrice = avgPrice;
        }

        /**
         *
         * @return
         *     The blockTitle
         */
        public String getBlockTitle() {
            return blockTitle;
        }

        /**
         *
         * @param blockTitle
         *     The block_title
         */
        public void setBlockTitle(String blockTitle) {
            this.blockTitle = blockTitle;
        }

        /**
         *
         * @return
         *     The bodyHint
         */
        public String getBodyHint() {
            return bodyHint;
        }

        /**
         *
         * @param bodyHint
         *     The body_hint
         */
        public void setBodyHint(String bodyHint) {
            this.bodyHint = bodyHint;
        }

        /**
         *
         * @return
         *     The bodyLabel
         */
        public String getBodyLabel() {
            return bodyLabel;
        }

        /**
         *
         * @param bodyLabel
         *     The body_label
         */
        public void setBodyLabel(String bodyLabel) {
            this.bodyLabel = bodyLabel;
        }

        /**
         *
         * @return
         *     The bodyPlaceholder
         */
        public String getBodyPlaceholder() {
            return bodyPlaceholder;
        }

        /**
         *
         * @param bodyPlaceholder
         *     The body_placeholder
         */
        public void setBodyPlaceholder(String bodyPlaceholder) {
            this.bodyPlaceholder = bodyPlaceholder;
        }

        /**
         *
         * @return
         *     The bodyToAddInfo
         */
        public String getBodyToAddInfo() {
            return bodyToAddInfo;
        }

        /**
         *
         * @param bodyToAddInfo
         *     The body_to_add_info
         */
        public void setBodyToAddInfo(String bodyToAddInfo) {
            this.bodyToAddInfo = bodyToAddInfo;
        }

        /**
         *
         * @return
         *     The bodyTpl
         */
        public Map<String, String> getBodyTpl() {
            return bodyTpl;
        }

        /**
         *
         * @param bodyTpl
         *     The body_tpl
         */
        public void setBodyTpl(Map<String, String> bodyTpl) {
            this.bodyTpl = bodyTpl;
        }

        /**
         *
         * @return
         *     The btnText
         */
        public String getBtnText() {
            return btnText;
        }

        /**
         *
         * @param btnText
         *     The btn_text
         */
        public void setBtnText(String btnText) {
            this.btnText = btnText;
        }

        /**
         *
         * @return
         *     The collapsedForm
         */
        public String getCollapsedForm() {
            return collapsedForm;
        }

        /**
         *
         * @param collapsedForm
         *     The collapsed_form
         */
        public void setCollapsedForm(String collapsedForm) {
            this.collapsedForm = collapsedForm;
        }

        /**
         *
         * @return
         *     The descriptorToken
         */
        public String getDescriptorToken() {
            return descriptorToken;
        }

        /**
         *
         * @param descriptorToken
         *     The descriptor_token
         */
        public void setDescriptorToken(String descriptorToken) {
            this.descriptorToken = descriptorToken;
        }

        /**
         *
         * @return
         *     The detailsForm
         */
        public String getDetailsForm() {
            return detailsForm;
        }

        /**
         *
         * @param detailsForm
         *     The details_form
         */
        public void setDetailsForm(String detailsForm) {
            this.detailsForm = detailsForm;
        }

        /**
         *
         * @return
         *     The exclude
         */
        public List<String> getExclude() {
            return exclude;
        }

        /**
         *
         * @param exclude
         *     The exclude
         */
        public void setExclude(List<String> exclude) {
            this.exclude = exclude;
        }

        /**
         *
         * @return
         *     The fastIcon
         */
        public String getFastIcon() {
            return fastIcon;
        }

        /**
         *
         * @param fastIcon
         *     The fast_icon
         */
        public void setFastIcon(String fastIcon) {
            this.fastIcon = fastIcon;
        }

        /**
         *
         * @return
         *     The fields
         */
        public String getFields() {
            return fields;
        }

        /**
         *
         * @param fields
         *     The fields
         */
        public void setFields(String fields) {
            this.fields = fields;
        }

        /**
         *
         * @return
         *     The highlightedImage
         */
        public String getHighlightedImage() {
            return highlightedImage;
        }

        /**
         *
         * @param highlightedImage
         *     The highlighted_image
         */
        public void setHighlightedImage(String highlightedImage) {
            this.highlightedImage = highlightedImage;
        }

        /**
         *
         * @return
         *     The mainQuestion
         */
        public String getMainQuestion() {
            return mainQuestion;
        }

        /**
         *
         * @param mainQuestion
         *     The main_question
         */
        public void setMainQuestion(String mainQuestion) {
            this.mainQuestion = mainQuestion;
        }

        /**
         *
         * @return
         *     The needLoad
         */
        public String getNeedLoad() {
            return needLoad;
        }

        /**
         *
         * @param needLoad
         *     The need_load
         */
        public void setNeedLoad(String needLoad) {
            this.needLoad = needLoad;
        }

        /**
         *
         * @return
         *     The options
         */
        public String[] getOptions() {
            return options;
        }

        /**
         *
         * @param options
         *     The options
         */
        public void setOptions(String[] options) {
            this.options = options;
        }

        /**
         *
         * @return
         *     The quantityField
         */
        public String getQuantityField() {
            return quantityField;
        }

        /**
         *
         * @param quantityField
         *     The quantity_field
         */
        public void setQuantityField(String quantityField) {
            this.quantityField = quantityField;
        }

        /**
         *
         * @return
         *     The required
         */
        public List<String> getRequired() {
            return required;
        }

        /**
         *
         * @param required
         *     The required
         */
        public void setRequired(List<String> required) {
            this.required = required;
        }

        /**
         *
         * @return
         *     The shipOpts
         */
        public String getShipOpts() {
            return shipOpts;
        }

        /**
         *
         * @param shipOpts
         *     The ship_opts
         */
        public void setShipOpts(String shipOpts) {
            this.shipOpts = shipOpts;
        }

        /**
         *
         * @return
         *     The shipTime
         */
        public boolean getShipTime() {
            return shipTime;
        }

        /**
         *
         * @param shipTime
         *     The ship_time
         */
        public void setShipTime(boolean shipTime) {
            this.shipTime = shipTime;
        }

        /**
         *
         * @return
         *     The showBody
         */
        public String getShowBody() {
            return showBody;
        }

        /**
         *
         * @param showBody
         *     The show_body
         */
        public void setShowBody(String showBody) {
            this.showBody = showBody;
        }

        /**
         *
         * @return
         *     The titleFormat
         */
        public String getTitleFormat() {
            return titleFormat;
        }

        /**
         *
         * @param titleFormat
         *     The title_format
         */
        public void setTitleFormat(String titleFormat) {
            this.titleFormat = titleFormat;
        }

        /**
         *
         * @return
         *     The titleHint
         */
        public String getTitleHint() {
            return titleHint;
        }

        /**
         *
         * @param titleHint
         *     The title_hint
         */
        public void setTitleHint(String titleHint) {
            this.titleHint = titleHint;
        }

        /**
         *
         * @return
         *     The titleLabel
         */
        public String getTitleLabel() {
            return titleLabel;
        }

        /**
         *
         * @param titleLabel
         *     The title_label
         */
        public void setTitleLabel(String titleLabel) {
            this.titleLabel = titleLabel;
        }

        /**
         *
         * @return
         *     The titleMulti
         */
        public String getTitleMulti() {
            return titleMulti;
        }

        /**
         *
         * @param titleMulti
         *     The title_multi
         */
        public void setTitleMulti(String titleMulti) {
            this.titleMulti = titleMulti;
        }

        /**
         *
         * @return
         *     The titlePlaceholder
         */
        public String getTitlePlaceholder() {
            return titlePlaceholder;
        }

        /**
         *
         * @param titlePlaceholder
         *     The title_placeholder
         */
        public void setTitlePlaceholder(String titlePlaceholder) {
            this.titlePlaceholder = titlePlaceholder;
        }

        /**
         *
         * @return
         *     The titleType
         */
        public String getTitleType() {
            return titleType;
        }

        /**
         *
         * @param titleType
         *     The title_type
         */
        public void setTitleType(String titleType) {
            this.titleType = titleType;
        }

        /**
         *
         * @return
         *     The weightUnit
         */
        public String getWeightUnit() {
            return weightUnit;
        }

        /**
         *
         * @param weightUnit
         *     The weight_unit
         */
        public void setWeightUnit(String weightUnit) {
            this.weightUnit = weightUnit;
        }

        /**
         *
         * @return
         *     The yaisland
         */
        public String getYaisland() {
            return yaisland;
        }

        /**
         *
         * @param yaisland
         *     The yaisland
         */
        public void setYaisland(String yaisland) {
            this.yaisland = yaisland;
        }

    }
}
