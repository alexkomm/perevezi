/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.Calendar;
import java.util.Collection;

import javax.inject.Inject;

import ru.perevezi.data.entities.User;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.mapper.ResponseMapper;

public class UserMapper implements ResponseMapper<User, PereveziAccount> {

    @Inject
    public UserMapper() {
    }

    @Override
    public PereveziAccount mapResponse(User user) {
        final PereveziAccount pereveziAccount = new PereveziAccount(user.getMail(), null);

        pereveziAccount.setExtId(user.getUid());
        pereveziAccount.setPassword(user.getPassword());
        pereveziAccount.setEmail(user.getMail());
        pereveziAccount.setName(user.getName());
        pereveziAccount.setFullName(user.getProfileName());
        pereveziAccount.setPhone(user.getProfilePhone());
        pereveziAccount.setVendor(user.isProvider());
        pereveziAccount.setClient(user.isClient());

        pereveziAccount.setCountLots(user.getCountLots());
        pereveziAccount.setCompletedLots(user.getCompletedLots());

        pereveziAccount.setCountBids(user.getCountBids());
        pereveziAccount.setCompletedBids(user.getCompletedBids());

        pereveziAccount.setCars(user.getCars());

        Calendar registrationDate = Calendar.getInstance();
        registrationDate.setTimeInMillis(user.getCreated() * 1000);

        pereveziAccount.setRegistrationDate(registrationDate);

        Calendar accessDate = Calendar.getInstance();
        accessDate.setTimeInMillis(user.getAccess() * 1000);

        pereveziAccount.setLastAccessDate(accessDate);

        pereveziAccount.setCompany(user.getProfileCompany());
        pereveziAccount.setHomeRegion(user.getProfileRegion());

        pereveziAccount.setPositiveVotes(user.getPositiveVotes());
        pereveziAccount.setNeutralVotes(user.getNeutralVotes());
        pereveziAccount.setNegativeVotes(user.getNegativeVotes());
        pereveziAccount.setInfo(user.getProfileInfo());
        pereveziAccount.setProfileImageUrl(user.getProfilePicture());
        pereveziAccount.setRating(user.getRating());

        pereveziAccount.setBalance(user.getBalance());
        pereveziAccount.setProDaysLeft(user.getProDaysLeft());
        pereveziAccount.setSmsLeft(user.getLotUserSmsAvailable());

        pereveziAccount.setPhoneConfirmStatus(user.getPhoneConfirmStatus());

        return pereveziAccount;
    }

    @Override
    public Collection<PereveziAccount> mapResponse(Collection<User> entityCollection) {
        return null;
    }
}
