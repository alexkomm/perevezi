/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class PaymentInfo {

    @Expose
    public int bid;

    @Expose
    public BigDecimal amount;
    @Expose
    public BigDecimal commission;
    @Expose
    public BigDecimal balance;

    @Expose
    @SerializedName("confirm_status")
    public int confirmStatus;

}
