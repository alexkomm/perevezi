/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import ru.perevezi.data.entities.Category;
import ru.perevezi.domain.entities.PereveziCategory;
import ru.perevezi.domain.mapper.ResponseMapper;

public class CategoryMapper implements ResponseMapper<Category, PereveziCategory> {

    @Inject
    public CategoryMapper() {
    }

    @Override
    public PereveziCategory mapResponse(Category category) {
        PereveziCategory pereveziCategory = new PereveziCategory(category.getName());
        pereveziCategory.setId(category.getTid());
        pereveziCategory.setDrawableResName(category.getPath());

        PereveziCategory.Settings settings = new PereveziCategory.Settings();

        if (!category.getSettings().getTitleLabel().equals("<none>")) {
            settings.setTitleHint(category.getSettings().getTitlePlaceholder());
        }


        settings.setAddMoreLabel(category.getSettings().getAddMore());
        settings.setQuantityHint(category.getSettings().getQuantityField());
        settings.setTitleAutocomplete(category.getSettings().getOptions());
        settings.setDetailsTags(category.getSettings().getBodyTpl());
        settings.setExcludedFields(category.getSettings().getExclude());
        settings.setRequiredFields(category.getSettings().getRequired());
        settings.setTimeNeeded(category.getSettings().getShipTime());

        pereveziCategory.setSettings(settings);

        return pereveziCategory;
    }

    @Override
    public List<PereveziCategory> mapResponse(Collection<Category> entityCollection) {
        List<PereveziCategory> categoryList = new ArrayList<>();
        PereveziCategory pereveziCategory;

        for (Category categoryEntity : entityCollection) {
            pereveziCategory = mapResponse(categoryEntity);
            categoryList.add(pereveziCategory);
        }

        return categoryList;
    }
}
