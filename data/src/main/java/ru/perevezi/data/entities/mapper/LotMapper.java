/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities.mapper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import ru.perevezi.data.entities.Lot;
import ru.perevezi.domain.entities.PereveziLot;
import ru.perevezi.domain.mapper.BidirectionalMapper;

public class LotMapper implements BidirectionalMapper<Lot, PereveziLot> {

    private final BidMapper mBidMapper;
    private final UserMapper mUserMapper;

    @Inject
    public LotMapper(BidMapper bidMapper, UserMapper userMapper) {
        mBidMapper = bidMapper;
        mUserMapper = userMapper;
    }

    @Override
    public PereveziLot mapResponse(Lot lot) {
        PereveziLot pereveziLot = new PereveziLot();

        pereveziLot.setAuthor(mUserMapper.mapResponse(lot.getAuthor()));

        List<PereveziLot.RoutePoint> routePoints = new ArrayList<>();
        for (Lot.RoutePoint rp : lot.getRoutePoints()) {
            PereveziLot.RoutePoint routePoint = new PereveziLot.RoutePoint();
            routePoint.address = rp.getAddress();
            routePoint.locality = rp.getCity();
            routePoint.region = rp.getRegion();
            routePoint.country = rp.getCountry();

            routePoints.add(routePoint);
        }

        pereveziLot.setNid(lot.getNid());

        Calendar changedDate = Calendar.getInstance();
        changedDate.setTimeInMillis(lot.getCreated() * 1000);

        pereveziLot.setCreatedDate(changedDate);

        pereveziLot.setStatus(lot.getLotStatus());

        pereveziLot.setCategoryId(lot.getCategory());

        if (lot.getWeight() != null) {
            pereveziLot.setWeight(lot.getWeight().getAmount());
            pereveziLot.setWeightUnit(lot.getWeight().getUnit());
        }

        pereveziLot.setVolume(lot.getVolume());

        pereveziLot.setTitleString(lot.getTitle());
        pereveziLot.setDetails(lot.getBody());

        if (lot.getShipDate() > 0) {
            Calendar shipDate = Calendar.getInstance();
            shipDate.setTimeInMillis(lot.getShipDate() * 1000);
            pereveziLot.setShipDate(shipDate);
        }

        pereveziLot.setWithShipTime(lot.isWithShipTime());

        if (lot.getExpiry() > 0) {
            Calendar expiry = Calendar.getInstance();
            expiry.setTimeInMillis(lot.getExpiry() * 1000);
            pereveziLot.setExpirationDate(expiry);
        }

        pereveziLot.setMaxPrice(lot.getStartPrice());
        pereveziLot.setBlitzPrice(lot.getPrice());

        pereveziLot.setRoutePoints(routePoints);

        pereveziLot.setCountBids(lot.getCountBids());
        pereveziLot.setForPro(lot.getPro() > 0);

        if (lot.getRelatedBid() != null) {
            pereveziLot.setRelatedBid(mBidMapper.mapResponse(lot.getRelatedBid()));
        }

        pereveziLot.setMinBid(lot.getMinBid());
        pereveziLot.setAuthorPhone(lot.getAuthorPhone());

        pereveziLot.setActionCode(lot.getActionCode());

        pereveziLot.setReview(lot.getReview());
        pereveziLot.setMark(lot.getMark());

        pereveziLot.setCacheUpdatedTimestamp(lot.getCacheUpdatedTimestamp());

        return pereveziLot;
    }

    @Override
    public Collection<PereveziLot> mapResponse(Collection<Lot> entityCollection) {
        List<PereveziLot> lotList = new ArrayList<>();
        PereveziLot pereveziLot;

        for (Lot lotEntity : entityCollection) {
            pereveziLot = mapResponse(lotEntity);
            lotList.add(pereveziLot);
        }

        return lotList;
    }

    @Override
    public Lot mapRequest(PereveziLot pereveziLot) {
        Lot lot = new Lot();

        lot.setCategory(pereveziLot.getCategoryId());
        lot.setBody(pereveziLot.getDetails());

        if (pereveziLot.getCargos() != null) {
            List<Lot.TitleItem> itemsList = new ArrayList<>();
            for (PereveziLot.Cargo cargo : pereveziLot.getCargos()) {
                Lot.TitleItem titleItem = new Lot.TitleItem();

                titleItem.setTitle(cargo.title);
                titleItem.setQuantity(cargo.amount);

                itemsList.add(titleItem);
            }
            lot.setTitleItems(itemsList);
        }

        Lot.Weight weight = new Lot.Weight();
        weight.setAmount(pereveziLot.getWeight());

        lot.setWeight(weight);
        lot.setVolume(pereveziLot.getVolume());

        if (pereveziLot.getShipDate() != null) {
            lot.setShipDate(TimeUnit.MILLISECONDS.toSeconds(pereveziLot.getShipDate().getTimeInMillis()));
        }

        lot.setNeedLoad(pereveziLot.getLoadingOptions());

        PereveziLot.RoutePoint fromRoutePoint = pereveziLot.getRoutePoints().get(0);

        lot.setSenderAddress(fromRoutePoint.address);
        lot.setSenderCity(fromRoutePoint.locality);
        lot.setSenderCountry(fromRoutePoint.country);
        lot.setSenderRegion(fromRoutePoint.region);

        PereveziLot.RoutePoint toRoutePoint = pereveziLot.getRoutePoints().get(1);

        lot.setRecipientAddress(toRoutePoint.address);
        lot.setRecipientCity(toRoutePoint.locality);
        lot.setRecipientCountry(toRoutePoint.country);
        lot.setRecipientRegion(toRoutePoint.region);

        lot.setPrice(pereveziLot.getBlitzPrice());
        lot.setStartPrice(pereveziLot.getMaxPrice());
        lot.setPro(pereveziLot.isForPro() ? 1 : 0);

        return lot;
    }

    @Override
    public Collection<Lot> mapRequest(Collection<PereveziLot> entityCollection) {
        List<Lot> lotList = new ArrayList<>();
        Lot pereveziLot;

        for (PereveziLot lotEntity : entityCollection) {
            pereveziLot = mapRequest(lotEntity);
            lotList.add(pereveziLot);
        }

        return lotList;
    }
}
