/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Settings {

    @SerializedName("lot_decline_bid_reasons")
    @Expose
    private List<Map<String, String>> mDeclineBidReasons;

    @SerializedName("lot_loading_options")
    @Expose
    private List<Map<String, String>> mLotLoadingOptions;

    @SerializedName("lot_car_moving_options")
    @Expose
    private List<Map<String, String>> mLotCarMovingOptions;

    @SerializedName("lot_shipping_options")
    @Expose List<Map<String, String>> mLotShippingOptions;

    @SerializedName("categories")
    @Expose
    private List<Category> mCategories;

    @SerializedName("paylater_deposit")
    @Expose
    private BigDecimal mPayLaterDeposit;

    /**
     *
     * @return
     * The lotLoadingOptions
     */
    public List<Map<String, String>> getLotLoadingOptions() {
        return mLotLoadingOptions;
    }

    /**
     *
     * @param lotLoadingOptions
     * The lot_loading_options
     */
    public void setLotLoadingOptions(List<Map<String, String>> lotLoadingOptions) {
        mLotLoadingOptions = lotLoadingOptions;
    }

    public List<Map<String, String>> getLotCarMovingOptions() {
        return mLotCarMovingOptions;
    }

    public void setLotCarMovingOptions(List<Map<String, String>> lotCarMovingOptions) {
        mLotCarMovingOptions = lotCarMovingOptions;
    }

    public List<Map<String, String>> getLotShippingOptions() {
        return mLotShippingOptions;
    }

    public void setLotShippingOptions(List<Map<String, String>> lotShippingOptions) {
        mLotShippingOptions = lotShippingOptions;
    }

    public List<Category> getCategories() {
        return mCategories;
    }

    public void setCategories(List<Category> categories) {
        mCategories = categories;
    }

    public List<Map<String, String>> getDeclineBidReasons() {
        return mDeclineBidReasons;
    }

    public void setDeclineBidReasons(List<Map<String, String>> declineBidReasons) {
        mDeclineBidReasons = declineBidReasons;
    }

    public BigDecimal getPayLaterDeposit() {
        return mPayLaterDeposit;
    }

    public void setPayLaterDeposit(BigDecimal payLaterDeposit) {
        mPayLaterDeposit = payLaterDeposit;
    }
}
