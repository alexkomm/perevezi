/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class User {

    @Expose
    private int uid;
    @Expose
    private String password;

    @Expose
    private String name;
    @Expose
    private String mail;

    @Expose
    private long created;

    @Expose
    private long access;

    @SerializedName("profile_company")
    @Expose
    private String profileCompany;
    @SerializedName("is_provider")
    @Expose
    private boolean provider;
    @SerializedName("is_client")
    @Expose
    private boolean client;
    @SerializedName("profile_name")
    @Expose
    private String profileName;
    @SerializedName("profile_phone")
    @Expose
    private String profilePhone;
    @SerializedName("profile_info")
    @Expose
    private String profileInfo;
    @SerializedName("profile_capacity")
    @Expose
    private String profileCapacity;
    @SerializedName("profile_bodytype")
    @Expose
    private String profileBodytype;
    @SerializedName("profile_region")
    @Expose
    private String profileRegion;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("region_id")
    @Expose
    private int regionId;
    @SerializedName("profile_sex")
    @Expose
    private String profileSex;
    @SerializedName("profile_motorpark")
    @Expose
    private String profileMotorpark;

    @Expose
    private List<Map<String, Object>> cars;

    @SerializedName("count_lots")
    @Expose
    private int countLots;

    @SerializedName("completed_lots")
    @Expose
    private int completedLots;

    @SerializedName("count_bids")
    @Expose
    private int countBids;

    @SerializedName("completed_bids")
    @Expose
    private int completedBids;

    @SerializedName("positive_votes")
    @Expose
    private int positiveVotes;

    @SerializedName("neutral_votes")
    @Expose
    private int neutralVotes;

    @SerializedName("negative_votes")
    @Expose
    private int negativeVotes;

    @Expose
    private float rating;

    @Expose
    private BigDecimal balance;

    @SerializedName("pro_days_left")
    @Expose
    int proDaysLeft;

    @SerializedName("lot_user_sms_available")
    @Expose
    int lotUserSmsAvailable;

    @SerializedName("phone_confirm_status")
    @Expose
    int phoneConfirmStatus;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileCompany() {
        return profileCompany;
    }

    public void setProfileCompany(String profileCompany) {
        this.profileCompany = profileCompany;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfilePhone() {
        return profilePhone;
    }

    public void setProfilePhone(String profilePhone) {
        this.profilePhone = profilePhone;
    }

    public String getProfileInfo() {
        return profileInfo;
    }

    public void setProfileInfo(String profileInfo) {
        this.profileInfo = profileInfo;
    }

    public String getProfileCapacity() {
        return profileCapacity;
    }

    public void setProfileCapacity(String profileCapacity) {
        this.profileCapacity = profileCapacity;
    }

    public String getProfileBodytype() {
        return profileBodytype;
    }

    public void setProfileBodytype(String profileBodytype) {
        this.profileBodytype = profileBodytype;
    }

    public String getProfileRegion() {
        return profileRegion;
    }

    public void setProfileRegion(String profileRegion) {
        this.profileRegion = profileRegion;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getProfileSex() {
        return profileSex;
    }

    public void setProfileSex(String profileSex) {
        this.profileSex = profileSex;
    }

    public String getProfileMotorpark() {
        return profileMotorpark;
    }

    public void setProfileMotorpark(String profileMotorpark) {
        this.profileMotorpark = profileMotorpark;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public boolean isProvider() {
        return provider;
    }

    public void setProvider(boolean provider) {
        this.provider = provider;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getCountLots() {
        return countLots;
    }

    public void setCountLots(int countLots) {
        this.countLots = countLots;
    }

    public List<Map<String, Object>> getCars() {
        return cars;
    }

    public void setCars(List<Map<String, Object>> cars) {
        this.cars = cars;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getAccess() {
        return access;
    }

    public void setAccess(long access) {
        this.access = access;
    }

    public int getCompletedLots() {
        return completedLots;
    }

    public void setCompletedLots(int completedLots) {
        this.completedLots = completedLots;
    }

    public int getCountBids() {
        return countBids;
    }

    public void setCountBids(int countBids) {
        this.countBids = countBids;
    }

    public int getCompletedBids() {
        return completedBids;
    }

    public void setCompletedBids(int completedBids) {
        this.completedBids = completedBids;
    }

    public int getPositiveVotes() {
        return positiveVotes;
    }

    public void setPositiveVotes(int positiveVotes) {
        this.positiveVotes = positiveVotes;
    }

    public int getNeutralVotes() {
        return neutralVotes;
    }

    public void setNeutralVotes(int neutralVotes) {
        this.neutralVotes = neutralVotes;
    }

    public int getNegativeVotes() {
        return negativeVotes;
    }

    public void setNegativeVotes(int negativeVotes) {
        this.negativeVotes = negativeVotes;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getProDaysLeft() {
        return proDaysLeft;
    }

    public void setProDaysLeft(int proDaysLeft) {
        this.proDaysLeft = proDaysLeft;
    }

    public int getLotUserSmsAvailable() {
        return lotUserSmsAvailable;
    }

    public void setLotUserSmsAvailable(int lotUserSmsAvailable) {
        this.lotUserSmsAvailable = lotUserSmsAvailable;
    }

    public int getPhoneConfirmStatus() {
        return phoneConfirmStatus;
    }

    public void setPhoneConfirmStatus(int phoneConfirmStatus) {
        this.phoneConfirmStatus = phoneConfirmStatus;
    }
}
