/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit.RequestInterceptor;
import ru.perevezi.domain.account.PereveziAccountManager;
import ru.perevezi.domain.entities.PereveziAccount;
import ru.perevezi.domain.utils.PereveziPreferences;

public class ApiRequestInterceptor implements RequestInterceptor {

    private final PereveziAccountManager mAccountManager;
    private final PereveziPreferences mPereveziPreferences;

    public ApiRequestInterceptor(PereveziAccountManager accountManager, PereveziPreferences pereveziPreferences) {
        mAccountManager = accountManager;
        mPereveziPreferences = pereveziPreferences;
    }

    @Override
    public void intercept(RequestFacade request) {
        final String credentials = "perevezi:ShipDev";

        String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        request.addHeader("Authorization", string);

        PereveziAccount account = mAccountManager.getAccount(mPereveziPreferences
                .getCurrentAccount());

        if (account == null) {
            return;
        }

        final String authToken = account.getAuthToken();


        if (authToken != null) {
            request.addHeader("Cookie", authToken);
        }

        final String csrfToken = getCsrfToken();

        if (csrfToken != null) {
            request.addHeader("X-CSRF-TOKEN", csrfToken);
        }
    }

    private String getCsrfToken() {
        PereveziAccount account = mAccountManager.getAccount(mPereveziPreferences.getCurrentAccount());

        if (account == null || account.getAuthToken() == null) {
            return null;
        }

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String[] explodedSession = account.getAuthToken().split("=");

            md.update((explodedSession[1] + Constants.API_PRIVATE_KEY_TYPE + Constants.API_PRIVATE_KEY).getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
