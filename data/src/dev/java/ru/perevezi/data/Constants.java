/*
 * Copyright (c) 2015. Перевези.рф
 */

package ru.perevezi.data;

public class Constants {

    public static final String API_URL_ROOT = "http://dev.perevezi.ru";
    public static final String API_URL = API_URL_ROOT + "/api/v1";
    public static final String API_PRIVATE_KEY = "oTQ-6irnsqqIqO3Wa8QsvaxtFiFzWf-k6HR6GoGSDBE";
    public static final String API_PRIVATE_KEY_TYPE = "services";

    public static final boolean API_DEBUG = true;

    public static final int CONNECT_TIMEOUT_MILLIS = 60000;

    public static final int READ_TIMEOUT_MILLIS = 60000;

    // API error codes
    public static final int API_RESPONSE_ALREADY_REGISTERED_ERROR = 1;
    public static final int API_RESPONSE_SMS_CONFIRM_INVALID = 3;
    public static final int API_RESPONSE_SMS_CONFIRM_NOT_FOUND = 4;

}
